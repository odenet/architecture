// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:fluttermvvmtemplate/core/components/form/base_form.dart';

Widget wrapWithMaterialApp(Widget appRaisedButton) {
    return MaterialApp(
      home: appRaisedButton,
    );
  }

void main() {
  testWidgets('Custom DatePicker is Available', (WidgetTester tester) async {
  
  final widget = CustomDatePicker(text: Text('Date Picker'), startYear: 2000, endYear: 2020);

  await tester.pumpWidget(wrapWithMaterialApp(widget));

  expect(find.byType(CustomDatePicker), findsOneWidget);

  });

  testWidgets('Test Date Picker', (WidgetTester tester) async {

    final widget = CustomDatePicker(text: Text('Picker'), startYear: 2000, endYear: 2020);

    await tester.pumpWidget(wrapWithMaterialApp(widget));

    var picker = tester.widget<CustomDatePicker>(find.byType(CustomDatePicker));

    expect(picker.startYear, 2000);
    expect(picker.endYear, 2020);
    expect(picker.text.data,'Picker');
    
  });

  testWidgets('OnPressed Test', (WidgetTester tester) async {

    var isTap = false;

    final widget = RaisedButton(onPressed: (){
      isTap = true;
    });

    await tester.pumpWidget(wrapWithMaterialApp(widget));

    await tester.tap(find.byType(RaisedButton));

    expect(isTap, true);
    
  });

}
