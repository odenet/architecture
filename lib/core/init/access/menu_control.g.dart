// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'menu_control.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MenuControl _$MenuControlFromJson(Map<String, dynamic> json) {
  $checkKeys(json, requiredKeys: const ['pageId']);
  return MenuControl(
    id: json['id'] as int ?? -1,
    name: json['name'] as String ?? '',
    pageId: json['pageId'] as String,
  );
}

Map<String, dynamic> _$MenuControlToJson(MenuControl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'pageId': instance.pageId,
    };
