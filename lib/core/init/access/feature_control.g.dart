// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'feature_control.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FeatureControl _$FeatureControlFromJson(Map<String, dynamic> json) {
  $checkKeys(json, requiredKeys: const ['pageId']);
  return FeatureControl(
    pageId: json['pageId'] as String,
    children: (json['children'] as List)?.map((e) => e as int)?.toList() ?? [],
    name: json['name'] as String ?? '',
    active: json['active'] as bool ?? false,
    file: json['file'] as String ?? '',
    code: json['code'] as int ?? -1,
    isPublic: json['isPublic'] as bool ?? false,
    description: json['description'] as String ?? '',
  );
}

Map<String, dynamic> _$FeatureControlToJson(FeatureControl instance) =>
    <String, dynamic>{
      'pageId': instance.pageId,
      'name': instance.name,
      'description': instance.description,
      'children': instance.children,
      'active': instance.active,
      'file': instance.file,
      'code': instance.code,
      'isPublic': instance.isPublic,
    };
