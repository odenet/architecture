import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

import 'control.dart';

part 'menu_control.g.dart';

@JsonSerializable()
class MenuControl implements Control  {
  static MenuControl _instance;
  MenuControl get instance => _instance;

  @JsonKey(defaultValue: -1)
  final int id;

  @JsonKey(defaultValue: '')
  final String name;

  @JsonKey(required: true)
  final String pageId;

  MenuControl({this.id, this.name, @required this.pageId});

  bool isAllowed(String pageId) {}

  factory MenuControl.fromJson(Map<String, dynamic> json) =>
      _$MenuControlFromJson(json);

  Map<String, dynamic> toJson() => _$MenuControlToJson(this);
}
