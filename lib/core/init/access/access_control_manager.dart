import 'package:fluttermvvmtemplate/core/init/access/control.dart';
import 'package:fluttermvvmtemplate/core/init/access/feature_control.dart';
import 'package:fluttermvvmtemplate/core/init/access/menu_control.dart';
import 'package:fluttermvvmtemplate/core/init/access/permission_control.dart';
import 'package:fluttermvvmtemplate/core/init/access/session_control.dart';

class AccessControlManager implements Control {
  bool isAllowed(pageId) {

    FeatureControl obj = FeatureControl(pageId: pageId);
    bool isFeatureAllowed = obj.isAllowed(pageId);
    bool isMenuAllowed = MenuControl(pageId: pageId).instance.isAllowed(pageId);
    bool isSessionAllowed = SessionControl().instance.isAllowed(pageId);
    bool isPermissionAllowed = PermissionControl(pageId: pageId).isAllowed(pageId);

    if (isPermissionAllowed &&
        isMenuAllowed &&
        isSessionAllowed &&
        isPermissionAllowed) {
      return true;
    } else {
      return false;
    }
  }
}
