import 'package:json_annotation/json_annotation.dart';

import 'control.dart';

part 'session_control.g.dart';

@JsonSerializable()
class SessionControl implements Control {
  static SessionControl _instance;
  SessionControl get instance => _instance;

  @JsonKey(defaultValue: -1)
  final int id;

  @JsonKey(defaultValue: '')
  final String name;

  @JsonKey(required: true)
  final String pageId;

  bool isAllowed(String pageId) {}

  SessionControl({this.id, this.name, this.pageId});

  factory SessionControl.fromJson(Map<String, dynamic> json) =>
      _$SessionControlFromJson(json);

  Map<String, dynamic> toJson() => _$SessionControlToJson(this);
}
