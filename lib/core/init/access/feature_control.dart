import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/init/access/control.dart';
import 'package:json_annotation/json_annotation.dart';

part 'feature_control.g.dart';

@JsonSerializable()
class FeatureControl implements Control {
  static final List instances = fetchJson();

  @JsonKey(required: true)
  final String pageId;

  @JsonKey(defaultValue: '')
  final String name;

  @JsonKey(defaultValue: '')
  final String description;

  @JsonKey(defaultValue: [])
  final List<int> children; // list of objct ids
  
  @JsonKey(defaultValue: false)
  final bool active;

  @JsonKey(defaultValue: '')
  final String file;

  @JsonKey(defaultValue: -1)
  final int code;

  @JsonKey(defaultValue: false)
  final bool isPublic;

  FeatureControl({
    @required this.pageId,
    this.children,
    this.name,
    this.active,
    this.file,
    this.code,
    this.isPublic,
    this.description,
  });


  factory FeatureControl.fromJson(Map<String, dynamic> json) => _$FeatureControlFromJson(json);

  bool isAllowed(String pageId) {
    
    FeatureControl obj;

    instances.forEach((element) {
      if(element.pageId == this.pageId){
        obj = element;
      }
    });

    if (obj.active && obj.isPublic) {
      return true;
    } else {
      return false;
    }
  }

  static List<FeatureControl> fetchJson() {
    final List<Map<String, dynamic>> versionTestData = [
      {
        'pageId': '11',
        'name': 'Version 1.0',
        'description': 'bla',
        'children': [1, 2, 3, 4],
        'active': true,
        'file': ' ',
        'code': 11,
        'isPublic': true
      }
    ];

    return versionTestData.map((e) => FeatureControl.fromJson(e)).toList();
  }
}
