// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'permission_control.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PermissionControl _$PermissionControlFromJson(Map<String, dynamic> json) {
  $checkKeys(json, requiredKeys: const ['pageId']);
  return PermissionControl(
    id: json['id'] as int ?? -1,
    name: json['name'] as String ?? '',
    pageId: json['pageId'] as String,
  );
}

Map<String, dynamic> _$PermissionControlToJson(PermissionControl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'pageId': instance.pageId,
    };
