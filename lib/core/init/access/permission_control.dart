import 'package:json_annotation/json_annotation.dart';

import 'control.dart';

part 'permission_control.g.dart';

@JsonSerializable()
class PermissionControl implements Control {

  @JsonKey(defaultValue: -1)
  final int id;

  @JsonKey(defaultValue: '')
  final String name;

  @JsonKey(required: true)
  final String pageId;

  bool isAllowed(String pageId) {}

  PermissionControl({this.id, this.name, this.pageId});

  factory PermissionControl.fromJson(Map<String, dynamic> json) =>
      _$PermissionControlFromJson(json);

  Map<String, dynamic> toJson() => _$PermissionControlToJson(this);
}
