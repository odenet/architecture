// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'session_control.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SessionControl _$SessionControlFromJson(Map<String, dynamic> json) {
  $checkKeys(json, requiredKeys: const ['pageId']);
  return SessionControl(
    id: json['id'] as int ?? -1,
    name: json['name'] as String ?? '',
    pageId: json['pageId'] as String,
  );
}

Map<String, dynamic> _$SessionControlToJson(SessionControl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'pageId': instance.pageId,
    };
