import 'package:flutter/foundation.dart';

class DataType {
  static var shared = DataType();

  final ValueNotifier<String> _dataType = ValueNotifier('');
  final ValueNotifier<String> _dataTypeLabel = ValueNotifier('');
  final ValueNotifier<String> _module = ValueNotifier('');

  set dataType(String type) => _dataType.value = type;

  set dataTypeLabel(String label) => _dataTypeLabel.value = label;

  set module(String label) => _module.value = label;

  String get dataType => _dataType.value;

  String get dataTypeLabel => _dataTypeLabel.value;

  String get module => _module.value;

  ValueNotifier<String> get listenable => _dataType;

  ValueNotifier<String> get listenableLabel => _dataTypeLabel;

  ValueNotifier<String> get listenableModule => _module;
}
