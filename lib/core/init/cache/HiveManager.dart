import 'package:hive/hive.dart';

class HiveManager {
  final String boxName;

  HiveManager(this.boxName);

 static Future<void> openBoxes() async {
    var boxes = <String>['default'];

    for(var box in boxes){
      await Hive.openBox(box);
    }
  }

  dynamic getData(dynamic key)  {
    var box;
    box = Hive.box(boxName);

    var a = box.get(key);
    print(a);
    return a;
  }

  Future<void> putData<T>(String key, T data) async {
    var box = Hive.box(boxName);
    await box.put(key, data);
  }

}
