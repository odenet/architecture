import 'package:fluttermvvmtemplate/core/base/model/base_model.dart';
import 'package:json_annotation/json_annotation.dart';


part 'language.g.dart';

@JsonSerializable()
class Language extends BaseModel<Language>{
  @JsonKey(defaultValue: -1)
  int recordedId;
  @JsonKey(defaultValue: '')
  String languageCode;

  Language({this.recordedId, this.languageCode});

  @override
  Language fromJson(Map<String, Object> json) {
    return _$LanguageFromJson(json);
  }

  @override
  Map<String, Object> toJson() {
    return _$LanguageToJson(this);
  }
}