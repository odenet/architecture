import 'package:fluttermvvmtemplate/core/base/model/base_model.dart';
import 'package:json_annotation/json_annotation.dart';


part 'theme.g.dart';

@JsonSerializable()
class Theme extends BaseModel<Theme>{
  @JsonKey(defaultValue: '')
  String mergedId;

  Theme({this.mergedId});

  @override
  Theme fromJson(Map<String, Object> json) {
    return _$ThemeFromJson(json);
  }

  @override
  Map<String, Object> toJson() {
    return _$ThemeToJson(this);
  }
}