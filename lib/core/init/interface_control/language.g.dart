// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'language.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Language _$LanguageFromJson(Map<String, dynamic> json) {
  return Language(
    recordedId: json['recordedId'] as int ?? -1,
    languageCode: json['languageCode'] as String ?? '',
  )
    ..name = json['name'] as String ?? ''
    ..description = json['description'] as String ?? ''
    ..slug = json['slug'] as String ?? ''
    ..isPublic = json['isPublic'] as bool ?? true
    ..isActive = json['isActive'] as bool ?? true
    ..targetRoute = json['targetRoute'] as String;
}

Map<String, dynamic> _$LanguageToJson(Language instance) => <String, dynamic>{
      'name': instance.name,
      'description': instance.description,
      'slug': instance.slug,
      'isPublic': instance.isPublic,
      'isActive': instance.isActive,
      'targetRoute': instance.targetRoute,
      'recordedId': instance.recordedId,
      'languageCode': instance.languageCode,
    };
