import 'language.dart';
import 'theme.dart';

class InterFaceControlManager {

  InterFaceControlManager get instance => InterFaceControlManager();

  Language language;
  Theme theme;
  
  static void init({Language language, Theme theme}){
    language = language;
    theme = theme;
  }

}