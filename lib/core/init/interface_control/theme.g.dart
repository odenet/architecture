// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'theme.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Theme _$ThemeFromJson(Map<String, dynamic> json) {
  return Theme(
    mergedId: json['mergedId'] as String ?? '',
  )
    ..name = json['name'] as String ?? ''
    ..description = json['description'] as String ?? ''
    ..slug = json['slug'] as String ?? ''
    ..isPublic = json['isPublic'] as bool ?? true
    ..isActive = json['isActive'] as bool ?? true
    ..targetRoute = json['targetRoute'] as String;
}

Map<String, dynamic> _$ThemeToJson(Theme instance) => <String, dynamic>{
      'name': instance.name,
      'description': instance.description,
      'slug': instance.slug,
      'isPublic': instance.isPublic,
      'isActive': instance.isActive,
      'targetRoute': instance.targetRoute,
      'mergedId': instance.mergedId,
    };
