import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LanguageManager {
  static LanguageManager _instance;

  static LanguageManager get instance {
    if (_instance == null) _instance = LanguageManager._init();
    return _instance;
  }

  LanguageManager._init();

  Map<String, Locale> localesMap = {
    'en': Locale("en", "US"),
    'tr': Locale("tr", "TR"),
    'ar': Locale("ar", "AR")
  };

  List<Locale> _getLocalesAsList() {
    List<Locale> list = [];

    localesMap.forEach((key, value) {
      list.add(value);
    });

    return list;
  }

  List<Locale> get supportedLocales => _getLocalesAsList();

  Future<String> get currentLocale async {
    final _preferences = await SharedPreferences.getInstance();
    final _strLocale = (_preferences.getString('locale') != null)
        ? _preferences.getString('locale').split('_')[0]
        : localesMap['tr'].toString().split('_')[0];

    return _strLocale;
  }

  Future<bool> changeLanguage(Locale languageCode) async {
    final _preferences = await SharedPreferences.getInstance();
    _preferences.setString('locale', languageCode.toString());
  }
}
