import 'package:dio/dio.dart';
import 'package:fluttermvvmtemplate/core/constants/enums/locale_keys_enum.dart';
import 'package:fluttermvvmtemplate/core/init/cache/locale_manager.dart';
import 'package:fluttermvvmtemplate/core/init/lang/language_manager.dart';

String BASE_URL = 'http://10.0.2.2:8000/Odenet-Ap/server';
const TOKEN ='eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiMjcyODQ2YzZhNDUzNjQyOTU4Y2M5ZDBlZTI4ZGM0NDdhMGQ1ZmQ4MDNlMzgyNTliMDBmMTZiODY5MTg2ZWE2MWM5OWQ2YTczMmJmNjRkODEiLCJpYXQiOjE2MTc2OTU5NDgsIm5iZiI6MTYxNzY5NTk0OCwiZXhwIjoxNjQ5MjMxOTQ4LCJzdWIiOiIxMiIsInNjb3BlcyI6W119.hQoPb6M_GpC9H-FaW_d3r17Wy1t-1kFAzJsj9CMsxu-nVdLExgeu-RpwYNyN9dNGnwziDJ1th-B0-8MygRBIpNi-sXDfqe_RBdZZX6YBpXBYuXAPMjmDiKrnygUQ2jIdX-ZlQYMoWhsZWVSK-5tMB93U11KCOdT4GN3BuHV5IUJ8mmuLzmQ79Qd_k1l4AQpbTTW1jT8HymHGjQ7N6gOuPrHYnAjR1K0omVJqeH6zwwTTNgQYd-PpM8s8ezGnDhMcvXZQLH97MbcI-jb4hTuNK2WPhd7L_mjqtHQUp3d3OnMidM0KrnK3Gy6K7ehOia_bIdC2ShgoPBXt68Y-c4q9Aoj7nuUC4Wd7UlJxe0BSwthSyTXxInUNSbUyE-wTIk6SWirg7edCxSVqjevBQH2M76kIxuCulMpP48l7lAc-7V9TqJZZkkfrKPkw0CxvW8pv5Liz7WzXE22t2eOvXANBGoG3zPv2A51kuOUUxhElLtJUpNXRQ1b1GMDiqIkHKDDo-KXURsZ1EliSt6JqM5ZwS2OSbNijmjaPSgFpgfSKuYcrw8nCGzBxyhH-akiKe-nVnpHq9EN9DngSgLdFbnzXYJIvdv-7PJZXFvLtaRwapl3Y9KqsTw8sPQ3zFiivtSN_xYhwrJ9wMNv3o5KdRQxJhxKG0MMsaP6grkNDgyO6x6c';

class HttpClient {
  Dio _client;

  HttpClient() {
    _client = Dio();
    _client
      ..interceptors.add(_header())
      ..options.baseUrl = BASE_URL
      ..options.connectTimeout = 60 * 1000
      ..options.receiveTimeout = 60 * 1000
      ..options.followRedirects = false;
  }

  Interceptor _header() {
    return InterceptorsWrapper(onRequest: (RequestOptions request) async {
      request.headers['Authorization'] =
          'Bearer ${LocaleManager.instance.getStringValue(PreferencesKeys.TOKEN)}';
      request.headers['ContentType'] = 'application/json';
      request.headers['Accept'] = 'application/json';
      request.headers['Language'] =
          await LanguageManager.instance.currentLocale;

      return request;
    });
  }

  Future<Response> get(String url, {Map<String, dynamic> params}) =>
      _client.get(url, queryParameters: params);

  Future<Response> post(String url,
          {Map<String, dynamic> body, Map<String, dynamic> params}) =>
      _client.post(url, data: body, queryParameters: params);

  Future<Response> put(String url,
          {Map<String, dynamic> params, Map<String, dynamic> body}) =>
      _client.put(url, queryParameters: params, data: body);

  Future<Response> delete(String url,
          {Map<String, dynamic> params, Map<String, dynamic> body}) =>
      _client.delete(url, queryParameters: params, data: body);
}
