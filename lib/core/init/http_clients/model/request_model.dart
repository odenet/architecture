class RequestModel<T> {
  String endpoint;
  dynamic body;
  Map<String, dynamic> params;

  RequestModel(this.endpoint, {this.body, this.params});
}
