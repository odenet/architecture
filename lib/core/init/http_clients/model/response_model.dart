import 'package:fluttermvvmtemplate/core/base/model/error_message.dart';
import 'package:fluttermvvmtemplate/view/common/models/network_error_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/pagination_model.dart';

class ResponseModel<T> {
  String message = "";
  T data;
  bool success = false;
  NetworkErrorModel networkError;
  PaginationModel pagination;
  ErrorMessageModel errorMessageModel;

  ResponseModel(
      {this.message,
      this.data,
      this.success,
      this.networkError,
      this.pagination,
      this.errorMessageModel});
}
