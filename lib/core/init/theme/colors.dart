import 'package:flutter/material.dart';

class AppColors{
  static Color CREATE = Colors.green;
  static Color UPDATE = Colors.blue;
  static Color DELETE = Colors.red;
  static Color READ = Colors.orange;
}