import 'package:flutter/cupertino.dart';
import 'package:fluttermvvmtemplate/core/base/model/base_navigation_model.dart';
import 'package:url_launcher/url_launcher.dart';

import 'INavigationService.dart';

class NavigationService implements INavigationService {
  static final NavigationService _instance = NavigationService._init();
  static NavigationService get instance => _instance;

  NavigationService._init();

  GlobalKey<NavigatorState> navigatorKey = GlobalKey();
  final removeAllOldRoutes = (Route<dynamic> route) => false;

  @override
  Future<void> navigateToPage({String path, Object data}) async {
    await navigatorKey.currentState.pushNamed(path, arguments: data);
  }

  @override
  Future<void> navigateToPageClear({String path, Object data}) async {
    await navigatorKey.currentState
        .pushNamedAndRemoveUntil(path, removeAllOldRoutes, arguments: data);
  }

  @override
  Future<void> navigate(BaseNavigationModel navigationModel,
      {Object data}) async {
    if (navigationModel.isUrl) {
      await _launchURL(navigationModel.route);
    } else {
      await navigatorKey.currentState
          .pushNamed(navigationModel.route, arguments: data);
    }
  }

  Future<void> _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
