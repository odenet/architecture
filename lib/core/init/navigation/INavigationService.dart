import 'package:fluttermvvmtemplate/core/base/model/base_navigation_model.dart';

abstract class INavigationService {
  Future<void> navigateToPage({String path, Object data});
  Future<void> navigateToPageClear({String path, Object data});
  Future<void> navigate(BaseNavigationModel navigationModel);
}
