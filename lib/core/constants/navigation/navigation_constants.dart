import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/components/card/not_found_navigation_widget.dart';
import 'package:fluttermvvmtemplate/view/authenticate/onboard/view/on_board_view.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/view/category_list_view.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/view/available_languages_list_view.dart';
import 'package:fluttermvvmtemplate/view/modules/system/object/view/object_home.dart';
import 'package:fluttermvvmtemplate/view/modules/system/types/view/type_list_view.dart';
import 'package:fluttermvvmtemplate/view/modules/system/version/view/version_list_view.dart';
import 'package:fluttermvvmtemplate/view/modules/system/watchlist/view/watchlist_home.dart';

class NavigationConstants {
  static Widget getView(name, arguments) {
    var routes = <String, dynamic>{
      'onboard': OnBoardView(),
      'object': ObjectHome(), //ObjectListView()
      'type': TypeView(),
      'category': CategoryListView(),
      'available-languages': AvailableLanguagesListView(),
      'version-control': VersionListView(),
      'watchlist': WatchlistHome()
    };

    if (routes.containsKey(name)) {
      return routes[name];
    } else {
      return NotFoundNavigationWidget();
    }
  }
}
