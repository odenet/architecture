class ApplicationConstants {
  static const LANG_ASSET_PATH = 'asset/lang';
  static const IPAD_NAME = 'IPAD';
  static const FONT_FAMILY = 'POPPINS';
  static const COMPANY_NAME = 'ATLAS';

  static const POST = 'post';
  static const PUT = 'put';
  static const GET = 'get';
  static const DELETE = 'delete';

  // Validation constants
  static const REQUIRED = 'required';
  static const MAX = 'max';
  static const MIN = 'min';
  static const REGEX = 'regex';

  static const EMAIL_REGIEX = r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}\$';

  // DATA TYPES
  static const OBJECT = 'object';
  static const OBJECT_LABEL = 'Object';
  static const SYSTEM_TYPE = 'systemType';
  static const SYSTEM_TYPE_LABEL = 'Type';
  static const SYSTEM_FEATURE = 'categoryFeatures';
  static const SYSTEM_FEATURE_LABEL = 'Feature';
  static const SYSTEM_CATEGORY = 'systemCategory';
  static const SYSTEM_CATEGORY_LABEL = 'Category';
  static const WATCHLIST = 'watchList';
  static const WATCHLIST_LABEL = 'Watchist';
  static const LOG = 'LOG';
  static const LOG_LABEL = 'LogList';

  // pagination
  static const PER_PAGE = 6;
}
