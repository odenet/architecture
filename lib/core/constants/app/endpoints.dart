class Endpoints {
  static const OBJECTS = 'objects/';
  static const RELATED_CATEGORY = 'related-category/';
  static const RELATED_FEATURES = 'related-features/';
  static const SYSTEM_TYPE = 'system-type/';
  static const SYSTEM_CATEGORY = 'system-category/';
  static const SYSTEM_FEATURE = 'category-features/';
  static const AVAILABLE_LANGUAGES = 'available-language/';
  static const SYSTEM_INFO = 'system-info/';
  static const SEARCH = 'search';
  static const CATEGORY_RELATED_FEATURES =
      'related-features/select/category-features';
  static const CATEGORY_RELATED_FEATURES_VALUE =
      'related-features/select/related-features';
  static const VERSION_CONTROL = 'versions';
  static const WATCHLIST = 'watch-list/';
  static const LOG = 'log/';
}
