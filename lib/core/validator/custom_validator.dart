import 'package:fluttermvvmtemplate/core/constants/app/app_constants.dart';
import 'package:validators/validators.dart';

class CustomValidator {
  static String runMultipleValidator(List validators) {
    for (var validation in validators) {
      var validationResult = validation;

      if (validationResult.isNotEmpty) {
        return validationResult;
      }
    }
  }

  static String nonNumericStringValidator(value) {
    RegExp re = RegExp(r'^\D+$');

    if (re.hasMatch(value)) {
      return null;
    } else {
      return 'Do not use numbers';
    }
  }

  static String regExValidator(String regex,String source) {
    var re = RegExp(regex);

    if (re.hasMatch(source)) {
      return null;
    } else {
      return 'Please enter valid input';
    }
  }

  static String _minLenght(String value, int length) {
    if (value.length >= length) {
      return null;
    } else {
      return 'Input length should be at most ' +
          length.toString() +
          ' character';
    }
  }

  static String _maxLenght(String value, int length) {
    if (value.length <= length) {
      return null;
    } else {
      return 'Input length should be at least ' +
          length.toString() +
          ' character';
    }
  }

  static String _requiredValidator(String value, bool param) {
    if (!param) {
      return null;
    } else {
      if (!isNull(value) && value.isNotEmpty) {
        return null;
      } else {
        return 'This field is required';
      }
    }
  }

  static dynamic selectAndRunFunction(Map rules,resource) {
    for (var data in rules.entries) {
      var validated = startValidation(key: data.key,param: data.value,resource: resource);
      if (validated != null) {
        return validated;
      }
    }
    return null;
  }

  static dynamic startValidation({String key, dynamic param, String resource}) {
    switch (key) {
      case ApplicationConstants.REQUIRED:
        return _requiredValidator(resource, param);
        break;
      case ApplicationConstants.MIN:
        return _minLenght(resource, param);
        break;
      case ApplicationConstants.MAX:
        return _maxLenght(resource, param);
        break;
      case ApplicationConstants.REGEX:
        return regExValidator(param,resource);  
      default:
        return null;
    }
  }
}
