import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/base/model/error_message.dart';
import 'package:fluttermvvmtemplate/core/components/alerts/custom_form_alert.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/response_model.dart';
import 'package:easy_localization/easy_localization.dart';

class AlertHelper {
  static var shared = AlertHelper();

  Future<void> startProgress(
      BuildContext context, ResponseModel response) async {
    if (response.success) {
      showAlert(context, 'success'.tr().toUpperCase(), response.message,
          CustomAlertType.SUCCESS);
      await Future.delayed(Duration(milliseconds: 1000), () {});
    } else {
      showAlert(context, 'error'.tr().toUpperCase(), response.message,
          CustomAlertType.ERROR);
      await Future.delayed(Duration(milliseconds: 3000), () {});
    }
  }

  void showProgressDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (_) {
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  void showAlert(BuildContext context, String title, String message,
      CustomAlertType type) {
    showDialog(
      context: context,
      builder: (_) {
        return CustomFormAlert(title: title, message: message, alertType: type,onConfirmText: 'Tamam',);
      },
    );
  }

  Future deleteDetector(BuildContext context, Function onPressed) {
    return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (ctx) {
          return AlertDialog(
            title: Text('WARNING !'),
            content: Text('Are you sure you want to delete this item'),
            actions: [
              FlatButton(
                  onPressed: (){
                    onPressed();
                    Navigator.pop(ctx);
                  },
                  child: Text(
                    'Delete',
                    style: TextStyle(color: Colors.red),
                  )),
              FlatButton(
                  onPressed: () {
                    Navigator.pop(ctx);
                  },
                  child: Text('Close'))
            ],
          );
        });
  }

  Future<void> errorMessages(BuildContext context, ErrorMessageModel errorMessageModel) {
    return showDialog(
        context: context,
        builder: (_) {
          return AlertDialog(
            title: Text(
              'Somethings Went Wrong',
              style: TextStyle(
                  fontSize: 14, fontWeight: FontWeight.bold, color: Colors.red),
              textAlign: TextAlign.center,
            ),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: errorMessageModel
                  .getValidateMessages()
                  .asMap()
                  .entries
                  .map((data) => Column(
                      children: data.value
                          .map((e) => Card(
                              elevation: 8,
                              child: ListTile(
                                title: Text(
                                  e,
                                  style: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.bold),
                                ),
                              )))
                          .toList()))
                  .toList(),
            ),
            actions: [
              Align(
                alignment: Alignment.center,
                child: ElevatedButton.icon(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: Icon(Icons.close),
                    label: Text('Close')),
              ),
            ],
          );
        });
  }
}
