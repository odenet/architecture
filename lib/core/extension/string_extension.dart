import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../constants/app/app_constants.dart';

extension StringLocalization on String {
  String get locale => this.tr();

  String get isValidEmail => contains(RegExp(ApplicationConstants.EMAIL_REGIEX))
      ? null
      : 'Email does not valid';

  bool get isValidEmails =>
      RegExp(ApplicationConstants.EMAIL_REGIEX).hasMatch(this);
}

extension ImagePathExtension on String {
  String get toSVG => 'asset/svg/$this.svg';
}

extension CustomTextFieldType on String {
  TextInputType makeType() {
    switch (this) {
      case 'name':
        return TextInputType.name;
        break;
      case 'number':
        return TextInputType.number;
        break;
      case 'emailAddress':
        return TextInputType.emailAddress;
        break;
      case 'phone':
        return TextInputType.phone;
        break;
      default:
        return TextInputType.name;
    }
  }
}
