import 'package:flutter/material.dart';

extension MakeToolTip on Widget {
  Widget addToolTip(String message) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        this,
        Tooltip(
          message: message,
          child: Icon(Icons.info),
        )
      ],
    );
  }

  Widget addSnackBar({@required String message, @required GlobalKey<ScaffoldState> key}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        this,
        GestureDetector(
            onTap: () {
              key.currentState.showSnackBar(SnackBar(
                behavior: SnackBarBehavior.floating,
                content: Text(message,style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold)),
                duration: Duration(seconds: 2),
                backgroundColor: Color(0xFF304352),
              ));
            },
            child: Icon(
              Icons.info,
              size: 24,
            ))
      ],
    );
  }
}
