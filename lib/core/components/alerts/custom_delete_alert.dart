import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/components/form/base_form.dart';

class CustomDeleteAlert extends StatelessWidget {
  final String title;
  final String message;
  final VoidCallback onConfirm;

  CustomDeleteAlert({@required this.title, @required this.message, this.onConfirm});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      clipBehavior: Clip.none,
      backgroundColor: Colors.transparent,
      child: Container(
          clipBehavior: Clip.none,
          width: MediaQuery.of(context).size.width * 0.8,
          decoration: BoxDecoration(
              color: Color(0xFF5D5C61), borderRadius: BorderRadius.circular(8)),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: 8,
              ),
              Text(
                title,
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
              SizedBox(
                height: 8,
              ),
              Text(
                message,
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
              SizedBox(
                height: 8,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  CustomNormalButton(onPressed: onConfirm,buttonColor: Colors.red,text: 'Delete',textColor: Colors.white,),
                  ElevatedButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text('Cancel'))
                ],
              ),
              SizedBox(
                height: 8,
              ),
            ],
          )),
    );
  }
}
