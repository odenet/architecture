import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/components/form/base_form.dart';

enum CustomAlertType { ERROR, SUCCESS, WARNING }

class CustomFormAlert extends StatelessWidget {
  final String title;
  final String message;
  final CustomAlertType alertType;
  final String onConfirmText;
  final String onCloseText;
  final VoidCallback onConfirm;
  final VoidCallback onClose;
  final bool showIsCancel;

  CustomFormAlert(
      {@required this.title,
      @required this.message,
      @required this.alertType,
      this.onConfirm,
      this.onClose,
      this.onCloseText = 'CLOSE',
      this.onConfirmText = 'OKEY',
      this.showIsCancel = false});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      clipBehavior: Clip.none,
      backgroundColor: Colors.transparent,
      child: Container(
        clipBehavior: Clip.none,
        height: MediaQuery.of(context).size.height * 0.31,
        width: MediaQuery.of(context).size.width * 0.8,
        decoration: BoxDecoration(
            color: Colors.blueGrey, borderRadius: BorderRadius.circular(8)),
        child: Stack(
          fit: StackFit.expand,
          clipBehavior: Clip.none,
          children: [
            Positioned(
                top: -45,
                left: (MediaQuery.of(context).size.width * 0.8) / 2 - 45,
                child: SizedBox(
                  height: 90,
                  width: 90,
                  child: Image.asset(changeAlertImage()),
                )),
            Positioned(
              top: 70,
              left: 20,
              right: 20,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    title,
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                  SizedBox(height: 16),
                  Padding(
                    padding: const EdgeInsets.only(left: 8, right: 8),
                    child: Text(
                      message,
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white),
                      maxLines: 3,
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CustomNormalButton(
                        onPressed: () {
                          if (onConfirm != null) {
                            onConfirm();
                          }
                        },
                        text: onConfirmText,
                        textColor: Colors.white,
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      showIsCancel
                          ? CustomNormalButton(
                              onPressed: () {
                                if (onClose != null) {
                                  onClose();
                                }
                              },
                              text: onCloseText,
                              textColor: Colors.white,
                              buttonColor: Colors.red,
                            )
                          : Container(),
                    ],
                  ),
                  SizedBox(height: 8)
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  String changeAlertImage() {
    var imagePath = 'asset/image/alerts/';

    switch (alertType) {
      case CustomAlertType.ERROR:
        return imagePath + 'error.png';
        break;
      case CustomAlertType.SUCCESS:
        return imagePath + 'successful.png';
        break;
      case CustomAlertType.WARNING:
        return imagePath + 'warning.png';
        break;
      default:
        return imagePath + 'successful.png';
    }
  }
}
