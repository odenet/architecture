import 'package:flutter/material.dart';

class CustomCircleProgress {
  static showIndicator(BuildContext context) {
    showGeneralDialog(
      context: context,
      barrierColor: Colors.black12.withOpacity(0.6),
      transitionDuration: Duration(milliseconds: 700),
      pageBuilder: (_, __, ___) {
        return SizedBox.expand(
          child: Center(child: CircularProgressIndicator()),
        );
      },
    );
  }
}
