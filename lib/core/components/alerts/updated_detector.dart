import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

import 'custom_form_alert.dart';

Future<void> updatedDetector(BuildContext context, dynamic viewModel) async {
  if (viewModel.isEdit) {
    await showDialog(
        context: viewModel.context,
        builder: (_) {
          return CustomFormAlert(
            title: 'common.change_detected_title'.tr(),
            message: 'common.change_detected_text'.tr(),
            alertType: CustomAlertType.WARNING,
            showIsCancel: true,
            onConfirm: () {
              Navigator.pop(context);
              viewModel.setIsEdit(false);
            },
            onClose: () {
              viewModel.setIsEdit(false);
            },
          );
        });
  }
}
