import 'package:flutter/material.dart';
import 'package:giffy_dialog/giffy_dialog.dart';

class GiffyAlert {
  static var shared = GiffyAlert();

  Future show(String titleText,String description,{BuildContext context,EntryAnimation animation = EntryAnimation.DEFAULT,VoidCallback okButtonPressed}) {
    return showDialog(
        context: context,
        builder: (_) => AssetGiffyDialog(
              title: Text(
                titleText,
                style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600),
              ),
              description: Text(description),
              entryAnimation: animation,
              onOkButtonPressed: () {
                if(okButtonPressed != null){
                  okButtonPressed();
                }
              }, image: Image.asset('asset/image/alerts/men_wearing_jacket.gif',fit: BoxFit.fill,),
            ));
  }
}
