import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/base/model/base_navigation_model.dart';
import 'package:fluttermvvmtemplate/core/init/navigation/navigation_service.dart';
import 'package:page_transition/page_transition.dart';

class NavigationListView extends StatelessWidget {
  final List<Map> list;
  final String title;

  const NavigationListView({Key key, this.list, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(title ?? '')),
        body: Container(
            child: ListView.builder(
                padding: const EdgeInsets.all(8),
                itemCount: list.length,
                itemBuilder: (BuildContext context, int index) {
                  return Card(
                      child: ListTile(
                    trailing: Icon(Icons.arrow_forward_ios_outlined),
                    leading: Icon(IconData(list[index]['icon'],
                        fontFamily: 'MaterialIcons')),
                    title: Text(list[index]['label']),
                    onTap: () {
                      if (list[index]['route'] is List) {
                        return Navigator.push(
                          context,
                          PageTransition(
                              type: PageTransitionType.rightToLeft,
                              child: NavigationListView(
                                list: list[index]['route'],
                                title: list[index]['label'],
                              )),
                        );
                      } else {
                        return NavigationService.instance.navigateToPageClear(
                            path: BaseNavigationModel()
                                .getModel(list[index])
                                .route,
                            data: list[index]['data']);
                      }
                    },
                  ));
                })));
  }
}
