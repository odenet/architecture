import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/components/list-view/bottom_sheet_list_view.dart';
import 'package:page_transition/page_transition.dart';
import 'package:easy_localization/easy_localization.dart';

enum ActionType { CRUD, MULTISELECTABLE }

class SelectableNavigationListView extends StatefulWidget {
  final List<dynamic> list;
  final List<dynamic> fullList;
  final String title;
  final Function(List) onSelect;
  final Function(int) onUpdate;
  final ActionType actionType;
  final Function(SelectedAction, dynamic model) onSelectAction;
  final List<SelectedAction> selectedActions;
  final String updateText;
  final String createText;
  final String deleteText;
  final String listText;
  final bool isAppBarAllowed;
  final Function(int) onTap;
  final bool isChoose;

  SelectableNavigationListView(
      {Key key,
      this.list,
      this.title,
      @required this.onSelect,
      @required this.actionType,
      @required this.onUpdate,
      this.onSelectAction,
      this.fullList,
      this.updateText = 'Update',
      this.createText = 'Create',
      this.deleteText = 'Delete',
      this.listText = 'List',
      this.selectedActions,
      this.isChoose = false,
      this.onTap,
      this.isAppBarAllowed = false})
      : super(key: key);

  @override
  _SelectableNavigationListViewState createState() =>
      _SelectableNavigationListViewState();
}

class _SelectableNavigationListViewState
    extends State<SelectableNavigationListView> {
  List<bool> isSelected;
  List<String> selectedItems = [];

  @override
  void initState() {
    super.initState();
    isSelected = List.generate(widget.list.length, (index) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: widget.isAppBarAllowed ? AppBar(title: Text('Categories')) : null,
      body: ListView.builder(
          padding: const EdgeInsets.all(8),
          shrinkWrap: true,
          itemCount: widget.list.length,
          itemBuilder: (BuildContext context, int index) {
            var convertList = widget.fullList
                .where((element) => element.parent == widget.list[index].slug)
                .toList();

            return Card(
                child: ListTile(
              trailing: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  (convertList.isNotEmpty &&
                          ActionType.CRUD == widget.actionType)
                      ? Icon(Icons.chevron_right_sharp)
                      : Container(),
                  widget.actionType == ActionType.CRUD
                      ? GestureDetector(
                          onTap: () {
                            showBottomSheet(context, widget.list[index]);
                          },
                          child: Icon(Icons.more_horiz))
                      : Container(),
                ],
              ),
              leading: GestureDetector(
                  onTap: () => _setupAction(index),
                  child: CircleAvatar(
                    backgroundColor: (!widget.list[index].isActive ||
                            !widget.list[index].isPublic)
                        ? Colors.black
                        : Colors.grey,
                    child: isSelected[index]
                        ? Icon(Icons.check)
                        : Container(
                            child: Text(
                              widget.list[index].name[0].toUpperCase(),
                              style: TextStyle(color: Colors.black),
                            ),
                          ),
                  )),
              title: Text(widget.list[index].name),
              onTap: () {
                var clickedItem = widget.onTap(index);

                var convertList = widget.fullList
                    .where((element) => element.parent == clickedItem.slug)
                    .toList();

                if (convertList.isNotEmpty &&
                    ActionType.CRUD == widget.actionType) {
                  Navigator.push(
                    context,
                    PageTransition(
                        type: PageTransitionType.rightToLeft,
                        child: SelectableNavigationListView(
                            onTap: (index) {
                              return convertList[index];
                            },
                            onSelect: (value) {},
                            onUpdate: (value) {
                              setState(() {});
                            },
                            listText: 'system.category.list_features'.tr(),
                            createText: 'system.category.add_sub_category'.tr(),
                            selectedActions: widget.selectedActions,
                            actionType: widget.actionType,
                            list: convertList,
                            fullList: widget.fullList,
                            isAppBarAllowed: true,
                            onSelectAction: widget.onSelectAction)),
                  );
                } else {}
              },
            ));
          }),
      floatingActionButton: isSelected.contains(true)
          ? FloatingActionButton(
              onPressed: () {
                widget.onSelect(selectedItems);
              },
              child: Text('Gönder'),
            )
          : Container(),
    );
  }

  void _setupAction(int index) {
    if (widget.actionType == ActionType.CRUD) {
    } else {
      return _selectableAction(index);
    }
  }

  void _selectableAction(int index) {
    if (isSelected[index]) {
      selectedItems.remove(widget.list[index].slug);
    } else {
      selectedItems.add(widget.list[index].slug);
    }
    setState(() {
      isSelected[index] = !isSelected[index];
    });
  }

  void showBottomSheet(BuildContext context, dynamic model) {
    showModalBottomSheet(
        context: context,
        builder: (_) {
          return Container(
            child: Column(
                mainAxisSize: MainAxisSize.min, children: getActions(model)),
          );
        });
  }

  List<Widget> getActions(dynamic model) {
    List<Widget> result = [];
    var boldStyle = TextStyle(fontWeight: FontWeight.bold);

    if (widget.selectedActions.contains(SelectedAction.LIST)) {
      result.add(ListTile(
        leading: Icon(
          Icons.list,
          color: Colors.orange,
        ),
        title: Text(
          'common.list'.tr(),
          style: boldStyle,
        ),
        subtitle: Text('common.list_subtitle'.tr()),
        onTap: () {
          Navigator.pop(context);
          widget.onSelectAction(SelectedAction.LIST, model);
        },
      ));
    }

    if (widget.selectedActions.contains(SelectedAction.UPDATE)) {
      result.add(ListTile(
        leading: Icon(
          Icons.edit,
          color: Colors.blue,
        ),
        title: Text(
          'common.update'.tr(),
          style: boldStyle,
        ),
        subtitle: Text('common.update_subtitle'.tr()),
        onTap: () {
          Navigator.pop(context);
          widget.onSelectAction(SelectedAction.UPDATE, model);
        },
      ));
    }

    if (widget.selectedActions.contains(SelectedAction.CREATE)) {
      result.add(ListTile(
        leading: Icon(
          Icons.add_box,
          color: Colors.green,
        ),
        title: Text(widget.createText, style: boldStyle),
        subtitle: Text('common.create_subtitle'.tr()),
        onTap: () {
          Navigator.pop(context);
          widget.onSelectAction(SelectedAction.CREATE, model);
        },
      ));
    }

    if (widget.selectedActions.contains(SelectedAction.DELETE)) {
      result.add(ListTile(
          leading: Icon(
            Icons.delete,
            color: Colors.red,
          ),
          title: Text('common.delete'.tr(), style: boldStyle),
          subtitle: Text('common.delete_subtitle'.tr()),
          onTap: () {
            Navigator.pop(context);
            widget.onSelectAction(SelectedAction.DELETE, model);

            setState(() {});
          }));
    }

    return result;
  }
}
