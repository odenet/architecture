import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

enum SelectedAction { CREATE, LIST, DELETE, UPDATE }

class BottomSheetListView extends StatefulWidget {
  final List<dynamic> list;
  final Function(int) onTap;
  final Function(SelectedAction, dynamic model) onSelectAction;
  final List<SelectedAction> selectedActions;
  final String updateText;
  final String createText;
  final String deleteText;
  final String listText;
  final bool isMultiple;

  const BottomSheetListView(
      {Key key,
      @required this.list,
      @required this.onTap,
      @required this.onSelectAction,
      this.updateText = 'Update',
      this.createText = 'Create',
      this.deleteText = 'Delete',
      this.listText = 'List',
      this.selectedActions,
      this.isMultiple = false})
      : super(key: key);

  @override
  _BottomSheetListViewState createState() => _BottomSheetListViewState();
}

class _BottomSheetListViewState extends State<BottomSheetListView> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: widget.list.length,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return Card(
            child: ListTile(
                leading: CircleAvatar(
                  backgroundColor: (!widget.list[index].isActive ||
                          !widget.list[index].isPublic)
                      ? Colors.black
                      : Colors.grey,
                  child: Container(
                    child: Text(
                      widget.list[index].name[0].toUpperCase(),
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                ),
                title: Text(widget.list[index].name),
                trailing: IconButton(
                    icon: Icon(Icons.more_horiz),
                    onPressed: () {
                      showBottomSheet(context, widget.list[index]);
                    }),
                onTap: () {
                  widget.onTap(index);
                }),
          );
        });
  }

  void showBottomSheet(BuildContext context, dynamic model) {
    showModalBottomSheet(
        context: context,
        builder: (_) {
          return Container(
            child: Column(
                mainAxisSize: MainAxisSize.min, children: getActions(model)),
          );
        });
  }

  List<Widget> getActions(dynamic model) {
    List<Widget> result = [];
    var boldStyle = TextStyle(fontWeight: FontWeight.bold);

    if (widget.selectedActions.contains(SelectedAction.LIST)) {
      result.add(ListTile(
        leading: Icon(
          Icons.list,
          color: Colors.orange,
        ),
        title: Text(
          widget.listText,
          style: boldStyle,
        ),
        subtitle:
            Text('common.list_explanation'.tr(namedArgs: {'name': model.name})),
        onTap: () {
          Navigator.pop(context);
          widget.onSelectAction(SelectedAction.LIST, model);
        },
      ));
    }

    if (widget.selectedActions.contains(SelectedAction.UPDATE)) {
      result.add(ListTile(
        leading: Icon(
          Icons.edit,
          color: Colors.blue,
        ),
        title: Text(
          'common.update'.tr(),
          style: boldStyle,
        ),
        subtitle: Text(
            'common.update_explanation'.tr(namedArgs: {'name': model.name})),
        onTap: () {
          Navigator.pop(context);
          widget.onSelectAction(SelectedAction.UPDATE, model);
        },
      ));
    }

    if (widget.selectedActions.contains(SelectedAction.CREATE)) {
      result.add(ListTile(
        leading: Icon(
          Icons.add_box,
          color: Colors.green,
        ),
        title: Text(widget.createText, style: boldStyle),
        subtitle: Text(
            'common.create_explanation'.tr(namedArgs: {'name': model.name})),
        onTap: () {
          Navigator.pop(context);
          widget.onSelectAction(SelectedAction.CREATE, model);
        },
      ));
    }

    if (widget.selectedActions.contains(SelectedAction.DELETE)) {
      result.add(ListTile(
          leading: Icon(
            Icons.delete,
            color: Colors.red,
          ),
          title: Text('common.delete'.tr(), style: boldStyle),
          subtitle: Text(
              'common.delete_explanation'.tr(namedArgs: {'name': model.name})),
          onTap: () {
            Navigator.pop(context);
            widget.onSelectAction(SelectedAction.DELETE, model);
            setState(() {});
          }));
    }

    return result;
  }
}
