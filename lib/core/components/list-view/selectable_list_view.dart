import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/helpers/alert_helper.dart';
import 'package:easy_localization/easy_localization.dart';

class SelectableListView extends StatefulWidget {
  final List<dynamic> list;
  final Function(List) onSelect;
  final Function(int) onUpdate;
  final Function(int) onDelete;

  const SelectableListView(
      {@required this.list,
      @required this.onSelect,
      @required this.onUpdate,
      @required this.onDelete});

  @override
  _SelectableListViewState createState() => _SelectableListViewState();
}

class _SelectableListViewState extends State<SelectableListView> {
  List<bool> chooseList;
  final boldStyle = const TextStyle(fontWeight: FontWeight.bold);
  List selectedList = [];

  @override
  void initState() {
    chooseList = List.generate(widget.list.length, (index) => false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        chooseList.contains(true)
            ? Card(
                color: Color(0xFF37268c),
                child: ListTile(
                  leading: Icon(
                    Icons.my_library_add_outlined,
                    color: Colors.white,
                  ),
                  title: Text(
                    'Selected Items : ${chooseList.where((element) => element == true).length}',
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                  trailing: ElevatedButton.icon(
                    onPressed: () {
                      widget.onSelect(selectedList);
                    },
                    icon: Icon(Icons.arrow_right),
                    label: Text('Select'),
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Color(0xFF288f7e))),
                  ),
                ),
              )
            : Container(),
        Expanded(
          child: ListView.builder(
              itemCount: widget.list.length,
              itemBuilder: (context, index) {
                return Card(
                  child: ListTile(
                    trailing: IconButton(
                        icon: Icon(Icons.more_horiz),
                        onPressed: () {
                          showBottomSheet(context, index);
                        }),
                    leading: GestureDetector(
                        onTap: () {
                          if (chooseList[index]) {
                            selectedList.remove(widget.list[index]);
                          } else {
                            selectedList.add(widget.list[index]);
                          }
                          setState(() {
                            chooseList[index] = !chooseList[index];
                          });
                        },
                        child: CircleAvatar(
                          child: Text(
                            widget.list[index].name[0].toUpperCase(),
                            style: TextStyle(color: Colors.black),
                          ),
                          backgroundColor: (!widget.list[index].isActive ||
                                  !widget.list[index].isPublic)
                              ? Colors.black
                              : Colors.grey,
                        )),
                    title: Text(widget.list[index].name),
                  ),
                );
              }),
        ),
      ],
    );
  }

  void showBottomSheet(BuildContext context, int index) {
    showModalBottomSheet(
        context: context,
        builder: (_) {
          return Container(
            child: Column(mainAxisSize: MainAxisSize.min, children: [
              ListTile(
                leading: Icon(
                  Icons.edit,
                  color: Colors.blue,
                ),
                title: Text(
                  'common.update'.tr(),
                  style: boldStyle,
                ),
                subtitle: Text('common.update_subtitle'.tr()),
                onTap: () {
                  Navigator.pop(context);
                  widget.onUpdate(index);
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.delete,
                  color: Colors.red,
                ),
                title: Text(
                  'common.delete'.tr(),
                  style: boldStyle,
                ),
                subtitle: Text(
                  'common.delete_subtitle'.tr(),
                ),
                onTap: () {
                  Navigator.pop(context);
                  AlertHelper.shared.deleteDetector(context, () {
                    widget.onDelete(index);
                  });
                },
              )
            ]),
          );
        });
  }
}
