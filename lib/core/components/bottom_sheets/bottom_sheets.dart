import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/init/theme/colors.dart';

enum ActionType { SHOW_PROFILE, SHOW_DETAIL }

class DetailBottomSheetButton extends StatelessWidget {
  final Icon buttonIcon;
  final List<ActionType> detailActions;
  final Function(ActionType) onTap;

  const DetailBottomSheetButton({Key key, @required this.detailActions,@required this.onTap,this.buttonIcon = const Icon(Icons.more_horiz_rounded)})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
        icon: buttonIcon,
        onPressed: () => showBottomSheet(context));
  }

  void showBottomSheet(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (_) {
          return Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
                children: detailActions
                    .map(
                      (action) => ListTile(
                        onTap: () => onTap(action),
                        title: Text(action.rawValue,style: TextStyle(fontWeight: FontWeight.w600),),
                        leading: action.leadingIcon,
                        trailing: Icon(Icons.arrow_forward_ios_rounded),
                      ),
                    ).toList()),
          );
        });
  }
}

extension Configuration on ActionType {
  String get rawValue{
    switch (this) {
      case ActionType.SHOW_PROFILE:
        return 'Profili Göster';
      case ActionType.SHOW_DETAIL:
        return 'Kayıtlarını Göster';
      default:
        return 'Hata';
    }
  }

 Icon get leadingIcon{
   switch (this){
     case ActionType.SHOW_PROFILE:
       return Icon(Icons.account_circle_sharp,color: AppColors.UPDATE,);
     case ActionType.SHOW_DETAIL:
       return Icon(Icons.list_rounded,color: AppColors.READ,);
     default:
       return Icon(Icons.error);
   }
 }
}
