import 'package:flutter/material.dart';


class ErrorPage extends StatelessWidget {

  final String errorText;
  final Color textColor;

  ErrorPage({@required this.errorText,this.textColor = Colors.red});

  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [

          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Image.asset('asset/image/alerts/error_face.png',height: 90,width: 90,),
          ),
          
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(errorText,style: TextStyle(color: Colors.red,fontWeight: FontWeight.bold,fontSize: 22),),
          ),
        ],
      ),
    ));
  }
}