import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

class FlagAppBar extends StatelessWidget with PreferredSizeWidget {
  final Widget title;
  final List<Widget> actions;

  const FlagAppBar({Key key, this.title, this.actions}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: title,
      actions: [
        IconButton(
          icon: Text(context.locale.countryCode.toUpperCase()),
          onPressed: () {},
        ),
        ...actions ?? []
      ],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(55.0);
}
