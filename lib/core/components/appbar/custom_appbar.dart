import 'package:flutter/material.dart';

class CustomSearchAppBar<T> extends StatefulWidget
    implements PreferredSizeWidget {
  final Color appBarColor;
  final String title;
  final bool centerTitle;
  final bool isShowCreateButton;
  final VoidCallback onCreate;
  final Function(String) onSearch;
  final VoidCallback multiSearch;
  final PreferredSizeWidget bottom;

  const CustomSearchAppBar(
      {Key key,
      this.appBarColor,
      this.title,
      this.centerTitle,
      this.onCreate,
      this.onSearch,
      this.isShowCreateButton = false,
      this.bottom,
      this.multiSearch})
      : super(key: key);

  @override
  _CustomSearchAppBarState createState() => _CustomSearchAppBarState();

  @override
  Size get preferredSize => Size(500, 55);
}

class _CustomSearchAppBarState extends State<CustomSearchAppBar> {
  var isSearch = false;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: widget.appBarColor,
      title: isSearch ? _buildSearchTextField() : Text(widget.title),
      centerTitle: widget.centerTitle,
      bottom: widget.bottom,
      actions: [
        IconButton(
            icon: Icon(isSearch ? Icons.cancel : Icons.search),
            onPressed: () {
              setState(() {
                isSearch = !isSearch;
              });
            }),
        widget.isShowCreateButton ? IconButton(icon: Icon(Icons.add_circle), onPressed: widget.onCreate) : Container(),
      ],
    );
  }

  Widget _buildSearchTextField() {
    return TextField(
      decoration: InputDecoration(hintText: 'Search'),
      onChanged: widget.onSearch,
    );
  }
}
