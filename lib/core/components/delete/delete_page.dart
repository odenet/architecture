import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/components/form/base_form.dart';


class DeletePage extends StatelessWidget {

  final VoidCallback onPressed;
  final String buttonText;

  const DeletePage({Key key,@required this.onPressed,@required this.buttonText}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CustomNormalButton(onPressed: onPressed,text: buttonText,buttonColor: Colors.red,textColor: Colors.white,)
        ],
      ),
    ));
  }
}