import 'package:clipboard/clipboard.dart';
import 'package:flutter/material.dart';

class SlugCard extends StatelessWidget {
  final String slug;

  const SlugCard({Key key, @required this.slug}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10),
      child: Card(
        child: ListTile(
          title: Text('Slug', style: TextStyle(fontWeight: FontWeight.bold)),
          subtitle: Text(
            slug,
          ),
          trailing: IconButton(
              icon: Icon(Icons.copy),
              onPressed: () {
                FlutterClipboard.copy(slug);
                Scaffold.of(context).showSnackBar(
                  SnackBar(
                    content: const Text('Copied'),
                    duration: const Duration(seconds: 1),
                  ),
                );
              }),
        ),
      ),
    );
  }
}
