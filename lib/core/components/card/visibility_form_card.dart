import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/components/form/visibility_form.dart';
import 'package:fluttermvvmtemplate/core/init/theme/colors.dart';
import 'package:page_transition/page_transition.dart';
import 'package:easy_localization/easy_localization.dart';

class VisibilityFormCard<T> extends StatelessWidget {
  final viewModel;
  final Function onUpdate;

  VisibilityFormCard({Key key, this.viewModel, this.onUpdate}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Card(
          child: Column(
        children: <Widget>[
          Row(
            children: [
              Expanded(
                child: ListTile(
                  title: Text(
                    'common.visibility'.tr(),
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Padding(
                  padding: EdgeInsets.fromLTRB(0, 0, 15, 0),
                  child: IconButton(
                      icon: Icon(Icons.edit, color: AppColors.UPDATE),
                      onPressed: () =>
                          navigateToVisibilityForm(context, viewModel)))
            ],
          ),
          Row(
            children: [
              Expanded(
                  child: ListTile(
                leading: viewModel.model.isPublic
                    ? Icon(Icons.public,color: Color(0xFF007d0f))
                    : Icon(Icons.public_off,color: Colors.red),
                title: viewModel.model.isPublic
                    ? Text('common.public'.tr(), style: TextStyle(color: Color(0xFF007d0f),fontWeight: FontWeight.bold))
                    : Text('common.private'.tr(), style: TextStyle(color: Colors.red,fontWeight: FontWeight.bold)),
              )),
              Expanded(
                  child: ListTile(
                leading: viewModel.model.isActive
                    ? Icon(Icons.thumb_up,color: Color(0xFF007d0f),)
                    : Icon(Icons.thumb_down,color: Colors.red),
                title: viewModel.model.isActive
                    ? Text('common.active'.tr(), style: TextStyle(color: Color(0xFF007d0f),fontWeight: FontWeight.bold))
                    : Text('common.passive'.tr(), style: TextStyle(color: Colors.red,fontWeight: FontWeight.bold)),
              )),
            ],
          )
        ],
      )),
    );
  }

  Future navigateToVisibilityForm(BuildContext context, dynamic viewModel) {
    return Navigator.push(
        context,
        PageTransition(
            type: PageTransitionType.rightToLeft,
            child: VisibilityForm(
              viewModel: viewModel,
              onUpdate: () {
                onUpdate();
              },
            )));
  }
}
