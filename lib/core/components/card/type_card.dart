import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

class TypeCard extends StatelessWidget {
  final String type;

  const TypeCard({Key key, this.type}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Card(
          child: Row(
        children: <Widget>[
          Expanded(
            child: ListTile(
              title: Text(
                'system.type.title'.tr().toUpperCase(),
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Expanded(
            child: ListTile(
              title: Text((type != null) ? type : ' '),
            ),
          )
        ],
      )),
    );
  }
}
