import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/init/theme/colors.dart';
import 'package:easy_localization/easy_localization.dart';

class CustomVisibilityFormCard extends StatefulWidget {
  final Function(bool public, bool active) onUpdate;
  final bool isActive;
  final bool isPublic;

  CustomVisibilityFormCard(
      {Key key, this.onUpdate, this.isActive, this.isPublic})
      : super(key: key);

  @override
  _CustomVisibilityFormCardState createState() =>
      _CustomVisibilityFormCardState();
}

class _CustomVisibilityFormCardState extends State<CustomVisibilityFormCard> {
  bool isActive;
  bool isPublic;

  @override
  void initState() {
    isActive = widget.isActive;
    isPublic = widget.isPublic;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Card(
          child: Column(
        children: <Widget>[
          Row(
            children: [
              Expanded(
                child: ListTile(
                  title: Text(
                    'VISIBILITY',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Padding(
                  padding: EdgeInsets.fromLTRB(0, 0, 15, 0),
                  child: IconButton(
                      icon: Icon(Icons.edit, color: AppColors.UPDATE),
                      onPressed: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (_) => EditVisibilityView(
                                    onUpdate: (public, active) {
                                      setState(() {
                                        isPublic = public;
                                        isActive = active;
                                        widget.onUpdate(public, active);
                                      });
                                    },
                                    isActive: isActive,
                                    isPublic: isPublic,
                                  )))))
            ],
          ),
          Row(
            children: [
              Expanded(
                  child: ListTile(
                leading: isPublic
                    ? Icon(Icons.public,color: Color(0xFF007d0f))
                    : Icon(Icons.public_off,color: Colors.red),
                title: isPublic
                    ? Text('common.public'.tr(), style: TextStyle(color: Color(0xFF007d0f),fontWeight: FontWeight.bold))
                    : Text('common.private'.tr(), style: TextStyle(color: Colors.red,fontWeight: FontWeight.bold)),
              )),
              Expanded(
                  child: ListTile(
                leading: isActive
                    ? Icon(Icons.thumb_up,color: Color(0xFF007d0f),)
                    : Icon(Icons.thumb_down,color: Colors.red),
                title:isActive
                    ? Text('common.active'.tr(), style: TextStyle(color: Color(0xFF007d0f),fontWeight: FontWeight.bold))
                    : Text('common.passive'.tr(), style: TextStyle(color: Colors.red,fontWeight: FontWeight.bold)),
              )),
            ],
          )
        ],
      )),
    );
  }
}

class EditVisibilityView extends StatefulWidget {
  final Function(bool public, bool active) onUpdate;
  final bool isActive;
  final bool isPublic;

  const EditVisibilityView(
      {Key key, this.onUpdate, this.isActive, this.isPublic})
      : super(key: key);

  @override
  _EditVisibilityViewState createState() => _EditVisibilityViewState();
}

class _EditVisibilityViewState extends State<EditVisibilityView> {
  bool isActive;
  bool isPublic;

  @override
  void initState() {
    isActive = widget.isActive;
    isPublic = widget.isPublic;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Visibility'),
      ),
      body: Column(
        children: [
          CheckboxListTile(
            value: isActive,
            onChanged: (val) {
              setState(() {
                isActive = val;
              });
            },
            title: Text('Active'),
          ),
          CheckboxListTile(
            value: isPublic,
            onChanged: (val) {
              setState(() {
                isPublic = val;
              });
            },
            title: Text('Public'),
          ),
          ElevatedButton.icon(
              onPressed: () {
                widget.onUpdate(isPublic, isActive);
                Navigator.pop(context);
              },
              icon: Icon(Icons.save),
              label: Text('Save'))
        ],
      ),
    );
  }
}
