import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/init/theme/colors.dart';
import 'package:easy_localization/easy_localization.dart';

class ShowMoreCard extends StatefulWidget {
  final List list;
  final Function() onCreate;
  final Function() onUpdate;

  ShowMoreCard({Key key, this.list, this.onCreate, this.onUpdate})
      : super(key: key);

  _ShowMoreCardState createState() => _ShowMoreCardState();
}

class _ShowMoreCardState extends State<ShowMoreCard> {
  bool descTextShowFlag = false;

  @override
  Widget build(BuildContext context) {
    return (widget.list.isNotEmpty)
        ? Container(
            margin: EdgeInsets.all(16.0),
            child: Card(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ListTile(
                      leading: Icon(Icons.list),
                      title: Text(
                        ' CATEGORIES ',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 15),
                      ),
                      trailing: IconButton(
                          icon: Icon(Icons.edit, color: AppColors.UPDATE),
                          onPressed: () => widget.onUpdate())),
                  getList(context, widget.list,
                      descTextShowFlag ? widget.list.length : 1),
                  InkWell(
                    onTap: () {
                      setState(() {
                        descTextShowFlag = !descTextShowFlag;
                      });
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        descTextShowFlag
                            ? Padding(
                                padding: EdgeInsets.all(10),
                                child: Text(
                                  "Show Less",
                                  style: TextStyle(color: Colors.blue),
                                ))
                            : Padding(
                                padding: EdgeInsets.all(10),
                                child: Text("Show More",
                                    style: TextStyle(color: Colors.blue)),
                              ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          )
        : Container(
            child: Card(
                child: ListTile(
                    title: Text('system.object.create'),
                    onTap: () {
                      return widget.onCreate();
                    })));
  }

  Widget getList(BuildContext context, List list, int count) {
    return ListView.builder(
        itemCount: count,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return ListTile(
              title: (list[index].name != null)
                  ? Text(list[index].name)
                  : Text(''));
        });
  }
}
