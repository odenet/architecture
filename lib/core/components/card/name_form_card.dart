import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/components/form/update_form.dart';
import 'package:fluttermvvmtemplate/core/init/theme/colors.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/model/language_info_model.dart';
import 'package:page_transition/page_transition.dart';
import 'package:easy_localization/easy_localization.dart';

class NameFormCard extends StatelessWidget {
  final viewModel;
  final Function(dynamic dataModel) onUpdate;

  const NameFormCard({Key key, this.viewModel, this.onUpdate})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Card(
          child: Column(
            children: [
              ListTile(
                leading: viewModel.languageModel.name.isEmpty
                    ? Icon(Icons.info, color: Colors.red)
                    : null,
                trailing: IconButton(
                    icon: Icon(
                        viewModel.languageModel.name.isEmpty
                            ? Icons.add
                            : Icons.edit,
                        color: viewModel.languageModel.name.isEmpty
                            ? AppColors.CREATE
                            : AppColors.UPDATE),
                    onPressed: () {
                      viewModel.dirtyLanguageModel =
                          LanguageInfoModel.clone(viewModel.languageModel);
                      return navigateToUpdateForm(context, viewModel);
                    }),
                title: viewModel.languageModel.name.isEmpty
                    ? Text('common.need_translate'.tr())
                    : Text(viewModel.languageModel.name),
                subtitle: viewModel.languageModel.description.isEmpty
                    ? Text('common.add_translate'.tr(),
                        style: TextStyle(color: Colors.red))
                    : Text(viewModel.languageModel.description),
              ),
            ],
          ),
        ));
  }

  Future navigateToUpdateForm(BuildContext context, dynamic viewModel) {
    return Navigator.push(
        context,
        PageTransition(
            type: PageTransitionType.rightToLeft,
            child: UpdateFormView(
              viewModel: viewModel,
              onUpdate: (dataModel) {
                onUpdate(dataModel);
              },
            )));
  }
}
