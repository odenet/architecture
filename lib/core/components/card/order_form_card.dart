import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/components/form/order_form.dart';
import 'package:fluttermvvmtemplate/core/init/theme/colors.dart';
import 'package:easy_localization/easy_localization.dart';

typedef SelectedValue = void Function(int value);
typedef SelectedSortState = void Function(SortState state);

enum SortState {
  ALPHABET_DESC,
  ALPHABET_ASC,
  RANK_ASC,
  RANK_DESC,
  DATE_DESC,
  DATE_ASC
}

class OrderFormCard extends StatefulWidget {
  final dynamic viewModel;
  final Function() onUpdate;

  OrderFormCard({Key key, this.viewModel, this.onUpdate}) : super(key: key);

  @override
  _OrderFormCardState createState() => _OrderFormCardState();
}

class _OrderFormCardState extends State<OrderFormCard> {
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(8),
      elevation: 7,
      child: Column(
        children: [
          Row(
            children: [
              SizedBox(
                width: 16,
              ),
              Text(
                'common.arrangement'.tr(),
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              ),
              Spacer(),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 0, 15, 0),
                child: IconButton(
                    icon: Icon(
                      Icons.edit,
                      color: AppColors.UPDATE,
                    ),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (_) => OrderForm(
                              viewModel: widget.viewModel,
                              onUpdate: widget.onUpdate)));
                    }),
              )
            ],
          ),
          ListTile(
              leading: Icon(Icons.sort),
              title: Row(
                children: [
                  Text('common.order'.tr() + '  ',
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  Text(widget.viewModel.model.order ?? ' ')
                ],
              )),
          ListTile(
              leading: Icon(Icons.text_rotation_angledown),
              title: Row(
                children: [
                  Text('common.rank'.tr() + '  ',
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  Text((widget.viewModel.model.rank != null)
                      ? widget.viewModel.model.rank.toString()
                      : ' ')
                ],
              )),
          ListTile(
            leading: Icon(Icons.adjust),
            title: Row(
              children: [
                Text('common.level'.tr() + '  ',
                    style: TextStyle(fontWeight: FontWeight.bold)),
                Text((widget.viewModel.model.level != null)
                    ? widget.viewModel.model.level.toString()
                    : ' ')
              ],
            ),
          ),
        ],
      ),
    );
  }
}

extension SortRawValue on SortState {
  String rawValue() {
    return toString().split('.').last;
  }
}
