import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/components/bottom_sheets/bottom_sheets.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:fluttermvvmtemplate/view/common/models/blamable_model.dart';

enum IDENTITY_ACTION { UPDATE, CREATE }

class IdentityCard extends StatelessWidget {
  final BlamableModel blamable;
  final String datetime;
  final actionType;

  IdentityCard({Key key, this.blamable, this.datetime, this.actionType})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (blamable != null) {
      return Container(
        child: Card(
          child: Column(
            children: [
              ListTile(
                leading: Icon(Icons.perm_identity),
                title: Text(IDENTITY_ACTION.UPDATE == actionType
                    ? '${'common.updated_by'.tr() + blamable.name}'
                    : '${'common.created_by_by'.tr() + blamable.name}'),
                subtitle: Text((blamable != null) ? datetime : ' '),
                onTap: () {},
                trailing: DetailBottomSheetButton(detailActions: [
                  ActionType.SHOW_PROFILE,
                  ActionType.SHOW_DETAIL
                ], onTap: (actionType) {}),
              ),
            ],
          ),
        ),
      );
    } else {
      return Text(' ');
    }
  }
}
