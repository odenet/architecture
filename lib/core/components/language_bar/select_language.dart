import 'package:fluttermvvmtemplate/core/init/lang/language_manager.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

class SelectLanguage extends StatelessWidget {
  final dynamic viewModel;

  SelectLanguage({Key key, this.viewModel, BuildContext context})
      : super(key: key);

  @override
  Widget build(context) {
    return IconButton(
        icon: Text(
          context.locale.countryCode.toString().toUpperCase(),
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        onPressed: () async {
          showDialog(
              context: context,
              builder: (_) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              });
          await viewModel.getAvailableLanguages();
          Navigator.pop(context);
          showModalBottomSheet(
              context: context,
              builder: (_) {
                return Container(
                  child: Column(mainAxisSize: MainAxisSize.min, children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Align(
                        alignment: Alignment.center,
                        child: Text('Select Your Interface Language',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 17)),
                      ),
                    ),
                    ...viewModel.availableLanguages
                        .map(
                          (e) => ListTile(
                            leading: Icon(
                              Icons.public,
                              color: Colors.blue,
                            ),
                            title: Text(
                              e.name,
                            ),
                            subtitle: Text(e.description),
                            onTap: () {
                              context.locale = LanguageManager
                                  .instance.localesMap[e.langCode];
                              Navigator.pop(context);
                              Scaffold.of(context).showSnackBar(
                                  SnackBar(content: Text("Language Changed")));
                            },
                          ),
                        )
                        .toList(),
                  ]),
                );
              });
        });
  }
}
