import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/model/available_languages_model.dart';

class AvailableLanguageBar extends StatelessWidget with PreferredSizeWidget {
  final Function(int) onTap;
  final List<AvailableLanguagesModel> items;

  AvailableLanguageBar({Key key, @required this.onTap, this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TabBar(
        onTap: onTap,
        labelStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
        labelColor: Colors.white,
        isScrollable: true,
        tabs: items
            .map((language) => Tab(text: language.localName))
            .toList());
  }

  @override
  Size get preferredSize => Size.fromHeight(55);
}
