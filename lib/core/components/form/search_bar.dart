import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:fluttermvvmtemplate/view/common/models/search_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/viewmodel/available_languages_view_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/types/model/type_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/model/available_languages_model.dart';
import 'package:fluttermvvmtemplate/core/init/lang/language_manager.dart';

class HomeSearchBar extends StatefulWidget {
  final Function(String) onSearch;
  final String hintText;
  final SearchModel searchModel;
  final VoidCallback onClose;
  final List<TypeModel> typeList;

  const HomeSearchBar(
      {Key key,
      @required this.searchModel,
      @required this.onSearch,
      @required this.onClose,
      this.typeList,
      this.hintText = 'Search'})
      : super(key: key);

  @override
  _HomeSearchBarState createState() => _HomeSearchBarState();
}

class _HomeSearchBarState extends State<HomeSearchBar> {
  var isVisibility = false;
  var isVisibilityAdvanced = false;

  AvailableLanguagesViewModel languageViewModel;

  FocusNode focusNode = FocusNode();
  TextEditingController controller;
  TypeModel typeValue;
  AvailableLanguagesModel languageValue;

  @override
  void initState() {
    controller = TextEditingController();
    languageViewModel = AvailableLanguagesViewModel();
    languageViewModel.fetchItems()
      ..then((value) {
        setState(() {
          languageValue = languageViewModel.items.firstWhere(
              (element) =>
                  element.langCode ==
                  LanguageManager.instance.localesMap['tr'].languageCode,
              orElse: () => null);
        });
      });
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //print(LanguageManager.instance.trLocale.languageCode);
    return Row(
      children: [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ExpansionTile(
              childrenPadding: EdgeInsets.all(8),
              backgroundColor: Color(0xFFf5f5f7),
              title: Row(
                children: [
                  Expanded(
                    child: Container(
                      height: 37,
                      child: TextField(
                        focusNode: focusNode,
                        controller: controller,
                        onChanged: (str) {
                          if (str.isNotEmpty) {
                            isVisibility = true;
                          } else {
                            widget.onClose();
                            isVisibility = false;
                          }
                          setState(() {});
                          widget.searchModel.name = str;
                        },
                        textAlignVertical: TextAlignVertical.center,
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.fromLTRB(16, 0.0, 0, 0),
                            hintText: 'common.search'.tr(),
                            suffixIcon: Visibility(
                              visible: focusNode.hasFocus &&
                                  controller.text.isNotEmpty,
                              child: GestureDetector(
                                  onTap: () {
                                    FocusScope.of(context)
                                        .requestFocus(FocusNode());
                                    isVisibility = false;
                                    controller.clear();
                                    widget.onClose();
                                  },
                                  child: Icon(Icons.close)),
                            ),
                            filled: true,
                            fillColor: Color(0xFFe8e3e3),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8.0),
                                borderSide: BorderSide.none)),
                      ),
                    ),
                  ),
                  IconButton(
                      icon: Icon(
                        Icons.search,
                        color: Colors.blue,
                      ),
                      onPressed: () {
                        widget.onSearch(controller.text);
                        //pushAndRemoved(page: ObjectListView());
                      }),
                ],
              ),
              children: [
                Column(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.9,
                      decoration: BoxDecoration(
                          color: Color(0xFFe8e3e3),
                          borderRadius: BorderRadius.circular(36)),
                      child: DropdownButtonHideUnderline(
                        child: ButtonTheme(
                          alignedDropdown: true,
                          child: DropdownButton(
                            isExpanded: true,
                            key: widget.key,
                            items: [
                              DropdownMenuItem(
                                child: Text('common.all'.tr()),
                                value: SearchModel.ALL,
                              ),
                              DropdownMenuItem(
                                child: Text('common.public'.tr()),
                                value: '1',
                              ),
                              DropdownMenuItem(
                                child: Text('common.private'.tr()),
                                value: '0',
                              )
                            ],
                            onChanged: (selectedValue) {
                              setState(() {
                                widget.searchModel.isPublic = selectedValue;
                              });
                            },
                            value: widget.searchModel.isPublic,
                            hint: Text(widget.hintText),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.9,
                      decoration: BoxDecoration(
                          color: Color(0xFFe8e3e3),
                          borderRadius: BorderRadius.circular(36)),
                      child: DropdownButtonHideUnderline(
                        child: ButtonTheme(
                          alignedDropdown: true,
                          child: DropdownButton(
                            isExpanded: true,
                            key: widget.key,
                            items: [
                              DropdownMenuItem(
                                child: Text('common.all'.tr()),
                                value: SearchModel.ALL,
                              ),
                              DropdownMenuItem(
                                child: Text('common.active'.tr()),
                                value: '1',
                              ),
                              DropdownMenuItem(
                                child: Text('common.passive'.tr()),
                                value: '0',
                              )
                            ],
                            onChanged: (selectedValue) {
                              setState(() {
                                widget.searchModel.isActive = selectedValue;
                                print(selectedValue);
                              });
                            },
                            value: widget.searchModel.isActive,
                            hint: Text(widget.hintText),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    widget.typeList == null
                        ? Container()
                        : Container(
                            width: MediaQuery.of(context).size.width * 0.9,
                            decoration: BoxDecoration(
                                color: Color(0xFFe8e3e3),
                                borderRadius: BorderRadius.circular(36)),
                            child: DropdownButtonHideUnderline(
                              child: ButtonTheme(
                                alignedDropdown: true,
                                child: DropdownButton(
                                  isExpanded: true,
                                  key: widget.key,
                                  items: widget.typeList
                                      .map((element) => DropdownMenuItem(
                                            child: Text(element.name),
                                            value: element,
                                          ))
                                      .toList(),
                                  onChanged: (selectedValue) {
                                    setState(() {
                                      typeValue = selectedValue;
                                      widget.searchModel.typeSlug =
                                          typeValue.slug;
                                    });
                                  },
                                  value: typeValue,
                                  hint: Text('common.types'.tr()),
                                ),
                              ),
                            ),
                          ),
                    SizedBox(
                      height: 8,
                    ),
                    Observer(
                      builder: (_) => Container(
                        width: MediaQuery.of(context).size.width * 0.9,
                        decoration: BoxDecoration(
                            color: Color(0xFFe8e3e3),
                            borderRadius: BorderRadius.circular(36)),
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton<AvailableLanguagesModel>(
                              isExpanded: true,
                              key: widget.key,
                              items: languageViewModel.items
                                  .map((element) => DropdownMenuItem(
                                        child: Text(element.name),
                                        value: element,
                                      ))
                                  .toList(),
                              onChanged: (selectedValue) {
                                setState(() {
                                  languageValue = selectedValue;
                                  widget.searchModel.language =
                                      selectedValue.langCode;
                                });
                              },
                              value: languageValue,
                              hint: Text('Diller'),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
