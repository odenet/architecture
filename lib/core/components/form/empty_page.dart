import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

class EmptyView extends StatelessWidget {
  final VoidCallback onPressed;
  final String buttonText;

  // if search active then show result not found widget rather than
  // do not show empty widget with create button
  final bool isSearchActive;

  const EmptyView(
      {Key key,
      @required this.onPressed,
      @required this.buttonText,
      this.isSearchActive = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(child: isSearchActive ? noData() : resultNotFound());
  }

  Widget resultNotFound() {
    return Column(
      //crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(child: Image.asset('asset/image/feature/add.png')),
        ElevatedButton.icon(
          onPressed: onPressed,
          icon: Icon(Icons.add),
          label: Text(buttonText),
          style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(Colors.indigoAccent)),
        ),
        SizedBox(
          height: 60,
        )
      ],
    );
  }

  Widget noData() {
    return Column(
      //crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset('asset/image/no_data.png', height: 200, width: 200),
        Text('common.result_not_found'.tr()),
        SizedBox(
          height: 60,
        )
      ],
    );
  }
}
