import 'package:flutter/material.dart';

class SlidableModel{
  String name;
  Widget icon;
}

class SlidableView extends StatelessWidget {

  final List<SlidableModel> list;
  final VoidCallback onPressed;

  const SlidableView({Key key,@required this.list,@required this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: list.length,
      itemBuilder: (context,index){
        return GestureDetector(
          onTap: onPressed,
                  child: Container(
            padding: EdgeInsets.all(12),
            child: Center(child: Row(children: [
              list[index].icon,
              SizedBox(width: 8,),
              Text(list[index].name)
            ],),),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              boxShadow: [BoxShadow(offset: Offset(3,3),color: Colors.grey)]
            ),
          ),
        );
      }
      );
  }
}