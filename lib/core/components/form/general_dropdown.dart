import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';


class GeneralDropDown extends StatefulWidget {
  List<dynamic> items;
  bool autoFocus;
  String hintText;
  Function(dynamic) onChanged;
  int elevation;
  dynamic initValue;
  String errorText;

  GeneralDropDown(
      {@required this.items,
      @required this.onChanged,
      this.autoFocus = false,
      this.hintText = 'default',
      this.elevation = 8,
      this.initValue,
      this.errorText});

  @override
  _GeneralDropDownState createState() => _GeneralDropDownState();
}

class _GeneralDropDownState extends State<GeneralDropDown> {
  dynamic value;

  @override
  void initState() {
    super.initState();
    //if(widget.initValue != null) value = widget.initValue;
  }

  @override
  Widget build(BuildContext context) {
    return FormField<String>(
      builder: (FormFieldState<String> state) {
        return InputDecorator(
          decoration: InputDecoration(
              labelText: 'common.type'.tr(),
              errorText: widget.errorText,
              errorStyle: TextStyle(color: Colors.redAccent, fontSize: 16.0),
              hintText: 'common.type'.tr(),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(5.0))),
          isEmpty: widget.initValue == '',
          child: DropdownButtonHideUnderline(
            child: DropdownButton<String>(
              value: (widget.initValue == null)
                  ? widget.items.isNotEmpty
                      ? widget.items[0]?.slug
                      : null
                  : widget.initValue.toString(),
              isDense: true,
              onChanged: (String newValue) {
                setState(() {
                  widget.initValue = newValue;
                });

                if (widget.onChanged != null) {
                  widget.onChanged(newValue);
                }
              },
              items: widget.items.map((model) {
                return DropdownMenuItem<String>(
                  value: model.slug,
                  child: Text(model.name),
                );
              }).toList(),
            ),
          ),
        );
      },
    );
  }
}
