import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:fluttermvvmtemplate/core/components/alerts/updated_detector.dart';
import 'package:fluttermvvmtemplate/core/components/form/base_form.dart';
import 'package:fluttermvvmtemplate/core/helpers/alert_helper.dart';
import 'package:easy_localization/easy_localization.dart';

class UpdateFormView extends StatefulWidget {
  final dynamic viewModel;
  final Function(dynamic dataModel) onUpdate;

  UpdateFormView({Key key, this.viewModel, this.onUpdate}) : super(key: key);

  @override
  _UpdateFormViewState createState() => _UpdateFormViewState();
}

class _UpdateFormViewState extends State<UpdateFormView> {
  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();

    return SafeArea(
      child: WillPopScope(
        onWillPop: () async {
          await updatedDetector(context, widget.viewModel);
          return true;
        },
        child: Scaffold(
          appBar: AppBar(title: Text('common.language_update'.tr())),
          body: SingleChildScrollView(
            child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        initialValue: widget.viewModel.dirtyLanguageModel.name,
                        onChanged: (str) {
                          widget.viewModel.dirtyLanguageModel.name = str;
                          widget.viewModel.isEdit = true;
                        },
                        decoration: InputDecoration(
                            hintText: 'common.name'.tr(), border: OutlineInputBorder()),
                        validator: (str) {
                          /* return CustomValidator.selectAndRunFunction(
                              GeneralCustomValidations()
                                  .getValidationData('name'),
                              str); */
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        initialValue:
                            widget.viewModel.dirtyLanguageModel.description,
                        onChanged: (str) {
                          widget.viewModel.dirtyLanguageModel.description = str;
                          widget.viewModel.isEdit = true;
                        },
                        decoration: InputDecoration(
                            hintText: 'common.description'.tr(),
                            border: OutlineInputBorder()),
                        validator: (str) {
                          /* return CustomValidator.selectAndRunFunction(
                              GeneralCustomValidations()
                                  .getValidationData('name'),
                              str); */
                        },
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Observer(
                          builder: (_) => CustomNormalButton(
                            onPressed: widget.viewModel.isEdit
                                ? () async {
                                    if (_formKey.currentState.validate()) {
                                      AlertHelper.shared
                                          .showProgressDialog(context);
                                      var response;
                                      if (widget.viewModel.languageModel.slug
                                          .isEmpty) {
                                        response = await widget.viewModel
                                            .createAndLoadLanguageInfo();
                                      } else {
                                        response = await widget.viewModel
                                            .updateAndLoadLanguageInfo();
                                      }

                                      await AlertHelper.shared
                                          .startProgress(context, response);

                                      Navigator.pop(context);
                                      if (response.success) {
                                        Navigator.pop(
                                            context, true); // remove loading
                                        Navigator.pop(context,
                                            true); // remove create form
                                        widget.onUpdate(response.data);
                                      } else {}
                                    }
                                  }
                                : null,
                            text: widget.viewModel.languageModel.slug.isEmpty
                                ? 'common.create'.tr()
                                : 'common.update'.tr(),
                          ),
                        ),
                        SizedBox(
                          width: 16,
                        ),
                        CustomNormalButton(
                          onPressed: () async {
                            if (!widget.viewModel.isEdit) {
                              Navigator.pop(context);
                            } else {
                              await updatedDetector(context, widget.viewModel);
                            }
                          },
                          text: 'common.cancel'.tr(),
                          buttonColor: Colors.red,
                          textColor: Colors.white,
                        ),
                        SizedBox(
                          width: 16,
                        ),
                      ],
                    ),
                  ],
                )),
          ),
        ),
      ),
    );
  }
}
