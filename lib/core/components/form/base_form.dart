import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_material_color_picker/flutter_material_color_picker.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx/mobx.dart';

const INPUT = 'input';
const DATE = 'date';
const TIME = 'time';
const DATETIME = 'datetime';
const DATERANGE = 'daterange';
const DROPDOWN = 'dropdown';
const CHECKBOXLIST = 'checkboxlist';
const RADIOBUTTONLIST = 'radiobuttonlist';
const BUTTON = 'button';
const COLORPICKER = 'colorpicker';

class FormBuilder<T extends Store> extends StatefulWidget {
  final dynamic viewModel;
  final VoidCallback onDispose;
  final Key formKey;
  final List<Widget> formElements;

  const FormBuilder(
      {Key key,
      @required this.viewModel,
      @required this.formKey,
      @required this.formElements,
      this.onDispose})
      : super(key: key);

  // return form element based on type
  // type will be retrieved from API and form gonna be build dynamically
  Widget getFormElement(Map<String, dynamic> element, dynamic viewModel) {
    switch (element['type']) {
      case INPUT:
        return Container();
      case DATE:
        return CustomDatePicker(
            text: element['text'],
            startYear: element['startYear'],
            endYear: element['endYear']);
      case TIME:
        return CustomDatePicker.timePicker(text: element['text']);
      case DATETIME:
      // return datetime;
      case DATERANGE:
        return CustomDatePicker.range(
            text: element['text'], yearInterval: element['interval']);
      case DROPDOWN:
        return Container();
      case CHECKBOXLIST:
      // return checkboxlist
      case RADIOBUTTONLIST:
      // return radiobuttonlist
      case BUTTON:
        return CustomNormalButton(onPressed: element['onPressed']);
      case COLORPICKER:
        return CustomColorPicker(onChangeColor: element['onChanged']);
      default:
        return CustomTextField();
    }
  }

  @override
  _FormBuilderState<T> createState() => _FormBuilderState<T>();
}

class _FormBuilderState<T extends Store> extends State<FormBuilder<T>> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    widget.viewModel.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: widget.formKey,
        autovalidateMode: AutovalidateMode.always,
        child: Container(
            padding: EdgeInsets.all(10),
            child: Column(children: widget.formElements)));
  }
}


// CUSTOM DROPDOWN BUTTON



class CustomDropDownButton<T> extends StatefulWidget {
  List<T> items;
  bool autoFocus;
  String hintText;
  Function(T) onChanged;
  int elevation;
  T initValue;
  double width;

  CustomDropDownButton(
      {@required this.items,
      @required this.onChanged,
      this.autoFocus = false,
      this.hintText = 'default',
      this.elevation = 8,
      this.width = 0.1,
      this.initValue});

  @override
  _CustomDropDownButtonState<T> createState() =>
      _CustomDropDownButtonState<T>();
}

class _CustomDropDownButtonState<T> extends State<CustomDropDownButton<T>> {
  T value;

  @override
  void initState() {
    super.initState();
    if (widget.initValue != null) {
      
      value = widget.initValue;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: MediaQuery.of(context).size.width * widget.width,
          child: DropdownButtonHideUnderline(
            child: ButtonTheme(
              alignedDropdown: true,
              child: DropdownButton(
                isExpanded: true,
                autofocus: widget.autoFocus,
                key: widget.key,
                items: widget.items
                    .map((element) => DropdownMenuItem(
                          child: Text(element.toString()),
                          value: element,
                        ))
                    .toList(),
                onChanged: (selectedValue) {
                  if (widget.onChanged != null) {
                    widget.onChanged(selectedValue);
                  }
                  setState(() {
                    value = selectedValue;
                  });
                },
                elevation: widget.elevation,
                value: value,
                hint: Text(widget.hintText),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class CustomTextField extends StatelessWidget {
  final InputDecoration decoration;
  final dynamic controller;
  final bool autofocus;
  final Function(String) onChanged;
  final TextInputType type;
  final dynamic viewModel;

  void func(String value) => viewModel.name = value;

  const CustomTextField(
      {key,
      @required this.viewModel,
      this.onChanged,
      this.decoration,
      this.controller,
      this.autofocus = false,
      this.type = TextInputType.name})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Observer(
        builder: (_) => TextField(
              keyboardType: TextInputType.name,
              controller: controller ?? TextEditingController(),
              onChanged: onChanged ?? (value) => viewModel.name = value,
              decoration: decoration ??
                  InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.blue),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.red),
                      ),
                      labelText: 'Name',
                      hintText: 'Enter a name',
                      errorText: viewModel.error.name),
              autofocus: true,
            ));
  }
}

enum PickerType { date, time, range }

enum CalendarType { cupertino, android }

class CustomDatePicker extends StatefulWidget {
  final PickerType type;
  final CalendarType calendarType;
  final Text text;
  final int startYear;
  final int endYear;
  final int yearInterval;

  const CustomDatePicker(
      {Key key,
      this.type = PickerType.date,
      @required this.text,
      @required this.startYear,
      @required this.endYear,
      this.yearInterval,
      this.calendarType = CalendarType.cupertino})
      : super(key: key);

  const CustomDatePicker.timePicker(
      {Key key,
      this.type = PickerType.time,
      @required this.text,
      this.startYear,
      this.endYear,
      this.yearInterval,
      this.calendarType = CalendarType.cupertino})
      : super(key: key);

  const CustomDatePicker.range(
      {Key key,
      this.type = PickerType.range,
      @required this.text,
      this.startYear,
      this.endYear,
      @required this.yearInterval,
      this.calendarType = CalendarType.cupertino})
      : super(key: key);

  @override
  _CustomDatePickerState createState() => _CustomDatePickerState();
}

class _CustomDatePickerState extends State<CustomDatePicker> {
  DateTime _fromDate = DateTime.now();
  TimeOfDay _fromTime = TimeOfDay.fromDateTime(DateTime.now());
  DateTimeRange _fromRange =
      DateTimeRange(start: DateTime.now(), end: DateTime.now());

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      ElevatedButton(
        child: widget.text,
        onPressed: () {
          switch (widget.type) {
            case PickerType.date:
              _showDatePicker(context);
              break;
            case PickerType.time:
              _showTimePicker(context);
              break;
            case PickerType.range:
              _showDateRangePicker(context);
              break;
          }
        },
      )
    ]);
  }

  Future<void> _showDatePicker(BuildContext context) async {
    if (widget.calendarType == CalendarType.cupertino) {
      await DatePicker.showDatePicker(context,
          showTitleActions: true,
          minTime: DateTime(widget.startYear, 1),
          maxTime: DateTime(widget.endYear),
          theme: DatePickerTheme(
              headerColor: Colors.lightBlue,
              backgroundColor: Colors.blue,
              itemStyle: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 18),
              doneStyle: TextStyle(color: Colors.white, fontSize: 16)),
          onChanged: (date) {
        setState(() {
          _fromDate = date;
        });
      }, currentTime: DateTime.now(), locale: LocaleType.tr);
    } else {
      final picked = await showDatePicker(
        context: context,
        initialDate: _fromDate,
        firstDate: DateTime(widget.startYear, 1),
        lastDate: DateTime(widget.endYear),
      );
      if (picked != null && picked != _fromDate) {
        setState(() {
          _fromDate = picked;
        });
      }
    }
  }

  Future<void> _showTimePicker(BuildContext context) async {
    if (widget.calendarType == CalendarType.cupertino) {
      await DatePicker.showTimePicker(context, showTitleActions: true,
          onChanged: (date) {
        setState(() {
          _fromTime = TimeOfDay.fromDateTime(date);
        });
      }, onConfirm: (date) {
        print('confirm $date');
      }, currentTime: DateTime.now());
    } else {
      final picked = await showTimePicker(
        context: context,
        initialTime: TimeOfDay.now(),
      );
      if (picked != null && picked != _fromTime) {
        setState(() {
          _fromTime = picked;
        });
      }
    }
  }

  Future<void> _showDateRangePicker(BuildContext context) async {
    final picked = await showDateRangePicker(
        useRootNavigator: false,
        context: context,
        firstDate: DateTime(DateTime.now().year - widget.yearInterval),
        lastDate: DateTime(DateTime.now().year + widget.yearInterval),
        builder: (BuildContext context, Widget child) {
          return FittedBox(
            child: Theme(
              child: child,
              data: ThemeData(
                primaryColor: Colors.purple[300],
              ),
            ),
          );
        });
    if (picked != null) {
      setState(() {
        _fromRange = picked;
      });
    }
  }
}

enum SelectableType { SELECT, SELECTAG }

class CustomSelectableList<T> extends StatelessWidget {
  final List<T> items;
  final Function(T) selected;

  CustomSelectableList({@required this.items, this.selected});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        showDialog(context: context, builder: (_) => dialog(context));
      },
      child: Container(
        padding: EdgeInsets.all(8),
        child: Text('Selectable List',
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16), color: Color(0xFF304352)),
      ),
    );
  }

  Widget dialog(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.grey,
      child: Container(
        height: MediaQuery.of(context).size.height * 0.5,
        width: MediaQuery.of(context).size.width * 0.8,
        child: ListView.builder(
            itemCount: items.length,
            itemBuilder: (context, index) {
              return Card(
                child: ListTile(
                  onTap: () {
                    selected(items[index]);
                    Navigator.pop(context);
                  },
                  tileColor: Color(0xFF304352),
                  title: Text('${index + 1} - ' + items[index].toString(),
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold)),
                  trailing: Icon(
                    Icons.arrow_right,
                    color: Colors.white,
                    size: 26,
                  ),
                ),
              );
            }),
      ),
    );
  }
}

class CustomNormalButton extends StatelessWidget {
  final String text;
  final VoidCallback onPressed;
  final Color textColor;
  final Color buttonColor;

  const CustomNormalButton(
      {Key key,
      @required this.onPressed,
      this.text,
      this.textColor,
      this.buttonColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(buttonColor),
          padding: MaterialStateProperty.all(EdgeInsets.all(8)),
          elevation: MaterialStateProperty.all(8)),
      onPressed: onPressed,
      child: Text(
        text,
        style: TextStyle(color: textColor),
      ),
    );
  }
}

class CustomColorPicker extends StatefulWidget {
  final Function(Color) onChangeColor;
  final String title;
  final String subTitle;

  CustomColorPicker(
      {@required this.onChangeColor,
      this.title = 'Select Color',
      this.subTitle =
          'It is will change circle color when you tap circle and select'});

  @override
  _CustomColorPickerState createState() => _CustomColorPickerState();
}

class _CustomColorPickerState extends State<CustomColorPicker> {
  Color color = Colors.blue;
  Color selectedColor = Colors.blue;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(widget.title),
      subtitle: Text(widget.subTitle),
      trailing: GestureDetector(
          onTap: () {
            _openFullMaterialColorPicker(context);
          },
          child: CircleAvatar(
            backgroundColor: selectedColor,
          )),
    );
  }

  void _openFullMaterialColorPicker(BuildContext context) async {
    _openDialog(
        'Full Material Color picker',
        MaterialColorPicker(
          colors: fullMaterialColors,
          onColorChange: (color) {
            this.color = color;
          },
        ),
        context);
  }

  void _openDialog(String title, Widget content, BuildContext context) {
    showDialog(
      context: context,
      builder: (_) {
        return AlertDialog(
          contentPadding: const EdgeInsets.all(6.0),
          title: Text(title),
          content: content,
          actions: [
            FlatButton(
              child: Text('CANCEL'),
              onPressed: Navigator.of(context).pop,
            ),
            FlatButton(
              child: Text('SUBMIT'),
              onPressed: () {
                setState(() {
                  selectedColor = color;
                  widget.onChangeColor(selectedColor);
                  Navigator.pop(context);
                });
              },
            ),
          ],
        );
      },
    );
  }
}

class CustomRadioButtonList<T> extends StatefulWidget {
  final List<T> titleList;
  final Function(T) completion;

  CustomRadioButtonList({@required this.titleList, @required this.completion});

  @override
  _CustomRadioButtonListState<T> createState() =>
      _CustomRadioButtonListState<T>();
}

class _CustomRadioButtonListState<T> extends State<CustomRadioButtonList<T>> {
  T groupValue;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: ListView.builder(
              itemCount: widget.titleList.length,
              itemBuilder: (context, index) {
                return RadioListTile(
                    title: Text(widget.titleList[index].toString()),
                    value: widget.titleList[index],
                    groupValue: groupValue,
                    onChanged: (value) {
                      setState(() {
                        groupValue = value;
                      });
                    });
              }),
          floatingActionButton: AnimatedOpacity(
            opacity: groupValue == null ? 0.0 : 1.0,
            duration: Duration(milliseconds: 750),
            child: FloatingActionButton(
              onPressed: () {
                widget.completion(groupValue);
              },
              child: Icon(
                Icons.verified,
                color: Colors.white,
              ),
            ),
          )),
    );
  }
}

class CustomCheckBoxButtonList<T> extends StatefulWidget {
  final List<T> titleList;
  final Function(List<T>) completion;

  CustomCheckBoxButtonList(
      {@required this.titleList, @required this.completion});

  @override
  _CustomCheckBoxButtonListState<T> createState() =>
      _CustomCheckBoxButtonListState<T>();
}

class _CustomCheckBoxButtonListState<T>
    extends State<CustomCheckBoxButtonList<T>> {
  T groupValue;
  List<bool> statusList = [];
  List<T> selectedList = [];

  @override
  void initState() {
    statusList = List.generate(widget.titleList.length, (index) => false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: ListView.builder(
              itemCount: widget.titleList.length,
              itemBuilder: (context, index) {
                return CheckboxListTile(
                    value: statusList[index],
                    title: Text(widget.titleList[index].toString()),
                    onChanged: (value) {
                      setState(() {
                        statusList[index] = value;
                        if (value) {
                          selectedList.add(widget.titleList[index]);
                        } else {
                          selectedList.remove(widget.titleList[index]);
                        }
                      });
                    });
              }),
          floatingActionButton: AnimatedOpacity(
            opacity: selectedList.isNotEmpty ? 1.0 : 0.0,
            duration: Duration(milliseconds: 750),
            child: FloatingActionButton(
              onPressed: () {
                widget.completion(selectedList);
              },
              child: Icon(
                Icons.verified,
                color: Colors.white,
              ),
            ),
          )),
    );
  }
}

class CustomMultiSelectableList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        //Navigator.push(context, MaterialPageRoute(builder: (_) => TaggingList()));
      },
      child: Container(
        padding: EdgeInsets.all(8),
        child: Text('Multi Selectable List',
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16), color: Color(0xFF304352)),
      ),
    );
  }
}
