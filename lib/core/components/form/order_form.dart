import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/components/alerts/custom_circle_alert.dart';
import 'package:fluttermvvmtemplate/core/components/form/base_form.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:fluttermvvmtemplate/core/helpers/alert_helper.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/response_model.dart';

enum SortState { ALPHABET, DATE, ASC, DESC, RANK }

class OrderForm extends StatefulWidget {
  final dynamic viewModel;
  final Function() onUpdate;

  const OrderForm({Key key, this.viewModel, this.onUpdate}) : super(key: key);

  @override
  _OrderFormState createState() => _OrderFormState();
}

class _OrderFormState extends State<OrderForm> {
  final _formKey = GlobalKey<FormState>();
  
  List<String> items;

  @override
  void initState() {
    items = ['common.alphabet_desc'.tr(),'common.alphabet_asc'.tr(),'common.date_desc'.tr(),'common.date_asc'.tr(),'common.rank_desc'.tr(),'common.rank_asc'.tr()];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //items = ['common.alphabet_desc'.tr(),'common.alphabet_asc'.tr(),'common.date_desc'.tr(),'common.date_asc'.tr(),'common.rank_desc'.tr(),'common.rank_asc'.tr()];
    return Scaffold(
      appBar: AppBar(
        title: Text('common.order_form_title'.tr()),
      ),
      body: Form(
        key: _formKey,
        child: Card(
          margin: EdgeInsets.all(8),
          elevation: 8,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              CustomDropDownButton(
                width: 0.8,
                items: items,
                onChanged: (value) {
                  widget.viewModel.dirtyModel.order = value;
                },
                hintText: 'common.order'.tr(),
                initValue: items[0],
              ),
              CustomDropDownButton(
                width: 0.8,
                items: List.generate(5, (index) => index + 1),
                onChanged: (value) {
                  widget.viewModel.dirtyModel.rank = value;
                },
                hintText: 'common.rank'.tr(),
                initValue: widget.viewModel.dirtyModel.rank,
              ),
              CustomDropDownButton(
                width: 0.8,
                items: List.generate(20, (index) => index + 1),
                onChanged: (value) {
                  widget.viewModel.dirtyModel.level = value;
                },
                hintText: 'common.level'.tr(),
                initValue: widget.viewModel.dirtyModel.level,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CustomNormalButton(
                    onPressed: () async {
                      if (_formKey.currentState.validate()) {
                        CustomCircleProgress.showIndicator(context);
                        var response = await widget.viewModel.update() as ResponseModel;

                        Navigator.pop(context);

                        await AlertHelper.shared
                            .startProgress(context, response);

                        Navigator.pop(context);
                        if (response.success) {
                          Navigator.pop(context); // remove loading
                          widget.viewModel.model.rank = widget.viewModel.dirtyModel.rank;
                          widget.viewModel.model.level = widget.viewModel.dirtyModel.level;
                          widget.viewModel.model.order = widget.viewModel.dirtyModel.order;
                          widget.onUpdate();
                        }
                      }
                    },
                    text: 'common.select'.tr(),
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  CustomNormalButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    text: 'common.cancel'.tr(),
                    buttonColor: Colors.red,
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

SortState getSortState(String rawValue) {
  switch (rawValue) {
    case 'ALPHABET':
      return SortState.ALPHABET;
    case 'DATE':
      return SortState.DATE;
    case 'ASC':
      return SortState.ASC;
    case 'DESC':
      return SortState.DESC;
    case 'RANK':
      return SortState.RANK;
    default:
      return SortState.ALPHABET;
  }
}

extension SortRawValue on SortState {
  String rawValue() {
    return toString().split('.').last;
  }
}
