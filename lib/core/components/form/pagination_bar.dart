import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/view/common/models/pagination_model.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class PaginationBar extends StatefulWidget {
  final PaginationModel paginationModel;

  final Future Function({int page}) fetchFunction;

  const PaginationBar(
      {Key key, @required this.paginationModel, this.fetchFunction})
      : super(key: key);

  @override
  _PaginationBarState createState() => _PaginationBarState();
}

class _PaginationBarState extends State<PaginationBar> {
  @override
  Widget build(BuildContext context) {
    return widget.paginationModel.lastPage == 1
        ? Container()
        : Container(
            height: 40,
            width: double.maxFinite,
            color: Color(0xFFEBF3FF),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // bulunduğu sayfa * gelen sayfa
                IconButton(
                    icon: Icon(FontAwesomeIcons.angleDoubleLeft),
                    onPressed: widget.paginationModel.currentPage == 1
                        ? null
                        : () {
                            widget.fetchFunction(page: 1);
                          }),
                IconButton(
                    icon: Icon(Icons.arrow_back_ios),
                    onPressed: widget.paginationModel.currentPage == 1
                        ? null
                        : () {
                            widget.fetchFunction(
                                page: widget.paginationModel.currentPage - 1);
                          }),
                Spacer(),
                ElevatedButton(
                  style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all(Color(0xFFCAE5FF)),
                      elevation: MaterialStateProperty.all(0)),
                  onPressed: () {},
                  child: Text(
                    '${widget.paginationModel.currentPage} / ${widget.paginationModel.lastPage}',
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                ),
                Spacer(),
                IconButton(
                    icon: Icon(Icons.arrow_forward_ios),
                    onPressed: widget.paginationModel.currentPage ==
                            widget.paginationModel.lastPage
                        ? null
                        : () {
                            widget.fetchFunction(
                                page: widget.paginationModel.currentPage + 1);
                          }),
                IconButton(
                    icon: Icon(FontAwesomeIcons.angleDoubleRight),
                    onPressed: widget.paginationModel.currentPage ==
                            widget.paginationModel.lastPage
                        ? null
                        : () {
                            widget.fetchFunction(
                                page: widget.paginationModel.lastPage);
                          }),
              ],
            ),
          );
  }
}
