import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:fluttermvvmtemplate/core/components/alerts/custom_circle_alert.dart';
import 'package:fluttermvvmtemplate/core/components/alerts/updated_detector.dart';
import 'package:fluttermvvmtemplate/core/components/form/base_form.dart';
import 'package:fluttermvvmtemplate/core/helpers/alert_helper.dart';
import 'package:easy_localization/easy_localization.dart';

class VisibilityForm extends StatefulWidget {
  final dynamic viewModel;
  final VoidCallback onUpdate;

  const VisibilityForm({Key key, this.viewModel, this.onUpdate})
      : super(key: key);

  @override
  _VisibilityFormState createState() => _VisibilityFormState();
}

class _VisibilityFormState extends State<VisibilityForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: WillPopScope(
            onWillPop: () async {
              await updatedDetector(context, widget.viewModel);
              return true;
            },
            child: Scaffold(
                appBar: AppBar(title: Text('common.visibility'.tr())),
                body: SingleChildScrollView(
                  child: Form(
                      key: _formKey,
                      child: Column(children: <Widget>[
                        CheckboxListTile(
                            value: widget.viewModel.dirtyModel.isActive,
                            onChanged: (value) {
                              setState(() {
                                widget.viewModel.dirtyModel.isActive = value;
                                widget.viewModel.isEdit = true;
                              });
                            },
                            title: Text('common.active'.tr(),)),
                        CheckboxListTile(
                          value: widget.viewModel.dirtyModel.isPublic,
                          title: Text('common.public'.tr(),),
                          onChanged: (value) {
                            setState(() {
                              widget.viewModel.dirtyModel.isPublic = value;
                              widget.viewModel.isEdit = true;
                            });
                          },
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Observer(
                              builder: (_) => CustomNormalButton(
                                onPressed: widget.viewModel.isEdit
                                    ? () async {
                                        if (_formKey.currentState.validate()) {
                                          CustomCircleProgress.showIndicator(
                                              context);
                                          var response =
                                              await widget.viewModel.update();
                                          Navigator.pop(context);

                                          await AlertHelper.shared
                                              .startProgress(context, response);

                                          Navigator.pop(context);
                                          if (response.success) {
                                            widget.viewModel.model.isPublic =
                                                widget.viewModel.dirtyModel
                                                    .isPublic;
                                            widget.viewModel.model.isActive =
                                                widget.viewModel.dirtyModel
                                                    .isActive;
                                            Navigator.pop(
                                                context); // remove loading
                                            widget.onUpdate();
                                          } else {}
                                        }
                                      }
                                    : null,
                                text:
                                    widget.viewModel.languageModel.slug.isEmpty
                                        ? 'common.create'.tr()
                                        : 'common.update'.tr(),
                              ),
                            ),
                            SizedBox(
                              width: 16,
                            ),
                            CustomNormalButton(
                              onPressed: () async {
                                if (!widget.viewModel.isEdit) {
                                  Navigator.pop(context);
                                } else {
                                  await updatedDetector(
                                      context, widget.viewModel);
                                }
                              },
                              text: 'common.cancel'.tr(),
                              buttonColor: Colors.red,
                              textColor: Colors.white,
                            ),
                            SizedBox(
                              width: 16,
                            ),
                          ],
                        ),
                      ])),
                ))));
  }
}
