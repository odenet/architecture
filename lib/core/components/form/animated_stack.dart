import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/components/form/animated_stack_base.dart';

class AnimatedStackView extends StatelessWidget {
  final Widget child;
  final List<IconTile> iconTileList;
  final bool Function() animatedButton;

  const AnimatedStackView(
      {Key key, @required this.iconTileList, this.child, this.animatedButton})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedStack(
      backgroundColor: Color(0xFFdbd9d5),
      onPressed: animatedButton,
        columnWidget: Column(
          children: iconTileList
              .map((e) => Padding(
                    padding: EdgeInsets.only(bottom: 20),
                    child: e,
                  ))
              .toList(),
        ),
        bottomWidget: Container(
          decoration: BoxDecoration(
            //color: Color(0xff645478),
            borderRadius: BorderRadius.all(
              Radius.circular(50),
            ),
          ),
          //width: 260,
          //height: 50,
        ),
        buttonIcon: Icons.menu,
        foregroundWidget: child);
  }
}

class IconTile extends StatelessWidget {
  final IconData iconData;
  final VoidCallback onPressed;

  const IconTile({Key key, @required this.iconData, @required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
          child: Container(
        width: 50,
        height: 50,
        decoration:
            BoxDecoration(color: Color(0xff645478), shape: BoxShape.circle),
        child: Icon(
          iconData,
          color: Colors.white,
        ),
      ),
    );
  }
}
