import 'package:flutter/material.dart';


class IconNormalButton extends StatelessWidget {
  final VoidCallback onPressed;
  final IconData icon;

  const IconNormalButton({Key key, this.onPressed, @required this.icon}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      key: ValueKey(1),
      onPressed: onPressed,
    );
  }
}
