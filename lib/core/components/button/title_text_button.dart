import 'package:flutter/material.dart';


class TitleTextButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String text;

  const TitleTextButton({Key key, this.onPressed, @required this.text}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: onPressed,
    );
  }
}
