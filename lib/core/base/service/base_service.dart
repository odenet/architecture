import 'package:dio/dio.dart';
import 'package:fluttermvvmtemplate/core/base/model/error_message.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/http_client.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/request_model.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/response_model.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/network_exceptions.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:fluttermvvmtemplate/view/common/models/pagination_model.dart';

class BaseService {
  HttpClient _httpClient = HttpClient();
  final dynamic model;

  BaseService(this.model);

  Future<ResponseModel> fetchList(RequestModel requestModel) async {
    List<dynamic> myList = [];
    var response = Response();
    var pagination = PaginationModel();
    bool success = false;

    try {
      response = await _httpClient.get(requestModel.endpoint,
          params: requestModel.params);
      if (response.data['success']) {
        success = true;
        //success message
        response.statusMessage = 'common.success'.tr();

        // load items into a list
        myList = loadToList(response.data);

        //set pagination
        if (response.data['paginate'] != null) {
          pagination = PaginationModel.fromJson(response.data['paginate']);
        }
      } else {
        response.statusMessage = 'common.error'.tr();
      }
    } catch (e) {
      response.statusMessage = NetworkExceptions.getErrorMessage(
          NetworkExceptions.getDioException(e));
    }

    return ResponseModel(
        data: myList,
        message: response.statusMessage,
        success: success,
        pagination: pagination);
  }

  Future<ResponseModel> searchList(RequestModel requestModel) async {
    List<dynamic> myList = [];
    var response = Response();
    var pagination = PaginationModel();
    bool success = false;

    try {
      response = await _httpClient.post(requestModel.endpoint,
          body: requestModel.body, params: requestModel.params);
      if (response.data['success']) {
        success = true;
        //success message
        response.statusMessage = 'common.success'.tr();

        // load items into a list
        myList = loadToList(response.data);

        //set pagination
        if (response.data['paginate'].isNotEmpty) {
          pagination = PaginationModel.fromJson(response.data['paginate']);
        }
      } else {
        response.statusMessage = 'common.error'.tr();
      }
    } catch (e) {
      response.statusMessage = NetworkExceptions.getErrorMessage(
          NetworkExceptions.getDioException(e));
    }

    return ResponseModel(
        data: myList,
        message: response.statusMessage,
        success: success,
        pagination: pagination);
  }

  Future<ResponseModel> fetchItem(RequestModel requestModel) async {
    var response = Response();
    var obj;
    bool success = false;

    try {
      response = await _httpClient.get(requestModel.endpoint);

      if (response.data['success']) {
        success = true;
        if (response.data['data'] is List && response.data['data'].isNotEmpty) {
          obj = model.fromJson(response.data['data'][0]);
        } else if (response.data['data'] is Map) {
          obj = model.fromJson(response.data['data']);
        }
      }
    } catch (e) {
      response.statusMessage = NetworkExceptions.getErrorMessage(
          NetworkExceptions.getDioException(e));
    }

    return ResponseModel(
        data: obj, message: response.statusMessage, success: success);
  }

  Future<ResponseModel> deleteItem(RequestModel requestModel) async {
    var obj;
    var response = Response();
    bool success = false;

    try {
      response = await _httpClient.delete(requestModel.endpoint,
          body: requestModel.body);

      if (response.data['success']) {
        success = true;
      }
    } catch (e) {
      response.statusMessage = NetworkExceptions.getErrorMessage(
          NetworkExceptions.getDioException(e));
    }

    return ResponseModel(
        data: obj, message: response.statusMessage, success: success);
  }

  Future<ResponseModel> updateItem(RequestModel requestModel) async {
    var response = Response();
    var obj;
    bool success = false;

    try {
      response =
          await _httpClient.put(requestModel.endpoint, body: requestModel.body);

      if (response.data['success']) {
        success = true;
        obj = response.data;
      }
    } catch (e) {
      response.statusMessage = NetworkExceptions.getErrorMessage(
          NetworkExceptions.getDioException(e));
    }

    return ResponseModel(
        data: obj, message: response.statusMessage, success: success);
  }

  Future<ResponseModel> createItem(RequestModel requestModel) async {
    var response = Response();
    var obj;
    ErrorMessageModel errorMessageModel;
    bool success = false;

    try {
      response = await _httpClient.post(requestModel.endpoint,
          body: requestModel.body);
      if (response.data['success']) {
        success = true;
        response.statusMessage = 'common.success'.tr();
        obj = response.data['data'];
      } else {
        errorMessageModel = ErrorMessageModel(response.data['errorMessage']);
        response.statusMessage = 'common.error'.tr();
      }
    } catch (e) {
      response.statusMessage = NetworkExceptions.getErrorMessage(
          NetworkExceptions.getDioException(e));
    }

    return ResponseModel(
        data: obj,
        message: response.statusMessage,
        success: success,
        errorMessageModel: errorMessageModel);
  }

  List<dynamic> loadToList(dynamic data) {
    List resultList = [];

    if (data is Map) {
      resultList =
          (data['data'] as List).map((i) => model.fromJson(i)).toList();
    } else if (data is List) {
      resultList = data.map((i) => model.fromJson(i)).toList();
    }

    return resultList;
  }
}
