import 'package:fluttermvvmtemplate/view/modules/system/category/model/category_model.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

enum ContainerType { CATEGORY, TYPE }

extension ContainerTypeRaw on ContainerType {
  String get rawValue {
    switch (this) {
      case ContainerType.CATEGORY:
        return 'category';
      default:
        return 'category';
    }
  }
}

class BaseHiveService {
  static var shared = BaseHiveService();
  final _adapterList = [CategoryModelAdapter()];

  // İnit Hive
  Future<void> setupHive() async {
    await Hive.initFlutter();

    // Register TypeAdapters
    _adapterList.forEach((adapter) => Hive.registerAdapter(adapter));

    // Opening Boxes
    for (var box in ContainerType.values) {
      await Hive.openBox(box.rawValue);
    }
  }

  // CRUD Operations
  T getData<T>(ContainerType type, dynamic key) {
    var box = Hive.box(type.rawValue);
    return box.get(key);
  }

  Future<int> add<T>(ContainerType type, T data) async {
    var box = Hive.box(type.rawValue);
    return await box.add(data);
  }

  Future<Iterable<int>> addAll<T>(ContainerType type, List<T> list) async {
    var box = Hive.box(type.rawValue);
    return await box.addAll(list);
  }

  Future<void> put<T>(ContainerType type, T data, dynamic key) async {
    var box = Hive.box(type.rawValue);
    await box.put(key, data);
  }

  Future<void> delete<T>(ContainerType type, dynamic key) async {
    var box = Hive.box(type.rawValue);
    await box.delete(key);
  }

  List<T> getValues<T>(ContainerType type) {
    var box = Hive.box(type.rawValue);
    return List<T>.from(box.values);
  }

  List<dynamic> getKeys<T>(ContainerType type) {
    var box = Hive.box(type.rawValue);
    return box.keys.toList();
  }
}
