import 'package:dio/dio.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/http_client.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/request_model.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/response_model.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/network_exceptions.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/model/language_info_model.dart';

class LanguageInfoService {
  final HttpClient _httpClient = HttpClient();

  LanguageInfoService();

  Future<ResponseModel> fetchItem(RequestModel requestModel) async {
    var response = Response();
    var obj;
    bool success = false;

    try {
      response = await _httpClient.get(requestModel.endpoint,
          params: requestModel.params);
      success = true;

      if (response.data['success']) {
        obj = LanguageInfoModel.fromJson(response.data['data']);
      }
    } catch (e) {
      response.statusMessage = NetworkExceptions.getErrorMessage(
          NetworkExceptions.getDioException(e));
    }

    return ResponseModel(
        data: obj, message: response.statusMessage, success: success);
  }

  Future<ResponseModel> delete(RequestModel requestModel) async {
    var response = Response();
    var obj;
    bool success = false;

    try {
      response = await _httpClient.delete(requestModel.endpoint,
          body: requestModel.body);

      if (response.data['success']) {
        success = true;
      }
    } catch (e) {
      response.statusMessage = NetworkExceptions.getErrorMessage(
          NetworkExceptions.getDioException(e));
    }

    return ResponseModel(
        data: obj, message: response.statusMessage, success: success);
  }

  Future<ResponseModel> update(RequestModel requestModel) async {
    var response = Response();
    var obj;
    bool success = false;

    try {
      response =
          await _httpClient.put(requestModel.endpoint, body: requestModel.body);
      if (response.data['success']) {
        success = true;
        obj = LanguageInfoModel.fromJson(response.data['data']);
      }
    } catch (e) {
      response.statusMessage = NetworkExceptions.getErrorMessage(
          NetworkExceptions.getDioException(e));
    }

    return ResponseModel(
        data: obj, message: response.statusMessage, success: success);
  }

  Future<ResponseModel> create(RequestModel requestModel) async {
    var response = Response();
    var obj;
    bool success = false;

    try {
      response = await _httpClient.post(requestModel.endpoint,
          body: requestModel.body);

      if (response.data['success']) {
        success = true;
        obj = LanguageInfoModel.fromJson(response.data['data']);
      }
    } catch (e) {
      response.statusMessage = NetworkExceptions.getErrorMessage(
          NetworkExceptions.getDioException(e));
    }

    return ResponseModel(
        data: obj, message: response.statusMessage, success: success);
  }
}
