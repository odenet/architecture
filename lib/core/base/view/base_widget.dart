import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/components/alerts/custom_form_alert.dart';
import 'package:fluttermvvmtemplate/core/init/notifier/data_type.dart';
import 'package:fluttermvvmtemplate/view/modules/access_control/model/session_control_model.dart';
import 'package:fluttermvvmtemplate/view/modules/access_control/service/access_control_service.dart';
import 'package:mobx/mobx.dart';

class BaseView<T extends Store> extends StatefulWidget {
  final Widget Function(BuildContext context, T value) onPageBuilder;
  final T viewModel;
  final Function(T model) onModelReady;
  final VoidCallback onDispose;
  final int pageId;
  final int objectId;
  final String objectSlug;
  final String dataType;

  const BaseView(
      {Key key,
      @required this.viewModel,
      @required this.onPageBuilder,
      this.onModelReady,
      this.onDispose,
      this.pageId,
      this.dataType,
      this.objectSlug,
      this.objectId})
      : super(key: key);

  @override
  _BaseViewState<T> createState() => _BaseViewState<T>();
}

class _BaseViewState<T extends Store> extends State<BaseView<T>> {
  T model;
  SessionControlModel sessionControlModel;

  @override
  void initState() {
    super.initState();
    model = widget.viewModel;
    // session control
    // TODO get logged in user ID
    sessionControlModel =
        SessionControlModel(userId: 2, objectId: widget.objectId);

    widget.onModelReady(model);
  }

  @override
  void dispose() {
    if (widget.onDispose != null) widget.onDispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return widget.onPageBuilder(context, model);
    /* return FutureBuilder(
      future: AccessControlService().isAllowed(
          sessionControlModel: sessionControlModel, pageId: widget.pageId),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
            return new Center(child: CircularProgressIndicator());
          case ConnectionState.waiting:
            return new Center(child: CircularProgressIndicator());

          case ConnectionState.active:
            return new Center(child: CircularProgressIndicator());
          
          case ConnectionState.done:
            if (snapshot.hasError)
              return new Text('Error: ${snapshot.error}');
            else {
              if (snapshot.data['success']) {
                return widget.onPageBuilder(context, model);
              } else {
                Future.delayed(
                    Duration.zero,
                    () => showAlert(context, 'OOPS', snapshot.data['message'],
                        CustomAlertType.ERROR));
                return Container(
                  child: Text(" "),
                );
              }
            }
        }
      },
    ); */
  }

  void showAlert(BuildContext context, String title, String message,
      CustomAlertType type) {
    showDialog(
        context: context,
        builder: (_) {
          return CustomFormAlert(
              title: title, message: message, alertType: type);
        });
  }
}
