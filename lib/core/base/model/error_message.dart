import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class ErrorMessageModel{

  @JsonKey(name: 'errorMessage')
  List<dynamic> errorMesage;

  ErrorMessageModel(this.errorMesage);

  List<List<String>> getValidateMessages(){
    var data = Map<String,List<dynamic>>.from(errorMesage.first['validateMessage']).values.toList();
    var list = data.map((e) => List<String>.from(e)).toList();
    return list;
     //return data.first['validateMessage'].values;
  }
}
