import 'package:json_annotation/json_annotation.dart';

part 'base_navigation_model.g.dart';

@JsonSerializable()
class BaseNavigationModel {
  String route;
  bool isUrl;
  String label;

  BaseNavigationModel({this.route, this.isUrl, this.label});

  factory BaseNavigationModel.fromJson(Map<String, dynamic> json) =>
      _$BaseNavigationModelFromJson(json);

  Map<String, dynamic> toJson() => _$BaseNavigationModelToJson(this);

  BaseNavigationModel getModel(Map json) {
    return BaseNavigationModel.fromJson(json);
  }
}

List<dynamic> menuList = [
  {'route': false, 'isUrl': false, 'label': 'Home', 'icon': 0xe7ba},
  {'route': false, 'isUrl': false, 'label': 'Payment Users', 'icon': 0xea43},
  {'route': false, 'isUrl': false, 'label': 'My Company', 'icon': 0xe689},
  {'route': false, 'isUrl': false, 'label': 'Employees', 'icon': 0xe560},
  {'route': false, 'isUrl': false, 'label': 'Cards', 'icon': 0xe68d},
  {'route': false, 'isUrl': false, 'label': 'Sales', 'icon': 0xe87e},
  {'route': false, 'isUrl': false, 'label': 'Deposit', 'icon': 0xe9dc},
  {'route': false, 'isUrl': false, 'label': 'My Account', 'icon': 0xe900},
  {
    'route': [
      {'isUrl': false, 'route': 'object', 'label': 'Objects', 'icon': 0xe567},
      {
        'isUrl': false,
        'route': 'watchlist',
        'label': 'Watchlist',
        'icon': 0xe567
      },
      {
        'isUrl': false,
        'route': 'available-languages',
        'label': 'Available Languages',
        'icon': 0xe567
      },
      {
        'isUrl': false,
        'route': 'version-control',
        'label': 'Version Control',
        'icon': 0xe567
      },
    ],
    'isUrl': false,
    'label': 'System',
    'icon': 0xea57
  },
];
