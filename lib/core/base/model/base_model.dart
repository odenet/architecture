import 'package:json_annotation/json_annotation.dart';

abstract class BaseModel<T> {
  @JsonKey(defaultValue: '')
  String name;

  @JsonKey(defaultValue: '')
  String description;

  @JsonKey(defaultValue: '')
  String slug;

  @JsonKey(defaultValue: true)
  bool isPublic;

  @JsonKey(defaultValue: true)
  bool isActive;

  @JsonKey(nullable: true)
  String targetRoute;

  Map<String, dynamic> toJson();
  T fromJson(Map<String, Object> json);
}
