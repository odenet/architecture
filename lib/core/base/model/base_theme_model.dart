import 'package:flutter/material.dart';

class BaseThemeModel {
  int primaryColor;
  int accentColor;
  int canvasColor;

  BaseThemeModel({this.primaryColor,this.accentColor,this.canvasColor});

  BaseThemeModel.fromJson(Map<String,dynamic> json){
    primaryColor = json['primaryColor'];
    accentColor = json['accentColor'];
    canvasColor = json['canvasColor'];
  }

  ThemeData getThemeData(){
    return ThemeData(
      primaryColor: Color(primaryColor),
      accentColor: Color(accentColor),
      canvasColor: Color(canvasColor)
    );
  }

}