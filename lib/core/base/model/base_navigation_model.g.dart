// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'base_navigation_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BaseNavigationModel _$BaseNavigationModelFromJson(Map<String, dynamic> json) {
  return BaseNavigationModel(
    route: json['route'] as String,
    isUrl: json['isUrl'] as bool,
    label: json['label'] as String,
  );
}

Map<String, dynamic> _$BaseNavigationModelToJson(
        BaseNavigationModel instance) =>
    <String, dynamic>{
      'route': instance.route,
      'isUrl': instance.isUrl,
      'label': instance.label,
    };
