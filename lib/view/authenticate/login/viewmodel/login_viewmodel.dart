import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/base/model/base_view_model.dart';
import 'package:fluttermvvmtemplate/core/components/alerts/custom_form_alert.dart';
import 'package:fluttermvvmtemplate/core/constants/enums/locale_keys_enum.dart';
import 'package:fluttermvvmtemplate/core/init/cache/locale_manager.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/response_model.dart';
import 'package:fluttermvvmtemplate/view/authenticate/login/service/auth_service.dart';
import 'package:fluttermvvmtemplate/view/authenticate/onboard/view/on_board_view.dart';
import 'package:fluttermvvmtemplate/view/authenticate/login/model/auth_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/network_error_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/model/available_languages_model.dart';
import 'package:mobx/mobx.dart';
part 'login_viewmodel.g.dart';

class LoginViewModel = _LoginViewModelBase with _$LoginViewModel;

abstract class _LoginViewModelBase with Store, BaseViewModel {
  final AuthService _authService = AuthService();

  @observable
  ObservableFuture<List<AvailableLanguagesModel>> items;

  @override
  void setContext(BuildContext context) => this.context = context;

  @override
  void init() {}

  Future<void> signIn({String email, String password}) async {
    try {
      var responseModel =
          await _authService.signIn(email: email, password: password);
      if (responseModel.success) {
        await saveAndNavigate(responseModel);
      } else {
        await showDialog(
            context: context,
            builder: (_) => CustomFormAlert(
                title: 'Auth',
                message: responseModel.message,
                alertType: CustomAlertType.WARNING));
      }
    } on NetworkErrorModel catch (e) {
      await showDialog(
          context: context,
          builder: (_) => CustomFormAlert(
                title: 'Auth Error',
                message: e.errorText,
                alertType: CustomAlertType.ERROR,
                onClose: () {
                  Navigator.of(context).pop();
                },
                onConfirm: () {
                  Navigator.of(context).pop();
                },
              ));
    }
  }

  Future<void> signUp(
      {String name,
      String email,
      String password,
      String citizenNo,
      String gsm,
      String language}) async {
    try {
      var responseModel = await _authService.signUp(
          name: name,
          email: email,
          password: password,
          citizenNo: citizenNo,
          gsm: gsm,
          language: language);
      if (responseModel.success) {
        await saveAndNavigate(responseModel);
      } else {
        await showDialog(
            context: context,
            builder: (_) => CustomFormAlert(
                title: 'Auth',
                message: responseModel.message,
                alertType: CustomAlertType.WARNING));
      }
    } on NetworkErrorModel catch (e) {
      await showDialog(
          context: context,
          builder: (_) => CustomFormAlert(
              title: 'Auth Error',
              message: e.errorText,
              alertType: CustomAlertType.ERROR));
    }
  }

  Future<void> saveAndNavigate(ResponseModel<AuthModel> responseModel) async {
    await showDialog(
        context: context,
        builder: (_) => CustomFormAlert(
              title: 'Auth',
              message: responseModel.message,
              alertType: CustomAlertType.SUCCESS,
              onConfirm: () {
                Navigator.of(context).pop();
              },
            ));
    await LocaleManager.instance
        .setStringValue(PreferencesKeys.TOKEN, responseModel.data.access_token);
    await Navigator.pushAndRemoveUntil(context,
        MaterialPageRoute(builder: (_) => OnBoardView()), (route) => false);
  }
}
