import 'package:flutter/material.dart';

import 'code_confirm.dart';

class AuthTabModel {
  String title;
  Widget icon;

  AuthTabModel({this.title, this.icon});
}

class ResetPasswordView extends StatefulWidget {
  @override
  _ResetPasswordViewState createState() => _ResetPasswordViewState();
}

class _ResetPasswordViewState extends State<ResetPasswordView> {
  int selectedTab = 0;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: DefaultTabController(
      initialIndex: selectedTab,
      length: tabList.length,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Reset Password'),
        ),
        body: Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                color: Color(0xFF1d1d1f),
                height: 70,
                width: MediaQuery.of(context).size.width,
                child: TabBar(
                  tabs: tabList
                      .map((item) => Tab(text: item.title, icon: item.icon))
                      .toList(),
                  onTap: (index) {
                    setState(() {
                      selectedTab = index;
                    });
                  },
                  unselectedLabelColor: Colors.grey,
                  indicatorColor: Colors.orange,
                ),
              ),
              SizedBox(
                height: 36,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: Container(
                  height: 45,
                  child: TextFormField(
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(0, 0.0, 0, 0),
                        hintText: tabList[selectedTab].title,
                        prefixIcon: tabList[selectedTab].icon,
                        filled: true,
                        fillColor: Color(0xFFe8e3e3),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8.0),
                            borderSide: BorderSide.none)),
                  ),
                ),
              ),
              SizedBox(
                height: 16,
              ),
              Align(
                alignment: Alignment.center,
                child: ElevatedButton.icon(
                  onPressed: () {},
                  icon: tabList[selectedTab].icon,
                  label: Text('Kod Gönder'),
                  style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all(Color(0xFF0071e3))),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Do you have a code ?',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    TextButton(
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (_) => CodeConfirmView()));
                      },
                      child: Text('Forward',style: TextStyle(color:Color(0xFF0071e3)),),
                    )
                  ],
                ),
              ),
              Center(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Text(
                    'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12,color: Colors.black54),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    ));
  }
}

extension DataSource on _ResetPasswordViewState {
  List<AuthTabModel> get tabList => [
        AuthTabModel(title: 'Tel No', icon: Icon(Icons.phone)),
        AuthTabModel(
            title: 'Kimlik No', icon: Icon(Icons.insert_drive_file)),
        AuthTabModel(title: 'Email', icon: Icon(Icons.mail)),
      ];
}
