import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

enum MethodStyle { PHONE, MAIL }

class CodeConfirmView extends StatefulWidget {
  @override
  _CodeConfirmViewState createState() => _CodeConfirmViewState();
}

class _CodeConfirmViewState extends State<CodeConfirmView> {
  int segmentedControlGroupValue = 0;
  int selectedAction = 0;

  final Map<int, Widget> myTabs = const <int, Widget>{
    0: Text('Mail'),
    1: Text('Phone')
  };

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        title: Text('Code Confirm'),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              'Which method would you like to receive password?',
              style: TextStyle(
                  color: Color(0xFF1d1d1f),
                  fontWeight: FontWeight.bold,
                  fontSize: 13),
            ),
          ),
          Center(
            child: CupertinoSlidingSegmentedControl(
                groupValue: segmentedControlGroupValue,
                children: myTabs,
                onValueChanged: (i) {
                  selectedAction = i;
                  setState(() {
                    segmentedControlGroupValue = i;
                  });
                }),
          ),
          SizedBox(height: 16,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Container(
              height: 45,
              child: TextFormField(
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(0, 0.0, 0, 0),
                    hintText: 'Your Code',
                    prefixIcon: Icon(Icons.insert_drive_file_outlined),
                    filled: true,
                    fillColor: Color(0xFFe8e3e3),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8.0),
                        borderSide: BorderSide.none)),
              ),
            ),
          ),
          SizedBox(height: 16,),
          Align(
            alignment: Alignment.center,
            child: ElevatedButton.icon(
              onPressed: () {},
              icon: Icon(Icons.receipt),
              label: Text('Gönder'),
              style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Color(0xFF0071e3))),
            ),
          ),
        ],
      ),
    ));
  }
}
