import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/base/view/base_widget.dart';
import 'package:fluttermvvmtemplate/core/components/form/base_form.dart';
import 'package:fluttermvvmtemplate/core/helpers/alert_helper.dart';
import 'package:fluttermvvmtemplate/view/authenticate/login/viewmodel/login_viewmodel.dart';

// ignore: must_be_immutable
class RegisterView extends StatelessWidget {
  TextEditingController _nameController;
  TextEditingController _citizenController;
  TextEditingController _emailController;
  TextEditingController _passwordController;
  TextEditingController _passwordVerificationController;
  TextEditingController _telephoneController;
  GlobalKey<FormState> _formState;

  @override
  Widget build(BuildContext context) {
    return BaseView<LoginViewModel>(
        key: PageStorageKey(1),
        onModelReady: (viewModel) {
          viewModel.init();
          viewModel.setContext(context);
          _nameController = TextEditingController();
          _citizenController = TextEditingController();
          _emailController = TextEditingController();
          _passwordController = TextEditingController();
          _passwordVerificationController = TextEditingController();
          _telephoneController = TextEditingController();
          _formState = GlobalKey<FormState>();
        },
        onDispose: () {
          _nameController.dispose();
          _citizenController.dispose();
          _emailController.dispose();
          _passwordController.dispose();
          _passwordVerificationController.dispose();
          _telephoneController.dispose();
        },
        viewModel: LoginViewModel(),
        onPageBuilder: (_, viewModel) {
          return SafeArea(
              child: Scaffold(
            backgroundColor: Colors.transparent,
            body: SingleChildScrollView(
              child: Container(
                child: Form(
                    key: _formState,
                    child: Column(
                      children: [
                        SizedBox(
                          height: 26,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 46),
                          child: Image.asset('asset/image/odenet_logo.png'),
                        ),
                        SizedBox(
                          height: 36,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 30),
                          child: Container(
                            height: 65,
                            child: TextFormField(
                              controller: _nameController,
                              decoration: InputDecoration(
                                  prefixIcon:
                                      Icon(Icons.account_circle_rounded),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(16)),
                                  hintText: 'name',
                                  labelText: 'name'),
                              validator: (str) {
                                return str.length > 3
                                    ? null
                                    : 'name must be longer than 3 characters';
                              },
                            ),
                          ),
                        ),
                        SizedBox(height: 16),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 30),
                          child: Container(
                            height: 65,
                            child: TextFormField(
                              controller: _citizenController,
                              decoration: InputDecoration(
                                  prefixIcon: Icon(Icons.person_add_alt),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(16)),
                                  hintText: 'citizen no',
                                  labelText: 'citizen no'),
                              validator: (str) {
                                return str.length > 9
                                    ? null
                                    : 'name must be longer than 9 characters';
                              },
                            ),
                          ),
                        ),
                        SizedBox(height: 16),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 30),
                          child: Container(
                            height: 65,
                            child: TextFormField(
                              controller: _emailController,
                              decoration: InputDecoration(
                                  prefixIcon: Icon(Icons.mail),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(16)),
                                  hintText: 'email',
                                  labelText: 'email'),
                              validator: (str) {
                                return str.contains('@')
                                    ? null
                                    : 'Does not match the e-mail address format';
                              },
                            ),
                          ),
                        ),
                        SizedBox(height: 16),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 30),
                          child: Container(
                            height: 65,
                            child: TextFormField(
                              controller: _passwordController,
                              obscureText: true,
                              decoration: InputDecoration(
                                prefixIcon: Icon(Icons.lock),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(16)),
                                hintText: 'password',
                                labelText: 'password',
                              ),
                              validator: (str) {
                                return str.length > 6
                                    ? null
                                    : 'name must be longer than 6 characters';
                              },
                            ),
                          ),
                        ),
                        SizedBox(height: 16),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 30),
                          child: Container(
                            height: 65,
                            child: TextFormField(
                              controller: _passwordVerificationController,
                              obscureText: true,
                              decoration: InputDecoration(
                                prefixIcon: Icon(Icons.lock),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(16)),
                                hintText: 'password verification',
                                labelText: 'password verification',
                              ),
                              validator: (str) {
                                return str == _passwordController.text &&
                                        str.isNotEmpty
                                    ? null
                                    : 'your password does not match';
                              },
                            ),
                          ),
                        ),
                        SizedBox(height: 16),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 30),
                          child: Container(
                            height: 65,
                            child: TextFormField(
                              controller: _telephoneController,
                              decoration: InputDecoration(
                                prefixIcon: Icon(Icons.phone),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(16)),
                                hintText: 'phone',
                                labelText: 'phone',
                              ),
                              validator: (str) {
                                return str.length > 11
                                    ? null
                                    : 'name must be longer than 11 characters';
                              },
                            ),
                          ),
                        ),
                        SizedBox(height: 16),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.language,
                              color: Colors.blue,
                            ),
                            CustomDropDownButton(
                              items: ['Türkçe', 'Arabic', 'German', 'English'],
                              onChanged: (value) {},
                              hintText: 'Language',
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 26,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            ElevatedButton.icon(
                              onPressed: () async{
                                if (_formState.currentState.validate()) {
                                  AlertHelper.shared.showProgressDialog(context);
                                  await viewModel.signUp(
                                      email: _emailController.text,
                                      name: _nameController.text,
                                      password: _passwordController.text,
                                      gsm: _telephoneController.text,
                                      language: 'tr',
                                      citizenNo: _citizenController.text);
                                  Navigator.of(context).pop();    
                                }
                              },
                              icon: Icon(Icons.app_registration),
                              label: Text('  Register  '),
                              style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all(Colors.blue)),
                            ),
                            ElevatedButton.icon(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              icon: Icon(Icons.arrow_back_ios_outlined),
                              label: Text('  Back  '),
                              style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all(Colors.black)),
                            ),
                          ],
                        ),
                      ],
                    )),
              ),
            ),
          ));
        });
  }
}
