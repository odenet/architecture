import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/base/view/base_widget.dart';
import 'package:fluttermvvmtemplate/core/helpers/alert_helper.dart';
import 'package:fluttermvvmtemplate/core/validator/custom_validator.dart';
import 'package:fluttermvvmtemplate/view/authenticate/login/view/register_view.dart';
import 'package:fluttermvvmtemplate/view/authenticate/login/view/reset_password.dart';
import 'package:fluttermvvmtemplate/view/authenticate/login/viewmodel/login_viewmodel.dart';

class LoginView extends StatelessWidget {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final GlobalKey<FormState> _formState = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BaseView<LoginViewModel>(
        onModelReady: (model) {
          model.setContext(context);
          model.init();
        },
        onDispose: () {
          _emailController.dispose();
          _passwordController.dispose();
        },
        viewModel: LoginViewModel(),
        onPageBuilder: (context, viewModel) {
          return SafeArea(
              child: DefaultTabController(
            length: tabList.length,
            child: Scaffold(
              body: SingleChildScrollView(
                child: Container(
                  child: Form(
                    key: _formState,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 45,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 46),
                          child: Image.asset('asset/image/odenet_logo.png'),
                        ),
                        SizedBox(
                          height: 39,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 30),
                          child: Center(
                            child: TextFormField(
                              controller: _emailController,
                              decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(12.0),
                                  prefixIcon:
                                      Icon(Icons.account_circle_rounded),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(16)),
                                  hintText: 'user name',
                                  labelText: 'user name'),
                              validator: (str) {
                                return str.contains('@')
                                    ? null
                                    : 'Does not match the e-mail address format';
                              },
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 30),
                          child: Center(
                            child: TextFormField(
                              controller: _passwordController,
                              obscureText: true,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.all(12.0),
                                prefixIcon: Icon(Icons.lock),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(16)),
                                hintText: 'password',
                                labelText: 'password',
                              ),
                              validator: (str) {
                                return str.isNotEmpty
                                    ? null
                                    : 'This field cannot be left blank';
                              },
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Align(
                          alignment: Alignment.topRight,
                          child: TextButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) => ResetPasswordView()));
                            },
                            child: Padding(
                              padding: const EdgeInsets.only(right: 20),
                              child: Text(
                                'Forgot Password',
                                style: TextStyle(color: Colors.black),
                              ),
                            ),
                          ),
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ElevatedButton.icon(
                              onPressed: () async {
                                if (_formState.currentState.validate()) {
                                  AlertHelper.shared.showProgressDialog(context);
                                  await viewModel.signIn(
                                      email: _emailController.text,
                                      password: _passwordController.text);
                                  Navigator.pop(context);
                                }
                              },
                              icon: Icon(Icons.login),
                              label: Text('  Login  '),
                              style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all(Colors.blue)),
                            ),
                            SizedBox(
                              width: 16,
                            ),
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'Don\'t have an account ?',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  TextButton(
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (_) => RegisterView()));
                                    },
                                    child: Text('Register  '),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ));
        });
  }
}

extension DataSource on LoginView {
  List<AuthTabModel> get tabList => [
        AuthTabModel(title: 'Kurumsal', icon: Icon(Icons.insert_drive_file)),
        AuthTabModel(title: 'Bireysel', icon: Icon(Icons.person)),
      ];
}
