import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'auth_model.g.dart';

@JsonSerializable()
class AuthModel{
  String access_token;
  String token_type;
  String expires_at;

  AuthModel({@required this.access_token, @required this.token_type,@required this.expires_at});
  factory AuthModel.fromJson(Map<String, dynamic> json) => _$AuthModelFromJson(json);

  get message => null;
  Map<String, dynamic> toJson() => _$AuthModelToJson(this);
}