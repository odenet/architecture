import 'package:dio/dio.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/http_client.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/response_model.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/network_exceptions.dart';
import 'package:fluttermvvmtemplate/view/authenticate/login/model/auth_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/network_error_model.dart';

class AuthService {
  final HttpClient _client = HttpClient();

  Future<ResponseModel<AuthModel>> signIn(
      {String email, String password}) async {
    final body = {'email': email, 'password': password};

    var response = Response();
    AuthModel userModel;
    var networkErrorModel = NetworkErrorModel(hasError: false);

    try {
      response = await _client.post('login', body: body);
      if (response.data['success']) {
        userModel = AuthModel.fromJson(response.data['data']);
      }
    } catch (e) {
      networkErrorModel.errorText = NetworkExceptions.getErrorMessage(
          NetworkExceptions.getDioException(e));
      networkErrorModel.hasError = true;
      throw networkErrorModel;
    }

    return ResponseModel(
        data: userModel,
        success: response.data['success'],
        message: response.data['message'],
        networkError: networkErrorModel);
  }

  Future<ResponseModel<AuthModel>> signUp(
      {String name,
      String email,
      String password,
      String citizenNo,
      String gsm,
      String language}) async {
    final body = {
      'name': name,
      'email': email,
      'password': password,
      'citizenNo': citizenNo,
      'gsm': gsm,
      'language': language
    };

    AuthModel userModel;
    var response = Response();
    var networkErrorModel = NetworkErrorModel(hasError: false);

    try {
      response = await _client.post('register', body: body);
      if (response.data['success']) {
        userModel = AuthModel.fromJson(response.data['data']);
      }
    } on DioError catch (error) {
      networkErrorModel.hasError = true;
      networkErrorModel.errorText = error.response.data['message'];
      throw networkErrorModel;
    }

    return ResponseModel(
        data: userModel,
        success: response.data['success'],
        message: response.data['message'],
        networkError: networkErrorModel);
  }
}
