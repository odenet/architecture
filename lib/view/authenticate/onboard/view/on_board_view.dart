import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/view/authenticate/onboard/view/chart_view.dart';
import 'package:fluttermvvmtemplate/view/modules/menu_management/view/menu_view.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../../../core/base/view/base_widget.dart';
import '../viewModel/on_board_view_model.dart';
import 'package:fluttermvvmtemplate/core/components/language_bar/select_language.dart';

class Presenter {
  static Widget view() {
    return OnBoardView();
  }
}

class OnBoardView extends StatefulWidget {
  @override
  _OnBoardViewState createState() => _OnBoardViewState();
}

class _OnBoardViewState extends State<OnBoardView> {
  @override
  Widget build(BuildContext context) {
    return BaseView<OnBoardViewModel>(
        pageId: 25,
        viewModel: OnBoardViewModel(),
        onModelReady: (model) {
          model.setContext(context);
          model.init();
        },
        onPageBuilder: (BuildContext context, OnBoardViewModel viewModel) =>
            SafeArea(
              child: Scaffold(
                appBar: AppBar(
                  centerTitle: true,
                  title: Text(
                    'landing.welcome'.tr(),
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 18.0),
                  ),
                  actions: [SelectLanguage(viewModel: viewModel)],
                ),
                extendBody: true,
                body: Container(
                    color: Color(0xFFF3F2F8),
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(height: 16),
                          Container(
                            height: MediaQuery.of(context).size.height * 0.37,
                            margin: EdgeInsets.symmetric(horizontal: 8),
                            clipBehavior: Clip.hardEdge,
                            child: PageView(
                              children: [
                                OnboardChartView(),
                                OnboardChartView(),
                              ],
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(16)),
                          ),
                          SizedBox(height: 16),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Shoppings',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 22),
                                ),
                                Text(
                                  'Show details',
                                  style: TextStyle(
                                      color: Color(0xFF3788e6),
                                      fontWeight: FontWeight.w600),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 8),
                          Expanded(
                            child: Container(
                                margin: EdgeInsets.symmetric(horizontal: 16),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(16)),
                                child: ListView.separated(
                                    shrinkWrap: true,
                                    itemCount: 6,
                                    separatorBuilder: (_, index) => Divider(),
                                    itemBuilder: (_, index) {
                                      return ListTile(
                                        leading: Image.asset(
                                          'asset/image/onboard/pc.png',
                                          height: 40,
                                          width: 40,
                                        ),
                                        title: Text(
                                          'Shopping',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                        subtitle: Text('6 Transcations'),
                                        trailing: Icon(Icons.arrow_right),
                                      );
                                    })),
                          ),
                        ],
                      ),
                    )),
                floatingActionButton: FloatingActionButton(
                    backgroundColor: Colors.red,
                    child: Icon(
                      Icons.attach_money,
                      color: Colors.white,
                    ),
                    onPressed: () {}),
                floatingActionButtonLocation:
                    FloatingActionButtonLocation.centerDocked,
                bottomNavigationBar: CustomBottomNavigationBar(
                  onPage: (currentPageIndex) {
                    print('Sayfa : $currentPageIndex');
                  },
                ),
              ),
            ));
  }
}

class CustomBottomNavigationBar extends StatefulWidget {
  final Function(int) onPage;

  const CustomBottomNavigationBar({
    @required this.onPage,
    Key key,
  }) : super(key: key);

  @override
  _CustomBottomNavigationBarState createState() =>
      _CustomBottomNavigationBarState();
}

class _CustomBottomNavigationBarState extends State<CustomBottomNavigationBar> {
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      shape: CircularNotchedRectangle(),
      child: Container(
        height: 50,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            IconButton(
              iconSize: 30.0,
              padding: EdgeInsets.only(left: 28.0),
              icon: Icon(Icons.shopping_bag_outlined),
              onPressed: () {
                setState(() {
                  //_myPage.jumpToPage(0);
                });
              },
            ),
            IconButton(
              iconSize: 30.0,
              padding: EdgeInsets.only(right: 28.0),
              icon: Icon(Icons.home),
              onPressed: () {
                setState(() {
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(builder: (_) => OnBoardView()),
                      (route) => false);
                });
              },
            ),
            IconButton(
              iconSize: 30.0,
              padding: EdgeInsets.only(left: 28.0),
              icon: Icon(Icons.notifications),
              onPressed: () {
                setState(() {
                  //_myPage.jumpToPage(2);
                });
              },
            ),
            IconButton(
              iconSize: 30.0,
              padding: EdgeInsets.only(right: 28.0),
              icon: Icon(Icons.list),
              onPressed: () {
                setState(() {
                  //_myPage.jumpToPage(3);
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => MenuView()));
                });
              },
            )
          ],
        ),
      ),
    );
  }
}
