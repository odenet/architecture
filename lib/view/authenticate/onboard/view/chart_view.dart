import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:flutter/material.dart';

class BarChart<T, K> {
  BarChart(this.x, this.y);
  final T x;
  final K y;
}

class OnboardChartView extends StatelessWidget {
  
  final dataSource = [
    BarChart<String, int>('Jan', 50),
    BarChart<String, int>('Feb', 28),
    BarChart<String, int>('Mar', 34),
    BarChart<String, int>('Apr', 32),
    BarChart<String, int>('May', 21)
  ];

  final LinearGradient _linearGradient = LinearGradient(
    colors: <Color>[
      Color(0xFF43cea2),
      Color(0xFF185a9d),
    ],
    begin: Alignment.bottomLeft,
    end: Alignment.topRight,
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SfCartesianChart(
          backgroundColor: Color(0xFFFFFFFF),
          title: ChartTitle(text: 'Half yearly sales analysis'),
          tooltipBehavior: TooltipBehavior(enable: true),
          // Initialize category axis
          primaryXAxis: CategoryAxis(),
          series: <ChartSeries>[
            ColumnSeries<BarChart<String, int>, int>(
                gradient: _linearGradient,
                dataSource: dataSource,
                xValueMapper: (BarChart sales, _) => sales.y,
                yValueMapper: (BarChart sales, _) => sales.y,
                width: 0.8, // Width of the columns
                spacing: 0.2, // Spacing between the columns
                animationDuration: 2000,
                dataLabelSettings: DataLabelSettings(
                  isVisible: true,
                  // Positioning the data label
                  useSeriesColor: true,
                ),
                name: 'Prices')
          ],
        ),
      ),
    );
  }
}
