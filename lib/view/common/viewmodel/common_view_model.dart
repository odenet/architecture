import 'package:fluttermvvmtemplate/core/constants/enums/store_state.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/response_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/network_error_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/model/available_languages_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/service/available_languages_service.dart';
import 'package:fluttermvvmtemplate/view/modules/system/types/model/type_model.dart';
import 'package:mobx/mobx.dart';
import 'package:fluttermvvmtemplate/view/modules/system/types/service/type_service.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/service/category_service.dart';

part 'common_view_model.g.dart';

class CommonViewModel = _CommonViewModelBase with _$CommonViewModel;

abstract class _CommonViewModelBase with Store {
  AvailableLanguagesService availableLanguageService =
      AvailableLanguagesService();

  var typeService = TypeService();
  var categoryService = CategoryService();

  @observable
  ObservableList<TypeModel> types = ObservableList<TypeModel>();

  @observable
  bool typeIsCompleted = false;

  @observable
  ObservableList categories;

  @observable
  bool isSearchActive;

  @observable
  ObservableFuture future;

  @observable
  bool isEdit = false;

  @observable
  List<AvailableLanguagesModel> availableLanguages = [];

  @observable
  NetworkErrorModel networkError = NetworkErrorModel(hasError: false);

  @computed
  StoreState get state {
    if (future == null || future.status == FutureStatus.rejected) {
      return StoreState.initial;
    }

    if (future.status == FutureStatus.pending) {
      return StoreState.loading;
    } else if (future.status == FutureStatus.fulfilled) {
      if (networkError.hasError) {
        return StoreState.loadingError;
      } else {
        return StoreState.loaded;
      }
    }
  }

  @action
  void setIsEdit(bool value) {
    isEdit = value;
  }

  @action
  Future<void> getAvailableLanguages() async {
    var response = ResponseModel(success: false, message: " ");
    try {
      var langfuture = ObservableFuture(availableLanguageService.fetchList());
      response = await langfuture as ResponseModel;
      availableLanguages = List<AvailableLanguagesModel>.from(response.data);
    } catch (e) {
      networkError = NetworkErrorModel(errorText: response.message);
    }

    return response;
  }

  @action
  Future<void> getTypes() async {
    future = ObservableFuture(typeService.fetchList());
    var response = await future as ResponseModel;
    var responseList = List<TypeModel>.from(response.data);
    types = responseList.asObservable();
    typeIsCompleted = true;
  }

  @action
  Future<void> getCategories() async {
    future = ObservableFuture(categoryService.fetchList());
    var response = await future as ResponseModel;

    categories = ObservableList.of(response.data);
  }
}
