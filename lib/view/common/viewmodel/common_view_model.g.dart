// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'common_view_model.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CommonViewModel on _CommonViewModelBase, Store {
  Computed<StoreState> _$stateComputed;

  @override
  StoreState get state =>
      (_$stateComputed ??= Computed<StoreState>(() => super.state,
              name: '_CommonViewModelBase.state'))
          .value;

  final _$typesAtom = Atom(name: '_CommonViewModelBase.types');

  @override
  ObservableList<TypeModel> get types {
    _$typesAtom.reportRead();
    return super.types;
  }

  @override
  set types(ObservableList<TypeModel> value) {
    _$typesAtom.reportWrite(value, super.types, () {
      super.types = value;
    });
  }

  final _$typeIsCompletedAtom =
      Atom(name: '_CommonViewModelBase.typeIsCompleted');

  @override
  bool get typeIsCompleted {
    _$typeIsCompletedAtom.reportRead();
    return super.typeIsCompleted;
  }

  @override
  set typeIsCompleted(bool value) {
    _$typeIsCompletedAtom.reportWrite(value, super.typeIsCompleted, () {
      super.typeIsCompleted = value;
    });
  }

  final _$categoriesAtom = Atom(name: '_CommonViewModelBase.categories');

  @override
  ObservableList<dynamic> get categories {
    _$categoriesAtom.reportRead();
    return super.categories;
  }

  @override
  set categories(ObservableList<dynamic> value) {
    _$categoriesAtom.reportWrite(value, super.categories, () {
      super.categories = value;
    });
  }

  final _$isSearchActiveAtom =
      Atom(name: '_CommonViewModelBase.isSearchActive');

  @override
  bool get isSearchActive {
    _$isSearchActiveAtom.reportRead();
    return super.isSearchActive;
  }

  @override
  set isSearchActive(bool value) {
    _$isSearchActiveAtom.reportWrite(value, super.isSearchActive, () {
      super.isSearchActive = value;
    });
  }

  final _$futureAtom = Atom(name: '_CommonViewModelBase.future');

  @override
  ObservableFuture<dynamic> get future {
    _$futureAtom.reportRead();
    return super.future;
  }

  @override
  set future(ObservableFuture<dynamic> value) {
    _$futureAtom.reportWrite(value, super.future, () {
      super.future = value;
    });
  }

  final _$isEditAtom = Atom(name: '_CommonViewModelBase.isEdit');

  @override
  bool get isEdit {
    _$isEditAtom.reportRead();
    return super.isEdit;
  }

  @override
  set isEdit(bool value) {
    _$isEditAtom.reportWrite(value, super.isEdit, () {
      super.isEdit = value;
    });
  }

  final _$availableLanguagesAtom =
      Atom(name: '_CommonViewModelBase.availableLanguages');

  @override
  List<AvailableLanguagesModel> get availableLanguages {
    _$availableLanguagesAtom.reportRead();
    return super.availableLanguages;
  }

  @override
  set availableLanguages(List<AvailableLanguagesModel> value) {
    _$availableLanguagesAtom.reportWrite(value, super.availableLanguages, () {
      super.availableLanguages = value;
    });
  }

  final _$networkErrorAtom = Atom(name: '_CommonViewModelBase.networkError');

  @override
  NetworkErrorModel get networkError {
    _$networkErrorAtom.reportRead();
    return super.networkError;
  }

  @override
  set networkError(NetworkErrorModel value) {
    _$networkErrorAtom.reportWrite(value, super.networkError, () {
      super.networkError = value;
    });
  }

  final _$getAvailableLanguagesAsyncAction =
      AsyncAction('_CommonViewModelBase.getAvailableLanguages');

  @override
  Future<void> getAvailableLanguages() {
    return _$getAvailableLanguagesAsyncAction
        .run(() => super.getAvailableLanguages());
  }

  final _$getTypesAsyncAction = AsyncAction('_CommonViewModelBase.getTypes');

  @override
  Future<void> getTypes() {
    return _$getTypesAsyncAction.run(() => super.getTypes());
  }

  final _$getCategoriesAsyncAction =
      AsyncAction('_CommonViewModelBase.getCategories');

  @override
  Future<void> getCategories() {
    return _$getCategoriesAsyncAction.run(() => super.getCategories());
  }

  final _$_CommonViewModelBaseActionController =
      ActionController(name: '_CommonViewModelBase');

  @override
  void setIsEdit(bool value) {
    final _$actionInfo = _$_CommonViewModelBaseActionController.startAction(
        name: '_CommonViewModelBase.setIsEdit');
    try {
      return super.setIsEdit(value);
    } finally {
      _$_CommonViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
types: ${types},
typeIsCompleted: ${typeIsCompleted},
categories: ${categories},
isSearchActive: ${isSearchActive},
future: ${future},
isEdit: ${isEdit},
availableLanguages: ${availableLanguages},
networkError: ${networkError},
state: ${state}
    ''';
  }
}
