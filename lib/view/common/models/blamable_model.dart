import 'package:json_annotation/json_annotation.dart';

part 'blamable_model.g.dart';

@JsonSerializable()
class BlamableModel {
  @JsonKey(nullable: true)
  int id;

  @JsonKey(nullable: true)
  String name;

  BlamableModel({this.id, this.name});

  @override
  static BlamableModel fromJson(Map<String, dynamic> json) {
    return _$BlamableModelFromJson(json);
  }

  @override
  Map<String, dynamic> toJson({BlamableModel model}) {
    return _$BlamableModelToJson(model);
  }
}
