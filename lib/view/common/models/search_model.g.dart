// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SearchModel _$SearchModelFromJson(Map<String, dynamic> json) {
  return SearchModel(
    name: json['name'] as String,
    language: json['language'] as String,
    isPublic: json['isPublic'] as String,
    isActive: json['isActive'] as String,
    typeSlug: json['typeSlug'] as String,
  );
}

Map<String, dynamic> _$SearchModelToJson(SearchModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'isActive': instance.isActive,
      'isPublic': instance.isPublic,
      'language': instance.language,
      'typeSlug': instance.typeSlug,
    };
