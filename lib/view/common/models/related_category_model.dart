import 'package:json_annotation/json_annotation.dart';

part 'related_category_model.g.dart';

@JsonSerializable()
class RelatedCategoryModel {
  @JsonKey(nullable: true)
  String slug;

  @JsonKey(nullable: true)
  String name;

  @JsonKey(nullable: true)
  String description;

  @JsonKey(nullable: true)
  String categorySlug;

  @JsonKey(nullable: true)
  String dataSlug;

  @JsonKey(nullable: true)
  String dataType;

  @JsonKey(defaultValue: false)
  bool isMain;

  @JsonKey(defaultValue: true)
  bool isPublic;

  @JsonKey(defaultValue: true)
  bool isActive;

  RelatedCategoryModel(
      {this.slug,
      this.name,
      this.description,
      this.categorySlug,
      this.isActive = true,
      this.isPublic = true,
      this.dataType,
      this.dataSlug,
      this.isMain});

  @override
  static RelatedCategoryModel fromJson(Map<String, dynamic> json) {
    return _$RelatedCategoryModelFromJson(json);
  }

  @override
  static Map<String, dynamic> toJson({RelatedCategoryModel model}) {
    var json = _$RelatedCategoryModelToJson(model);
    json['dataType'] = 'object';

    return json;
  }
}
