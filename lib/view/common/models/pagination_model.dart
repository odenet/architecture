import 'package:json_annotation/json_annotation.dart';

part 'pagination_model.g.dart';

@JsonSerializable()
class PaginationModel {
  @JsonKey(nullable: true)
  int currentPage;

  @JsonKey(nullable: true)
  int from;

  @JsonKey(nullable: true)
  int lastPage;

  @JsonKey(nullable: true)
  int perPage;

  @JsonKey(nullable: true)
  int to;

  @JsonKey(nullable: true)
  int total;

  PaginationModel(
      {this.currentPage,
      this.from,
      this.lastPage,
      this.perPage,
      this.to,
      this.total});

  @override
  static PaginationModel fromJson(Map<String, dynamic> json) {
    return _$PaginationModelFromJson(json);
  }

  @override
  Map<String, dynamic> toJson({PaginationModel model}) {
    return _$PaginationModelToJson(model);
  }
}
