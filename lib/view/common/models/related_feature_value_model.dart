import 'package:json_annotation/json_annotation.dart';

import 'blamable_model.dart';

part 'related_feature_value_model.g.dart';

@JsonSerializable()
class RelatedFeatureValueModel {
  @JsonKey(nullable: true)
  String languageLabel;

  @JsonKey(nullable: true)
  String language;

  @JsonKey(nullable: true)
  String dataType;

  @JsonKey(nullable: true)
  String slug;

  @JsonKey(nullable: true)
  String featureValue;

  @JsonKey(nullable: true)
  String featureLabel;

  @JsonKey(defaultValue: true)
  bool isActive;

  @JsonKey(defaultValue: true)
  bool isPublic;

  @JsonKey(nullable: true)
  String order;

  @JsonKey(nullable: true)
  int level;

  @JsonKey(nullable: true)
  int rank;

  @JsonKey(defaultValue: '')
  String createdAt;

  @JsonKey(defaultValue: '')
  String updatedAt;

  @JsonKey(nullable: true)
  BlamableModel createdBy;

  @JsonKey(nullable: true)
  BlamableModel updatedBy;

  RelatedFeatureValueModel(
      {this.dataType,
      this.slug,
      this.featureLabel,
      this.featureValue,
      this.isActive = true,
      this.isPublic = true,
      this.order,
      this.level,
      this.rank});

  RelatedFeatureValueModel.clone(RelatedFeatureValueModel model) {
    slug = model.slug;
    featureLabel = model.featureLabel;
    featureValue = model.featureValue;
    isActive = model.isActive;
    isPublic = model.isPublic;
    order = model.order;
    level = model.level;
    rank = model.rank;
  }

  @override
  RelatedFeatureValueModel fromJson(Map<String, dynamic> json) {
    return _$RelatedFeatureValueModelFromJson(json);
  }

  @override
  Map<String, dynamic> toJson() {
    return _$RelatedFeatureValueModelToJson(this);
  }
}
