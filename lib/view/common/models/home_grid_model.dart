import 'package:flutter/material.dart';

class HomeGridModel {
  String title;
  Color color;
  Widget image;
  int count;
  Widget page;

  HomeGridModel({this.title, this.color, this.image, this.count, this.page});
}
