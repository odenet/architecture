import 'package:json_annotation/json_annotation.dart';

part 'search_model.g.dart';

@JsonSerializable()
class SearchModel {
  static const ALL = '_ALL';

  @JsonKey(nullable: true)
  String name;

  @JsonKey(nullable: true)
  String isActive;

  @JsonKey(nullable: true)
  String isPublic;

  @JsonKey(nullable: true)
  String language;

  @JsonKey(nullable: true)
  String typeSlug;

  SearchModel(
      {this.name = '',
      this.language,
      this.isPublic = '1',
      this.isActive = '1',
        this.typeSlug
      });

  @override
  static SearchModel fromJson(Map<String, dynamic> json) {
    return _$SearchModelFromJson(json);
  }

  @override
  static Map<String, dynamic> toJson({SearchModel model}) {
    return _$SearchModelToJson(model);
  }
}
