// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pagination_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaginationModel _$PaginationModelFromJson(Map<String, dynamic> json) {
  return PaginationModel(
    currentPage: json['currentPage'] as int,
    from: json['from'] as int,
    lastPage: json['lastPage'] as int,
    perPage: json['perPage'] as int,
    to: json['to'] as int,
    total: json['total'] as int,
  );
}

Map<String, dynamic> _$PaginationModelToJson(PaginationModel instance) =>
    <String, dynamic>{
      'currentPage': instance.currentPage,
      'from': instance.from,
      'lastPage': instance.lastPage,
      'perPage': instance.perPage,
      'to': instance.to,
      'total': instance.total,
    };
