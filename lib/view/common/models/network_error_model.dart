class NetworkErrorModel implements Exception{
  String errorText;
  bool hasError;

  NetworkErrorModel({this.errorText, this.hasError = true});
}
