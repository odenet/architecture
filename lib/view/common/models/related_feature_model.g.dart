// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'related_feature_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RelatedFeatureModel _$RelatedFeatureModelFromJson(Map<String, dynamic> json) {
  return RelatedFeatureModel(
    slug: json['slug'] ?? '',
    name: json['name'] ?? '',
    description: json['description'] ?? '',
    isActive: json['isActive'] ?? true,
    isPublic: json['isPublic'] ?? true,
    order: json['order'] as String,
    level: json['level'] as int,
    rank: json['rank'] as int,
    featureValue: json['featureValue'] as String,
    featureSlug: json['featureSlug'] as String,
    dataType: json['dataType'] as String,
    dataSlug: json['dataSlug'] as String,
    language: json['language'] as String,
    isMain: json['isMain'] as bool ?? false,
  )
    ..targetRoute = json['targetRoute'] as String
    ..createdAt = json['createdAt'] as String ?? ''
    ..updatedAt = json['updatedAt'] as String ?? ''
    ..createdBy = json['createdBy'] == null
        ? null
        : BlamableModel.fromJson(json['createdBy'] as Map<String, dynamic>)
    ..updatedBy = json['updatedBy'] == null
        ? null
        : BlamableModel.fromJson(json['updatedBy'] as Map<String, dynamic>);
}

Map<String, dynamic> _$RelatedFeatureModelToJson(
        RelatedFeatureModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'description': instance.description,
      'slug': instance.slug,
      'isPublic': instance.isPublic,
      'isActive': instance.isActive,
      'targetRoute': instance.targetRoute,
      'dataType': instance.dataType,
      'dataSlug': instance.dataSlug,
      'language': instance.language,
      'featureSlug': instance.featureSlug,
      'featureValue': instance.featureValue,
      'isMain': instance.isMain,
      'order': instance.order,
      'level': instance.level,
      'rank': instance.rank,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
      'createdBy': instance.createdBy,
      'updatedBy': instance.updatedBy,
    };
