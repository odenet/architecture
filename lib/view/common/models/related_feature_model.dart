import 'package:json_annotation/json_annotation.dart';
import 'package:fluttermvvmtemplate/core/base/model/base_model.dart';

import 'blamable_model.dart';

part 'related_feature_model.g.dart';

@JsonSerializable()
class RelatedFeatureModel extends BaseModel<RelatedFeatureModel> {
  @JsonKey(nullable: true)
  String dataType;

  @JsonKey(nullable: true)
  String dataSlug;

  @JsonKey(nullable: true)
  String language;

  @JsonKey(nullable: true)
  String featureSlug;

  @JsonKey(nullable: true)
  String featureValue;

  @JsonKey(defaultValue: false)
  bool isMain;

  @JsonKey(nullable: true)
  String order;

  @JsonKey(nullable: true)
  int level;

  @JsonKey(nullable: true)
  int rank;

  @JsonKey(defaultValue: '')
  String createdAt;

  @JsonKey(defaultValue: '')
  String updatedAt;

  @JsonKey(nullable: true)
  BlamableModel createdBy;

  @JsonKey(nullable: true)
  BlamableModel updatedBy;

  RelatedFeatureModel(
      {slug,
      name,
      description,
      isActive = true,
      isPublic = true,
      this.order,
      this.level,
      this.rank,
      this.featureValue,
      this.featureSlug,
      this.dataType,
      this.dataSlug,
      this.language,
      this.isMain}) {
    {
      this.slug = slug;
      this.name = name;
      this.description = description;
      this.isActive = isActive;
      this.isPublic = isPublic;
    }
  }

  @override
  RelatedFeatureModel fromJson(Map<String, dynamic> json) {
    return _$RelatedFeatureModelFromJson(json);
  }

  @override
  Map<String, dynamic> toJson() {
    return _$RelatedFeatureModelToJson(this);
  }
}
