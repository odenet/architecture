// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'blamable_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BlamableModel _$BlamableModelFromJson(Map<String, dynamic> json) {
  return BlamableModel(
    id: json['id'] as int,
    name: json['name'] as String,
  );
}

Map<String, dynamic> _$BlamableModelToJson(BlamableModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };
