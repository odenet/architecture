// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'related_category_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RelatedCategoryModel _$RelatedCategoryModelFromJson(Map<String, dynamic> json) {
  return RelatedCategoryModel(
    slug: json['slug'] as String,
    name: json['name'] as String,
    description: json['description'] as String,
    categorySlug: json['categorySlug'] as String,
    isActive: json['isActive'] as bool ?? true,
    isPublic: json['isPublic'] as bool ?? true,
    dataType: json['dataType'] as String,
    dataSlug: json['dataSlug'] as String,
    isMain: json['isMain'] as bool ?? false,
  );
}

Map<String, dynamic> _$RelatedCategoryModelToJson(
        RelatedCategoryModel instance) =>
    <String, dynamic>{
      'slug': instance.slug,
      'name': instance.name,
      'description': instance.description,
      'categorySlug': instance.categorySlug,
      'dataSlug': instance.dataSlug,
      'dataType': instance.dataType,
      'isMain': instance.isMain,
      'isPublic': instance.isPublic,
      'isActive': instance.isActive,
    };
