// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'related_feature_value_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RelatedFeatureValueModel _$RelatedFeatureValueModelFromJson(
    Map<String, dynamic> json) {
  return RelatedFeatureValueModel(
    dataType: json['dataType'] as String,
    slug: json['slug'] as String,
    featureLabel: json['featureLabel'] as String,
    featureValue: json['featureValue'] as String,
    isActive: json['isActive'] as bool ?? true,
    isPublic: json['isPublic'] as bool ?? true,
    order: json['order'] as String,
    level: json['level'] as int,
    rank: json['rank'] as int,
  )
    ..languageLabel = json['languageLabel'] as String
    ..language = json['language'] as String
    ..createdAt = json['createdAt'] as String ?? ''
    ..updatedAt = json['updatedAt'] as String ?? ''
    ..createdBy = json['createdBy'] == null
        ? null
        : BlamableModel.fromJson(json['createdBy'] as Map<String, dynamic>)
    ..updatedBy = json['updatedBy'] == null
        ? null
        : BlamableModel.fromJson(json['updatedBy'] as Map<String, dynamic>);
}

Map<String, dynamic> _$RelatedFeatureValueModelToJson(
        RelatedFeatureValueModel instance) =>
    <String, dynamic>{
      'languageLabel': instance.languageLabel,
      'language': instance.language,
      'dataType': instance.dataType,
      'slug': instance.slug,
      'featureValue': instance.featureValue,
      'featureLabel': instance.featureLabel,
      'isActive': instance.isActive,
      'isPublic': instance.isPublic,
      'order': instance.order,
      'level': instance.level,
      'rank': instance.rank,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
      'createdBy': instance.createdBy,
      'updatedBy': instance.updatedBy,
    };
