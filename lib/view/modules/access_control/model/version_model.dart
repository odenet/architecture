import 'package:fluttermvvmtemplate/core/constants/objects/object_map.dart';
import 'package:json_annotation/json_annotation.dart';

part 'version_model.g.dart';

@JsonSerializable()
class VersionModel {
  @JsonKey(defaultValue: -1)
  final int id;

  @JsonKey(defaultValue: '')
  final String name;

  @JsonKey(defaultValue: -1)
  final int parent;

  VersionModel({this.id, this.name, this.parent});

  Future<Map<String, dynamic>> isFeatureAllowedInThisVersion(
      {pageId, objectId}) async {
    List objectVersion = versionOne[pageId];

    if (objectVersion != null) {
      if (objectVersion.asMap().containsValue(objectId)) {
        return {'success': true, 'message': ''};
      }
    }

    return {
      'success': false,
      'message': 'This feature is not enabled on this version'
    };
  }

  @override
  static VersionModel fromJson(var json) {
    return _$VersionModelFromJson(json);
  }

  @override
  static Map<String, dynamic> toJson(VersionModel model) {
    return _$VersionModelToJson(model);
  }
}
