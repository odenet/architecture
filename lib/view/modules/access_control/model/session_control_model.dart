import 'package:json_annotation/json_annotation.dart';

part 'session_control_model.g.dart';

@JsonSerializable()
class SessionControlModel {
  @JsonKey(defaultValue: -1)
  int userId;

  @JsonKey(defaultValue: -1)
  int objectId;

  @JsonKey(defaultValue: true)
  bool isAllowed;

  SessionControlModel({this.userId, this.objectId, this.isAllowed = false});

  @override
  static SessionControlModel fromJson(var json) {
    return _$SessionControlModelFromJson(json);
  }

  @override
  static Map<String, dynamic> toJson(SessionControlModel model) {
    return _$SessionControlModelToJson(model);
  }
}
