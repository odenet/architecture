// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'session_control_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SessionControlModel _$SessionControlModelFromJson(Map<String, dynamic> json) {
  return SessionControlModel(
    userId: json['userId'] as int ?? -1,
    objectId: json['objectId'] as int ?? -1,
    isAllowed: json['isAllowed'] as bool ?? true,
  );
}

Map<String, dynamic> _$SessionControlModelToJson(
        SessionControlModel instance) =>
    <String, dynamic>{
      'userId': instance.userId,
      'objectId': instance.objectId,
      'isAllowed': instance.isAllowed,
    };
