import 'package:fluttermvvmtemplate/core/init/http_clients/http_client.dart';
import 'package:fluttermvvmtemplate/view/modules/access_control/model/session_control_model.dart';

class SessionControlService {
  HttpClient _httpClient = HttpClient();

  @override
  Future<Map<String, dynamic>> isThisSessionAllowed(
      SessionControlModel model) async {
    var response = await _httpClient.get("session");

    if (response.statusCode == 200) {
      return {'success': true, 'message': ''};
    }


    return {'success': true, 'message': 'Session is not permitted'};
  }
}
