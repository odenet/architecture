import 'package:fluttermvvmtemplate/view/modules/access_control/model/session_control_model.dart';
import 'package:fluttermvvmtemplate/view/modules/access_control/model/version_model.dart';
import 'package:fluttermvvmtemplate/view/modules/access_control/service/session_control_service.dart';

class AccessControlService {
  Future<Map> isAllowed(
      {SessionControlModel sessionControlModel, int pageId}) async {
    Map session =
        await SessionControlService().isThisSessionAllowed(sessionControlModel);

    Map version = await VersionModel().isFeatureAllowedInThisVersion(
        pageId: pageId, objectId: sessionControlModel.objectId);

    if (!session['success']) {
      return session;
    } else if (!version['success']) {
      return version;
    }



    return {'success': true, 'message': ''};
  }
}
