import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/base/view/base_widget.dart';
import 'package:fluttermvvmtemplate/core/constants/app/app_constants.dart';
import 'package:fluttermvvmtemplate/core/init/notifier/data_type.dart';
import 'package:fluttermvvmtemplate/view/authenticate/onboard/view/chart_view.dart';
import 'package:fluttermvvmtemplate/view/authenticate/onboard/view/on_board_view.dart';
import 'package:fluttermvvmtemplate/view/common/models/home_grid_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/view/category_list_view.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:fluttermvvmtemplate/view/modules/system/types/view/type_list_view.dart';
import 'package:fluttermvvmtemplate/view/modules/system/watchlist/view/watchlist_create_view.dart';
import 'package:fluttermvvmtemplate/view/modules/system/watchlist/viewmodel/watchlist_view_model.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'watchlist_list_view.dart';
import 'package:fluttermvvmtemplate/core/components/language_bar/select_language.dart';

class WatchlistHome extends StatefulWidget {
  @override
  _WatchlistHomeState createState() => _WatchlistHomeState();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'system.object.title'.tr();
  }
}

class _WatchlistHomeState extends State<WatchlistHome> {
  String searchText = '';
  TextEditingController _textEditingController;
  final focus = FocusNode();
  int yourActiveIndex = 0;

  @override
  void initState() {
    _textEditingController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _textEditingController.dispose();
    focus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    DataType.shared.dataType = ApplicationConstants.WATCHLIST;
    DataType.shared.dataTypeLabel = ApplicationConstants.WATCHLIST_LABEL;
    DataType.shared.module = ApplicationConstants.WATCHLIST;

    return BaseView<WatchlistViewModel>(
        dataType: DataType.shared.dataType,
        onModelReady: (model) {
          model.setContext(context);
        },
        viewModel: WatchlistViewModel(),
        onPageBuilder: (context, WatchlistViewModel viewModel) {
          return Scaffold(
            backgroundColor: Color(0xFFFAFAFB),
            appBar: AppBar(
              title: Text('${DataType.shared.dataTypeLabel}'),
              actions: [
                IconButton(
                    icon: Icon(Icons.search),
                    onPressed: () {
                      pushPage(page: WatchlistListView());
                    }),
                IconButton(
                    icon: Icon(Icons.add_circle_rounded),
                    onPressed: () {
                      pushPage(page: WatchlistCreateView());
                    }),
                SelectLanguage(viewModel: viewModel)
              ],
            ),
            body: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                      height: MediaQuery.of(context).size.height * 0.35,
                      child: PageView(
                        onPageChanged: (index) {
                          setState(() {
                            yourActiveIndex = index;
                          });
                        },
                        children: [OnboardChartView(), OnboardChartView()],
                      )),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: AnimatedSmoothIndicator(
                      activeIndex: yourActiveIndex,
                      count: 2,
                      effect: WormEffect(dotHeight: 10, dotWidth: 10),
                    ),
                  ),
                  GridView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: homeGridList.length,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          childAspectRatio: 1.35, crossAxisCount: 2),
                      itemBuilder: (context, index) {
                        var data = homeGridList[index];
                        return GestureDetector(
                          onTap: () {
                            pushAndRemoved(page: homeGridList[index].page);
                          },
                          child: Card(
                            elevation: 3,
                            color: Color(0xFFFFFFFF),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                data.image,
                                SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  data.title,
                                  style: TextStyle(
                                      color: data.color,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14),
                                ),
                              ],
                            ),
                          ),
                        );
                      })
                ],
              ),
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: () {},
              child: Icon(Icons.monetization_on),
            ),
            floatingActionButtonLocation:
                FloatingActionButtonLocation.centerDocked,
            bottomNavigationBar: CustomBottomNavigationBar(
              onPage: (currentPageIndex) {},
            ),
          );
        });
  }

  void pushAndRemoved({Widget page}) {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (_) => page), (route) => false);
  }

  void pushPage({Widget page}) {
    Navigator.of(context).push(MaterialPageRoute(builder: (_) => page));
  }

  Future showLinkOptions(BuildContext context) {
    return showModalBottomSheet(
        context: context,
        builder: (_) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              ListTile(
                onTap: () {
                  pushAndRemoved(
                      page:
                          WatchlistListView()); // Tıklanan nesnesnin id sine göre listesi.
                },
                leading: Icon(
                  Icons.list,
                  color: Colors.orange,
                ),
                title: Text(
                  'Show Object List',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              ListTile(
                onTap: () {
                  // Tıklanan objenin idsini güncelleme.
                },
                leading: Icon(
                  Icons.edit,
                  color: Colors.blue,
                ),
                title: Text(
                  'Update Type',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              )
            ],
          );
        });
  }
}

extension DataSource on _WatchlistHomeState {
  List<HomeGridModel> get homeGridList => [
        HomeGridModel(
          title: 'Objects List',
          color: Colors.blue,
          image: Image.asset('asset/image/object_home/dashboard.png'),
          page: WatchlistListView(),
        ),
        HomeGridModel(
          title: 'Objects Categories',
          color: Colors.purpleAccent,
          image: Image.asset('asset/image/object_home/category.png'),
          page: CategoryListView(),
        ),
        HomeGridModel(
          title: 'Object Types',
          color: Color(0xFF8BC34A),
          image: Image.asset('asset/image/object_home/types.png'),
          page: TypeView(),
        ),
      ];
}
