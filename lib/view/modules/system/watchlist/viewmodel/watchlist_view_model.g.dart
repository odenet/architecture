// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'watchlist_view_model.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$WatchlistViewModel on _WatchlistViewModelBase, Store {
  Computed<bool> _$canSubmitComputed;

  @override
  bool get canSubmit =>
      (_$canSubmitComputed ??= Computed<bool>(() => super.canSubmit,
              name: '_WatchlistViewModelBase.canSubmit'))
          .value;
  Computed<RelatedCategoryModel> _$mainCategoryComputed;

  @override
  RelatedCategoryModel get mainCategory => (_$mainCategoryComputed ??=
          Computed<RelatedCategoryModel>(() => super.mainCategory,
              name: '_WatchlistViewModelBase.mainCategory'))
      .value;

  final _$languagesDataAtom =
      Atom(name: '_WatchlistViewModelBase.languagesData');

  @override
  Map<String, Object> get languagesData {
    _$languagesDataAtom.reportRead();
    return super.languagesData;
  }

  @override
  set languagesData(Map<String, Object> value) {
    _$languagesDataAtom.reportWrite(value, super.languagesData, () {
      super.languagesData = value;
    });
  }

  final _$filteredListItemsAtom =
      Atom(name: '_WatchlistViewModelBase.filteredListItems');

  @override
  List<WatchlistModel> get filteredListItems {
    _$filteredListItemsAtom.reportRead();
    return super.filteredListItems;
  }

  @override
  set filteredListItems(List<WatchlistModel> value) {
    _$filteredListItemsAtom.reportWrite(value, super.filteredListItems, () {
      super.filteredListItems = value;
    });
  }

  final _$currentPageAtom = Atom(name: '_WatchlistViewModelBase.currentPage');

  @override
  int get currentPage {
    _$currentPageAtom.reportRead();
    return super.currentPage;
  }

  @override
  set currentPage(int value) {
    _$currentPageAtom.reportWrite(value, super.currentPage, () {
      super.currentPage = value;
    });
  }

  final _$isSubmittedAtom = Atom(name: '_WatchlistViewModelBase.isSubmitted');

  @override
  bool get isSubmitted {
    _$isSubmittedAtom.reportRead();
    return super.isSubmitted;
  }

  @override
  set isSubmitted(bool value) {
    _$isSubmittedAtom.reportWrite(value, super.isSubmitted, () {
      super.isSubmitted = value;
    });
  }

  final _$watchListAtom = Atom(name: '_WatchlistViewModelBase.watchList');

  @override
  ObservableList<WatchlistModel> get watchList {
    _$watchListAtom.reportRead();
    return super.watchList;
  }

  @override
  set watchList(ObservableList<WatchlistModel> value) {
    _$watchListAtom.reportWrite(value, super.watchList, () {
      super.watchList = value;
    });
  }

  final _$featuresListAtom = Atom(name: '_WatchlistViewModelBase.featuresList');

  @override
  ObservableList<dynamic> get featuresList {
    _$featuresListAtom.reportRead();
    return super.featuresList;
  }

  @override
  set featuresList(ObservableList<dynamic> value) {
    _$featuresListAtom.reportWrite(value, super.featuresList, () {
      super.featuresList = value;
    });
  }

  final _$relatedFeatureValuesAtom =
      Atom(name: '_WatchlistViewModelBase.relatedFeatureValues');

  @override
  ObservableList<dynamic> get relatedFeatureValues {
    _$relatedFeatureValuesAtom.reportRead();
    return super.relatedFeatureValues;
  }

  @override
  set relatedFeatureValues(ObservableList<dynamic> value) {
    _$relatedFeatureValuesAtom.reportWrite(value, super.relatedFeatureValues,
        () {
      super.relatedFeatureValues = value;
    });
  }

  final _$isLoadingAtom = Atom(name: '_WatchlistViewModelBase.isLoading');

  @override
  bool get isLoading {
    _$isLoadingAtom.reportRead();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.reportWrite(value, super.isLoading, () {
      super.isLoading = value;
    });
  }

  final _$modelAtom = Atom(name: '_WatchlistViewModelBase.model');

  @override
  WatchlistModel get model {
    _$modelAtom.reportRead();
    return super.model;
  }

  @override
  set model(WatchlistModel value) {
    _$modelAtom.reportWrite(value, super.model, () {
      super.model = value;
    });
  }

  final _$languageModelAtom =
      Atom(name: '_WatchlistViewModelBase.languageModel');

  @override
  LanguageInfoModel get languageModel {
    _$languageModelAtom.reportRead();
    return super.languageModel;
  }

  @override
  set languageModel(LanguageInfoModel value) {
    _$languageModelAtom.reportWrite(value, super.languageModel, () {
      super.languageModel = value;
    });
  }

  final _$dirtyModelAtom = Atom(name: '_WatchlistViewModelBase.dirtyModel');

  @override
  WatchlistModel get dirtyModel {
    _$dirtyModelAtom.reportRead();
    return super.dirtyModel;
  }

  @override
  set dirtyModel(WatchlistModel value) {
    _$dirtyModelAtom.reportWrite(value, super.dirtyModel, () {
      super.dirtyModel = value;
    });
  }

  final _$dirtyLanguageModelAtom =
      Atom(name: '_WatchlistViewModelBase.dirtyLanguageModel');

  @override
  LanguageInfoModel get dirtyLanguageModel {
    _$dirtyLanguageModelAtom.reportRead();
    return super.dirtyLanguageModel;
  }

  @override
  set dirtyLanguageModel(LanguageInfoModel value) {
    _$dirtyLanguageModelAtom.reportWrite(value, super.dirtyLanguageModel, () {
      super.dirtyLanguageModel = value;
    });
  }

  final _$paginationModelAtom =
      Atom(name: '_WatchlistViewModelBase.paginationModel');

  @override
  PaginationModel get paginationModel {
    _$paginationModelAtom.reportRead();
    return super.paginationModel;
  }

  @override
  set paginationModel(PaginationModel value) {
    _$paginationModelAtom.reportWrite(value, super.paginationModel, () {
      super.paginationModel = value;
    });
  }

  final _$searchTextAtom = Atom(name: '_WatchlistViewModelBase.searchText');

  @override
  String get searchText {
    _$searchTextAtom.reportRead();
    return super.searchText;
  }

  @override
  set searchText(String value) {
    _$searchTextAtom.reportWrite(value, super.searchText, () {
      super.searchText = value;
    });
  }

  final _$searchModelAtom = Atom(name: '_WatchlistViewModelBase.searchModel');

  @override
  SearchModel get searchModel {
    _$searchModelAtom.reportRead();
    return super.searchModel;
  }

  @override
  set searchModel(SearchModel value) {
    _$searchModelAtom.reportWrite(value, super.searchModel, () {
      super.searchModel = value;
    });
  }

  final _$tabIndexAtom = Atom(name: '_WatchlistViewModelBase.tabIndex');

  @override
  int get tabIndex {
    _$tabIndexAtom.reportRead();
    return super.tabIndex;
  }

  @override
  set tabIndex(int value) {
    _$tabIndexAtom.reportWrite(value, super.tabIndex, () {
      super.tabIndex = value;
    });
  }

  final _$languageFutureAtom =
      Atom(name: '_WatchlistViewModelBase.languageFuture');

  @override
  ObservableFuture<dynamic> get languageFuture {
    _$languageFutureAtom.reportRead();
    return super.languageFuture;
  }

  @override
  set languageFuture(ObservableFuture<dynamic> value) {
    _$languageFutureAtom.reportWrite(value, super.languageFuture, () {
      super.languageFuture = value;
    });
  }

  final _$searchItemsAsyncAction =
      AsyncAction('_WatchlistViewModelBase.searchItems');

  @override
  Future<ResponseModel<dynamic>> searchItems({dynamic page = 1}) {
    return _$searchItemsAsyncAction.run(() => super.searchItems(page: page));
  }

  final _$getFeaturesAsyncAction =
      AsyncAction('_WatchlistViewModelBase.getFeatures');

  @override
  Future<void> getFeatures(dynamic lang) {
    return _$getFeaturesAsyncAction.run(() => super.getFeatures(lang));
  }

  final _$getRelatedFeatureValuesAsyncAction =
      AsyncAction('_WatchlistViewModelBase.getRelatedFeatureValues');

  @override
  Future<void> getRelatedFeatureValues() {
    return _$getRelatedFeatureValuesAsyncAction
        .run(() => super.getRelatedFeatureValues());
  }

  final _$validateNameAsyncAction =
      AsyncAction('_WatchlistViewModelBase.validateName');

  @override
  Future<dynamic> validateName(String value) {
    return _$validateNameAsyncAction.run(() => super.validateName(value));
  }

  final _$validateDescriptionAsyncAction =
      AsyncAction('_WatchlistViewModelBase.validateDescription');

  @override
  Future<dynamic> validateDescription(String value) {
    return _$validateDescriptionAsyncAction
        .run(() => super.validateDescription(value));
  }

  final _$validateTypeAsyncAction =
      AsyncAction('_WatchlistViewModelBase.validateType');

  @override
  Future<dynamic> validateType(String value) {
    return _$validateTypeAsyncAction.run(() => super.validateType(value));
  }

  final _$updateAndLoadLanguageInfoAsyncAction =
      AsyncAction('_WatchlistViewModelBase.updateAndLoadLanguageInfo');

  @override
  Future<ResponseModel<dynamic>> updateAndLoadLanguageInfo() {
    return _$updateAndLoadLanguageInfoAsyncAction
        .run(() => super.updateAndLoadLanguageInfo());
  }

  final _$createAndLoadLanguageInfoAsyncAction =
      AsyncAction('_WatchlistViewModelBase.createAndLoadLanguageInfo');

  @override
  Future<ResponseModel<dynamic>> createAndLoadLanguageInfo() {
    return _$createAndLoadLanguageInfoAsyncAction
        .run(() => super.createAndLoadLanguageInfo());
  }

  final _$updateLanguageInfoAsyncAction =
      AsyncAction('_WatchlistViewModelBase.updateLanguageInfo');

  @override
  Future<ResponseModel<dynamic>> updateLanguageInfo() {
    return _$updateLanguageInfoAsyncAction
        .run(() => super.updateLanguageInfo());
  }

  final _$createLanguageInfoAsyncAction =
      AsyncAction('_WatchlistViewModelBase.createLanguageInfo');

  @override
  Future<ResponseModel<dynamic>> createLanguageInfo() {
    return _$createLanguageInfoAsyncAction
        .run(() => super.createLanguageInfo());
  }

  final _$_WatchlistViewModelBaseActionController =
      ActionController(name: '_WatchlistViewModelBase');

  @override
  void setSearchText(String value) {
    final _$actionInfo = _$_WatchlistViewModelBaseActionController.startAction(
        name: '_WatchlistViewModelBase.setSearchText');
    try {
      return super.setSearchText(value);
    } finally {
      _$_WatchlistViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setLanguageModel(AvailableLanguagesModel value, int index) {
    final _$actionInfo = _$_WatchlistViewModelBaseActionController.startAction(
        name: '_WatchlistViewModelBase.setLanguageModel');
    try {
      return super.setLanguageModel(value, index);
    } finally {
      _$_WatchlistViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  Future<void> detail() {
    final _$actionInfo = _$_WatchlistViewModelBaseActionController.startAction(
        name: '_WatchlistViewModelBase.detail');
    try {
      return super.detail();
    } finally {
      _$_WatchlistViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setSubmitStatus() {
    final _$actionInfo = _$_WatchlistViewModelBaseActionController.startAction(
        name: '_WatchlistViewModelBase.setSubmitStatus');
    try {
      return super.setSubmitStatus();
    } finally {
      _$_WatchlistViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
languagesData: ${languagesData},
filteredListItems: ${filteredListItems},
currentPage: ${currentPage},
isSubmitted: ${isSubmitted},
watchList: ${watchList},
featuresList: ${featuresList},
relatedFeatureValues: ${relatedFeatureValues},
isLoading: ${isLoading},
model: ${model},
languageModel: ${languageModel},
dirtyModel: ${dirtyModel},
dirtyLanguageModel: ${dirtyLanguageModel},
paginationModel: ${paginationModel},
searchText: ${searchText},
searchModel: ${searchModel},
tabIndex: ${tabIndex},
languageFuture: ${languageFuture},
canSubmit: ${canSubmit},
mainCategory: ${mainCategory}
    ''';
  }
}

mixin _$FormErrorState on _FormErrorState, Store {
  Computed<bool> _$hasErrorsComputed;

  @override
  bool get hasErrors =>
      (_$hasErrorsComputed ??= Computed<bool>(() => super.hasErrors,
              name: '_FormErrorState.hasErrors'))
          .value;

  final _$nameAtom = Atom(name: '_FormErrorState.name');

  @override
  String get name {
    _$nameAtom.reportRead();
    return super.name;
  }

  @override
  set name(String value) {
    _$nameAtom.reportWrite(value, super.name, () {
      super.name = value;
    });
  }

  final _$descriptionAtom = Atom(name: '_FormErrorState.description');

  @override
  String get description {
    _$descriptionAtom.reportRead();
    return super.description;
  }

  @override
  set description(String value) {
    _$descriptionAtom.reportWrite(value, super.description, () {
      super.description = value;
    });
  }

  final _$typeAtom = Atom(name: '_FormErrorState.type');

  @override
  String get type {
    _$typeAtom.reportRead();
    return super.type;
  }

  @override
  set type(String value) {
    _$typeAtom.reportWrite(value, super.type, () {
      super.type = value;
    });
  }

  @override
  String toString() {
    return '''
name: ${name},
description: ${description},
type: ${type},
hasErrors: ${hasErrors}
    ''';
  }
}
