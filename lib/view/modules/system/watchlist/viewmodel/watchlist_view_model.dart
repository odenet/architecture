import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/base/model/base_view_model.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/response_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/network_error_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/pagination_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/related_category_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/related_feature_value_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/search_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/model/language_info_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/watchlist/model/watchlist_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/watchlist/service/watchlist_control_service.dart';
import 'package:mobx/mobx.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/model/available_languages_model.dart';
import 'package:validators/validators.dart';
import 'package:fluttermvvmtemplate/view/common/viewmodel/common_view_model.dart';

part 'watchlist_view_model.g.dart';

class WatchlistViewModel = _WatchlistViewModelBase with _$WatchlistViewModel;

abstract class _WatchlistViewModelBase extends CommonViewModel
    with Store, BaseViewModel {
  void setContext(BuildContext context) => this.context = context;

  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();
  WatchlistControlService feedService;

  FormErrorState error = FormErrorState();

  @observable
  var languagesData = {
    'default': 'TR',
    'translates': [
      {
        'language': 'TR',
        'name': 'Object Ismi',
        'description ': 'Açıklama',
        'id': 0
      },
      {
        'language': 'EN',
        'name': 'Object Name',
        'description': 'Description Text',
        'id': 1
      }
    ]
  };

  @observable
  List<WatchlistModel> filteredListItems = [];

  @observable
  int currentPage = 1;

  @observable
  bool isSubmitted = false;

  @observable
  ObservableList<WatchlistModel> watchList;

  @observable
  ObservableList<dynamic> featuresList;

  @observable
  ObservableList<dynamic> relatedFeatureValues;

  @observable
  bool isLoading = true;

  @computed
  bool get canSubmit => !error.hasErrors;

  @observable
  WatchlistModel model;

  @observable
  LanguageInfoModel languageModel;

  @observable
  WatchlistModel dirtyModel = WatchlistModel();

  @observable
  LanguageInfoModel dirtyLanguageModel;

  @observable
  PaginationModel paginationModel;

  @observable
  String searchText = '';

  @observable
  SearchModel searchModel = SearchModel();

  @observable
  int tabIndex = 0;

  @observable
  ObservableFuture languageFuture;

  void init() async {
    feedService = WatchlistControlService();
  }

  @computed
  RelatedCategoryModel get mainCategory {
    var main;

    if (model.relatedCategories.isNotEmpty) {
      main = model.relatedCategories.firstWhere((e) => e.isMain == true);
    }

    return main;
  }

  @action
  void setSearchText(String value) {
    searchText = value;
  }

  @action
  Future<ResponseModel> searchItems({page = 1}) async {
    var response = ResponseModel(success: false, message: " ");
    try {
      isSearchActive = true;
      future =
          ObservableFuture(feedService.searchList(searchModel, page: page));
      response = await future as ResponseModel;
      paginationModel = response.pagination;

      var responseList = List<WatchlistModel>.from(response.data);
      watchList = responseList.asObservable();
      filteredListItems = watchList;
    } catch (e) {
      networkError = NetworkErrorModel(errorText: response.message);
    }

    return response;
  }

  int calculateTabIndex() {
    dynamic defaultLang =
        model.systemInfo.firstWhere((element) => element.isRef == true);

    tabIndex = availableLanguages
        .indexWhere((element) => element.langCode == defaultLang.language);
  }

  @action
  void setLanguageModel(AvailableLanguagesModel value, int index) {
    try {
      tabIndex = index;
      languageModel = model.systemInfo
          .firstWhere((element) => element.language == value.langCode);
    } catch (e) {
      languageModel =
          LanguageInfoModel(language: value.langCode, dataSlug: model.slug);
    }

    dirtyLanguageModel = LanguageInfoModel.clone(languageModel);
  }

  @action
  Future<void> detail() {
    future = ObservableFuture(
        Future.wait([view(), getAvailableLanguages()]).then((value) {
      calculateTabIndex();
    }));
  }

  @action
  Future<void> getFeatures(lang) async {
    future =
        ObservableFuture(feedService.getFeatures(model, mainCategory, lang));
    var response = await future as ResponseModel;

    featuresList = ObservableList.of(response.data);
  }

  @action
  Future<void> getRelatedFeatureValues() async {
    future = ObservableFuture(feedService.getRelatedFeatureValues(model));
    var response = await future as ResponseModel;

    relatedFeatureValues = ObservableList.of(response.data);
  }

  @action
  void setSubmitStatus() {
    isSubmitted = true;
  }

  Future<void> view() async {
    var response = ResponseModel();
    try {
      future = ObservableFuture(feedService.fetchItem(model.slug));
      response = await future;
      model = response.data;
      dirtyModel = WatchlistModel.clone(model);
    } catch (e) {
      networkError = NetworkErrorModel(errorText: response.message);
    }
  }

  Future<ResponseModel> delete(WatchlistModel deletedModel) async {
    var response = ResponseModel();
    try {
      response = await feedService.deleteItem(deletedModel.toJson());
    } catch (e) {
      networkError = NetworkErrorModel(errorText: response.message);
    }

    return response;
  }

  Future<ResponseModel> deleteRelatedFeature(
      RelatedFeatureValueModel model) async {
    var response = ResponseModel();
    try {
      response = await feedService.deleteItem(model.toJson());
    } catch (e) {
      networkError = NetworkErrorModel(errorText: response.message);
    }

    return response;
  }

  Future<ResponseModel> deleteRelatedCategory(List data) async {
    var response = ResponseModel();
    try {
      response = await feedService.deleteRelatedCategory(data);
    } catch (e) {
      networkError = NetworkErrorModel(errorText: response.message);
    }

    return response;
  }

  Future<ResponseModel> update() async {
    var response = ResponseModel();
    try {
      future = ObservableFuture(feedService.updateItem(dirtyModel.toJson()));

      response = await future;
    } catch (e) {
      networkError = NetworkErrorModel(errorText: response.message);
    }

    return response;
  }

  Future<ResponseModel> updateWatchlistCategory() async {
    var response = ResponseModel();
    try {
      var data = dirtyModel.relatedCategories.map((e) {
        var data = RelatedCategoryModel.toJson(model: e);
        data['dataSlug'] = model.slug;

        return data;
      }).toList();

      response = await feedService.updateWatchlistCategory(data);
    } catch (e) {
      networkError = NetworkErrorModel(errorText: response.message);
    }

    return response;
  }

  Future<ResponseModel> createWatchlistCategory(List data) async {
    var response = ResponseModel();
    try {
      response = await feedService.createWatchlistCategory(data);
    } catch (e) {
      networkError = NetworkErrorModel(errorText: response.message);
    }

    return response;
  }

  Future<ResponseModel> updateWatchlistFeature() async {
    var response = ResponseModel();
    try {
      response = await feedService
          .updateWatchlistFeature(dirtyModel.toJson()['relatedFeatures']);
    } catch (e) {
      networkError = NetworkErrorModel(errorText: response.message);
    }

    return response;
  }

  Future<ResponseModel> updateWatchlistFeatureValue(
      RelatedFeatureValueModel data) async {
    var response = ResponseModel();
    try {
      response = await feedService.updateWatchlistFeatureValue(data.toJson());
    } catch (e) {
      networkError = NetworkErrorModel(errorText: response.message);
    }

    return response;
  }

  Future<ResponseModel> createWatchlistFeature(List data) async {
    var response = ResponseModel();
    try {
      var list = [];

      data.forEach((element) {
        list.add(element.toJson());
      });

      response = await feedService.createWatchlistFeature(list);
      return response;
    } catch (e) {
      networkError = NetworkErrorModel(errorText: response.message);
    }

    return response;
  }

  Future<ResponseModel> create() async {
    var response = ResponseModel();
    try {
      response = await feedService.createItem(dirtyModel.toJson());

      watchList.add(response.data);
    } catch (e) {
      networkError = NetworkErrorModel(errorText: response.message);
    }

    return response;
  }

  Future fetchItems({int page = 1}) async {
    var response = ResponseModel();
    try {
      isSearchActive = false;
      future = ObservableFuture(feedService.fetchList(page: page));
      response = await future;
      var responseList = List<WatchlistModel>.from(response.data);
      watchList = responseList.asObservable();
      filteredListItems = watchList;
      paginationModel = response.pagination;
    } catch (e) {
      networkError = NetworkErrorModel(errorText: response.message);
    }

    return response;
  }

  @action
  Future validateName(String value) async {
    if (isNull(value) || value.isEmpty) {
      error.name = 'Name cannot be blank';
      return;
    } else {
      error.name = null;
    }
  }

  @action
  Future validateDescription(String value) async {
    if (isNull(value) || value.isEmpty) {
      error.description = 'Description cannot be blank';
      return;
    } else {
      error.description = null;
    }
  }

  @action
  Future validateType(String value) async {
    if (value == null) {
      error.type = 'Type cannot be blank';
      return;
    } else {
      error.type = null;
    }
  }

  @action
  Future<ResponseModel> updateAndLoadLanguageInfo() async {
    var responseModel = ResponseModel(success: false, message: " ");
    future = ObservableFuture(Future.wait([updateLanguageInfo(), view()]));
    await future.then((value) => responseModel = value[0]);

    return responseModel;
  }

  @action
  Future<ResponseModel> createAndLoadLanguageInfo() async {
    var responseModel = ResponseModel();
    future = ObservableFuture(Future.wait([createLanguageInfo(), view()]));
    await future.then((value) => responseModel = value[0]);

    return responseModel;
  }

  @action
  Future<ResponseModel> updateLanguageInfo() async {
    var response = await feedService
        .updateLangInfo(LanguageInfoModel.toJson(model: dirtyLanguageModel));

    return response;
  }

  @action
  Future<ResponseModel> createLanguageInfo() async {
    var response = await feedService
        .createLangInfo(LanguageInfoModel.toJson(model: dirtyLanguageModel));

    return response;
  }

  Map getLanguageInfoWithDataType(languageModel) {
    Map langInfo = LanguageInfoModel.toJson(model: languageModel);
    langInfo['dataType'] = model.dataType;

    return langInfo;
  }
}

class FormErrorState = _FormErrorState with _$FormErrorState;

abstract class _FormErrorState with Store {
  @observable
  String name;

  @observable
  String description;

  @observable
  String type;

  @computed
  bool get hasErrors => !isNull(name) || !isNull(description) || !isNull(type);
}
