import 'package:fluttermvvmtemplate/core/base/service/base_service.dart';
import 'package:fluttermvvmtemplate/core/base/service/language_info_service.dart';
import 'package:fluttermvvmtemplate/core/constants/app/app_constants.dart';
import 'package:fluttermvvmtemplate/core/constants/app/endpoints.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/request_model.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/response_model.dart';
import 'package:fluttermvvmtemplate/core/init/lang/language_manager.dart';
import 'package:fluttermvvmtemplate/core/init/notifier/data_type.dart';
import 'package:fluttermvvmtemplate/view/common/models/related_category_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/related_feature_value_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/search_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/model/feature_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/watchlist/model/watchlist_model.dart';

class WatchlistControlService {
  BaseService service = BaseService(WatchlistModel());
  LanguageInfoService langService = LanguageInfoService();

  @override
  Future<ResponseModel> fetchList({int page = 1}) async {
    var requestModel = RequestModel(Endpoints.WATCHLIST,
        params: {'page': page, 'perPage': ApplicationConstants.PER_PAGE});
    return service.fetchList(requestModel);
  }

  @override
  Future<ResponseModel> fetchItem(String slug) async {
    var requestModel = RequestModel('${Endpoints.WATCHLIST + slug}');

    return service.fetchItem(requestModel);
  }

  Future<ResponseModel> deleteItem(Map body) async {
    body['dataType'] = DataType.shared.dataType;

    List allowedFields = [
      'slug',
      'dataType',
      'name',
      'description',
      'isActive',
      'isPublic',
      'type'
    ];

    body.removeWhere((key, value) => !allowedFields.contains(key));

    var requestBody = {
      'data': [body]
    };
    var requestModel =
        RequestModel('${Endpoints.WATCHLIST}', body: requestBody);
    return service.deleteItem(requestModel);
  }

  Future<ResponseModel> deleteRelatedCategory(List data) async {
    String dataType = DataType.shared.dataType;
    var body = data.map((e) => {'slug': e, 'dataType': dataType}).toList();

    var requestBody = {'data': body};
    var requestModel =
        RequestModel('${Endpoints.RELATED_CATEGORY}', body: requestBody);
    return service.deleteItem(requestModel);
  }

  Future<ResponseModel> searchList(SearchModel searchModel,
      {int page = 1}) async {
    var requestModel =
        RequestModel('${Endpoints.WATCHLIST + Endpoints.SEARCH}', body: {
      'name': searchModel.name,
      'language': searchModel.language,
      'page': page,
      'isActive': searchModel.isActive,
      'isPublic': searchModel.isPublic,
      'dataType': DataType.shared.dataType
    });

    return service.searchList(requestModel);
  }

  @override
  Future<ResponseModel> createItem(Map body) async {
    body['dataType'] = DataType.shared.dataType;
    var requestBody = {
      'data': [body]
    };
    var requestModel =
        RequestModel('${Endpoints.WATCHLIST}', body: requestBody);
    return service.createItem(requestModel);
  }

  @override
  Future<ResponseModel> updateItem(Map body) async {
    List allowedFields = [
      'slug',
      'dataType',
      'type',
      'name',
      'description',
      'isActive',
      'isPublic',
    ];

    body.removeWhere((key, value) => !allowedFields.contains(key));

    body['dataType'] = DataType.shared.dataType;
    var requestBody = {
      'data': [body]
    };

    var requestModel =
        RequestModel('${Endpoints.WATCHLIST}', body: requestBody);
    return service.updateItem(requestModel);
  }

  // RELATED CATEGORIES

  Future<ResponseModel> updateWatchlistCategory(List body) async {
    var requestBody = {'data': body};
    var requestModel =
        RequestModel('${Endpoints.RELATED_CATEGORY}', body: requestBody);
    return service.updateItem(requestModel);
  }

  Future<ResponseModel> createWatchlistCategory(List body) async {
    var requestBody = {'data': body};
    var requestModel =
        RequestModel('${Endpoints.RELATED_CATEGORY}', body: requestBody);
    return service.createItem(requestModel);
  }

  // RELATED FEATURES
  Future<ResponseModel> updateWatchlistFeature(List body) async {
    var requestBody = {'data': body};
    var requestModel =
        RequestModel('${Endpoints.RELATED_FEATURES}', body: requestBody);
    return service.updateItem(requestModel);
  }

  // update of feature values will be one by one
  Future<ResponseModel> updateWatchlistFeatureValue(
      Map<String, dynamic> body) async {
    body['dataType'] = DataType.shared.dataType;
    var requestBody = {
      'data': [body]
    };
    var requestModel =
        RequestModel('${Endpoints.RELATED_FEATURES}', body: requestBody);
    return service.updateItem(requestModel);
  }

  Future<ResponseModel> createWatchlistFeature(List body) async {
    var requestBody = {'data': body};
    var requestModel =
        RequestModel('${Endpoints.RELATED_FEATURES}', body: requestBody);
    return service.createItem(requestModel);
  }

  // START LANGUAGE INFO SERVICE

  Future<ResponseModel> createLangInfo(Map body) async {
    body['dataType'] = ApplicationConstants.WATCHLIST;
    body['validationDataType'] = ApplicationConstants.WATCHLIST;

    var requestBody = {
      'data': [body]
    };
    var requestModel =
        RequestModel('${Endpoints.SYSTEM_INFO}', body: requestBody);
    return langService.create(requestModel);
  }

  Future<ResponseModel> updateLangInfo(Map body) async {
    body['dataType'] = ApplicationConstants.WATCHLIST;
    body['validationDataType'] = ApplicationConstants.WATCHLIST;

    var requestBody = {
      'data': [body]
    };

    var requestModel =
        RequestModel('${Endpoints.SYSTEM_INFO}', body: requestBody);
    return langService.update(requestModel);
  }

  Future<ResponseModel> getFeatures(WatchlistModel model,
      RelatedCategoryModel mainCategory, String language) {
    BaseService featureService = BaseService(FeatureModel());

    var map = {
      'categorySlug': mainCategory.categorySlug,
      'dataSlug': model.slug,
      'language': language
    };

    var requestModel =
        RequestModel('${Endpoints.CATEGORY_RELATED_FEATURES}', params: map);
    return featureService.fetchList(requestModel);
  }

  Future<ResponseModel> getRelatedFeatureValues(WatchlistModel model) {
    BaseService featureService = BaseService(
        RelatedFeatureValueModel(dataType: DataType.shared.dataType));

    var requestParams = {
      'dataSlug': model.slug,
    };

    var requestModel =
        RequestModel('${Endpoints.RELATED_FEATURES}', params: requestParams);

    return featureService.fetchList(requestModel);
  }
}
