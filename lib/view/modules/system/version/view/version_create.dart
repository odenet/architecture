import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/base/view/base_widget.dart';
import 'package:fluttermvvmtemplate/core/components/form/base_form.dart';
import 'package:fluttermvvmtemplate/core/helpers/alert_helper.dart';
import 'package:fluttermvvmtemplate/view/modules/system/version/viewmodel/version_viewmodel.dart';

class VersionCreateView extends StatefulWidget {
  final VoidCallback onUpdate;

  const VersionCreateView({Key key,@required this.onUpdate}) : super(key: key);

  @override
  _VersionCreateViewState createState() => _VersionCreateViewState();
}

class _VersionCreateViewState extends State<VersionCreateView> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController nameController;
  TextEditingController descriptionController;
  TextEditingController typeController;

  bool isActive = true;
  bool isPublic = false;

  @override
  Widget build(BuildContext context) {
    return BaseView<VersionViewModel>(
        pageId: 25,
        objectId: 27,
        viewModel: VersionViewModel(),
        onModelReady: (vm) {
          vm.init();
          vm.setContext(context);
          nameController = TextEditingController();
          descriptionController = TextEditingController();
          typeController = TextEditingController();
        },
        onDispose: () {
          nameController.dispose();
          descriptionController.dispose();
          typeController.dispose();
        },
        onPageBuilder: (context, viewModel) {
          return SafeArea(
            child: Scaffold(
                resizeToAvoidBottomInset: false,
                appBar: AppBar(
                  title: Text('Add Version'),
                ),
                body: buildForm(viewModel, context)),
          );
        });
  }

  Form buildForm(VersionViewModel vm, BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              controller: nameController,
              decoration: InputDecoration(
                                labelText: 'Name',

                  hintText: 'Name', border: OutlineInputBorder()),
              onChanged: (str) {
                vm.dirtyModel.name = str;
              },
              validator: (str) {
                if (str.length > 10 || str.length < 3) {
                  return 'max 10 min 3';
                } else {
                  return null;
                }
              },
            ),
          ),
          SizedBox(
            height: 16,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              controller: descriptionController,
              decoration: InputDecoration(
                labelText: 'Description',
                  hintText: 'Description', border: OutlineInputBorder()),
              onChanged: (str) {
                vm.dirtyModel.description = str;
              },
              validator: (str) {
                if (str.length > 10 || str.length < 3) {
                  return 'max 10 min 3';
                } else {
                  return null;
                }
              },
            ),
          ),
                    SizedBox(
            height: 16,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              controller: typeController,
              decoration: InputDecoration(
                labelText: 'Type',
                  hintText: 'Type', border: OutlineInputBorder()),
              onChanged: (str) {
                vm.dirtyModel.type = str;
              },
              validator: (str) {
                if (str.length > 10 || str.length < 3) {
                  return 'max 10 min 3';
                } else {
                  return null;
                }
              },
            ),
          ),
          SizedBox(
            height: 16,
          ),
          Row(
            children: [
              Expanded(
                child: CheckboxListTile(
                  value: isActive,
                  onChanged: (value) {
                    setState(() {
                      isActive = value;
                    });
                  },
                  title: Text('Active'),
                ),
              ),
              Expanded(
                child: CheckboxListTile(
                  value: isPublic,
                  onChanged: (value) {
                    setState(() {
                      isPublic = value;
                    });
                    vm.dirtyModel.isPublic = value;
                  },
                  title: Text('Public'),
                ),
              ),
            ],
          ),
          CustomNormalButton(
              textColor: Colors.white,
              buttonColor: Colors.green,
              text: '  Save  ',
              onPressed: () async {
                if (_formKey.currentState.validate()) {
                  AlertHelper.shared.showProgressDialog(context);
                  var response = await vm.create();
                  await AlertHelper.shared.startProgress(context, response);

                  Navigator.pop(context); // remove alert

                  if (response.success) {
                    widget.onUpdate();
                    Navigator.pop(context); // remove loading
                    Navigator.pop(context); // remove create form
                  }
                }
              }),
        ],
      ),
    );
  }
}
