import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:fluttermvvmtemplate/core/base/view/base_widget.dart';
import 'package:fluttermvvmtemplate/core/components/alerts/custom_form_alert.dart';
import 'package:fluttermvvmtemplate/core/components/errors/error_page.dart';
import 'package:fluttermvvmtemplate/core/constants/enums/store_state.dart';
import 'package:fluttermvvmtemplate/core/helpers/alert_helper.dart';
import 'package:fluttermvvmtemplate/view/authenticate/onboard/view/on_board_view.dart';
import 'package:fluttermvvmtemplate/view/modules/system/object/view/object_home.dart';
import 'package:fluttermvvmtemplate/view/modules/system/version/model/version_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/version/view/version_create.dart';
import 'package:fluttermvvmtemplate/view/modules/system/version/view/version_update.dart';
import 'package:fluttermvvmtemplate/view/modules/system/version/viewmodel/version_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:page_transition/page_transition.dart';

class VersionListView extends StatelessWidget {
  final boldStyle = const TextStyle(fontWeight: FontWeight.bold);

  @override
  Widget build(BuildContext context) {
    return BaseView<VersionViewModel>(
      viewModel: VersionViewModel(),
      onModelReady: (viewModel) {
        viewModel.init();
        viewModel.setContext(context);
        viewModel.fetchItems();
      },
      onPageBuilder: (context, viewModel) => Scaffold(
        appBar: AppBar(
          title: Text('Version Control'),
          leading: IconButton(
              icon: Icon(Icons.view_module),
              onPressed: () {
                Navigator.pushAndRemoveUntil(
                    context,
                    PageTransition(
                        type: PageTransitionType.rightToLeft,
                        child: ObjectHome()),
                    (route) => false);
              }),
        ),
        body: Observer(builder: (_) {
          switch (viewModel.state) {
            case StoreState.initial:
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircularProgressIndicator(),
                  Text('common.loading'.tr()),
                ],
              );
              break;

            case StoreState.loading:
              return Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Center(child: CircularProgressIndicator()),
                ],
              );
            case StoreState.loaded:
              return ListView.builder(
                  itemCount: viewModel.items.length,
                  itemBuilder: (context, index) {
                    return Card(
                      child: ListTile(
                          title: Text(viewModel.items[index].name),
                          subtitle: Text(viewModel.items[index].name),
                          leading: Icon(Icons.language),
                          trailing: IconButton(
                              icon: Icon(Icons.more_horiz),
                              onPressed: () {
                                showBottomSheet(
                                    context, viewModel, viewModel.items[index]);
                              })),
                    );
                  });
              break;
            case StoreState.loadingError:
              return Center(child: ErrorPage(errorText: 'error'.tr()));
            default:
              return Center(child: ErrorPage(errorText: 'error'.tr()));
          }
        }),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (_) => VersionCreateView(
                          onUpdate: () => viewModel.fetchItems(),
                        )));
          },
          child: Icon(Icons.add),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        bottomNavigationBar: CustomBottomNavigationBar(
          onPage: (currentPageIndex) {},
        ),
      ),
    );
  }

  void showBottomSheet(BuildContext context, VersionViewModel viewModel,
      VersionControlModel versionModel) {
    showModalBottomSheet(
        context: context,
        builder: (_) {
          return Container(
            child: Column(mainAxisSize: MainAxisSize.min, children: [
              ListTile(
                leading: Icon(
                  Icons.edit,
                  color: Colors.blue,
                ),
                title: Text(
                  'Update',
                  style: boldStyle,
                ),
                subtitle: Text('Update this item'),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) => VersionUpdateView(
                                onUpdate: () {
                                  viewModel.fetchItems();
                                },
                                model: versionModel,
                              )));
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.delete,
                  color: Colors.red,
                ),
                title: Text(
                  'Delete',
                  style: boldStyle,
                ),
                subtitle: Text('You can delete this item'),
                onTap: () {
                  AlertHelper.shared.deleteDetector(context, () {
                    Navigator.pop(context);
                    Navigator.pop(context);
                    delete(context, viewModel, versionModel);
                  });
                },
              )
            ]),
          );
        });
  }

  Future<void> delete(BuildContext context, VersionViewModel viewModel,
      VersionControlModel model) async {
    var response = await viewModel.delete(model);
    if (response.success) {
      viewModel.items.remove(model);
      CustomFormAlert(
          title: 'SUCCES',
          message: response.message,
          alertType: CustomAlertType.SUCCESS);
    } else {
      CustomFormAlert(
          title: 'ERROR',
          message: response.message,
          alertType: CustomAlertType.ERROR);
    }
  }
}
