import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/base/view/base_widget.dart';
import 'package:fluttermvvmtemplate/core/components/card/custom_visibility_card.dart';
import 'package:fluttermvvmtemplate/core/components/form/animated_stack.dart';
import 'package:fluttermvvmtemplate/core/components/form/base_form.dart';
import 'package:fluttermvvmtemplate/core/helpers/alert_helper.dart';
import 'package:fluttermvvmtemplate/core/init/theme/colors.dart';
import 'package:fluttermvvmtemplate/core/validator/custom_validator.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/viewmodel/available_language_custom_validations.dart';
import 'package:fluttermvvmtemplate/view/modules/system/version/model/version_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/version/viewmodel/version_viewmodel.dart';
import 'package:page_transition/page_transition.dart';

class VersionUpdateView extends StatefulWidget {
  final VersionControlModel model;
  final VoidCallback onUpdate;

  VersionUpdateView({Key key, this.model, this.onUpdate}) : super(key: key);

  @override
  _VersionUpdateViewState createState() => _VersionUpdateViewState();
}

class _VersionUpdateViewState extends State<VersionUpdateView> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController nameController;

  TextEditingController typeController;

  TextEditingController descriptionController;

  var animatedButton = false;

  @override
  Widget build(BuildContext context) {
    return BaseView<VersionViewModel>(
        pageId: 25,
        objectId: 27,
        viewModel: VersionViewModel(),
        onModelReady: (vm) {
          vm.init();
          vm.setContext(context);
          vm.dirtyModel = VersionControlModel.clone(widget.model);

          nameController = TextEditingController(text: widget.model.name);
          typeController = TextEditingController(text: widget.model.type);
          descriptionController =
              TextEditingController(text: widget.model.description);
        },
        onDispose: () {
          nameController.dispose();
          typeController.dispose();
          descriptionController.dispose();
        },
        onPageBuilder: (context, viewModel) {
          return SafeArea(
            child: AnimatedStackView(
              iconTileList: [
                IconTile(iconData: Icons.category, onPressed: () {
                  
                }),
              ],
              animatedButton: () {
                animatedButton = !animatedButton;
                return animatedButton;
              },
              child: Scaffold(
                appBar: AppBar(
                  title: Text('${widget.model.name} Detail'),
                ),
                body: buildForm(viewModel, context),
              ),
            ),
          );
        });
  }

  Form buildForm(VersionViewModel vm, BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              controller: nameController,
              decoration: InputDecoration(
                  labelText: 'Name',
                  hintText: 'Name',
                  border: OutlineInputBorder()),
              validator: (str) {
                return CustomValidator.selectAndRunFunction(
                    AvailableLanguageCustomValidations().getValidationData('name'),str);
              },
              onChanged: (str) {
                vm.dirtyModel.name = str;
                if (str != widget.model.name ||
                    nameController.text != widget.model.name) {
                  vm.setIsEdit(true);
                } else {
                  vm.setIsEdit(false);
                }
              },
            ),
          ),
          SizedBox(
            height: 8,
          ),

          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              controller: descriptionController,
              decoration: InputDecoration(
                  labelText: 'Description',
                  hintText: 'Description',
                  border: OutlineInputBorder()),
              onChanged: (str) {
                vm.dirtyModel.description = str;
              },
              validator: (str) {
                return CustomValidator.selectAndRunFunction(
                    AvailableLanguageCustomValidations()
                        .getValidationData('name'),
                    str);
              },
            ),
          ),

          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              controller: typeController,
              decoration: InputDecoration(
                  labelText: 'Type',
                  hintText: 'Type',
                  border: OutlineInputBorder()),
              validator: (str) {
                return CustomValidator.selectAndRunFunction(
                    AvailableLanguageCustomValidations()
                        .getValidationData('name'),
                    str);
              },
              onChanged: (str) {
                vm.dirtyModel.name = str;
                if (str != widget.model.description ||
                    nameController.text != widget.model.name) {
                  vm.setIsEdit(true);
                } else {
                  vm.setIsEdit(false);
                }
              },
            ),
          ),

          CustomVisibilityFormCard(
              isActive: vm.dirtyModel.isActive,
              isPublic: vm.dirtyModel.isPublic,
              onUpdate: (public, active) {
                vm.dirtyModel.isActive = active;
                vm.dirtyModel.isPublic = public;
              }),

          CustomNormalButton(
              textColor: Colors.white,
              buttonColor: Colors.green,
              text: '  Update  ',
              onPressed: () async {
                AlertHelper.shared.showProgressDialog(context);
                var response = await vm.update();
                await AlertHelper.shared.startProgress(context, response);

                Navigator.pop(context); // remove alert

                if (response.success) {
                  widget.onUpdate();
                  Navigator.pop(context); // remove loading
                  Navigator.pop(context); // remove create form
                }
              })
          //buildSaveButton()
        ],
      ),
    );
  }
}
