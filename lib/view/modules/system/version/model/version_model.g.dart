// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'version_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VersionControlModel _$VersionControlModelFromJson(Map<String, dynamic> json) {
  $checkKeys(json, requiredKeys: const ['type', 'parent']);
  return VersionControlModel(
    slug: json['slug'] ?? '',
    name: json['name'] ?? '',
    description: json['description'] ?? '',
    isActive: json['isActive'] ?? true,
    isPublic: json['isPublic'] ?? true,
    type: json['type'] as String,
    level: json['level'] as String,
    parent: json['parent'] as String,
    rank: json['rank'] as String,
  )..targetRoute = json['targetRoute'] as String;
}

Map<String, dynamic> _$VersionControlModelToJson(
        VersionControlModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'description': instance.description,
      'slug': instance.slug,
      'isPublic': instance.isPublic,
      'isActive': instance.isActive,
      'targetRoute': instance.targetRoute,
      'type': instance.type,
      'parent': instance.parent,
      'rank': instance.rank,
      'level': instance.level,
    };
