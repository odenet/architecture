import 'package:fluttermvvmtemplate/core/base/model/base_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'version_model.g.dart';

@JsonSerializable()
class VersionControlModel extends BaseModel<VersionControlModel> {
  @JsonKey(required: true, name: 'type')
  String type;

  @JsonKey(required: true, name: 'parent')
  String parent;

  @JsonKey(name: 'rank')
  String rank;

  @JsonKey(name: 'level')
  String level;

  VersionControlModel(
      {slug,
      name,
      description,
      isActive = true,
      isPublic = true,
      this.type,
      this.level,
      this.parent,
      this.rank}) {
    this.slug = slug;
    this.name = name;
    this.description = description;
    this.isActive = isActive;
    this.isPublic = isPublic;
  }

  VersionControlModel.clone(VersionControlModel model) {
    name = model.name;
    description = model.description;
    slug = model.slug;
    isActive = model.isActive;
    isPublic = model.isPublic;
    rank = model.rank;
    parent = model.parent;
    type = model.type;
    level = model.level;
  }

  @override
  VersionControlModel fromJson(Map json) {
    return _$VersionControlModelFromJson(json);
  }

  @override
  Map<String, dynamic> toJson() {
    return _$VersionControlModelToJson(this);
  }
}
