import 'package:fluttermvvmtemplate/core/base/service/base_service.dart';
import 'package:fluttermvvmtemplate/core/constants/app/app_constants.dart';
import 'package:fluttermvvmtemplate/core/constants/app/endpoints.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/request_model.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/response_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/model/available_languages_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/version/model/version_model.dart';

class VersionService {
  BaseService service = BaseService(VersionControlModel());

  @override
  Future<ResponseModel> fetchList({int page = 1}) async {
    var requestModel = RequestModel(Endpoints.VERSION_CONTROL,
        params: {'page': page, 'perPage': ApplicationConstants.PER_PAGE});
    return service.fetchList(requestModel);
  }

  @override
  Future<ResponseModel> fetchItem(String slug) async {
    var requestModel = RequestModel('${Endpoints.VERSION_CONTROL + slug}');

    return service.fetchItem(requestModel);
  }

  @override
  Future<ResponseModel> deleteItem(Map<String,dynamic> body) async {
    Map<String,dynamic> requestBody = {
      'data': [
        {'slug': body['slug'], 'dataType': body['dataType']}
      ]
    };

    var requestModel =
        RequestModel('${Endpoints.VERSION_CONTROL}', body: requestBody);
    return service.deleteItem(requestModel);
  }

  @override
  Future<ResponseModel> updateItem(Map<String,dynamic> body) async {

    var requestBody = {
      'data': [body]
    };

    var requestModel =
        RequestModel('${Endpoints.VERSION_CONTROL}', body: requestBody);
    return service.updateItem(requestModel);
  }

  @override
  Future<ResponseModel> createItem(Map<String,dynamic> body) async {
    var requestBody = {
      'data': [body]
    };
    var requestModel =
        RequestModel('${Endpoints.VERSION_CONTROL}', body: requestBody);
    return service.createItem(requestModel);
  }
}
