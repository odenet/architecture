// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'version_viewmodel.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$VersionViewModel on _VersionViewModelBase, Store {
  Computed<StoreState> _$stateComputed;

  @override
  StoreState get state =>
      (_$stateComputed ??= Computed<StoreState>(() => super.state,
              name: '_VersionViewModelBase.state'))
          .value;

  final _$networkErrorAtom = Atom(name: '_VersionViewModelBase.networkError');

  @override
  NetworkErrorModel get networkError {
    _$networkErrorAtom.reportRead();
    return super.networkError;
  }

  @override
  set networkError(NetworkErrorModel value) {
    _$networkErrorAtom.reportWrite(value, super.networkError, () {
      super.networkError = value;
    });
  }

  final _$itemsAtom = Atom(name: '_VersionViewModelBase.items');

  @override
  ObservableList<VersionControlModel> get items {
    _$itemsAtom.reportRead();
    return super.items;
  }

  @override
  set items(ObservableList<VersionControlModel> value) {
    _$itemsAtom.reportWrite(value, super.items, () {
      super.items = value;
    });
  }

  final _$modelAtom = Atom(name: '_VersionViewModelBase.model');

  @override
  VersionControlModel get model {
    _$modelAtom.reportRead();
    return super.model;
  }

  @override
  set model(VersionControlModel value) {
    _$modelAtom.reportWrite(value, super.model, () {
      super.model = value;
    });
  }

  final _$dirtyModelAtom = Atom(name: '_VersionViewModelBase.dirtyModel');

  @override
  VersionControlModel get dirtyModel {
    _$dirtyModelAtom.reportRead();
    return super.dirtyModel;
  }

  @override
  set dirtyModel(VersionControlModel value) {
    _$dirtyModelAtom.reportWrite(value, super.dirtyModel, () {
      super.dirtyModel = value;
    });
  }

  final _$isEditAtom = Atom(name: '_VersionViewModelBase.isEdit');

  @override
  bool get isEdit {
    _$isEditAtom.reportRead();
    return super.isEdit;
  }

  @override
  set isEdit(bool value) {
    _$isEditAtom.reportWrite(value, super.isEdit, () {
      super.isEdit = value;
    });
  }

  final _$futureAtom = Atom(name: '_VersionViewModelBase.future');

  @override
  ObservableFuture<dynamic> get future {
    _$futureAtom.reportRead();
    return super.future;
  }

  @override
  set future(ObservableFuture<dynamic> value) {
    _$futureAtom.reportWrite(value, super.future, () {
      super.future = value;
    });
  }

  final _$fetchItemsAsyncAction =
      AsyncAction('_VersionViewModelBase.fetchItems');

  @override
  Future<dynamic> fetchItems() {
    return _$fetchItemsAsyncAction.run(() => super.fetchItems());
  }

  final _$_VersionViewModelBaseActionController =
      ActionController(name: '_VersionViewModelBase');

  @override
  void setIsEdit(bool value) {
    final _$actionInfo = _$_VersionViewModelBaseActionController.startAction(
        name: '_VersionViewModelBase.setIsEdit');
    try {
      return super.setIsEdit(value);
    } finally {
      _$_VersionViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void changeVisibility({bool isActive, bool isPublic}) {
    final _$actionInfo = _$_VersionViewModelBaseActionController.startAction(
        name: '_VersionViewModelBase.changeVisibility');
    try {
      return super.changeVisibility(isActive: isActive, isPublic: isPublic);
    } finally {
      _$_VersionViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
networkError: ${networkError},
items: ${items},
model: ${model},
dirtyModel: ${dirtyModel},
isEdit: ${isEdit},
future: ${future},
state: ${state}
    ''';
  }
}
