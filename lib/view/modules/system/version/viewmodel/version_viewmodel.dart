import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/base/model/base_view_model.dart';
import 'package:fluttermvvmtemplate/core/constants/app/app_constants.dart';
import 'package:fluttermvvmtemplate/core/constants/enums/store_state.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/response_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/network_error_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/viewmodel/available_languages_process_manager.dart';
import 'package:fluttermvvmtemplate/view/modules/system/version/model/version_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/version/service/version_service.dart';
import 'package:mobx/mobx.dart';
part 'version_viewmodel.g.dart';

class VersionViewModel = _VersionViewModelBase with _$VersionViewModel;

abstract class _VersionViewModelBase with Store, BaseViewModel {
  VersionService feed = VersionService();

  @override
  void setContext(BuildContext context) => this.context = context;

  @override
  void init() {
    model = VersionControlModel(isActive: true, isPublic: false, parent: '0');
    dirtyModel =
        VersionControlModel(isActive: true, isPublic: false, parent: '0');
  }

  @observable
  NetworkErrorModel networkError = NetworkErrorModel(hasError: false);

  @observable
  ObservableList<VersionControlModel> items =
      ObservableList<VersionControlModel>();

  @observable
  VersionControlModel model;

  @observable
  VersionControlModel dirtyModel;

  @observable
  bool isEdit = false;

  @observable
  ObservableFuture future;

  @action
  void setIsEdit(bool value) {
    isEdit = value;
  }

  @action
  void changeVisibility({bool isActive, bool isPublic}) {
    dirtyModel.isActive = isActive ?? false;
    dirtyModel.isPublic = isPublic ?? false;
  }

  @action
  Future fetchItems() async {
    var processManager = AvailableLanguageProcessManager(
      requestType: ApplicationConstants.GET,
      model: VersionControlModel(),
    ).status();

    var response = ResponseModel(success: false, message: " ");

    if (processManager) {
      try {
        future = ObservableFuture(feed.fetchList());
        response = await future;
        var responseList = List<VersionControlModel>.from(response.data);

        items = responseList.asObservable();
      } catch (e) {
        networkError = NetworkErrorModel(errorText: response.message);
      }
    }

    return response;
  }

  Future<ResponseModel> delete(VersionControlModel language) async {
    var response;
    try {
      future = ObservableFuture(feed.deleteItem(language.toJson()));
      response = await future;
    } catch (e) {
      networkError = NetworkErrorModel(errorText: response.message);
    }

    return response;
  }

  Future<ResponseModel> update() async {
    var processManager = AvailableLanguageProcessManager(
      requestType: ApplicationConstants.PUT,
      model: dirtyModel,
    ).status();
    var response;

    if (processManager) {
      try {
        future = ObservableFuture(feed.updateItem(dirtyModel.toJson()));
        response = await future;
      } catch (e) {
        networkError = NetworkErrorModel(errorText: response.message);
      }
    }

    return response;
  }

  // Process manager calls validate rule and if it is successfull then create item
  // send request type to process manager
  // process manager will return only processes related to create
  // if process manager return true then continue to create object

  Future<ResponseModel> create() async {
    var processManager = AvailableLanguageProcessManager(
      requestType: ApplicationConstants.POST,
      model: dirtyModel,
    ).status();
    var response = ResponseModel(success: false, message: " ");

    if (processManager) {
      try {
        future = ObservableFuture(feed.createItem(dirtyModel.toJson()));
        response = await future;
      } catch (e) {
        networkError = NetworkErrorModel(errorText: response.message);
      }
    }

    return response;
  }

  @computed
  StoreState get state {
    if (future == null || future.status == FutureStatus.rejected) {
      return StoreState.initial;
    }

    if (future.status == FutureStatus.pending) {
      return StoreState.loading;
    } else if (future.status == FutureStatus.fulfilled) {
      if ((networkError != null) && networkError.hasError) {
        return StoreState.loadingError;
      } else {
        return StoreState.loaded;
      }
    }
  }
}
