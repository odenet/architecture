import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:fluttermvvmtemplate/core/base/view/base_widget.dart';
import 'package:fluttermvvmtemplate/core/components/errors/error_page.dart';
import 'package:fluttermvvmtemplate/core/constants/enums/store_state.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/model/feature_model.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:fluttermvvmtemplate/core/init/notifier/data_type.dart';
import 'package:fluttermvvmtemplate/view/modules/system/log/viewmodel/log_view_model.dart';

class LogRelatedFeaturesListView extends StatelessWidget {
  final Function(List<FeatureModel> items) onSelect;
  final dynamic viewmodel;
  final String language;

  LogRelatedFeaturesListView(
      {Key key, @required this.onSelect, this.viewmodel, this.language})
      : super(key: key);

  List<dynamic> items;
  List<dynamic> filteredItems;

  @override
  Widget build(BuildContext context) {
    return BaseView<LogViewModel>(
        pageId: 25,
        objectId: 27,
        viewModel: viewmodel,
        onModelReady: (vm) {
          vm.setContext(context);
          vm.init();
          vm.getFeatures(language);
        },
        onPageBuilder: (BuildContext context, LogViewModel viewModel) {
          return Scaffold(
              backgroundColor: Colors.transparent,
              appBar: AppBar(
                title: Text(DataType.shared.dataTypeLabel +
                    ' ' +
                    'related.feature_select'.tr()),
              ),
              body: Observer(builder: (_) {
                switch (viewModel.state) {
                  case StoreState.initial:
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CircularProgressIndicator(),
                        Text('common.loading'.tr()),
                      ],
                    );

                  case StoreState.loading:
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Center(child: CircularProgressIndicator()),
                      ],
                    );

                  case StoreState.loaded:
                    return SelectableListView(
                      list: List<FeatureModel>.from(viewModel.featuresList),
                      onSelect: onSelect,
                    );
                    return Container();
                  default:
                    return ErrorPage(errorText: 'error'.tr());
                }
              }));
        });
  }
}

class SelectableListView extends StatefulWidget {
  final List<FeatureModel> list;
  final Function(List<FeatureModel> items) onSelect;

  const SelectableListView({Key key, this.list, this.onSelect})
      : super(key: key);

  @override
  _SelectableListViewState createState() => _SelectableListViewState();
}

class _SelectableListViewState extends State<SelectableListView> {
  List<FeatureModel> mockList = [];
  List<bool> statusListItems;
  List<FeatureModel> selectedList = [];

  @override
  void initState() {
    mockList = widget.list;
    statusListItems = List.generate(mockList.length, (index) => false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Card(
          color: Color(0xFF426370),
          child: ListTile(
            title: Text(
              'common.selected_items'
                  .tr(namedArgs: {'count': selectedList.length.toString()}),
            ),
            leading: Icon(
              Icons.list,
              color: Colors.white,
            ),
            trailing: ElevatedButton(
              onPressed: () {
                widget.onSelect(selectedList);
              },
              child: Text('common.select'.tr()),
              style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Color(0xFF309912))),
            ),
          ),
        ),
        Expanded(
          child: ListView.builder(
              itemCount: mockList.length,
              itemBuilder: (ctx, index) {
                return Card(
                  child: ListTile(
                    onTap: () {
                      setState(() {
                        if (statusListItems[index]) {
                          selectedList.remove(mockList[index]);
                        } else {
                          selectedList.add(mockList[index]);
                        }
                        statusListItems[index] = !statusListItems[index];
                      });
                    },
                    leading: CircleAvatar(
                      backgroundColor:
                          statusListItems[index] ? Colors.blue : Colors.grey,
                    ),
                    title: Text(mockList[index].name),
                  ),
                );
              }),
        ),
      ],
    );
  }
}
