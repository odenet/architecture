import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:fluttermvvmtemplate/core/base/view/base_widget.dart';
import 'package:fluttermvvmtemplate/core/components/errors/error_page.dart';
import 'package:fluttermvvmtemplate/core/components/form/empty_page.dart';
import 'package:fluttermvvmtemplate/core/constants/enums/store_state.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/model/category_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/viewmodel/category_view_model.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:fluttermvvmtemplate/core/init/notifier/data_type.dart';

class LogRelatedCategoriesListView extends StatelessWidget {
  final Function(List<CategoryModel> items) onSelect;

  // if  previous items exist then do not disable select button
  final bool isMainExist;

  LogRelatedCategoriesListView(
      {Key key, @required this.onSelect, this.isMainExist = false})
      : super(key: key);

  List<dynamic> items;
  List<dynamic> filteredItems;

  @override
  Widget build(BuildContext context) {
    print(isMainExist);
    return BaseView<CategoryViewModel>(
        pageId: 25,
        objectId: 27,
        viewModel: CategoryViewModel(),
        onModelReady: (model) {
          model.setContext(context);
          model.init();
          model.fetchItems();
        },
        onPageBuilder: (BuildContext context, CategoryViewModel viewModel) {
          return Scaffold(
              appBar: AppBar(
                title: Text(DataType.shared.dataTypeLabel +
                    ' ' +
                    'related.category_select'.tr()),
              ),
              body: Observer(builder: (_) {
                switch (viewModel.state) {
                  case StoreState.initial:
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Center(child: CircularProgressIndicator()),
                      ],
                    );

                  case StoreState.loading:
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Center(child: CircularProgressIndicator()),
                      ],
                    );

                  case StoreState.loaded:
                    if (viewModel.filteredListItems.isNotEmpty) {
                      return SelectableListView(
                        isMainExist: isMainExist,
                        list: List<CategoryModel>.from(viewModel.items),
                        onSelect: onSelect,
                      );
                    } else {
                      return EmptyView(
                          onPressed: () {}, buttonText: 'common.empty'.tr());
                    }
                    break;
                  default:
                    return Center(child: ErrorPage(errorText: 'error'.tr()));
                }
              }));
        });
  }
}

class SelectableListView extends StatefulWidget {
  final List<CategoryModel> list;
  final Function(List<CategoryModel> items) onSelect;
  final bool isMainExist;

  const SelectableListView(
      {Key key, this.list, this.onSelect, this.isMainExist})
      : super(key: key);

  @override
  _SelectableListViewState createState() => _SelectableListViewState();
}

class _SelectableListViewState extends State<SelectableListView> {
  List<CategoryModel> mockList = [];
  List<bool> statusListItems;
  List<bool> mainListItems;
  List<CategoryModel> selectedList = [];

  @override
  void initState() {
    mockList = widget.list;
    statusListItems = List.generate(mockList.length, (index) => false);
    mainListItems =
        List.generate(mockList.length, (index) => mockList[index].isMain);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Card(
          color: Color(0xFF426370),
          child: ListTile(
            title: Text(
              'common.selected_items'
                  .tr(namedArgs: {'count': selectedList.length.toString()}),
            ),
            leading: Icon(
              Icons.list,
              color: Colors.white,
            ),
            trailing: ElevatedButton(
              onPressed: selectedList.isNotEmpty
                  ? () {
                      widget.onSelect(selectedList);
                    }
                  : null,
              child: Text('common.select'.tr()),
              style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Color(0xFF309912))),
            ),
          ),
        ),
        Expanded(
          child: ListView.builder(
              itemCount: mockList.length,
              itemBuilder: (ctx, index) {
                return Card(
                  child: ListTile(
                    onTap: () {
                      setState(() {
                        if (statusListItems[index]) {
                          selectedList.remove(mockList[index]);
                        } else {
                          selectedList.add(mockList[index]);
                        }
                        statusListItems[index] = !statusListItems[index];
                      });
                    },
                    leading: CircleAvatar(
                      backgroundColor:
                          statusListItems[index] ? Colors.blue : Colors.grey,
                      child: statusListItems[index]
                          ? Icon(
                              Icons.check,
                              color: Colors.white,
                            )
                          : Container(),
                    ),
                    trailing: widget.isMainExist
                        ? Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              SizedBox(
                                height: 2,
                              ),
                              Text('common.main'.tr()),
                              Expanded(
                                  child: Checkbox(
                                      value: mainListItems[index],
                                      onChanged: (val) {
                                        mainListItems = mainListItems
                                            .map((e) => false)
                                            .toList();
                                        if (mainListItems.contains(true) &&
                                            !mainListItems[index]) {
                                          mockList[index].isMain = val;
                                        } else {
                                          mainListItems[index] = val;
                                          mockList[index].isMain = val;
                                        }

                                        setState(() {});
                                      }))
                            ],
                          )
                        : SizedBox(
                            width: 1,
                          ),
                    title: Text(mockList[index].name),
                  ),
                );
              }),
        ),
      ],
    );
  }
}
