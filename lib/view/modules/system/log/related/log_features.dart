import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:fluttermvvmtemplate/core/base/view/base_widget.dart';
import 'package:fluttermvvmtemplate/core/components/errors/error_page.dart';
import 'package:fluttermvvmtemplate/core/components/form/empty_page.dart';
import 'package:fluttermvvmtemplate/core/components/language_bar/available_language_bar.dart';
import 'package:fluttermvvmtemplate/core/constants/enums/store_state.dart';
import 'package:fluttermvvmtemplate/core/helpers/alert_helper.dart';
import 'package:fluttermvvmtemplate/core/init/notifier/data_type.dart';
import 'package:fluttermvvmtemplate/view/common/models/related_feature_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/related_feature_value_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/object/viewmodel/related_feature_view_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/log/related/log_feature_value.dart';
import 'package:fluttermvvmtemplate/view/modules/system/log/related/log_feature_value_update_view.dart';
import 'package:fluttermvvmtemplate/view/modules/system/log/related/log_related_features_listview.dart';
import 'package:fluttermvvmtemplate/view/modules/system/log/viewmodel/log_view_model.dart';
import 'package:page_transition/page_transition.dart';
import 'package:easy_localization/easy_localization.dart';

class LogFeature extends StatefulWidget {
  final LogViewModel viewModel;
  final Function onUpdate;

  const LogFeature({Key key, this.viewModel, this.onUpdate})
      : super(key: key);

  @override
  _LogFeatureState createState() => _LogFeatureState();
}

class _LogFeatureState extends State<LogFeature> {
  final List<String> selectedList = [];
  final boldStyle = const TextStyle(fontWeight: FontWeight.bold);

  @override
  Widget build(BuildContext context) {
    return BaseView<RelatedFeatureViewModel>(
        pageId: 25,
        objectId: 27,
        viewModel: RelatedFeatureViewModel(),
        onModelReady: (model) {
          model.setContext(context);
          model.objectListModel = widget.viewModel.model;
          model.detail();
        },
        onPageBuilder:
            (BuildContext context, RelatedFeatureViewModel viewModel) {
          return Observer(builder: (_) {
            switch (viewModel.state) {
              case StoreState.initial:
                return Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Center(child: CircularProgressIndicator()),
                  ],
                );
              case StoreState.loading:
                return Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Center(child: CircularProgressIndicator()),
                  ],
                );
              case StoreState.loaded:
                return Container(
                    child: DefaultTabController(
                        initialIndex: viewModel.tabIndex,
                        length: viewModel.availableLanguages.length,
                        child: Scaffold(
                          backgroundColor: Colors.transparent,
                          appBar: AppBar(
                              title: selectedList.isNotEmpty
                                  ? Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Text(DataType.shared.dataTypeLabel +
                                            ' ' +
                                            selectedList.length.toString() +
                                            'common.selected_items'.tr()),
                                        IconButton(
                                          onPressed: () {
                                            // delete
                                          },
                                          icon: Icon(Icons.delete),
                                        )
                                      ],
                                    )
                                  : Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Text(DataType.shared.dataTypeLabel +
                                            ' ' +
                                            'related.features'.tr(namedArgs: {
                                              'dataType':
                                                  DataType.shared.dataTypeLabel
                                            })),
                                      ],
                                    ),
                              bottom: AvailableLanguageBar(
                                  onTap: (index) => viewModel.setLanguageModel(
                                      viewModel.availableLanguages[index],
                                      index),
                                  items: viewModel.availableLanguages)),
                          body: viewModel.valueItems.isNotEmpty
                              ? Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Expanded(
                                      child: ListView.builder(
                                          itemCount:
                                              viewModel.valueItems.length,
                                          shrinkWrap: true,
                                          itemBuilder: (context, index) {
                                            return Card(
                                              child: ListTile(
                                                  leading: Icon(
                                                    Icons.add,
                                                    color: Colors.red,
                                                  ),
                                                  title: Text(viewModel
                                                          .valueItems[index]
                                                          .featureLabel ??
                                                      'related.add_feature_label'
                                                          .tr()),
                                                  subtitle: Text(viewModel
                                                          .valueItems[index]
                                                          .featureValue ??
                                                      'related.feature'.tr()),
                                                  trailing: IconButton(
                                                      icon: Icon(
                                                          Icons.more_horiz),
                                                      onPressed: () {
                                                        showBottomSheet(
                                                            context,
                                                            viewModel
                                                                    .valueItems[
                                                                index],
                                                            viewModel);
                                                      }),
                                                  onTap: () {}),
                                            );
                                          }),
                                    ),
                                    SizedBox(
                                      height: 60,
                                    ),
                                    ElevatedButton.icon(
                                      onPressed: () {
                                        add(viewModel.languageModel.langCode);
                                      },
                                      icon: Icon(Icons.add),
                                      label: Text('common.create'.tr()),
                                      style: ButtonStyle(
                                          backgroundColor:
                                              MaterialStateProperty.all(
                                                  Colors.green)),
                                    ),
                                    SizedBox(
                                      height: 60,
                                    )
                                  ],
                                )
                              : Center(
                                  child: emptyList(
                                      viewModel.languageModel.langCode),
                                ),
                        )));
              case StoreState.loadingError:
                return Center(child: ErrorPage(errorText: 'error'.tr()));
              default:
                return ErrorPage(errorText: 'error'.tr());
            }
          });
        });
  }

  void showBottomSheet(BuildContext context, RelatedFeatureValueModel model,
      RelatedFeatureViewModel viewModel) {
    showModalBottomSheet(
        context: context,
        builder: (_) {
          return Container(
            child: Column(mainAxisSize: MainAxisSize.min, children: [
              ListTile(
                leading: Icon(
                  Icons.edit,
                  color: Colors.blue,
                ),
                title: Text(
                  'common.update'.tr(),
                  style: boldStyle,
                ),
                subtitle: Text('common.update_subtitle'.tr()),
                onTap: () {
                  viewModel.model = model;
                  viewModel.dirtyModel = RelatedFeatureValueModel.clone(model);

                  Navigator.push(
                      context,
                      PageTransition(
                          type: PageTransitionType.rightToLeft,
                          child: LogFeatureValueUpdateView(
                              viewModel: viewModel,
                              onUpdate: () {
                                setState(() {});
                              })));
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.delete,
                  color: Colors.red,
                ),
                title: Text(
                  'common.delete'.tr(),
                  style: boldStyle,
                ),
                subtitle: Text('common.delete_subtitle'.tr()),
                onTap: () {
                  AlertHelper.shared.deleteDetector(context, () {
                    Navigator.pop(context);
                    viewModel.valueItems
                        .removeWhere((element) => element.slug == model.slug);

                    viewModel.delete(model).then((value) {
                      if (!value.success) {
                        AlertHelper.shared.startProgress(context, value);
                      }
                    });
                  });
                },
              )
            ]),
          );
        });
  }

  Widget emptyList(language) {
    return EmptyView(
        isSearchActive: false,
        onPressed: () {
          add(language);
        },
        buttonText: 'add_feature'.tr());
  }

  void add(language) {
    Navigator.push(
        context,
        PageTransition(
            type: PageTransitionType.rightToLeft,
            child: LogRelatedFeaturesListView(
                language: language,
                viewmodel: widget.viewModel,
                onSelect: (items) async {
                  var data = items
                      .map((e) => RelatedFeatureModel(
                          dataType: DataType.shared.dataType,
                          featureSlug: e.slug,
                          dataSlug: widget.viewModel.model.slug,
                          name: e.name,
                          language: language))
                      .toList();

                  Navigator.push(
                      context,
                      PageTransition(
                          type: PageTransitionType.rightToLeft,
                          child: LogFeatureValue(
                            data,
                            viewModel: widget.viewModel,
                            onUpdate: () {
                              
                            },
                          )));
                })));
  }
}
