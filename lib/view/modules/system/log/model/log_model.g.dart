// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'log_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LogModel _$LogModelFromJson(Map<String, dynamic> json) {
  return LogModel(
    slug: json['slug'] ?? '',
    name: json['name'] ?? '',
    description: json['description'] ?? '',
    isActive: json['isActive'] ?? true,
    isPublic: json['isPublic'] ?? true,
    dataType: json['dataType'] as String ?? '',
    relatedCategories: (json['relatedCategories'] as List)
            ?.map((e) => e == null
                ? null
                : RelatedCategoryModel.fromJson(e as Map<String, dynamic>))
            ?.toList() ??
        [],
    relatedFeatures: (json['relatedFeatures'] as List)
            ?.map((e) => e == null
                ? null
                : RelatedFeatureValueModel().fromJson(e as Map<String, dynamic>))
            ?.toList() ??
        [],
    systemInfo: (json['systemInfo'] as List)
            ?.map((e) => e == null
                ? null
                : LanguageInfoModel.fromJson(e as Map<String, dynamic>))
            ?.toList() ??
        [],
  )
    ..targetRoute = json['targetRoute'] as String
    ..type = json['type'] as String ?? ''
    ..createdBy = json['createdBy'] == null
        ? null
        : BlamableModel.fromJson(json['createdBy'] as Map<String, dynamic>)
    ..updatedBy = json['updatedBy'] == null
        ? null
        : BlamableModel.fromJson(json['updatedBy'] as Map<String, dynamic>)
    ..createdAt = json['createdAt'] as String
    ..updatedAt = json['updatedAt'] as String;
}

Map<String, dynamic> _$LogModelToJson(LogModel instance) => <String, dynamic>{
      'name': instance.name,
      'description': instance.description,
      'slug': instance.slug,
      'isPublic': instance.isPublic,
      'isActive': instance.isActive,
      'targetRoute': instance.targetRoute,
      'systemInfo': instance.systemInfo,
      'type': instance.type,
      'dataType': instance.dataType,
      'createdBy': instance.createdBy,
      'updatedBy': instance.updatedBy,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
      'relatedCategories': instance.relatedCategories,
      'relatedFeatures': instance.relatedFeatures,
    };
