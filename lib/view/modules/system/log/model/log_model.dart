import 'package:fluttermvvmtemplate/view/common/models/blamable_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/related_category_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/related_feature_value_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/model/language_info_model.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:fluttermvvmtemplate/core/base/model/base_model.dart';

part 'log_model.g.dart';

@JsonSerializable()
class LogModel extends BaseModel<LogModel> {
  @JsonKey(defaultValue: [])
  List<LanguageInfoModel> systemInfo;

  @JsonKey(defaultValue: '')
  String type;

  @JsonKey(defaultValue: '')
  String dataType;

  @JsonKey(nullable: true)
  BlamableModel createdBy;

  @JsonKey(nullable: true)
  BlamableModel updatedBy;

  @JsonKey(nullable: true)
  String createdAt;

  @JsonKey(nullable: true)
  String updatedAt;

  @JsonKey(defaultValue: [])
  List<RelatedCategoryModel> relatedCategories;

  @JsonKey(defaultValue: [])
  List<RelatedFeatureValueModel> relatedFeatures;

  LogModel(
      {slug,
      name,
      description,
      isActive = true,
      isPublic = true,
      this.dataType,
      this.relatedCategories,
      this.relatedFeatures,
      this.systemInfo}) {
    {
      this.slug = slug;
      this.name = name;
      this.description = description;
      this.isActive = isActive;
      this.isPublic = isPublic;
    }
  }

  LogModel.clone(LogModel model) {
    name = model.name;
    description = model.description;
    slug = model.slug;
    type = model.type;
    isActive = model.isActive;
    isPublic = model.isPublic;
    createdBy = model.createdBy;
    updatedBy = model.updatedBy;
    createdAt = model.createdAt;
    updatedAt = model.updatedAt;
    systemInfo = model.systemInfo;
    relatedFeatures = model.relatedFeatures;
    relatedCategories = model.relatedCategories;
  }

  @override
  LogModel fromJson(Map<String, dynamic> json) {
    return _$LogModelFromJson(json);
  }

  @override
  Map<String, dynamic> toJson() {
    return _$LogModelToJson(this);
  }
}
