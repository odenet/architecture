import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:fluttermvvmtemplate/core/base/view/base_widget.dart';
import 'package:fluttermvvmtemplate/core/components/alerts/custom_form_alert.dart';
import 'package:fluttermvvmtemplate/core/components/card/identity_card.dart';
import 'package:fluttermvvmtemplate/core/components/card/name_form_card.dart';
import 'package:fluttermvvmtemplate/core/components/card/order_form_card.dart';
import 'package:fluttermvvmtemplate/core/components/card/slug_card.dart';
import 'package:fluttermvvmtemplate/core/components/card/type_card.dart';
import 'package:fluttermvvmtemplate/core/components/card/visibility_form_card.dart';
import 'package:fluttermvvmtemplate/core/components/errors/error_page.dart';
import 'package:fluttermvvmtemplate/core/components/form/animated_stack.dart';
import 'package:fluttermvvmtemplate/core/constants/enums/store_state.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/response_model.dart';
import 'package:fluttermvvmtemplate/core/components/language_bar/available_language_bar.dart';
import 'package:fluttermvvmtemplate/view/modules/system/log/model/log_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/log/related/log_categories.dart';
import 'package:fluttermvvmtemplate/view/modules/system/log/related/log_features.dart';
import 'package:fluttermvvmtemplate/view/modules/system/log/view/log_list.dart';
import 'package:fluttermvvmtemplate/view/modules/system/log/viewmodel/log_view_model.dart';
import 'package:page_transition/page_transition.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:fluttermvvmtemplate/core/helpers/alert_helper.dart';

class LogListDetailView extends StatefulWidget {
  final LogModel model;

  LogListDetailView({Key key, this.model}) : super(key: key);

  @override
  _LogListDetailViewState createState() => _LogListDetailViewState();
}

class _LogListDetailViewState extends State<LogListDetailView> {
  final PAGE_ID = 25;
  final FormErrorState store = FormErrorState();
  TextEditingController nameController;
  TextEditingController descriptionController;
  var animatedButton = false;

  @override
  Widget build(BuildContext context) {
    return BaseView<LogViewModel>(
        pageId: PAGE_ID,
        objectId: 27,
        viewModel: LogViewModel(),
        onModelReady: (vm) {
          nameController = TextEditingController(text: widget.model.name);
          descriptionController =
              TextEditingController(text: widget.model.description);
          vm.setContext(context);
          vm.init();
          vm.model = widget.model;
          vm.detail();
        },
        onDispose: () {
          nameController.dispose();
          descriptionController.dispose();
        },
        onPageBuilder: (BuildContext context, LogViewModel viewModel) {
          return SafeArea(child: Observer(builder: (_) {
            switch (viewModel.state) {
              case StoreState.initial:
                return Center(child: CircularProgressIndicator());
              case StoreState.loading:
                return Center(child: CircularProgressIndicator());
              case StoreState.loaded:
                viewModel.languageModel ??= viewModel.model?.systemInfo[0];
                return DefaultTabController(
                  initialIndex: viewModel.tabIndex,
                  length: viewModel.availableLanguages.length,
                  child: AnimatedStackView(
                      animatedButton: () {
                        animatedButton = !animatedButton;
                        return animatedButton;
                      },
                      iconTileList: [
                        IconTile(
                            iconData: Icons.category,
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  PageTransition(
                                      type: PageTransitionType.rightToLeft,
                                      child: LogCategories(
                                          viewModel: viewModel,
                                          onUpdate: () {
                                            Navigator.pop(context);
                                          })));
                            }),
                        IconTile(
                            iconData: Icons.featured_play_list,
                            onPressed: () {
                              if (viewModel.mainCategory == null) {
                                AlertHelper.shared.showAlert(
                                    context,
                                    'related.missing_category'.tr(),
                                    'related.select_main_category_first'.tr(),
                                    CustomAlertType.WARNING);
                              } else {
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.rightToLeft,
                                        child: LogFeature(
                                            viewModel: viewModel)));
                              }
                            })
                      ],
                      child: Scaffold(
                          appBar: AppBar(
                              leading: IconButton(
                                icon: Icon(Icons.arrow_back_ios),
                                onPressed: () {
                                  Navigator.of(context).pushAndRemoveUntil(
                                      MaterialPageRoute(
                                          builder: (_) => LogListView()),
                                      (route) => false);
                                },
                              ),
                              title: Text('system.object.update_title'.tr()),
                              bottom: AvailableLanguageBar(
                                  onTap: (index) => viewModel.setLanguageModel(
                                      viewModel.availableLanguages[index],
                                      index),
                                  items: viewModel.availableLanguages)),
                          body: SingleChildScrollView(
                            child: Column(
                              children: <Widget>[
                                SizedBox(height: 10),
                                NameFormCard(
                                  viewModel: viewModel,
                                  onUpdate: (dataModel) {
                                    viewModel.languageModel.name =
                                        dataModel.name;
                                    viewModel.languageModel.description =
                                        dataModel.description;
                                    setState(() {});
                                  },
                                ),

                                SlugCard(
                                  slug: viewModel.model.slug,
                                ),

                                TypeCard(type: viewModel.model.type),

                                VisibilityFormCard(
                                    viewModel: viewModel,
                                    onUpdate: () async {
                                      viewModel.model = viewModel.dirtyModel;
                                      setState(() {});
                                    }),

                                OrderFormCard(
                                    viewModel: viewModel,
                                    onUpdate: () async {
                                      viewModel.model = viewModel.dirtyModel;
                                      setState(() {});
                                    }),

                                IdentityCard(
                                  blamable: viewModel.model.createdBy,
                                  datetime: viewModel.model.createdAt,
                                ),
                                IdentityCard(
                                    blamable: viewModel.model.updatedBy,
                                    datetime: viewModel.model.updatedAt),
                                // BOTTOMSHEET
                              ],
                            ),
                          ))),
                );
              case StoreState.loadingError:
                return ErrorPage(errorText: 'common.network_error'.tr());
              default:
                return ErrorPage(errorText: 'common.error'.tr());
            }
          }));
        });
  }

  Future<void> save(BuildContext context, LogViewModel viewModel) async {
    if (viewModel.canSubmit) {
      showProgressDialog(context);
      var response = await viewModel.update();
      await startProgress(context, response);
    }
  }

  Future<void> startProgress(
      BuildContext context, ResponseModel response) async {
    if (response.success) {
      Navigator.pop(context);
      showAlert(context, 'SUCCES', response.message, CustomAlertType.SUCCESS);
      await Future.delayed(Duration(milliseconds: 1500), () {
        Navigator.pop(context);
      });
      Navigator.pop(context, true);
    } else {
      Navigator.pop(context);
      showAlert(context, 'OOPS', response.message, CustomAlertType.ERROR);
      await Future.delayed(Duration(milliseconds: 1500), () {
        Navigator.pop(context);
        Navigator.pop(context, false);
      });
    }
  }

  void showProgressDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (_) {
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  void showAlert(BuildContext context, String title, String message,
      CustomAlertType type) {
    showDialog(
        context: context,
        builder: (_) {
          return CustomFormAlert(
              title: title, message: message, alertType: type);
        });
  }
}
