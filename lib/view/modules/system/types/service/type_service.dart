import 'package:fluttermvvmtemplate/core/base/service/base_service.dart';
import 'package:fluttermvvmtemplate/core/base/service/language_info_service.dart';
import 'package:fluttermvvmtemplate/core/constants/app/endpoints.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/http_client.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/request_model.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/response_model.dart';
import 'package:fluttermvvmtemplate/core/init/lang/language_manager.dart';
import 'package:fluttermvvmtemplate/core/init/notifier/data_type.dart';
import 'package:fluttermvvmtemplate/view/common/models/search_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/types/model/type_model.dart';
import 'package:fluttermvvmtemplate/core/constants/app/app_constants.dart';

class TypeService {
  HttpClient httpClient;

  BaseService service = BaseService(TypeModel());
  LanguageInfoService langService = LanguageInfoService();

  Future<ResponseModel> fetchList() async {
    var requestModel = RequestModel(Endpoints.SYSTEM_TYPE);
    return service.fetchList(requestModel);
  }

  Future<ResponseModel> fetchItem(String slug) async {
    var requestModel = RequestModel('${Endpoints.SYSTEM_TYPE + slug}');

    return service.fetchItem(requestModel);
  }

  Future<ResponseModel> deleteItem(
      String slug, Map<String, dynamic> body) async {
    var requestBody = {
      'data': [
        {'slug': body['slug'], 'dataType': DataType.shared.dataType}
      ]
    };

    var requestModel =
        RequestModel('${Endpoints.SYSTEM_TYPE}', body: requestBody);
    return service.deleteItem(requestModel);
  }

  Future<ResponseModel> updateItem(
      TypeModel item, Map<String, dynamic> body) async {
    List allowedFields = [
      'slug',
      'dataType',
      'name',
      'description',
      'order',
      'rank',
      'level',
      'isActive',
      'isPublic',
      'parent',
      'dataSlug'
    ];

    body.removeWhere((key, value) => !allowedFields.contains(key));

    body['dataType'] = DataType.shared.dataType;
    var requestBody = {
      'data': [body]
    };

    var requestModel =
        RequestModel('${Endpoints.SYSTEM_TYPE}', body: requestBody);
    return await service.updateItem(requestModel);
  }

  Future<ResponseModel> searchList(SearchModel searchModel) async {
    var lang = await LanguageManager.instance.currentLocale;

    var requestModel =
        RequestModel('${Endpoints.SYSTEM_TYPE + Endpoints.SEARCH}', body: {
      'name': searchModel.name,
      'language': searchModel.language,
      'dataType': DataType.shared.dataType,
      'isActive': searchModel.isActive,
      'isPublic': searchModel.isPublic,
    });

    return service.searchList(requestModel);
  }

  Future<ResponseModel<TypeModel>> createItem(Map<String, dynamic> body) async {
    body['dataType'] = DataType.shared.dataType;
    var requestBody = {
      'data': [body]
    };
    var requestModel =
        RequestModel('${Endpoints.SYSTEM_TYPE}', body: requestBody);
    var response = await service.createItem(requestModel);
    return ResponseModel<TypeModel>(
        data: TypeModel().fromJson(response.data),
        success: response.success,
        message: response.message);
  }

  // START LANGUAGE INFO SERVICE
  @override
  Future<ResponseModel> createLangInfo(Map<String, dynamic> body) async {
    body['dataType'] = ApplicationConstants.SYSTEM_TYPE;
    body['validationDataType'] = DataType.shared.dataType;

    var requestBody = {
      'data': [body]
    };
    var requestModel =
        RequestModel('${Endpoints.SYSTEM_INFO}', body: requestBody);

    return langService.create(requestModel);
  }

  @override
  Future<ResponseModel> updateLangInfo(Map<String, dynamic> body) async {
    body['dataType'] = ApplicationConstants.SYSTEM_TYPE;
    body['validationDataType'] = DataType.shared.dataType;

    var requestBody = {
      'data': [body]
    };
    var requestModel =
        RequestModel('${Endpoints.SYSTEM_INFO}', body: requestBody);

    return langService.update(requestModel);
  }
}
