import 'package:fluttermvvmtemplate/core/constants/app/app_constants.dart';

class TypeCustomValidations {
  final requestType;
  final model;

  TypeCustomValidations({this.requestType, this.model});

  Map<String, Map> validationData = {
    'name': {
      'required': false,
      'min': 3,
    },
    'description': {
      'required': false,
      'min': 3,
    },
  };

  presenter() {
    Map a = evaluation();
    // might be reformatted later

    return a;
  }

  evaluation() {
    if (requestType == ApplicationConstants.GET) {
      return {'status': true, 'message': 'bla bla'};
    } else if (requestType == ApplicationConstants.POST) {
      return {'status': true, 'message': ' '};
    } else if (requestType == ApplicationConstants.PUT) {
    } else if (requestType == ApplicationConstants.DELETE) {
    } else {
      // generic error
    }
  }

  Map getValidationData(String field) {
    return validationData[field];
  }
}
