import 'package:fluttermvvmtemplate/core/constants/app/app_constants.dart';

class TypeRules {
  final requestType;
  final model;

  TypeRules(this.requestType, this.model);

  presenter() {
    return evaluation();
  }

  evaluation() {
    if (requestType == ApplicationConstants.GET) {
      /**
    * rules will be implemented here step by step in a while loop or foreach
    * if you don't have any rules or validations return view
    * update return url by json retrieved from  backend
    **/
      return {'status': true, 'message': 'bla bla'};
    } else if (requestType == ApplicationConstants.POST) {
      return {'status': true, 'message': 'bla bla'};
    } else if (requestType == ApplicationConstants.PUT) {
      return {'status': true, 'message': 'bla bla'};
    } else if (requestType == ApplicationConstants.DELETE) {
      return {'status': true, 'message': 'bla bla'};
    } else {
      return {'status': true, 'message': 'bla bla'};
    }
  }

  static Map rule1() {
    return {'status': true, 'message': 'bla bla'};
  }

  static Map rule2() {
    return {'status': true, 'message': 'bla bla'};
  }
}
