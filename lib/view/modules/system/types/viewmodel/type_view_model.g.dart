// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'type_view_model.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$TypeViewModel on _TypeViewModelBase, Store {
  final _$itemsAtom = Atom(name: '_TypeViewModelBase.items');

  @override
  ObservableList<TypeModel> get items {
    _$itemsAtom.reportRead();
    return super.items;
  }

  @override
  set items(ObservableList<TypeModel> value) {
    _$itemsAtom.reportWrite(value, super.items, () {
      super.items = value;
    });
  }

  final _$filteredListItemsAtom =
      Atom(name: '_TypeViewModelBase.filteredListItems');

  @override
  ObservableList<TypeModel> get filteredListItems {
    _$filteredListItemsAtom.reportRead();
    return super.filteredListItems;
  }

  @override
  set filteredListItems(ObservableList<TypeModel> value) {
    _$filteredListItemsAtom.reportWrite(value, super.filteredListItems, () {
      super.filteredListItems = value;
    });
  }

  final _$modelAtom = Atom(name: '_TypeViewModelBase.model');

  @override
  TypeModel get model {
    _$modelAtom.reportRead();
    return super.model;
  }

  @override
  set model(TypeModel value) {
    _$modelAtom.reportWrite(value, super.model, () {
      super.model = value;
    });
  }

  final _$dirtyModelAtom = Atom(name: '_TypeViewModelBase.dirtyModel');

  @override
  TypeModel get dirtyModel {
    _$dirtyModelAtom.reportRead();
    return super.dirtyModel;
  }

  @override
  set dirtyModel(TypeModel value) {
    _$dirtyModelAtom.reportWrite(value, super.dirtyModel, () {
      super.dirtyModel = value;
    });
  }

  final _$languageModelAtom = Atom(name: '_TypeViewModelBase.languageModel');

  @override
  LanguageInfoModel get languageModel {
    _$languageModelAtom.reportRead();
    return super.languageModel;
  }

  @override
  set languageModel(LanguageInfoModel value) {
    _$languageModelAtom.reportWrite(value, super.languageModel, () {
      super.languageModel = value;
    });
  }

  final _$dirtyLanguageModelAtom =
      Atom(name: '_TypeViewModelBase.dirtyLanguageModel');

  @override
  LanguageInfoModel get dirtyLanguageModel {
    _$dirtyLanguageModelAtom.reportRead();
    return super.dirtyLanguageModel;
  }

  @override
  set dirtyLanguageModel(LanguageInfoModel value) {
    _$dirtyLanguageModelAtom.reportWrite(value, super.dirtyLanguageModel, () {
      super.dirtyLanguageModel = value;
    });
  }

  final _$paginationAtom = Atom(name: '_TypeViewModelBase.pagination');

  @override
  PaginationModel get pagination {
    _$paginationAtom.reportRead();
    return super.pagination;
  }

  @override
  set pagination(PaginationModel value) {
    _$paginationAtom.reportWrite(value, super.pagination, () {
      super.pagination = value;
    });
  }

  final _$searchModelAtom = Atom(name: '_TypeViewModelBase.searchModel');

  @override
  SearchModel get searchModel {
    _$searchModelAtom.reportRead();
    return super.searchModel;
  }

  @override
  set searchModel(SearchModel value) {
    _$searchModelAtom.reportWrite(value, super.searchModel, () {
      super.searchModel = value;
    });
  }

  final _$tabIndexAtom = Atom(name: '_TypeViewModelBase.tabIndex');

  @override
  int get tabIndex {
    _$tabIndexAtom.reportRead();
    return super.tabIndex;
  }

  @override
  set tabIndex(int value) {
    _$tabIndexAtom.reportWrite(value, super.tabIndex, () {
      super.tabIndex = value;
    });
  }

  final _$fetchItemsAsyncAction = AsyncAction('_TypeViewModelBase.fetchItems');

  @override
  Future<dynamic> fetchItems({int page = 1}) {
    return _$fetchItemsAsyncAction.run(() => super.fetchItems(page: page));
  }

  final _$searchItemsAsyncAction =
      AsyncAction('_TypeViewModelBase.searchItems');

  @override
  Future<ResponseModel<dynamic>> searchItems({dynamic page = 1}) {
    return _$searchItemsAsyncAction.run(() => super.searchItems(page: page));
  }

  final _$deleteAsyncAction = AsyncAction('_TypeViewModelBase.delete');

  @override
  Future<ResponseModel<dynamic>> delete(TypeModel typeModel) {
    return _$deleteAsyncAction.run(() => super.delete(typeModel));
  }

  final _$updateAsyncAction = AsyncAction('_TypeViewModelBase.update');

  @override
  Future<ResponseModel<dynamic>> update() {
    return _$updateAsyncAction.run(() => super.update());
  }

  final _$createAsyncAction = AsyncAction('_TypeViewModelBase.create');

  @override
  Future<ResponseModel<TypeModel>> create() {
    return _$createAsyncAction.run(() => super.create());
  }

  final _$viewAsyncAction = AsyncAction('_TypeViewModelBase.view');

  @override
  Future<ResponseModel<dynamic>> view() {
    return _$viewAsyncAction.run(() => super.view());
  }

  final _$updateLanguageInfoAsyncAction =
      AsyncAction('_TypeViewModelBase.updateLanguageInfo');

  @override
  Future<ResponseModel<dynamic>> updateLanguageInfo() {
    return _$updateLanguageInfoAsyncAction
        .run(() => super.updateLanguageInfo());
  }

  final _$createLanguageInfoAsyncAction =
      AsyncAction('_TypeViewModelBase.createLanguageInfo');

  @override
  Future<ResponseModel<dynamic>> createLanguageInfo() {
    return _$createLanguageInfoAsyncAction
        .run(() => super.createLanguageInfo());
  }

  final _$updateAndLoadLanguageInfoAsyncAction =
      AsyncAction('_TypeViewModelBase.updateAndLoadLanguageInfo');

  @override
  Future<ResponseModel<dynamic>> updateAndLoadLanguageInfo() {
    return _$updateAndLoadLanguageInfoAsyncAction
        .run(() => super.updateAndLoadLanguageInfo());
  }

  final _$createAndLoadLanguageInfoAsyncAction =
      AsyncAction('_TypeViewModelBase.createAndLoadLanguageInfo');

  @override
  Future<ResponseModel<dynamic>> createAndLoadLanguageInfo() {
    return _$createAndLoadLanguageInfoAsyncAction
        .run(() => super.createAndLoadLanguageInfo());
  }

  final _$_TypeViewModelBaseActionController =
      ActionController(name: '_TypeViewModelBase');

  @override
  void setLanguageModel(AvailableLanguagesModel value, int index) {
    final _$actionInfo = _$_TypeViewModelBaseActionController.startAction(
        name: '_TypeViewModelBase.setLanguageModel');
    try {
      return super.setLanguageModel(value, index);
    } finally {
      _$_TypeViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  Future<void> detail() {
    final _$actionInfo = _$_TypeViewModelBaseActionController.startAction(
        name: '_TypeViewModelBase.detail');
    try {
      return super.detail();
    } finally {
      _$_TypeViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
items: ${items},
filteredListItems: ${filteredListItems},
model: ${model},
dirtyModel: ${dirtyModel},
languageModel: ${languageModel},
dirtyLanguageModel: ${dirtyLanguageModel},
pagination: ${pagination},
searchModel: ${searchModel},
tabIndex: ${tabIndex}
    ''';
  }
}
