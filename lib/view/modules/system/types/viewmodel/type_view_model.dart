import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/base/model/base_view_model.dart';
import 'package:fluttermvvmtemplate/core/constants/app/app_constants.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/response_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/network_error_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/pagination_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/search_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/model/available_languages_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/model/language_info_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/types/model/type_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/types/service/type_service.dart';
import 'package:fluttermvvmtemplate/view/modules/system/types/viewmodel/type_process_manager.dart';
import 'package:mobx/mobx.dart';
import 'package:fluttermvvmtemplate/view/common/viewmodel/common_view_model.dart';

part 'type_view_model.g.dart';

class TypeViewModel = _TypeViewModelBase with _$TypeViewModel;

abstract class _TypeViewModelBase extends CommonViewModel
    with Store, BaseViewModel {
  TypeService feed = TypeService();
  final TextEditingController textEditingController = TextEditingController();

  @override
  void setContext(BuildContext context) => this.context = context;

  @override
  void init() {
    dirtyModel = TypeModel(isActive: true, isPublic: false);
  }

  // full list of items
  @observable
  ObservableList<TypeModel> items;

  // displays filtered items based on search made on list view
  @observable
  ObservableList<TypeModel> filteredListItems;

  @observable
  TypeModel model;

  @observable
  TypeModel dirtyModel;

  @observable
  LanguageInfoModel languageModel;

  @observable
  LanguageInfoModel dirtyLanguageModel;

  @observable
  PaginationModel pagination;

  @observable
  SearchModel searchModel = SearchModel();

  @observable
  int tabIndex = 1;

  int calculateTabIndex() {
    dynamic defaultLang =
        model.systemInfo.firstWhere((element) => element.isRef == true);

    tabIndex = availableLanguages
        .indexWhere((element) => element.langCode == defaultLang.language);
  }

  @action
  void setLanguageModel(AvailableLanguagesModel value, int index) {
    try {
      tabIndex = index;
      languageModel = model.systemInfo
          .firstWhere((element) => element.language == value.langCode);
    } catch (e) {
      languageModel =
          LanguageInfoModel(language: value.langCode, dataSlug: model.slug);
    }

    dirtyLanguageModel = LanguageInfoModel.clone(languageModel);
  }

  @action
  Future fetchItems({int page = 1}) async {
    var response = ResponseModel(success: false, message: '');
    try {
      var processManager = TypeProcessManager(
              requestType: ApplicationConstants.GET, model: TypeModel())
          .status();

      if (processManager) {
        isSearchActive = false;
        future = ObservableFuture(feed.fetchList());
        response = await future;
        pagination = response.pagination;

        items = ObservableList.of(List<TypeModel>.from(response.data));

        if (response.data.length > pagination.perPage) {
          filteredListItems =
              ObservableList.of(items.sublist(0, pagination.perPage));
        } else {
          filteredListItems = ObservableList.of(items);
        }

        future = ObservableFuture.value(FutureStatus.fulfilled);
        pagination.currentPage = page;

        var pageStart = pagination.perPage * (pagination.currentPage - 1);

        var pageEnd = pagination.perPage * pagination.currentPage;

        if (pageEnd > items.length) {
          pageEnd = items.length; // index starts from 0
        }

        filteredListItems =
            ObservableList.of(items.sublist(pageStart, pageEnd));
      }

      return response;
    } catch (e) {
      networkError = NetworkErrorModel(errorText: response.message);
    }
  }

  @action
  Future<ResponseModel> searchItems({page = 1}) async {
    var response = ResponseModel(success: false, message: " ");
    try {
      isSearchActive = true;
      future = ObservableFuture(feed.searchList(searchModel));
      response = await future as ResponseModel;
      pagination = response.pagination;
      items = ObservableList.of(List<TypeModel>.from(response.data));
      filteredListItems = items;
    } catch (e) {
      networkError = NetworkErrorModel(errorText: response.message);
    }
    return response;
  }

  @action
  Future<void> detail() {
    future = ObservableFuture(Future.wait([view(), getAvailableLanguages()])
        .then((value) => calculateTabIndex()));
  }

  @action
  Future<ResponseModel> delete(TypeModel typeModel) async {
    var processManager = TypeProcessManager(
            requestType: ApplicationConstants.DELETE, model: typeModel)
        .status();

    var response = ResponseModel(success: false, message: " ");

    if (processManager) {
      response = await feed.deleteItem(typeModel.slug, typeModel.toJson());
    }

    return response;
  }

  @action
  Future<ResponseModel> update() async {
    var response = ResponseModel(success: false, message: " ");

    response = await feed.updateItem(dirtyModel, dirtyModel.toJson());

    return response;
  }

  @action
  Future<ResponseModel<TypeModel>> create() async {
    var response = ResponseModel<TypeModel>(success: false, message: " ");

    response = await feed.createItem(model.toJson());
    return response;
  }

  @action
  Future<ResponseModel> view() async {
    var processManager = TypeProcessManager(
            requestType: ApplicationConstants.GET, model: TypeModel())
        .status();

    var response = ResponseModel(success: false, message: " ");

    if (processManager) {
      future = ObservableFuture(feed.fetchItem(model.slug));
      response = await future as ResponseModel;

      String slug = model.slug;
      model = response.data;

      dirtyModel = TypeModel.clone(model);
      model.slug = slug;
    }

    return response;
  }

  @action
  Future<ResponseModel> updateLanguageInfo() async {
    var processManager = TypeProcessManager(
            requestType: ApplicationConstants.PUT, model: dirtyLanguageModel)
        .status();

    var response = ResponseModel(success: false, message: " ");

    if (processManager) {
      response = await feed
          .updateLangInfo(LanguageInfoModel.toJson(model: dirtyLanguageModel));
    }

    return response;
  }

  @action
  Future<ResponseModel> createLanguageInfo() async {
    var processManager = TypeProcessManager(
            requestType: ApplicationConstants.POST, model: dirtyLanguageModel)
        .status();
    var response = ResponseModel(success: false, message: " ");

    if (processManager) {
      response = await feed
          .createLangInfo(LanguageInfoModel.toJson(model: dirtyLanguageModel));
    }

    return response;
  }

  @action
  Future<ResponseModel> updateAndLoadLanguageInfo() async {
    var responseModel = ResponseModel(success: false, message: " ");
    future = ObservableFuture(Future.wait([updateLanguageInfo(), view()]));
    await future.then((value) => responseModel = value[0]);

    return responseModel;
  }

  @action
  Future<ResponseModel> createAndLoadLanguageInfo() async {
    var responseModel = ResponseModel();
    future = ObservableFuture(Future.wait([createLanguageInfo(), view()]));
    await future.then((value) => responseModel = value[0]);

    return responseModel;
  }

  void filterList(String searchText) {
    if (searchText.isEmpty) {
      filteredListItems = items;
    } else {
      filteredListItems = items
          .where((element) =>
              element.name.toLowerCase().contains(searchText.toLowerCase()))
          .toList()
          .asObservable();
    }
  }
}
