import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/constants/app/app_constants.dart';

class TypeProcessManager {
  final requestType;
  final model;

  TypeProcessManager({Key key, this.requestType, this.model});

  bool status() {
    if (requestType == ApplicationConstants.GET) {
      /**
    * rules will be implemented here step by step in a while loop or foreach
    * if you don't have any rules or validations return view
    * update return url by json retrieved from  backend
    **/

      return getRulesAndValidationRequest();
    } else if (requestType == ApplicationConstants.POST) {
      return getRulesAndValidationRequest();
    } else if (requestType == ApplicationConstants.PUT) {
      return getRulesAndValidationRequest();
    } else if (requestType == ApplicationConstants.DELETE) {
      return getRulesAndValidationRequest();
    } else {
      return false;
    }
  }

  bool getRulesAndValidationRequest() {
    /* var rules = TypeRules(requestType, model).presenter();
    var validation =
        TypeCustomValidations(requestType: requestType, model: model)
            .presenter();

    // ignore: omit_local_variable_types
    bool result = false;

    if (rules['status'] && validation['status']) {
      result = true;
    }
*/
    return true;
  }
}
