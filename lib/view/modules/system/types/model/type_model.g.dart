// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'type_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TypeModel _$TypeModelFromJson(Map<String, dynamic> json) {
  return TypeModel(
    slug: json['slug'] ?? '',
    name: json['name'] ?? '',
    description: json['description'] ?? '',
    isActive: json['isActive'] ?? true,
    isPublic: json['isPublic'] ?? true,
    order: json['order'] as String,
    rank: json['rank'] as int,
    level: json['level'] as int,
    dataType: json['dataType'] as String ?? '',
    systemInfo: (json['systemInfo'] as List)
            ?.map((e) => e == null
                ? null
                : LanguageInfoModel.fromJson(e as Map<String, dynamic>))
            ?.toList() ??
        [],
    createdBy: json['createdBy'] == null
        ? null
        : BlamableModel.fromJson(json['createdBy'] as Map<String, dynamic>),
    updatedBy: json['updatedBy'] == null
        ? null
        : BlamableModel.fromJson(json['updatedBy'] as Map<String, dynamic>),
  )
    ..targetRoute = json['targetRoute'] as String
    ..id = json['id'] as int ?? 0
    ..createdAt = json['createdAt'] as String
    ..updatedAt = json['updatedAt'] as String;
}

Map<String, dynamic> _$TypeModelToJson(TypeModel instance) => <String, dynamic>{
      'name': instance.name,
      'description': instance.description,
      'slug': instance.slug,
      'isPublic': instance.isPublic,
      'isActive': instance.isActive,
      'targetRoute': instance.targetRoute,
      'id': instance.id,
      'order': instance.order,
      'rank': instance.rank,
      'level': instance.level,
      'dataType': instance.dataType,
      'systemInfo': instance.systemInfo,
      'createdBy': instance.createdBy,
      'updatedBy': instance.updatedBy,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
    };
