import 'package:fluttermvvmtemplate/view/common/models/blamable_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/model/language_info_model.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:fluttermvvmtemplate/core/base/model/base_model.dart';

part 'type_model.g.dart';

@JsonSerializable()
class TypeModel extends BaseModel<TypeModel> {
  @JsonKey(defaultValue: 0)
  int id;

  @JsonKey(defaultValue: null)
  String order;

  @JsonKey(defaultValue: null)
  int rank;

  @JsonKey(defaultValue: null)
  int level;

  @JsonKey(defaultValue: '')
  String dataType;

  @JsonKey(defaultValue: [])
  List<LanguageInfoModel> systemInfo;

  @JsonKey(nullable: true)
  BlamableModel createdBy;

  @JsonKey(nullable: true)
  BlamableModel updatedBy;

  @JsonKey(nullable: true)
  String createdAt;

  @JsonKey(nullable: true)
  String updatedAt;

  TypeModel(
      {slug,
      name,
      description,
      isActive = true,
      isPublic = true,
      this.order,
      this.rank,
      this.level,
      this.dataType,
      this.systemInfo,
      this.createdBy,
      this.updatedBy}) {
    {
      this.slug = slug;
      this.name = name;
      this.description = description;
      this.isActive = isActive;
      this.isPublic = isPublic;
    }
  }

  TypeModel.clone(TypeModel model) {
    name = model.name;
    description = model.description;
    order = model.order;
    rank = model.rank;
    level = model.level;
    dataType = model.dataType;
    slug = model.slug;
    isActive = model.isActive;
    isPublic = model.isPublic;
    systemInfo = model.systemInfo;
    createdBy = model.createdBy;
    updatedBy = model.updatedBy;
  }

  @override
  TypeModel fromJson(Map<String, dynamic> json) {
    return _$TypeModelFromJson(json);
  }

  @override
  Map<String, dynamic> toJson() {
    return _$TypeModelToJson(this);
  }
}
