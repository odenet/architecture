import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:fluttermvvmtemplate/core/base/view/base_widget.dart';
import 'package:fluttermvvmtemplate/core/components/card/identity_card.dart';
import 'package:fluttermvvmtemplate/core/components/card/name_form_card.dart';
import 'package:fluttermvvmtemplate/core/components/card/order_form_card.dart';
import 'package:fluttermvvmtemplate/core/components/card/visibility_form_card.dart';
import 'package:fluttermvvmtemplate/core/components/errors/error_page.dart';
import 'package:fluttermvvmtemplate/core/components/language_bar/available_language_bar.dart';
import 'package:fluttermvvmtemplate/core/constants/enums/store_state.dart';
import 'package:fluttermvvmtemplate/core/init/notifier/data_type.dart';
import 'package:fluttermvvmtemplate/view/modules/system/types/model/type_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/types/viewmodel/type_view_model.dart';
import 'package:easy_localization/easy_localization.dart';

class TypeUpdateView extends StatefulWidget {
  final String TypeType;
  final TypeModel model;
  final VoidCallback onUpdate;

  TypeUpdateView(this.model, {Key key, this.TypeType = '', this.onUpdate})
      : super(key: key);

  @override
  _TypeUpdateViewState createState() => _TypeUpdateViewState();
}

class _TypeUpdateViewState extends State<TypeUpdateView> {
  @override
  Widget build(BuildContext context) {
    return BaseView<TypeViewModel>(
        pageId: 25,
        objectId: 27,
        onModelReady: (viewModel) {
          viewModel.init();
          viewModel.setContext(context);
          viewModel.model = widget.model;
          viewModel.detail();
        },
        viewModel: TypeViewModel(),
        onPageBuilder: (context, viewModel) {
          return SafeArea(child: Observer(builder: (_) {
            switch (viewModel.state) {
              case StoreState.initial:
                return Center(child: CircularProgressIndicator());
              case StoreState.loading:
                return Center(child: CircularProgressIndicator());
              case StoreState.loaded:
                viewModel.languageModel ??= viewModel.model.systemInfo[0];
                return DefaultTabController(
                    initialIndex: viewModel.tabIndex,
                    length: viewModel.availableLanguages.length,
                    child: Scaffold(
                        appBar: AppBar(
                            title:
                                Text('${DataType.shared.dataTypeLabel}' + ' '+'system.type.title'.tr() +  ' ' + 'common.update'.tr()),
                            bottom: AvailableLanguageBar(
                                onTap: (index) => viewModel.setLanguageModel(
                                    viewModel.availableLanguages[index], index),
                                items: viewModel.availableLanguages)),
                        body: SingleChildScrollView(
                          child: Column(
                            children: <Widget>[
                              SizedBox(height: 10),

                              NameFormCard(
                                viewModel: viewModel,
                                onUpdate: (dataModel) {
                                  viewModel.languageModel.name = dataModel.name;
                                  viewModel.languageModel.description =
                                      dataModel.description;
                                  setState(() {});
                                },
                              ),

                              VisibilityFormCard(
                                  viewModel: viewModel,
                                  onUpdate: () async {
                                    viewModel.model = viewModel.dirtyModel;
                                    setState(() {});
                                  }),

                              OrderFormCard(
                                  viewModel: viewModel,
                                  onUpdate: () async {
                                    setState(() {});
                                  }),

                              IdentityCard(
                                blamable: viewModel.model.createdBy,
                                datetime: viewModel.model.createdAt,
                              ),

                              IdentityCard(
                                  blamable: viewModel.model.updatedBy,
                                  datetime: viewModel.model.updatedAt),
                              // BOTTOMSHEET
                            ],
                          ),
                        )));
              case StoreState.loadingError:
                return ErrorPage(errorText: 'common.network_error'.tr());
              default:
                return ErrorPage(errorText: 'common.error'.tr());
            }
          }));
        });
  }
}
