import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:fluttermvvmtemplate/core/base/view/base_widget.dart';
import 'package:fluttermvvmtemplate/core/components/errors/error_page.dart';
import 'package:fluttermvvmtemplate/core/components/form/empty_page.dart';
import 'package:fluttermvvmtemplate/core/components/form/pagination_bar.dart';
import 'package:fluttermvvmtemplate/core/components/form/search_bar.dart';
import 'package:fluttermvvmtemplate/core/components/list-view/bottom_sheet_list_view.dart';
import 'package:fluttermvvmtemplate/core/constants/app/app_constants.dart';
import 'package:fluttermvvmtemplate/core/constants/enums/store_state.dart';
import 'package:fluttermvvmtemplate/core/helpers/alert_helper.dart';
import 'package:fluttermvvmtemplate/core/init/notifier/data_type.dart';
import 'package:fluttermvvmtemplate/view/modules/system/object/view/object_home.dart';
import 'package:fluttermvvmtemplate/view/modules/system/types/model/type_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/types/view/type_create_view.dart';
import 'package:fluttermvvmtemplate/view/modules/system/types/view/type_update_view.dart';
import 'package:fluttermvvmtemplate/view/modules/system/types/viewmodel/type_view_model.dart';
import 'package:page_transition/page_transition.dart';
import 'package:easy_localization/easy_localization.dart';

class TypeView extends StatelessWidget {
  TypeView({Key key}) : super(key: key);

  List<dynamic> items;
  List<dynamic> filteredItems;
  var selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    DataType.shared.dataType = ApplicationConstants.OBJECT;
    DataType.shared.dataTypeLabel = ApplicationConstants.OBJECT_LABEL;

    return BaseView<TypeViewModel>(
        pageId: 25,
        objectId: 27,
        viewModel: TypeViewModel(),
        onModelReady: (model) {
          model.setContext(context);
          model.init();
          model.fetchItems();
        },
        onPageBuilder: (BuildContext context, TypeViewModel viewModel) {
          return Scaffold(
              resizeToAvoidBottomInset: false,
              backgroundColor: Colors.transparent,
              appBar: AppBar(
                leading: IconButton(
                    icon: Icon(Icons.view_module),
                    onPressed: () {
                      Navigator.pushAndRemoveUntil(
                          context,
                          PageTransition(
                              type: PageTransitionType.rightToLeft,
                              child: ObjectHome()),
                          (route) => false);
                    }),
                title: Text('${DataType.shared.dataTypeLabel} ' +
                    'system.type.list_title'.tr()),
                actions: [
                  IconButton(
                      icon: Icon(Icons.add_circle_outline),
                      onPressed: () {
                        Navigator.push(
                            context,
                            PageTransition(
                                type: PageTransitionType.rightToLeft,
                                child: TypeCreateView(onUpdate: (model) {
                                  viewModel.filteredListItems.add(model);
                                  //viewModel.fetchItems();
                                })));
                      })
                ],
              ),
              body: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    HomeSearchBar(
                        searchModel: viewModel.searchModel,
                        onClose: () {
                          viewModel.fetchItems();
                        },
                        onSearch: (text) {
                          viewModel.searchItems();
                        }),
                    Expanded(child: Observer(builder: (_) {
                      switch (viewModel.state) {
                        case StoreState.initial:
                          return Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CircularProgressIndicator(),
                              Text('common.loading'.tr()),
                            ],
                          );

                        case StoreState.loading:
                          return Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Center(child: CircularProgressIndicator()),
                            ],
                          );

                        case StoreState.loaded:
                          if (viewModel.filteredListItems.isNotEmpty) {
                            return Column(
                              children: [
                                Expanded(
                                  child: BottomSheetListView(
                                      list: viewModel.filteredListItems,
                                      selectedActions: [
                                        SelectedAction.UPDATE,
                                        SelectedAction.DELETE,
                                      ],
                                      onSelectAction: (action, model) async {
                                        if (action == SelectedAction.DELETE) {
                                          await AlertHelper.shared
                                              .deleteDetector(context,
                                                  () async {
                                            viewModel.filteredListItems
                                                .removeWhere((element) =>
                                                    element.slug == model.slug);
                                            await viewModel
                                                .delete(model)
                                                .then((value) async {
                                              if (!value.success) {
                                                await AlertHelper.shared
                                                    .startProgress(
                                                        context, value);
                                              }
                                            });
                                          });
                                        } else if (action ==
                                            SelectedAction.UPDATE) {
                                          Navigator.push(
                                              context,
                                              PageTransition(
                                                  type: PageTransitionType
                                                      .rightToLeft,
                                                  child:
                                                      TypeUpdateView(model)));
                                        }
                                      },
                                      onTap: (index) {
                                        selectedIndex = index;
                                      }),
                                ),
                                // PAGINATION
                                PaginationBar(
                                    paginationModel: viewModel.pagination,
                                    fetchFunction: viewModel.isSearchActive
                                        ? viewModel.searchItems
                                        : viewModel.fetchItems),
                              ],
                            );
                          }
                          return EmptyView(
                              isSearchActive: viewModel.searchModel.name.isEmpty
                                  ? false
                                  : true,
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.rightToLeft,
                                        child: TypeCreateView(
                                            onUpdate: (TypeModel model) {
                                          viewModel.filteredListItems
                                              .add(model);
                                        })));
                              },
                              buttonText: 'system.type.create'.tr());

                        case StoreState.loadingError:
                          return Center(
                            child: ErrorPage(
                                errorText: 'common.network_error'.tr()),
                          );

                        default:
                          return ErrorPage(errorText: 'common.error'.tr());
                      }
                    })),
                  ]));
        });
  }

  void navigate({BuildContext context, TypeViewModel vm, int index}) {
    Navigator.push(
      context,
      PageTransition(
          type: PageTransitionType.rightToLeft,
          child: TypeUpdateView(
            vm.filteredListItems[index],
            onUpdate: () {
              vm.fetchItems();
            },
          )),
    );
  }
}
