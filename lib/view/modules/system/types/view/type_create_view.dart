import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/base/view/base_widget.dart';
import 'package:fluttermvvmtemplate/core/components/appbar/flag_appbar.dart';
import 'package:fluttermvvmtemplate/core/components/form/base_form.dart';
import 'package:fluttermvvmtemplate/core/helpers/alert_helper.dart';
import 'package:fluttermvvmtemplate/core/init/notifier/data_type.dart';
import 'package:fluttermvvmtemplate/core/validator/custom_validator.dart';
import 'package:fluttermvvmtemplate/view/modules/system/types/model/type_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/types/viewmodel/type_custom_validations.dart';
import 'package:fluttermvvmtemplate/view/modules/system/types/viewmodel/type_view_model.dart';
import 'package:easy_localization/easy_localization.dart';

class TypeCreateView extends StatefulWidget {
  final Function(TypeModel model) onUpdate;

  TypeCreateView({Key key, this.onUpdate}) : super(key: key);

  @override
  _TypeCreateViewState createState() => _TypeCreateViewState();
}

class _TypeCreateViewState extends State<TypeCreateView> {
  final _formKey = GlobalKey<FormState>();

  bool isSpec = false;

  @override
  Widget build(BuildContext context) {
    return BaseView<TypeViewModel>(
        pageId: 25,
        objectId: 27,
        onModelReady: (viewModel) {
          viewModel.init();
          viewModel.setContext(context);
          viewModel.model = TypeModel();
        },
        viewModel: TypeViewModel(),
        onPageBuilder: (context, viewModel) {
          return SafeArea(
            child: Scaffold(
              resizeToAvoidBottomInset: false,
              appBar: FlagAppBar(title: Text('${DataType.shared.dataTypeLabel + ' ' + 'system.type.create'.tr()}'),),
              body: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          onChanged: (value) {
                            viewModel.model.name = value;
                          },
                          decoration: InputDecoration(
                              hintText: 'system.type.form.name'.tr(),
                              border: OutlineInputBorder()),
                          validator: (str) {
                            return CustomValidator.selectAndRunFunction(
                                TypeCustomValidations()
                                    .getValidationData('name'),
                                str);
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          onChanged: (value) {
                            viewModel.model.description = value;
                          },
                          decoration: InputDecoration(
                              hintText: 'system.type.form.description'.tr(),
                              border: OutlineInputBorder()),
                          validator: (str) {
                            return CustomValidator.selectAndRunFunction(
                                TypeCustomValidations()
                                    .getValidationData('description'),
                                str);
                          },
                        ),
                      ),
                      CheckboxListTile(
                          value: viewModel.dirtyModel.isActive,
                          onChanged: (value) {
                            setState(() {
                              viewModel.dirtyModel.isActive = value;
                              viewModel.isEdit = true;
                            });
                          },
                          title: Text('common.active'.tr())),
                      CheckboxListTile(
                        value: viewModel.dirtyModel.isPublic,
                        title: Text('common.public'.tr()),
                        onChanged: (value) {
                          setState(() {
                            viewModel.dirtyModel.isPublic = value;
                            viewModel.isEdit = true;
                          });
                        },
                      ),
                      CustomNormalButton(
                        onPressed: () async {
                          if (_formKey.currentState.validate()) {
                            viewModel.model.isActive = viewModel.dirtyModel.isActive;
                            viewModel.model.isPublic = viewModel.dirtyModel.isPublic;
                            AlertHelper.shared.showProgressDialog(context);
                            var response = await viewModel.create();
                            await AlertHelper.shared.startProgress(context, response);

                            Navigator.pop(context); // remove alert

                            if (response.success) {
                              widget.onUpdate(response.data);
                              Navigator.pop(context); // remove loading
                              Navigator.pop(context); // remove create form
                            }
                          }
                        },
                        text: 'system.type.create'.tr(),
                      ),
                    ],
                  )),
            ),
          );
        });
  }
}
