import 'package:fluttermvvmtemplate/core/base/service/base_service.dart';
import 'package:fluttermvvmtemplate/core/base/service/language_info_service.dart';
import 'package:fluttermvvmtemplate/core/constants/app/app_constants.dart';
import 'package:fluttermvvmtemplate/core/constants/app/endpoints.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/request_model.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/response_model.dart';
import 'package:fluttermvvmtemplate/core/init/lang/language_manager.dart';
import 'package:fluttermvvmtemplate/core/init/notifier/data_type.dart';
import 'package:fluttermvvmtemplate/view/common/models/related_category_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/related_feature_value_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/search_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/model/feature_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/object/model/object_list_model.dart';

class ObjectControlService {
  BaseService service = BaseService(ObjectListModel());
  LanguageInfoService langService = LanguageInfoService();

  @override
  Future<ResponseModel> fetchObjectList({int page = 1}) async {
    var requestModel = RequestModel(Endpoints.OBJECTS,
        params: {'page': page, 'perPage': ApplicationConstants.PER_PAGE});
    return service.fetchList(requestModel);
  }

  @override
  Future<ResponseModel> fetchObject(String slug) async {
    var requestModel = RequestModel('${Endpoints.OBJECTS + slug}');

    return service.fetchItem(requestModel);
  }

  Future<ResponseModel> deleteObject(Map body) async {
    body['dataType'] = DataType.shared.dataType;

    List allowedFields = [
      'slug',
      'dataType',
      'name',
      'description',
      'order',
      'rank',
      'level',
      'isActive',
      'isPublic',
      'parent',
      'type'
    ];

    body.removeWhere((key, value) => !allowedFields.contains(key));

    var requestBody = {
      'data': [body]
    };
    var requestModel = RequestModel('${Endpoints.OBJECTS}', body: requestBody);
    return service.deleteItem(requestModel);
  }

  Future<ResponseModel> deleteRelatedCategory(List data) async {
    String dataType = DataType.shared.dataType;
    var body = data.map((e) => {'slug': e, 'dataType': dataType}).toList();

    var requestBody = {'data': body};
    var requestModel =
        RequestModel('${Endpoints.RELATED_CATEGORY}', body: requestBody);
    return service.deleteItem(requestModel);
  }

  Future<ResponseModel> searchList(SearchModel searchModel,
      {int page = 1}) async {
    var lang = await LanguageManager.instance.currentLocale;

    var requestModel =
        RequestModel('${Endpoints.OBJECTS + Endpoints.SEARCH}', body: {
      'name': searchModel.name,
      'language': searchModel.language,
      'page': page,
      'isActive': searchModel.isActive,
      'isPublic': searchModel.isPublic,
      'dataType': DataType.shared.dataType
    });

    return service.searchList(requestModel);
  }

  @override
  Future<ResponseModel> createObject(Map body) async {
    body['dataType'] = DataType.shared.dataType;
    var requestBody = {
      'data': [body]
    };
    var requestModel = RequestModel('${Endpoints.OBJECTS}', body: requestBody);
    return service.createItem(requestModel);
  }

  @override
  Future<ResponseModel> updateObject(Map body) async {
    List allowedFields = [
      'slug',
      'dataType',
      'name',
      'description',
      'order',
      'rank',
      'level',
      'isActive',
      'isPublic',
      'parent',
    ];

    body.removeWhere((key, value) => !allowedFields.contains(key));

    body['dataType'] = DataType.shared.dataType;
    var requestBody = {
      'data': [body]
    };

    var requestModel = RequestModel('${Endpoints.OBJECTS}', body: requestBody);
    return service.updateItem(requestModel);
  }

  // RELATED CATEGORIES

  Future<ResponseModel> updateObjectCategory(List body) async {
    var requestBody = {'data': body};
    var requestModel =
        RequestModel('${Endpoints.RELATED_CATEGORY}', body: requestBody);
    return service.updateItem(requestModel);
  }

  Future<ResponseModel> createObjectCategory(List body) async {
    var requestBody = {'data': body};
    var requestModel =
        RequestModel('${Endpoints.RELATED_CATEGORY}', body: requestBody);
    return service.createItem(requestModel);
  }

  // RELATED FEATURES
  Future<ResponseModel> updateObjectFeature(List body) async {
    var requestBody = {'data': body};
    var requestModel =
        RequestModel('${Endpoints.RELATED_FEATURES}', body: requestBody);
    return service.updateItem(requestModel);
  }

  // update of feature values will be one by one
  Future<ResponseModel> updateObjectFeatureValue(
      Map<String, dynamic> body) async {
    body['dataType'] = DataType.shared.dataType;
    var requestBody = {
      'data': [body]
    };
    var requestModel =
        RequestModel('${Endpoints.RELATED_FEATURES}', body: requestBody);
    return service.updateItem(requestModel);
  }

  Future<ResponseModel> createObjectFeature(List body) async {
    var requestBody = {'data': body};
    var requestModel =
        RequestModel('${Endpoints.RELATED_FEATURES}', body: requestBody);
    return service.createItem(requestModel);
  }

  // START LANGUAGE INFO SERVICE

  Future<ResponseModel> createLangInfo(Map body) async {
    body['dataType'] = ApplicationConstants.OBJECT;
    body['validationDataType'] = ApplicationConstants.OBJECT;

    var requestBody = {
      'data': [body]
    };
    var requestModel =
        RequestModel('${Endpoints.SYSTEM_INFO}', body: requestBody);
    return langService.create(requestModel);
  }

  Future<ResponseModel> updateLangInfo(Map body) async {
    body['dataType'] = ApplicationConstants.OBJECT;
    body['validationDataType'] = ApplicationConstants.OBJECT;

    var requestBody = {
      'data': [body]
    };

    var requestModel =
        RequestModel('${Endpoints.SYSTEM_INFO}', body: requestBody);
    return langService.update(requestModel);
  }

  Future<ResponseModel> getFeatures(ObjectListModel model,
      RelatedCategoryModel mainCategory, String language) {
    BaseService featureService = BaseService(FeatureModel());

    var map = {
      'categorySlug': mainCategory.categorySlug,
      'dataSlug': model.slug,
      'language': language
    };

    var requestModel =
        RequestModel('${Endpoints.CATEGORY_RELATED_FEATURES}', params: map);
    return featureService.fetchList(requestModel);
  }

  Future<ResponseModel> getRelatedFeatureValues(ObjectListModel model) {
    BaseService featureService = BaseService(
        RelatedFeatureValueModel(dataType: DataType.shared.dataType));

    var requestParams = {
      'dataSlug': model.slug,
    };

    var requestModel =
        RequestModel('${Endpoints.RELATED_FEATURES}', params: requestParams);

    return featureService.fetchList(requestModel);
  }
}
