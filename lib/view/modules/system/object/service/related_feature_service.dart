import 'package:fluttermvvmtemplate/core/base/service/base_service.dart';
import 'package:fluttermvvmtemplate/core/base/service/language_info_service.dart';
import 'package:fluttermvvmtemplate/core/constants/app/endpoints.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/request_model.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/response_model.dart';
import 'package:fluttermvvmtemplate/core/init/notifier/data_type.dart';
import 'package:fluttermvvmtemplate/view/common/models/related_feature_value_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/object/model/object_list_model.dart';

class RelatedFeatureService {
  BaseService service =
      BaseService(RelatedFeatureValueModel(dataType: DataType.shared.dataType));
  LanguageInfoService langService = LanguageInfoService();

  @override
  Future<ResponseModel> fetchList(
      ObjectListModel model, String language) async {
    BaseService featureService = BaseService(
        RelatedFeatureValueModel(dataType: DataType.shared.dataType));

    var requestParams = {
      'dataSlug': model.slug,
      'dataType': DataType.shared.dataType,
      'language': language,
    };

    var requestModel = RequestModel(
        '${Endpoints.CATEGORY_RELATED_FEATURES_VALUE}',
        params: requestParams);

    return featureService.fetchList(requestModel);
  }

  @override
  Future<ResponseModel> fetchObject(String slug) async {
    var requestModel = RequestModel('${Endpoints.OBJECTS + slug}');

    return service.fetchItem(requestModel);
  }

  @override
  Future<ResponseModel> deleteItem(Map body) async {
    List allowedFields = [
      'slug',
      'dataType',
    ];

    body.removeWhere((key, value) => !allowedFields.contains(key));

    body['dataType'] = DataType.shared.dataType;
    var requestBody = {
      'data': [body]
    };
    var requestModel =
        RequestModel('${Endpoints.RELATED_FEATURES}', body: requestBody);
    return service.deleteItem(requestModel);
  }

  @override
  Future<ResponseModel> createItem(Map body) async {
    body['dataType'] = DataType.shared.dataType;
    var requestBody = {
      'data': [body]
    };
    var requestModel = RequestModel('${Endpoints.OBJECTS}', body: requestBody);
    return service.createItem(requestModel);
  }

  @override
  Future<ResponseModel> updateItem(Map body) async {
    List allowedFields = [
      'slug',
      'dataType',
      'order',
      'rank',
      'level',
      'isActive',
      'isPublic',
      'featureLabel',
      'featureValue'
    ];

    body.removeWhere((key, value) => !allowedFields.contains(key));

    body['dataType'] = DataType.shared.dataType;
    var requestBody = {
      'data': [body]
    };

    var requestModel =
        RequestModel('${Endpoints.RELATED_FEATURES}', body: requestBody);
    return service.updateItem(requestModel);
  }
}
