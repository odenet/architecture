// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'object_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ObjectListModel _$ObjectListModelFromJson(Map<String, dynamic> json) {
  return ObjectListModel(
    slug: json['slug'] ?? '',
    name: json['name'] ?? '',
    description: json['description'] ?? '',
    isActive: json['isActive'] ?? true,
    isPublic: json['isPublic'] ?? true,
    id: json['id'] as int ?? -1,
    type: json['type'] as String ?? '',
    dataType: json['dataType'] as String ?? '',
    path: json['path'] as String ?? '',
    link: json['link'] as String ?? '',
    message: json['message'] as String ?? '',
    createdBy: json['createdBy'] == null
        ? null
        : BlamableModel.fromJson(json['createdBy'] as Map<String, dynamic>),
    updatedBy: json['updatedBy'] == null
        ? null
        : BlamableModel.fromJson(json['updatedBy'] as Map<String, dynamic>),
    order: json['order'] as String,
    level: json['level'] as int,
    rank: json['rank'] as int,
    createdAt: json['createdAt'] as String ?? '',
    systemInfo: (json['systemInfo'] as List)
            ?.map((e) => e == null
                ? null
                : LanguageInfoModel.fromJson(e as Map<String, dynamic>))
            ?.toList() ??
        [],
    relatedCategories: (json['relatedCategories'] as List)
            ?.map((e) => e == null
                ? null
                : RelatedCategoryModel.fromJson(e as Map<String, dynamic>))
            ?.toList() ??
        [],
    relatedFeatures: (json['relatedFeatures'] as List)
            ?.map((e) => e == null
                ? null
                : RelatedFeatureValueModel().fromJson(e as Map<String, dynamic>))
            ?.toList() ??
        [],
    updatedAt: json['updatedAt'] as String ?? '',
  )
    ..targetRoute = json['targetRoute'] as String
    ..embededCode = json['embededCode'] as String ?? '';
}

Map<String, dynamic> _$ObjectListModelToJson(ObjectListModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'description': instance.description,
      'slug': instance.slug,
      'isPublic': instance.isPublic,
      'isActive': instance.isActive,
      'targetRoute': instance.targetRoute,
      'id': instance.id,
      'type': instance.type,
      'dataType': instance.dataType,
      'path': instance.path,
      'link': instance.link,
      'embededCode': instance.embededCode,
      'message': instance.message,
      'createdBy': instance.createdBy,
      'updatedBy': instance.updatedBy,
      'order': instance.order,
      'level': instance.level,
      'rank': instance.rank,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
      'systemInfo': instance.systemInfo,
      'relatedCategories': instance.relatedCategories,
      'relatedFeatures': instance.relatedFeatures,
    };
