import 'package:fluttermvvmtemplate/view/common/models/blamable_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/related_category_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/related_feature_value_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/model/language_info_model.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:fluttermvvmtemplate/core/base/model/base_model.dart';

part 'object_list_model.g.dart';

@JsonSerializable()
class ObjectListModel extends BaseModel<ObjectListModel> {
  @JsonKey(defaultValue: -1)
  int id;

  @JsonKey(defaultValue: '')
  String type;

  @JsonKey(defaultValue: '')
  String dataType;

  @JsonKey(defaultValue: '')
  String path;

  @JsonKey(defaultValue: '')
  String link;

  @JsonKey(defaultValue: '')
  String embededCode;

  @JsonKey(defaultValue: '')
  String message;

  @JsonKey(nullable: true)
  BlamableModel createdBy;

  @JsonKey(nullable: true)
  BlamableModel updatedBy;

  @JsonKey(nullable: true)
  String order;

  @JsonKey(nullable: true)
  int level;

  @JsonKey(nullable: true)
  int rank;

  @JsonKey(defaultValue: '')
  String createdAt;

  @JsonKey(defaultValue: '')
  String updatedAt;

  @JsonKey(defaultValue: [])
  List<LanguageInfoModel> systemInfo;

  @JsonKey(defaultValue: [])
  List<RelatedCategoryModel> relatedCategories;

  @JsonKey(defaultValue: [])
  List<RelatedFeatureValueModel> relatedFeatures;

  ObjectListModel(
      {slug,
      name,
      description,
      isActive = true,
      isPublic = true,
      this.id,
      this.type,
      this.dataType,
      this.path,
      this.link,
      this.message,
      this.createdBy,
      this.updatedBy,
      this.order,
      this.level,
      this.rank,
      this.createdAt,
      this.systemInfo,
      this.relatedCategories,
      this.relatedFeatures,
      this.updatedAt}) {
    this.slug = slug;
    this.name = name;
    this.description = description;
    this.isActive = isActive;
    this.isPublic = isPublic;
  }

  ObjectListModel.clone(ObjectListModel model) {
    id = model.id;
    name = model.name;
    type = model.type;
    description = model.description;
    dataType = model.dataType;
    path = model.path;
    link = model.link;
    message = model.message;
    isPublic = model.isPublic;
    isActive = model.isActive;
    createdBy = model.createdBy;
    updatedBy = model.updatedBy;
    slug = model.slug;
    order = model.order;
    level = model.level;
    rank = model.rank;
    createdAt = model.createdAt;
    systemInfo = model.systemInfo;
    relatedCategories = model.relatedCategories;
    updatedAt = model.updatedAt;
    relatedFeatures = model.relatedFeatures;
  }

  @override
  ObjectListModel fromJson(var json) {
    return _$ObjectListModelFromJson(json);
  }

  @override
  Map<String, dynamic> toJson() {
    return _$ObjectListModelToJson(this);
  }
}
