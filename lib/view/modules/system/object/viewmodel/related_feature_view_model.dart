import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/base/model/base_view_model.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/response_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/network_error_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/related_feature_value_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/model/available_languages_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/object/model/object_list_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/object/service/related_feature_service.dart';
import 'package:mobx/mobx.dart';
import 'package:fluttermvvmtemplate/view/common/viewmodel/common_view_model.dart';

part 'related_feature_view_model.g.dart';

class RelatedFeatureViewModel = _RelatedFeatureViewModelBase
    with _$RelatedFeatureViewModel;

abstract class _RelatedFeatureViewModelBase extends CommonViewModel
    with Store, BaseViewModel {
  RelatedFeatureService feed = RelatedFeatureService();

  @override
  void setContext(BuildContext context) => this.context = context;

  @override
  void init() {}

  @observable
  ObservableList<dynamic> valueItems;

  @observable
  bool isEdit = false;

  @observable
  RelatedFeatureValueModel model;

  @observable
  RelatedFeatureValueModel dirtyModel;

  @observable
  dynamic objectListModel;

  @observable
  AvailableLanguagesModel languageModel;

  @action
  void setIsEdit(bool value) {
    isEdit = value;
  }

  @observable
  int tabIndex = 0;

  @action
  Future detail() {
    future = ObservableFuture(getAvailableLanguages()).then((value) {
      // set languageModel
      languageModel = availableLanguages[tabIndex];
      fetchItems();
    });
  }

  @action
  Future<void> setLanguageModel(
      AvailableLanguagesModel value, int index) async {
    tabIndex = index;
    languageModel = value;

    var response;
    try {
      var listFuture = ObservableFuture(
          feed.fetchList(objectListModel, languageModel.langCode));
      response = await listFuture;

      valueItems = ObservableList.of(response.data);
    } catch (e) {
      networkError = NetworkErrorModel(errorText: response.message);
    }
  }

  @action
  Future fetchItems() async {
    var response = ResponseModel(success: false, message: " ");
    try {
      future = ObservableFuture(
          feed.fetchList(objectListModel, languageModel.langCode));
      response = await future as ResponseModel;

      valueItems = ObservableList.of(response.data);
    } catch (e) {
      networkError = NetworkErrorModel(errorText: response.message);
    }

    return response;
  }

  @action
  Future<ResponseModel> delete(RelatedFeatureValueModel model) async {
    var response = ResponseModel(success: false, message: " ");

    response = await feed.deleteItem(model.toJson());

    return response;
  }

  @action
  Future<ResponseModel> update() async {
    var response = ResponseModel(success: false, message: " ");

    response = await feed.updateItem(dirtyModel.toJson());

    return response;
  }

  @action
  Future<ResponseModel> create() async {
    var response = ResponseModel(success: false, message: " ");

    response = await feed.createItem(model.toJson());

    return response;
  }
}
