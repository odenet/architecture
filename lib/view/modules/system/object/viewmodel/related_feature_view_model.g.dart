// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'related_feature_view_model.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$RelatedFeatureViewModel on _RelatedFeatureViewModelBase, Store {
  final _$valueItemsAtom =
      Atom(name: '_RelatedFeatureViewModelBase.valueItems');

  @override
  ObservableList<dynamic> get valueItems {
    _$valueItemsAtom.reportRead();
    return super.valueItems;
  }

  @override
  set valueItems(ObservableList<dynamic> value) {
    _$valueItemsAtom.reportWrite(value, super.valueItems, () {
      super.valueItems = value;
    });
  }

  final _$isEditAtom = Atom(name: '_RelatedFeatureViewModelBase.isEdit');

  @override
  bool get isEdit {
    _$isEditAtom.reportRead();
    return super.isEdit;
  }

  @override
  set isEdit(bool value) {
    _$isEditAtom.reportWrite(value, super.isEdit, () {
      super.isEdit = value;
    });
  }

  final _$modelAtom = Atom(name: '_RelatedFeatureViewModelBase.model');

  @override
  RelatedFeatureValueModel get model {
    _$modelAtom.reportRead();
    return super.model;
  }

  @override
  set model(RelatedFeatureValueModel value) {
    _$modelAtom.reportWrite(value, super.model, () {
      super.model = value;
    });
  }

  final _$dirtyModelAtom =
      Atom(name: '_RelatedFeatureViewModelBase.dirtyModel');

  @override
  RelatedFeatureValueModel get dirtyModel {
    _$dirtyModelAtom.reportRead();
    return super.dirtyModel;
  }

  @override
  set dirtyModel(RelatedFeatureValueModel value) {
    _$dirtyModelAtom.reportWrite(value, super.dirtyModel, () {
      super.dirtyModel = value;
    });
  }

  final _$objectListModelAtom =
      Atom(name: '_RelatedFeatureViewModelBase.objectListModel');

  @override
  dynamic get objectListModel {
    _$objectListModelAtom.reportRead();
    return super.objectListModel;
  }

  @override
  set objectListModel(dynamic value) {
    _$objectListModelAtom.reportWrite(value, super.objectListModel, () {
      super.objectListModel = value;
    });
  }

  final _$languageModelAtom =
      Atom(name: '_RelatedFeatureViewModelBase.languageModel');

  @override
  AvailableLanguagesModel get languageModel {
    _$languageModelAtom.reportRead();
    return super.languageModel;
  }

  @override
  set languageModel(AvailableLanguagesModel value) {
    _$languageModelAtom.reportWrite(value, super.languageModel, () {
      super.languageModel = value;
    });
  }

  final _$tabIndexAtom = Atom(name: '_RelatedFeatureViewModelBase.tabIndex');

  @override
  int get tabIndex {
    _$tabIndexAtom.reportRead();
    return super.tabIndex;
  }

  @override
  set tabIndex(int value) {
    _$tabIndexAtom.reportWrite(value, super.tabIndex, () {
      super.tabIndex = value;
    });
  }

  final _$setLanguageModelAsyncAction =
      AsyncAction('_RelatedFeatureViewModelBase.setLanguageModel');

  @override
  Future<void> setLanguageModel(AvailableLanguagesModel value, int index) {
    return _$setLanguageModelAsyncAction
        .run(() => super.setLanguageModel(value, index));
  }

  final _$fetchItemsAsyncAction =
      AsyncAction('_RelatedFeatureViewModelBase.fetchItems');

  @override
  Future<dynamic> fetchItems() {
    return _$fetchItemsAsyncAction.run(() => super.fetchItems());
  }

  final _$deleteAsyncAction =
      AsyncAction('_RelatedFeatureViewModelBase.delete');

  @override
  Future<ResponseModel<dynamic>> delete(RelatedFeatureValueModel model) {
    return _$deleteAsyncAction.run(() => super.delete(model));
  }

  final _$updateAsyncAction =
      AsyncAction('_RelatedFeatureViewModelBase.update');

  @override
  Future<ResponseModel<dynamic>> update() {
    return _$updateAsyncAction.run(() => super.update());
  }

  final _$createAsyncAction =
      AsyncAction('_RelatedFeatureViewModelBase.create');

  @override
  Future<ResponseModel<dynamic>> create() {
    return _$createAsyncAction.run(() => super.create());
  }

  final _$_RelatedFeatureViewModelBaseActionController =
      ActionController(name: '_RelatedFeatureViewModelBase');

  @override
  void setIsEdit(bool value) {
    final _$actionInfo = _$_RelatedFeatureViewModelBaseActionController
        .startAction(name: '_RelatedFeatureViewModelBase.setIsEdit');
    try {
      return super.setIsEdit(value);
    } finally {
      _$_RelatedFeatureViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  Future<dynamic> detail() {
    final _$actionInfo = _$_RelatedFeatureViewModelBaseActionController
        .startAction(name: '_RelatedFeatureViewModelBase.detail');
    try {
      return super.detail();
    } finally {
      _$_RelatedFeatureViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
valueItems: ${valueItems},
isEdit: ${isEdit},
model: ${model},
dirtyModel: ${dirtyModel},
objectListModel: ${objectListModel},
languageModel: ${languageModel},
tabIndex: ${tabIndex}
    ''';
  }
}
