import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:fluttermvvmtemplate/core/base/view/base_widget.dart';
import 'package:fluttermvvmtemplate/core/components/appbar/flag_appbar.dart';
import 'package:fluttermvvmtemplate/core/components/form/base_form.dart';
import 'package:fluttermvvmtemplate/core/components/form/general_dropdown.dart';
import 'package:fluttermvvmtemplate/core/constants/enums/store_state.dart';
import 'package:fluttermvvmtemplate/core/helpers/alert_helper.dart';
import 'package:fluttermvvmtemplate/core/validator/custom_validator.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/viewmodel/category_custom_validations.dart';
import 'package:fluttermvvmtemplate/view/modules/system/object/viewmodel/object_list_view_model.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:fluttermvvmtemplate/view/modules/system/types/view/type_create_view.dart';
import 'package:fluttermvvmtemplate/core/components/form/empty_page.dart';
import 'package:page_transition/page_transition.dart';

class ObjectCreateView extends StatefulWidget {
  final VoidCallback onCreate;

  const ObjectCreateView({Key key, this.onCreate}) : super(key: key);

  @override
  _ObjectCreateViewState createState() => _ObjectCreateViewState();
}

class _ObjectCreateViewState extends State<ObjectCreateView> {
  final GlobalKey _key = GlobalKey();

  final PAGE_ID = 25;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: BaseView<ObjectListViewModel>(
            pageId: PAGE_ID,
            objectId: PAGE_ID,
            viewModel: ObjectListViewModel(),
            onModelReady: (model) {
              model.setContext(context);
              model.init();
              model.getTypes();
            },
            onPageBuilder: (_, viewModel) {
              return Scaffold(
                  resizeToAvoidBottomInset: false,
                  appBar: FlagAppBar(
                    title: Text('system.object.create_title'.tr()),
                  ),
                  body: SingleChildScrollView(
                    child: Form(
                      key: _key,
                      child: Column(children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Observer(
                              builder: (_) => TextFormField(
                                    initialValue: viewModel.dirtyModel.name,
                                    keyboardType: TextInputType.name,
                                    onChanged: (str) {
                                      viewModel.dirtyModel.name = str;
                                    },
                                    validator: (str) {
                                      return CustomValidator
                                          .selectAndRunFunction(
                                              CategoryCustomValidations()
                                                  .getValidationData('name'),
                                              str);
                                    },
                                    decoration: InputDecoration(
                                        focusedBorder: OutlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colors.blue),
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colors.blue),
                                        ),
                                        labelText:
                                            'system.object.create.name_label'
                                                .tr(),
                                        hintText:
                                            'system.object.create.name_hint'
                                                .tr(),
                                        errorText: viewModel.error.name),
                                    autofocus: true,
                                  )),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Observer(
                              builder: (_) => TextFormField(
                                    initialValue:
                                        viewModel.dirtyModel.description,
                                    keyboardType: TextInputType.name,
                                    onChanged: (str) {
                                      viewModel.dirtyModel.description = str;
                                    },
                                    validator: (str) {
                                      return CustomValidator
                                          .selectAndRunFunction(
                                              CategoryCustomValidations()
                                                  .getValidationData(
                                                      'description'),
                                              str);
                                    },
                                    decoration: InputDecoration(
                                        focusedBorder: OutlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colors.blue),
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colors.blue),
                                        ),
                                        labelText:
                                            'system.object.create.description_label'
                                                .tr(),
                                        hintText:
                                            'system.object.create.description_hint'
                                                .tr(),
                                        errorText: viewModel.error.description),
                                  )),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Observer(builder: (_) {
                            switch (viewModel.state) {
                              case StoreState.initial:
                                return GeneralDropDown(
                                    errorText: viewModel.error.type,
                                    items: [],
                                    initValue: viewModel.dirtyModel.type,
                                    onChanged: (value) {
                                      viewModel.dirtyModel.type = value;
                                    });

                              case StoreState.loading:
                                return GeneralDropDown(
                                    errorText: viewModel.error.type,
                                    items: [],
                                    initValue: viewModel.dirtyModel.type,
                                    onChanged: (value) {
                                      viewModel.dirtyModel.type = value;
                                    });

                              case StoreState.loaded:
                                var listData =
                                    List<dynamic>.from(viewModel.types);

                                if (listData.isEmpty) {
                                  return EmptyView(
                                      isSearchActive: false,
                                      onPressed: () {
                                        Navigator.push(
                                            context,
                                            PageTransition(
                                                type: PageTransitionType
                                                    .rightToLeft,
                                                child: TypeCreateView()));
                                      },
                                      buttonText: 'system.type.create');
                                }
                                viewModel.dirtyModel.type = listData[0].slug;
                                return GeneralDropDown(
                                    errorText: viewModel.error.type,
                                    items: listData,
                                    initValue: listData[0].slug,
                                    onChanged: (value) {
                                      viewModel.dirtyModel.type = value;
                                    });

                              default:
                                return Text('common.error'.tr());
                            }
                          }),
                        ),
                        CheckboxListTile(
                            value: viewModel.dirtyModel.isActive,
                            onChanged: (value) {
                              setState(() {
                                viewModel.dirtyModel.isActive = value;
                                viewModel.isEdit = true;
                              });
                            },
                            title: Text('common.active'.tr())),
                        CheckboxListTile(
                          value: viewModel.dirtyModel.isPublic,
                          title: Text('common.public'.tr()),
                          onChanged: (value) {
                            setState(() {
                              viewModel.dirtyModel.isPublic = value;
                              viewModel.isEdit = true;
                            });
                          },
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        CustomNormalButton(
                          onPressed: () async {
                            AlertHelper.shared.showProgressDialog(context);
                            var response = await viewModel.create();
                            await AlertHelper.shared
                                .startProgress(context, response);
                            Navigator.pop(context); // remove alert

                            if (response.success) {
                              Navigator.pop(context); // remove loading
                              Navigator.pop(context); // remove create form
                              widget.onCreate();
                            }
                          },
                          text: 'system.object.create.create_button'.tr(),
                          buttonColor: Colors.green,
                          textColor: Colors.white,
                        ),
                      ]),
                    ),
                  ));
            }));
  }
}
