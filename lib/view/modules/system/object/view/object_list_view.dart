import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:fluttermvvmtemplate/core/base/view/base_widget.dart';
import 'package:fluttermvvmtemplate/core/components/errors/error_page.dart';
import 'package:fluttermvvmtemplate/core/components/form/empty_page.dart';
import 'package:fluttermvvmtemplate/core/components/form/pagination_bar.dart';
import 'package:fluttermvvmtemplate/core/components/form/search_bar.dart';
import 'package:fluttermvvmtemplate/core/components/list-view/selectable_list_view.dart';
import 'package:fluttermvvmtemplate/core/constants/enums/store_state.dart';
import 'package:fluttermvvmtemplate/view/authenticate/onboard/view/on_board_view.dart';
import 'package:fluttermvvmtemplate/view/modules/system/object/view/object_create_view.dart';
import 'package:fluttermvvmtemplate/view/modules/system/object/view/object_home.dart';
import 'package:fluttermvvmtemplate/view/modules/system/object/view/object_update_view.dart';
import 'package:fluttermvvmtemplate/view/modules/system/object/viewmodel/object_list_view_model.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:page_transition/page_transition.dart';
import 'package:fluttermvvmtemplate/core/helpers/alert_helper.dart';

class ObjectListView extends StatefulWidget {
  @override
  _ObjectListViewState createState() => _ObjectListViewState();
}

class _ObjectListViewState extends State<ObjectListView> {
  @override
  Widget build(BuildContext context) {
    return BaseView<ObjectListViewModel>(
        pageId: 25,
        objectId: 27,
        viewModel: ObjectListViewModel(),
        onModelReady: (model) {
          model.setContext(context);
          model.init();
          model.fetchItems();
          model.getTypes();
        },
        onPageBuilder: (BuildContext context, ObjectListViewModel viewModel) {
          return SafeArea(
            child: Scaffold(
              resizeToAvoidBottomInset: false,
              backgroundColor: Colors.transparent,
              appBar: AppBar(
                title: Text('system.object.list_title'.tr()),
                actions: [
                  IconButton(
                      icon: Icon(Icons.add_circle_rounded),
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (_) => ObjectCreateView(onCreate: () {
                                  viewModel.fetchItems();
                                })));
                      })
                ],
                leading: IconButton(
                    icon: Icon(Icons.view_module),
                    onPressed: () {
                      Navigator.pushAndRemoveUntil(
                          context,
                          PageTransition(
                              type: PageTransitionType.rightToLeft,
                              child: ObjectHome()),
                          (route) => false);
                    }),
              ),
              body: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Observer(builder: (_) {
                    if (viewModel.typeIsCompleted) {
                      return HomeSearchBar(
                        typeList: viewModel.types,
                        onClose: () {
                          viewModel.fetchItems();
                        },
                        onSearch: (text) {
                          viewModel.searchItems();
                        },
                        searchModel: viewModel.searchModel,
                      );
                    } else {
                      return Container();
                    }
                  }),
                  Expanded(child: Observer(builder: (_) {
                    switch (viewModel.state) {
                      case StoreState.initial:
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Center(child: CircularProgressIndicator()),
                          ],
                        );
                      case StoreState.loading:
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Center(child: CircularProgressIndicator()),
                          ],
                        );
                      case StoreState.loaded:
                        if (viewModel.filteredListItems.isNotEmpty) {
                          return Column(
                            children: [
                              Observer(
                                builder: (_) {
                                  return buildObjectListView(
                                      viewModel, context);
                                },
                              ),
                              PaginationBar(
                                  paginationModel: viewModel.paginationModel,
                                  fetchFunction: viewModel.isSearchActive
                                      ? viewModel.searchItems
                                      : viewModel.fetchItems),
                              SizedBox(
                                height: 32,
                              )
                            ],
                          );
                        }
                        return EmptyView(
                            isSearchActive: viewModel.searchModel.name.isEmpty
                                ? false
                                : true,
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  PageTransition(
                                      type: PageTransitionType.rightToLeft,
                                      child: ObjectCreateView()));
                            },
                            buttonText: 'common.create'.tr());
                      case StoreState.loadingError:
                        return Center(
                            child: ErrorPage(errorText: 'error'.tr()));
                      default:
                        return ErrorPage(errorText: 'error'.tr());
                    }
                  }))
                ],
              ),
              floatingActionButton: FloatingActionButton(
                backgroundColor: Color(0xFF6F8AB7),
                onPressed: () {
                  /*Navigator.push(context,
                      MaterialPageRoute(builder: (_) => ObjectCreateView()));*/
                },
                child: Icon(Icons.monetization_on),
              ),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.centerDocked,
              bottomNavigationBar: CustomBottomNavigationBar(
                onPage: (currentPageIndex) {},
              ),
            ),
          );
        });
  }

  void pushAndRemoved(BuildContext context, {Widget page}) {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (_) => page), (route) => false);
  }

  Widget buildCenter() =>
      Expanded(child: Center(child: CircularProgressIndicator()));

  Widget buildObjectListView(
      ObjectListViewModel viewModel, BuildContext context) {
    return Flexible(
        child: SelectableListView(
      onUpdate: (index) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (_) => ObjectDetailView(
                      model: viewModel.objectList[index],
                    )));
      },
      onDelete: (index) {
        var model = viewModel.objectList[index];
        viewModel.filteredListItems.removeWhere((element) => element.slug == model.slug);

          setState(() {}); 

          viewModel.delete(model).then((value) async {
            if (!value.success) {
              await AlertHelper.shared.startProgress(context, value);
            }
          });
      },
      list: viewModel.objectList,
      onSelect: (list) {},
    ));
  }
}
