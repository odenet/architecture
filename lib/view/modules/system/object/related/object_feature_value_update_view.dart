import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:fluttermvvmtemplate/core/components/card/identity_card.dart';
import 'package:fluttermvvmtemplate/core/components/card/order_form_card.dart';
import 'package:fluttermvvmtemplate/core/components/card/visibility_form_card.dart';
import 'package:fluttermvvmtemplate/view/modules/system/object/viewmodel/related_feature_view_model.dart';
import 'package:fluttermvvmtemplate/core/helpers/alert_helper.dart';
import 'package:fluttermvvmtemplate/core/init/notifier/data_type.dart';

class ObjectFeatureValueUpdateView extends StatefulWidget {
  final RelatedFeatureViewModel viewModel;
  final Function onUpdate;

  ObjectFeatureValueUpdateView({Key key, this.viewModel, this.onUpdate})
      : super(key: key);

  @override
  _ObjectFeatureValueUpdateViewState createState() =>
      _ObjectFeatureValueUpdateViewState();
}

class _ObjectFeatureValueUpdateViewState
    extends State<ObjectFeatureValueUpdateView> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            backgroundColor: Colors.transparent,
            appBar: AppBar(
              title: Text(DataType.shared.dataTypeLabel + ' ' + 'related.set_feature_value'.tr()),
            ),
            //body:
            body: SingleChildScrollView(
                child: Column(
              children: [
                SizedBox(height: 10),
                DataTable(columns: <DataColumn>[
                  DataColumn(
                    label: Text('common.name'.tr(),
                        style: TextStyle(
                          height: 3.0,
                          fontSize: 15.2,
                          fontWeight: FontWeight.bold,
                        )),
                  ),
                  DataColumn(
                      label: Text('common.value'.tr(),
                          style: TextStyle(
                            height: 3.0,
                            fontSize: 15.2,
                            fontWeight: FontWeight.bold,
                          ))),
                ], rows: [
                  buildTableBody(widget.viewModel)
                ]),
                VisibilityFormCard(
                    viewModel: widget.viewModel,
                    onUpdate: () async {
                      widget.viewModel.model = widget.viewModel.dirtyModel;
                      setState(() {});
                    }),
                OrderFormCard(
                    viewModel: widget.viewModel,
                    onUpdate: () async {
                      widget.viewModel.model = widget.viewModel.dirtyModel;
                      setState(() {});
                    }),
                IdentityCard(
                  blamable: widget.viewModel.model.createdBy,
                  datetime: widget.viewModel.model.createdAt,
                ),
                IdentityCard(
                    blamable: widget.viewModel.model.updatedBy,
                    datetime: widget.viewModel.model.updatedAt),
              ],
            )),
            floatingActionButton: FloatingActionButton(
              onPressed: () {},
              child: IconButton(
                  icon: Icon(Icons.save),
                  onPressed: () async {
                    AlertHelper.shared.showProgressDialog(context);
                    var response = await widget.viewModel.update();

                    await AlertHelper.shared.startProgress(context, response);

                    Navigator.pop(context);
                    if (response.success) {
                      widget.viewModel.model.featureValue =
                          widget.viewModel.dirtyModel.featureValue;
                      Navigator.pop(context, true); // remove loading
                      Navigator.pop(context, true); // remove create form
                      Navigator.pop(context, true); // remove create form
                      widget.onUpdate();
                    } else {}
                  }),
            )));
  }

  DataRow buildTableBody(RelatedFeatureViewModel viewModel) {
    return DataRow(
      cells: <DataCell>[
        DataCell(Text(viewModel.model.featureLabel ?? ' ')),
        DataCell(TextFormField(
          initialValue: viewModel.model.featureValue,
          keyboardType: TextInputType.name,
          onChanged: (str) {
            viewModel.dirtyModel.featureValue = str;
          },
          decoration: InputDecoration(
            hintText: 'common.related.feature.value_hint'.tr(),
          ),
        )),
      ],
    );
  }
}
