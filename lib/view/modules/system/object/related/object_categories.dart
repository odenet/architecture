import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttermvvmtemplate/core/components/alerts/custom_circle_alert.dart';
import 'package:fluttermvvmtemplate/core/components/form/empty_page.dart';
import 'package:fluttermvvmtemplate/core/helpers/alert_helper.dart';
import 'package:fluttermvvmtemplate/core/init/notifier/data_type.dart';
import 'package:fluttermvvmtemplate/view/common/models/related_category_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/object/related/object_related_categories_listview.dart';
import 'package:fluttermvvmtemplate/view/modules/system/object/view/object_update_view.dart';
import 'package:page_transition/page_transition.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:fluttermvvmtemplate/view/modules/system/object/viewmodel/object_list_view_model.dart';

class ObjectCategories extends StatefulWidget {
  final ObjectListViewModel viewModel;
  final Function onUpdate;

  const ObjectCategories({Key key, this.viewModel, this.onUpdate})
      : super(key: key);

  @override
  _ObjectCategoriesState createState() => _ObjectCategoriesState();
}

class _ObjectCategoriesState extends State<ObjectCategories> {
  List<String> selectedList = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          title: selectedList.isNotEmpty
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      DataType.shared.dataTypeLabel +
                          ' ' +
                          'common.selected_items'.tr(namedArgs: {
                            'count': selectedList.length.toString()
                          }),
                    ),
                    IconButton(
                      onPressed: () async {
                        CustomCircleProgress.showIndicator(context);
                        var response = await widget.viewModel
                            .deleteRelatedCategory(selectedList);

                        await AlertHelper.shared
                            .startProgress(context, response);

                        Navigator.pop(context);
                        if (response.success) {
                          Navigator.pop(context);

                          // remove data from model
                          widget.viewModel.model.relatedCategories.removeWhere(
                              (e) => selectedList.contains(e.slug));

                          // remove data from dirty model
                          widget.viewModel.dirtyModel.relatedCategories
                              .removeWhere(
                                  (e) => selectedList.contains(e.slug));

                          selectedList = [];
                          setState(() {});
                        } else {}
                      },
                      icon: Icon(Icons.delete),
                    )
                  ],
                )
              : Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text('system.category.list_title'.tr()),
                    Spacer(),
                    IconButton(
                      onPressed: () {
                        SystemChrome.setPreferredOrientations(
                            [DeviceOrientation.portraitUp]);
                        navigateObjectListView();
                      },
                      icon: Icon(Icons.add),
                    )
                  ],
                ),
        ),
        //body:
        body: widget.viewModel.dirtyModel.relatedCategories.isEmpty
            ? Center(
                child: EmptyView(
                    onPressed: () {
                      navigateObjectListView();
                    },
                    buttonText: 'common.create'.tr()))
            : ListView.builder(
                itemCount: widget.viewModel.dirtyModel.relatedCategories.length,
                itemBuilder: (_, index) {
                  var data = widget.viewModel.dirtyModel.relatedCategories;
                  var isSelected = selectedList.contains(data[index].slug);
                  return Card(
                    child: ExpansionTile(
                      title: Text(data[index].name),
                      leading: GestureDetector(
                        onTap: () {
                          _selectAction(data[index].slug);
                        },
                        child: CircleAvatar(
                          backgroundColor:
                              isSelected ? Colors.blue : Colors.grey,
                          child: isSelected
                              ? Icon(
                                  Icons.check,
                                  color: Colors.white,
                                )
                              : Container(
                                  child: Text(
                                    data[index].name[0].toUpperCase(),
                                    style: TextStyle(color: Colors.black),
                                  ),
                                ),
                        ),
                      ),
                      children: [
                        CheckboxListTile(
                          title: Text('common.main'.tr()),
                          value: data[index].isMain ? true : false,
                          onChanged: (value) {
                            setState(() {
                              data = data.map((e) {
                                e.isMain = false;
                                return e;
                              }).toList();
                              data[index].isMain = value;
                            });
                          },
                        ),
                        Divider(),
                        CheckboxListTile(
                          title: Text('common.active'.tr()),
                          value: data[index].isActive ? true : false,
                          onChanged: (value) {
                            setState(() {
                              data[index].isActive = value;
                            });
                          },
                        ),
                        Divider(),
                        CheckboxListTile(
                          title: Text('common.public'.tr()),
                          value: data[index].isPublic ? true : false,
                          onChanged: (value) {
                            setState(() {
                              data[index].isPublic = value;
                            });
                          },
                        ),
                      ],
                    ),
                  );
                }),
        floatingActionButton: FloatingActionButton(
          onPressed: () {},
          child: IconButton(
              icon: Icon(Icons.save),
              onPressed: () async {
                CustomCircleProgress.showIndicator(context);
                var response = await widget.viewModel.updateObjectCategory();

                await AlertHelper.shared.startProgress(context, response);

                Navigator.pop(context);
                if (response.success) {
                  Navigator.pop(context); // remove loading
                  widget.onUpdate();
                } else {}
              }),
        ));
  }

  void _selectAction(String slug) {
    if (selectedList.contains(slug)) {
      selectedList.remove(slug);
    } else {
      selectedList.add(slug);
    }
    setState(() {});
  }

  void navigateObjectListView() {
    final isMainExist = widget.viewModel.dirtyModel.relatedCategories
        .where((element) => element.isMain)
        .isEmpty;
    Navigator.push(
        context,
        PageTransition(
            type: PageTransitionType.rightToLeft,
            child: ObjectRelatedCategoriesListView(
                isMainExist: isMainExist,
                onSelect: (items) async {
                  var data = items
                      .map((e) => RelatedCategoryModel.toJson(
                          model: RelatedCategoryModel(
                              dataType: DataType.shared.dataType,
                              categorySlug: e.slug,
                              isMain: e.isMain,
                              dataSlug: widget.viewModel.model.slug)))
                      .toList();

                  CustomCircleProgress.showIndicator(context);
                  var response =
                      await widget.viewModel.createObjectCategory(data);
                  Navigator.pop(context);

                  await AlertHelper.shared.startProgress(context, response);

                  Navigator.pop(context);
                  if (response.success) {
                    Navigator.pop(context); // remove loading
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                            builder: (_) => ObjectDetailView(
                                model: widget.viewModel.model)),
                        (route) => false);
                  } else {}
                })));
  }
}
