import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/helpers/alert_helper.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:fluttermvvmtemplate/core/init/notifier/data_type.dart';
import 'package:fluttermvvmtemplate/view/common/models/related_feature_model.dart';

class ObjectFeatureValue extends StatefulWidget {
  final dynamic viewModel;
  final Function onUpdate;
  final List<RelatedFeatureModel> data;

  const ObjectFeatureValue(this.data, {Key key, this.viewModel, this.onUpdate})
      : super(key: key);

  @override
  _ObjectFeatureValueState createState() => _ObjectFeatureValueState();
}

class _ObjectFeatureValueState extends State<ObjectFeatureValue> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          title: Text(DataType.shared.dataTypeLabel +
              ' ' +
              'related.set_feature_values'.tr()),
        ),
        //body:
        body: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Column(
              children: [
                DataTable(columns: <DataColumn>[
                  DataColumn(
                      label: Text('',
                          style: TextStyle(
                            height: 3.0,
                            fontSize: 15.2,
                            fontWeight: FontWeight.bold,
                          ))),
                  DataColumn(
                    label: Text('common.name'.tr(),
                        style: TextStyle(
                          height: 3.0,
                          fontSize: 15.2,
                          fontWeight: FontWeight.bold,
                        )),
                  ),
                  DataColumn(
                      label: Text('common.value'.tr(),
                          style: TextStyle(
                            height: 3.0,
                            fontSize: 15.2,
                            fontWeight: FontWeight.bold,
                          ))),
                ], rows: buildTableBody(widget.data))
              ],
            )),
        floatingActionButton: FloatingActionButton(
          onPressed: () {},
          child: IconButton(
              icon: Icon(Icons.save),
              onPressed: () async {
                AlertHelper.shared.showProgressDialog(context);
                var response =
                    await widget.viewModel.createObjectFeature(widget.data);
                await AlertHelper.shared.startProgress(context, response);
                Navigator.pop(context); // remove alert

                if (response.success) {
                  Navigator.pop(context); // remove alert
                  Navigator.pop(context); // remove alert
                  Navigator.pop(context); // remove alert
                  Navigator.pop(context); // remove alert
                  widget.onUpdate();
                }
              }),
        ));
  }

  List<DataRow> buildTableBody(data) {
    List<DataRow> rows = [];

    data.forEach((element) {
      rows.add(DataRow(
        cells: <DataCell>[
          DataCell(
            GestureDetector(
                child: CircleAvatar(
              child: Container(
                child: Text(
                  element.name[0].toUpperCase(),
                  style: TextStyle(color: Colors.black),
                ),
              ),
            )),
          ),
          DataCell(Text(element.name)),
          DataCell(TextFormField(
            initialValue: element.featureValue,
            keyboardType: TextInputType.name,
            onChanged: (str) {
              element.featureValue = str;
            },
            decoration: InputDecoration(
              hintText: 'common.value'.tr(),
            ),
          )),
        ],
      ));
    });

    return rows;
  }
}
