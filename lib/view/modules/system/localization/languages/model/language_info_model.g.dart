// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'language_info_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LanguageInfoModel _$LanguageInfoModelFromJson(Map<String, dynamic> json) {
  return LanguageInfoModel(
    name: json['name'] as String ?? '',
    description: json['description'] as String ?? '',
    slug: json['slug'] as String ?? '',
    dataSlug: json['dataSlug'] as String ?? '',
    isRef: json['isRef'] as bool ?? false,
    isPublic: json['isPublic'] as bool ?? true,
    language: json['language'] as String ?? '',
    languageLabel: json['languageLabel'] as String ?? '',
    isActive: json['isActive'] as bool ?? true,
  );
}

Map<String, dynamic> _$LanguageInfoModelToJson(LanguageInfoModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'description': instance.description,
      'dataSlug': instance.dataSlug,
      'slug': instance.slug,
      'languageLabel': instance.languageLabel,
      'language': instance.language,
      'isRef': instance.isRef,
      'isPublic': instance.isPublic,
      'isActive': instance.isActive,
    };
