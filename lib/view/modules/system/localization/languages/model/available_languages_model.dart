import 'package:json_annotation/json_annotation.dart';
import 'package:fluttermvvmtemplate/core/base/model/base_model.dart';

part 'available_languages_model.g.dart';

@JsonSerializable()
class AvailableLanguagesModel extends BaseModel<AvailableLanguagesModel> {
  @JsonKey(required: true, name: 'localName')
  String localName;

  @JsonKey(required: true, name: 'langCode')
  String langCode;

  AvailableLanguagesModel({
    slug,
    name,
    description,
    isActive = true,
    isPublic = true,
    this.localName,
    this.langCode,
  }) {
    this.slug = slug;
    this.name = name;
    this.description = description;
    this.isActive = isActive;
    this.isPublic = isPublic;
  }

  AvailableLanguagesModel.clone(AvailableLanguagesModel model) {
    description = model.description;
    name = model.name;
    localName = model.localName;
    langCode = model.langCode;
    isActive = model.isActive;
    isPublic = model.isPublic;
    slug = model.slug;
  }

  @override
  AvailableLanguagesModel fromJson(Map json) {
    return _$AvailableLanguagesModelFromJson(json);
  }

  @override
  Map<String, dynamic> toJson() {
    return _$AvailableLanguagesModelToJson(this);
  }
}
