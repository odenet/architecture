import 'package:json_annotation/json_annotation.dart';

part 'language_info_model.g.dart';

@JsonSerializable()
class LanguageInfoModel {
  @JsonKey(defaultValue: '')
  String name;

  @JsonKey(defaultValue: '')
  String description;

  @JsonKey(defaultValue: '')
  String dataSlug;

  @JsonKey(defaultValue: '')
  String slug;

  @JsonKey(defaultValue: '')
  String languageLabel;

  @JsonKey(defaultValue: '')
  String language;

  @JsonKey(defaultValue: false)
  bool isRef;

  @JsonKey(defaultValue: true)
  bool isPublic;

  @JsonKey(defaultValue: true)
  bool isActive;

  LanguageInfoModel(
      {this.name = '',
      this.description = '',
      this.slug = '',
      this.dataSlug,
      this.isRef,
      this.isPublic = true,
      this.language,
      this.languageLabel,
      this.isActive = true});

  LanguageInfoModel.clone(LanguageInfoModel model) {
    name = model.name;
    description = model.description;
    slug = model.slug;
    dataSlug = model.dataSlug;
    language = model.language;
    isPublic = model.isPublic;
    isActive = model.isActive;
    languageLabel = model.languageLabel;
  }

  @override
  static LanguageInfoModel fromJson(Map<String, dynamic> json) {
    return _$LanguageInfoModelFromJson(json);
  }

  LanguageInfoModel getModel(Map json) {
    return LanguageInfoModel.fromJson(json);
  }

  @override
  static Map<String, dynamic> toJson({LanguageInfoModel model}) {
    return _$LanguageInfoModelToJson(model);
  }
}
