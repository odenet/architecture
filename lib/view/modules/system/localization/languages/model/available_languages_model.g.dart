// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'available_languages_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AvailableLanguagesModel _$AvailableLanguagesModelFromJson(
    Map<String, dynamic> json) {
  $checkKeys(json, requiredKeys: const ['localName', 'langCode']);
  return AvailableLanguagesModel(
    slug: json['slug'] ?? '',
    name: json['name'] ?? '',
    description: json['description'] ?? '',
    isActive: json['isActive'] ?? true,
    isPublic: json['isPublic'] ?? true,
    localName: json['localName'] as String,
    langCode: json['langCode'] as String,
  )..targetRoute = json['targetRoute'] as String;
}

Map<String, dynamic> _$AvailableLanguagesModelToJson(
        AvailableLanguagesModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'description': instance.description,
      'slug': instance.slug,
      'isPublic': instance.isPublic,
      'isActive': instance.isActive,
      'targetRoute': instance.targetRoute,
      'localName': instance.localName,
      'langCode': instance.langCode,
    };
