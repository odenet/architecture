import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:fluttermvvmtemplate/core/base/view/base_widget.dart';
import 'package:fluttermvvmtemplate/core/components/alerts/custom_form_alert.dart';
import 'package:fluttermvvmtemplate/core/components/card/visibility_form_card.dart';
import 'package:fluttermvvmtemplate/core/components/form/base_form.dart';
import 'package:fluttermvvmtemplate/core/helpers/alert_helper.dart';
import 'package:fluttermvvmtemplate/core/init/notifier/data_type.dart';
import 'package:fluttermvvmtemplate/core/validator/custom_validator.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/viewmodel/available_language_custom_validations.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/viewmodel/available_languages_view_model.dart';

class AvailableLanguagesCreateView extends StatefulWidget {
  final VoidCallback onUpdate;

  const AvailableLanguagesCreateView({Key key, @required this.onUpdate})
      : super(key: key);

  @override
  _AvailableLanguagesCreateViewState createState() =>
      _AvailableLanguagesCreateViewState();
}

class _AvailableLanguagesCreateViewState
    extends State<AvailableLanguagesCreateView> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController field1Controller;

  TextEditingController field2Controller;
  bool isActive = true;
  bool isPublic = false;

  @override
  Widget build(BuildContext context) {
    return BaseView<AvailableLanguagesViewModel>(
        pageId: 25,
        objectId: 27,
        viewModel: AvailableLanguagesViewModel(),
        onModelReady: (vm) {
          vm.init();
          vm.setContext(context);
          field1Controller = TextEditingController();
          field2Controller = TextEditingController();
        },
        onDispose: () {
          field1Controller.dispose();
          field2Controller.dispose();
        },
        onPageBuilder: (context, viewModel) {
          return SafeArea(
            child: Scaffold(
              resizeToAvoidBottomPadding: false,
                resizeToAvoidBottomInset: false,
                appBar: AppBar(
                  title: Text('Add ${DataType.shared.dataTypeLabel}'),
                ),
                body: buildForm(viewModel, context)),
          );
        });
  }

  Form buildForm(AvailableLanguagesViewModel vm, BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              decoration: InputDecoration(
                  hintText: 'Name', border: OutlineInputBorder()),
              onChanged: (str) {
                vm.dirtyModel.name = str;
              },
              validator: (str) {
                return CustomValidator.selectAndRunFunction(
                    AvailableLanguageCustomValidations()
                        .getValidationData('name'),
                    str);
              },
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              decoration: InputDecoration(
                  hintText: 'Local Name', border: OutlineInputBorder()),
              onChanged: (str) {
                vm.dirtyModel.localName = str;
              },
              validator: (str) {
                return CustomValidator.selectAndRunFunction(
                    AvailableLanguageCustomValidations()
                        .getValidationData('name'),
                    str);
              },
            ),
          ),
          SizedBox(
            height: 16,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              decoration: InputDecoration(
                  hintText: 'Description', border: OutlineInputBorder()),
              onChanged: (str) {
                vm.dirtyModel.description = str;
              },
              validator: (str) {
                return CustomValidator.selectAndRunFunction(
                    AvailableLanguageCustomValidations()
                        .getValidationData('name'),
                    str);
              },
            ),
          ),
          SizedBox(
            height: 16,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              decoration: InputDecoration(
                  hintText: 'Language Code', border: OutlineInputBorder()),
              onChanged: (str) {
                vm.dirtyModel.langCode = str;
              },
              validator: (str) {
                return CustomValidator.selectAndRunFunction(
                    AvailableLanguageCustomValidations()
                        .getValidationData('name'),
                    str);
              },
            ),
          ),
          SizedBox(
            height: 16,
          ),
          Row(
            children: [
              Expanded(
                child: CheckboxListTile(
                  value: isActive,
                  onChanged: (value) {
                    setState(() {
                      isActive = value;
                    });
                  },
                  title: Text('Active'),
                ),
              ),
              Expanded(
                child: CheckboxListTile(
                  value: isPublic,
                  onChanged: (value) {
                    setState(() {
                      isPublic = value;
                    });
                    vm.dirtyModel.isPublic = value;
                  },
                  title: Text('Public'),
                ),
              ),
            ],
          ),
          CustomNormalButton(
              textColor: Colors.white,
              buttonColor: Colors.green,
              text: '  Save  ',
              onPressed: () async {
                AlertHelper.shared.showProgressDialog(context);
                var response = await vm.create();
                if (response.success) {
                  widget.onUpdate();
                  Navigator.pop(context); // remove loading
                  Navigator.pop(context); // remove create form
                } else {
                  Navigator.pop(context);
                  await AlertHelper.shared.errorMessages(context, response.errorMessageModel);
                }
              }),
        ],
      ),
    );
  }
}
