import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:fluttermvvmtemplate/core/base/view/base_widget.dart';
import 'package:fluttermvvmtemplate/core/components/alerts/custom_form_alert.dart';
import 'package:fluttermvvmtemplate/core/components/errors/error_page.dart';
import 'package:fluttermvvmtemplate/core/components/form/search_bar.dart';
import 'package:fluttermvvmtemplate/core/constants/enums/store_state.dart';
import 'package:fluttermvvmtemplate/core/helpers/alert_helper.dart';
import 'package:fluttermvvmtemplate/core/init/notifier/data_type.dart';
import 'package:fluttermvvmtemplate/view/authenticate/onboard/view/on_board_view.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/model/available_languages_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/view/available_languages_create_view.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/view/available_language_update_view.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/viewmodel/available_languages_view_model.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:page_transition/page_transition.dart';
import 'package:fluttermvvmtemplate/view/modules/system/object/view/object_home.dart';
import 'package:fluttermvvmtemplate/core/components/form/empty_page.dart';

class AvailableLanguagesListView extends StatelessWidget {
  final boldStyle = const TextStyle(fontWeight: FontWeight.bold);
  @override
  Widget build(BuildContext context) {
    return BaseView<AvailableLanguagesViewModel>(
        pageId: 25,
        objectId: 27,
        viewModel: AvailableLanguagesViewModel(),
        onModelReady: (vm) {
          DataType.shared.dataTypeLabel = 'Available Language';
          vm.init();
          vm.setContext(context);
          vm.fetchItems();
        },
        onDispose: () {},
        onPageBuilder: (context, viewModel) {
          return SafeArea(
            child: Scaffold(
              backgroundColor: Colors.transparent,
              appBar: AppBar(
                actions: [
                  IconButton(
                    icon: Icon(Icons.add),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (_) => AvailableLanguagesCreateView(
                                    onUpdate: () {
                                      viewModel.fetchItems();
                                    },
                                  )));
                    },
                  )
                ],
                title: Text('List ${DataType.shared.dataTypeLabel}'),
                leading: IconButton(
                    icon: Icon(Icons.view_module),
                    onPressed: () {
                      Navigator.pushAndRemoveUntil(
                          context,
                          PageTransition(
                              type: PageTransitionType.rightToLeft,
                              child: ObjectHome()),
                          (route) => false);
                    }),
              ),
              body: Column(
                children: [
                  HomeSearchBar(
                    onClose: () {
                      viewModel.fetchItems();
                    },
                    onSearch: (text) {
                      viewModel.searchItems();
                    },
                    searchModel: viewModel.searchModel,
                  ),
                  Observer(builder: (_) {
                    switch (viewModel.state) {
                      case StoreState.initial:
                        return Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CircularProgressIndicator(),
                            Text('common.loading'.tr()),
                          ],
                        );
                        break;

                      case StoreState.loading:
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Center(child: CircularProgressIndicator()),
                          ],
                        );
                      case StoreState.loaded:
                        if (viewModel.filteredListItems.isNotEmpty) {
                          return Expanded(
                            child: ListView.builder(
                              itemCount: viewModel.items.length,
                              itemBuilder: (context, index) {
                                return Card(
                                  child: ListTile(
                                      title: Text(viewModel.items[index].name),
                                      subtitle: Text(
                                          viewModel.items[index].localName),
                                      leading: Icon(Icons.language),
                                      trailing: IconButton(
                                          icon: Icon(Icons.more_horiz),
                                          onPressed: () {
                                            showBottomSheet(context, viewModel,
                                                viewModel.items[index]);
                                          })),
                                );
                              },
                            ),

                            /*PaginationBar(
                                  paginationModel: viewModel.paginationModel,
                                  fetchFunction: viewModel.isSearchActive
                                      ? viewModel.searchItems
                                      : viewModel.fetchItems)*/
                          );
                        }
                        return Expanded(
                          child: EmptyView(
                              isSearchActive: viewModel.searchModel.name.isEmpty
                                  ? false
                                  : true,
                              onPressed: () {},
                              buttonText: 'Create'),
                        );
                        break;
                      case StoreState.loadingError:
                        return Center(
                            child: ErrorPage(errorText: 'error'.tr()));
                      default:
                        return Container();
                    }
                  }),
                ],
              ),
              floatingActionButton: FloatingActionButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) => AvailableLanguagesCreateView(
                                onUpdate: () {
                                  viewModel.fetchItems();
                                },
                              )));
                },
                child: Icon(Icons.add),
              ),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.centerDocked,
              bottomNavigationBar: CustomBottomNavigationBar(
                onPage: (currentPageIndex) {},
              ),
            ),
          );
        });
  }

  void showBottomSheet(BuildContext context,
      AvailableLanguagesViewModel viewModel, AvailableLanguagesModel language) {
    showModalBottomSheet(
        context: context,
        builder: (_) {
          return Container(
            child: Column(mainAxisSize: MainAxisSize.min, children: [
              ListTile(
                leading: Icon(
                  Icons.edit,
                  color: Colors.blue,
                ),
                title: Text(
                  'Update',
                  style: boldStyle,
                ),
                subtitle: Text('Update this item'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (_) => AvailableLanguagesUpdateView(
                          onUpdate: () {
                            viewModel.fetchItems();
                          },
                          model: language)));
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.delete,
                  color: Colors.red,
                ),
                title: Text(
                  'Delete',
                  style: boldStyle,
                ),
                subtitle: Text('You can delete this item'),
                onTap: () {
                  AlertHelper.shared.deleteDetector(context, () {
                    Navigator.pop(context);
                    Navigator.pop(context);
                    delete(context, viewModel, language);
                  });
                },
              )
            ]),
          );
        });
  }

  Future<void> delete(
      BuildContext context,
      AvailableLanguagesViewModel viewModel,
      AvailableLanguagesModel language) async {
    var response = await viewModel.delete(language);
    if (response.success) {
      viewModel.items.remove(language);
      CustomFormAlert(
          title: 'SUCCES',
          message: response.message,
          alertType: CustomAlertType.SUCCESS);
    } else {
      CustomFormAlert(
          title: 'ERROR',
          message: response.message,
          alertType: CustomAlertType.ERROR);
    }
  }
}
