import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/base/view/base_widget.dart';
import 'package:fluttermvvmtemplate/core/components/form/base_form.dart';
import 'package:fluttermvvmtemplate/core/helpers/alert_helper.dart';
import 'package:fluttermvvmtemplate/core/init/notifier/data_type.dart';
import 'package:fluttermvvmtemplate/core/validator/custom_validator.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/model/available_languages_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/viewmodel/available_language_custom_validations.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/viewmodel/available_languages_view_model.dart';
import 'package:fluttermvvmtemplate/core/components/card/custom_visibility_card.dart';

class AvailableLanguagesUpdateView extends StatefulWidget {
  final AvailableLanguagesModel model;
  final VoidCallback onUpdate;

  AvailableLanguagesUpdateView({Key key, this.model, this.onUpdate})
      : super(key: key);

  @override
  _AvailableLanguagesUpdateViewState createState() =>
      _AvailableLanguagesUpdateViewState();
}

class _AvailableLanguagesUpdateViewState
    extends State<AvailableLanguagesUpdateView> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController nameController;

  TextEditingController localNameController;

  TextEditingController descriptionController;

  TextEditingController langCodeController;

  @override
  Widget build(BuildContext context) {
    return BaseView<AvailableLanguagesViewModel>(
        pageId: 25,
        objectId: 27,
        viewModel: AvailableLanguagesViewModel(),
        onModelReady: (vm) {
          vm.init();
          vm.setContext(context);
          vm.dirtyModel = AvailableLanguagesModel.clone(widget.model);
          nameController = TextEditingController(text: widget.model.name);
          localNameController =
              TextEditingController(text: widget.model.localName);
          descriptionController =
              TextEditingController(text: widget.model.description);
          langCodeController =
              TextEditingController(text: widget.model.langCode);
        },
        onDispose: () {
          nameController.dispose();
          localNameController.dispose();
          descriptionController.dispose();
          langCodeController.dispose();
        },
        onPageBuilder: (context, viewModel) {
          return SafeArea(
            child: Scaffold(
              resizeToAvoidBottomInset: false,
              appBar: AppBar(
                title: Text('Detail ${DataType.shared.dataTypeLabel}'),
              ),
              body: buildForm(viewModel, context),
            ),
          );
        });
  }

  Form buildForm(AvailableLanguagesViewModel vm, BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              controller: nameController,
              decoration: InputDecoration(
                  hintText: 'Name', border: OutlineInputBorder()),
              validator: (str) {
                return CustomValidator.selectAndRunFunction(
                    AvailableLanguageCustomValidations()
                        .getValidationData('name'),
                    str);
              },
              onChanged: (str) {
                vm.dirtyModel.name = str;
                if (str != widget.model.name ||
                    localNameController.text != widget.model.localName) {
                  vm.setIsEdit(true);
                } else {
                  vm.setIsEdit(false);
                }
              },
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              controller: localNameController,
              decoration: InputDecoration(
                  hintText: 'Local Name', border: OutlineInputBorder()),
              validator: (str) {
                return CustomValidator.selectAndRunFunction(
                    AvailableLanguageCustomValidations()
                        .getValidationData('name'),
                    str);
              },
              onChanged: (str) {
                vm.dirtyModel.localName = str;
                if (str != widget.model.localName ||
                    nameController.text != widget.model.name) {
                  vm.setIsEdit(true);
                } else {
                  vm.setIsEdit(false);
                }
              },
            ),
          ),

          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              controller: descriptionController,
              decoration: InputDecoration(
                  hintText: 'Description', border: OutlineInputBorder()),
              onChanged: (str) {
                vm.dirtyModel.description = str;
              },
              validator: (str) {
                return CustomValidator.selectAndRunFunction(
                    AvailableLanguageCustomValidations()
                        .getValidationData('name'),
                    str);
              },
            ),
          ),

          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              controller: langCodeController,
              decoration: InputDecoration(
                  hintText: 'Language Code', border: OutlineInputBorder()),
              onChanged: (str) {
                vm.dirtyModel.langCode = str;
              },
              validator: (str) {
                return CustomValidator.selectAndRunFunction(
                    AvailableLanguageCustomValidations()
                        .getValidationData('name'),
                    str);
              },
            ),
          ),

          CustomVisibilityFormCard(
              isActive: vm.dirtyModel.isActive,
              isPublic: vm.dirtyModel.isPublic,
              onUpdate: (public, active) {
                vm.dirtyModel.isActive = active;
                vm.dirtyModel.isPublic = public;
              }),

          CustomNormalButton(
              textColor: Colors.white,
              buttonColor: Colors.green,
              text: '  Update  ',
              onPressed: () async {
                AlertHelper.shared.showProgressDialog(context);
                var response = await vm.update();
                await AlertHelper.shared.startProgress(context, response);

                Navigator.pop(context); // remove alert

                if (response.success) {
                  widget.onUpdate();
                  Navigator.pop(context); // remove loading
                  Navigator.pop(context); // remove create form
                }
              })
          //buildSaveButton()
        ],
      ),
    );
  }
}
