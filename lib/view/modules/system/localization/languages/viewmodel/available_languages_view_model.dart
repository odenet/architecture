import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/base/model/base_view_model.dart';
import 'package:fluttermvvmtemplate/core/constants/app/app_constants.dart';
import 'package:fluttermvvmtemplate/core/constants/enums/store_state.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/response_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/network_error_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/pagination_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/search_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/model/available_languages_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/service/available_languages_service.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/viewmodel/available_languages_process_manager.dart';
import 'package:mobx/mobx.dart';
import 'package:fluttermvvmtemplate/view/common/viewmodel/common_view_model.dart';

part 'available_languages_view_model.g.dart';

class AvailableLanguagesViewModel = _AvailableLanguagesViewModelBase
    with _$AvailableLanguagesViewModel;

abstract class _AvailableLanguagesViewModelBase extends CommonViewModel
    with Store, BaseViewModel {
  AvailableLanguagesService feed = AvailableLanguagesService();

  void setContext(BuildContext context) => this.context = context;

  void init() {
    model = AvailableLanguagesModel(isActive: true, isPublic: false);
    dirtyModel = AvailableLanguagesModel(isActive: true, isPublic: false);
  }

  @observable
  NetworkErrorModel networkError = NetworkErrorModel(hasError: false);

  @observable
  ObservableList<AvailableLanguagesModel> items =
      ObservableList<AvailableLanguagesModel>();
  @observable
  List<AvailableLanguagesModel> filteredListItems = [];

  @observable
  AvailableLanguagesModel model;

  @observable
  AvailableLanguagesModel dirtyModel;

  @observable
  bool isEdit = false;

  @observable
  ObservableFuture future;

  @observable
  SearchModel searchModel = SearchModel();

  @observable
  PaginationModel paginationModel;

  @action
  void setIsEdit(bool value) {
    isEdit = value;
  }

  @computed
  StoreState get state {
    if (future == null || future.status == FutureStatus.rejected) {
      return StoreState.initial;
    }

    if (future.status == FutureStatus.pending) {
      return StoreState.loading;
    } else if (future.status == FutureStatus.fulfilled) {
      if ((networkError != null) && networkError.hasError) {
        return StoreState.loadingError;
      } else {
        return StoreState.loaded;
      }
    }
  }

  @action
  void changeVisibility({bool isActive, bool isPublic}) {
    dirtyModel.isActive = isActive ?? false;
    dirtyModel.isPublic = isPublic ?? false;
  }

  @action
  Future fetchItems() async {
    var processManager = AvailableLanguageProcessManager(
      requestType: ApplicationConstants.GET,
      model: AvailableLanguagesModel(),
    ).status();

    var response = ResponseModel(success: false, message: " ");

    if (processManager) {
      try {
        future = ObservableFuture(feed.fetchList());
        response = await future;
        var responseList = List<AvailableLanguagesModel>.from(response.data);
        items = responseList.asObservable();
        filteredListItems = items;
        paginationModel = response.pagination;

        items = responseList.asObservable();
      } catch (e) {
        networkError = NetworkErrorModel(errorText: response.message);
      }
    }

    return response;
  }

  Future<ResponseModel> delete(AvailableLanguagesModel language) async {
    var processManager = AvailableLanguageProcessManager(
      requestType: ApplicationConstants.DELETE,
      model: language,
    ).status();
    var response;

    if (processManager) {
      try {
        future = ObservableFuture(feed.deleteItem(language.toJson()));
        response = await future;
      } catch (e) {
        networkError = NetworkErrorModel(errorText: response.message);
      }
    }

    return response;
  }

  Future<ResponseModel> update() async {
    var processManager = AvailableLanguageProcessManager(
      requestType: ApplicationConstants.PUT,
      model: dirtyModel,
    ).status();
    var response;

    if (processManager) {
      try {
        future = ObservableFuture(feed.updateItem(dirtyModel.toJson()));
        response = await future;
      } catch (e) {
        networkError = NetworkErrorModel(errorText: response.message);
      }
    }

    return response;
  }

  // Process manager calls validate rule and if it is successfull then create item
  // send request type to process manager
  // process manager will return only processes related to create
  // if process manager return true then continue to create object

  Future<ResponseModel> create() async {
    var processManager = AvailableLanguageProcessManager(
      requestType: ApplicationConstants.POST,
      model: dirtyModel,
    ).status();
    var response = ResponseModel(success: false, message: " ");

    if (processManager) {
      try {
        future = ObservableFuture(feed.createItem(dirtyModel.toJson()));
        response = await future;
      } catch (e) {
        networkError = NetworkErrorModel(errorText: response.message);
      }
    }

    return response;
  }

  @action
  Future<ResponseModel> searchItems({page = 1}) async {
    var response = ResponseModel(success: false, message: " ");
    try {
      isSearchActive = true;
      future = ObservableFuture(feed.searchList(searchModel, page: page));
      response = await future as ResponseModel;
      paginationModel = response.pagination;

      var responseList = List<AvailableLanguagesModel>.from(response.data);

      items = responseList.asObservable();
      filteredListItems = items;
    } catch (e) {
      networkError = NetworkErrorModel(errorText: response.message);
    }

    return response;
  }
}
