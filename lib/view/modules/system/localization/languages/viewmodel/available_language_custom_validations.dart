import 'package:fluttermvvmtemplate/core/constants/app/app_constants.dart';

class AvailableLanguageCustomValidations {
  final requestType;
  final model;

  AvailableLanguageCustomValidations({this.requestType, this.model});

  final Map<String, Map> validationData = {
    'name': {'required': true, 'min': 6, 'max': 18, 'regex': r'^\D+$'},
    'localName': {
      'required': true,
      'min': 3,
      'max': 12,
    }
  };

  presenter() {
    Map a = evaluation();
    // might be reformatted later

    return a;
  }

  evaluation() {
    if (requestType == ApplicationConstants.GET) {
      return {'status': true, 'message': 'bla bla'};
    } else if (requestType == ApplicationConstants.POST) {
      return {'status': true, 'message': ' '};
    } else if (requestType == ApplicationConstants.PUT) {
    } else if (requestType == ApplicationConstants.DELETE) {
    } else {
      // generic error
    }
  }

  Map getValidationData(String field) {
    return validationData[field];
  }
}
