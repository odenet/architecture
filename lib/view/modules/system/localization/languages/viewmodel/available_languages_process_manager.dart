import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/constants/app/app_constants.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/viewmodel/available_language_custom_validations.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/viewmodel/available_language_rules.dart';

class AvailableLanguageProcessManager {
  final requestType;
  final model;

   AvailableLanguageProcessManager(
      {Key key, this.requestType, this.model});

  bool status() {
    if (requestType == ApplicationConstants.GET) {
      /**
    * rules will be implemented here step by step in a while loop or foreach
    * if you don't have any rules or validations return view
    * update return url by json retrieved from  backend
    **/

      return getRulesAndValidationRequest();
    } else if (requestType == ApplicationConstants.POST) {
      return getRulesAndValidationRequest();
    } else if (requestType == ApplicationConstants.PUT) {
      return getRulesAndValidationRequest();
    } else if (requestType == ApplicationConstants.DELETE) {
      return getRulesAndValidationRequest();
    } else {
      return false;
    }
  }

  bool getRulesAndValidationRequest() {
    var rules = AvailableLanguageRules(requestType, model).presenter();
    var validation =
        AvailableLanguageCustomValidations( requestType: requestType, model:model)
            .presenter();

    // ignore: omit_local_variable_types


    return true;
  }




}
