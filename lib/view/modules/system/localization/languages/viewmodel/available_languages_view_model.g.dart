// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'available_languages_view_model.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AvailableLanguagesViewModel on _AvailableLanguagesViewModelBase, Store {
  Computed<StoreState> _$stateComputed;

  @override
  StoreState get state =>
      (_$stateComputed ??= Computed<StoreState>(() => super.state,
              name: '_AvailableLanguagesViewModelBase.state'))
          .value;

  final _$networkErrorAtom =
      Atom(name: '_AvailableLanguagesViewModelBase.networkError');

  @override
  NetworkErrorModel get networkError {
    _$networkErrorAtom.reportRead();
    return super.networkError;
  }

  @override
  set networkError(NetworkErrorModel value) {
    _$networkErrorAtom.reportWrite(value, super.networkError, () {
      super.networkError = value;
    });
  }

  final _$itemsAtom = Atom(name: '_AvailableLanguagesViewModelBase.items');

  @override
  ObservableList<AvailableLanguagesModel> get items {
    _$itemsAtom.reportRead();
    return super.items;
  }

  @override
  set items(ObservableList<AvailableLanguagesModel> value) {
    _$itemsAtom.reportWrite(value, super.items, () {
      super.items = value;
    });
  }

  final _$filteredListItemsAtom =
      Atom(name: '_AvailableLanguagesViewModelBase.filteredListItems');

  @override
  List<AvailableLanguagesModel> get filteredListItems {
    _$filteredListItemsAtom.reportRead();
    return super.filteredListItems;
  }

  @override
  set filteredListItems(List<AvailableLanguagesModel> value) {
    _$filteredListItemsAtom.reportWrite(value, super.filteredListItems, () {
      super.filteredListItems = value;
    });
  }

  final _$modelAtom = Atom(name: '_AvailableLanguagesViewModelBase.model');

  @override
  AvailableLanguagesModel get model {
    _$modelAtom.reportRead();
    return super.model;
  }

  @override
  set model(AvailableLanguagesModel value) {
    _$modelAtom.reportWrite(value, super.model, () {
      super.model = value;
    });
  }

  final _$dirtyModelAtom =
      Atom(name: '_AvailableLanguagesViewModelBase.dirtyModel');

  @override
  AvailableLanguagesModel get dirtyModel {
    _$dirtyModelAtom.reportRead();
    return super.dirtyModel;
  }

  @override
  set dirtyModel(AvailableLanguagesModel value) {
    _$dirtyModelAtom.reportWrite(value, super.dirtyModel, () {
      super.dirtyModel = value;
    });
  }

  final _$isEditAtom = Atom(name: '_AvailableLanguagesViewModelBase.isEdit');

  @override
  bool get isEdit {
    _$isEditAtom.reportRead();
    return super.isEdit;
  }

  @override
  set isEdit(bool value) {
    _$isEditAtom.reportWrite(value, super.isEdit, () {
      super.isEdit = value;
    });
  }

  final _$futureAtom = Atom(name: '_AvailableLanguagesViewModelBase.future');

  @override
  ObservableFuture<dynamic> get future {
    _$futureAtom.reportRead();
    return super.future;
  }

  @override
  set future(ObservableFuture<dynamic> value) {
    _$futureAtom.reportWrite(value, super.future, () {
      super.future = value;
    });
  }

  final _$searchModelAtom =
      Atom(name: '_AvailableLanguagesViewModelBase.searchModel');

  @override
  SearchModel get searchModel {
    _$searchModelAtom.reportRead();
    return super.searchModel;
  }

  @override
  set searchModel(SearchModel value) {
    _$searchModelAtom.reportWrite(value, super.searchModel, () {
      super.searchModel = value;
    });
  }

  final _$paginationModelAtom =
      Atom(name: '_AvailableLanguagesViewModelBase.paginationModel');

  @override
  PaginationModel get paginationModel {
    _$paginationModelAtom.reportRead();
    return super.paginationModel;
  }

  @override
  set paginationModel(PaginationModel value) {
    _$paginationModelAtom.reportWrite(value, super.paginationModel, () {
      super.paginationModel = value;
    });
  }

  final _$fetchItemsAsyncAction =
      AsyncAction('_AvailableLanguagesViewModelBase.fetchItems');

  @override
  Future<dynamic> fetchItems() {
    return _$fetchItemsAsyncAction.run(() => super.fetchItems());
  }

  final _$searchItemsAsyncAction =
      AsyncAction('_AvailableLanguagesViewModelBase.searchItems');

  @override
  Future<ResponseModel<dynamic>> searchItems({dynamic page = 1}) {
    return _$searchItemsAsyncAction.run(() => super.searchItems(page: page));
  }

  final _$_AvailableLanguagesViewModelBaseActionController =
      ActionController(name: '_AvailableLanguagesViewModelBase');

  @override
  void setIsEdit(bool value) {
    final _$actionInfo = _$_AvailableLanguagesViewModelBaseActionController
        .startAction(name: '_AvailableLanguagesViewModelBase.setIsEdit');
    try {
      return super.setIsEdit(value);
    } finally {
      _$_AvailableLanguagesViewModelBaseActionController
          .endAction(_$actionInfo);
    }
  }

  @override
  void changeVisibility({bool isActive, bool isPublic}) {
    final _$actionInfo = _$_AvailableLanguagesViewModelBaseActionController
        .startAction(name: '_AvailableLanguagesViewModelBase.changeVisibility');
    try {
      return super.changeVisibility(isActive: isActive, isPublic: isPublic);
    } finally {
      _$_AvailableLanguagesViewModelBaseActionController
          .endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
networkError: ${networkError},
items: ${items},
filteredListItems: ${filteredListItems},
model: ${model},
dirtyModel: ${dirtyModel},
isEdit: ${isEdit},
future: ${future},
searchModel: ${searchModel},
paginationModel: ${paginationModel},
state: ${state}
    ''';
  }
}
