import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/base/view/base_widget.dart';
import 'package:fluttermvvmtemplate/core/components/appbar/flag_appbar.dart';
import 'package:fluttermvvmtemplate/core/components/form/base_form.dart';
import 'package:fluttermvvmtemplate/core/helpers/alert_helper.dart';
import 'package:fluttermvvmtemplate/core/init/notifier/data_type.dart';
import 'package:fluttermvvmtemplate/core/validator/custom_validator.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/model/feature_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/viewmodel/category_custom_validations.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/viewmodel/feature_view_model.dart';

class FeatureCreateView extends StatefulWidget {
  final FeatureModel model;
  final VoidCallback onUpdate;

  FeatureCreateView({Key key, this.model, this.onUpdate}) : super(key: key);

  @override
  _FeatureCreateViewState createState() => _FeatureCreateViewState();
}

class _FeatureCreateViewState extends State<FeatureCreateView> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController nameController;
  TextEditingController descriptionController;

  @override
  Widget build(BuildContext context) {
    return BaseView<FeatureViewModel>(
        onModelReady: (vm) {
          vm.setContext(context);
          vm.init();
          vm.model = FeatureModel(categorySlug: widget.model.categorySlug);
          nameController = TextEditingController();
          descriptionController = TextEditingController();
        },
        onDispose: () {
          nameController.dispose();
          descriptionController.dispose();
        },
        viewModel: FeatureViewModel(),
        onPageBuilder: (context, viewModel) {
          return SafeArea(
            child: Scaffold(
              resizeToAvoidBottomInset: false,
              appBar: FlagAppBar(title: Text('${DataType.shared.dataTypeLabel + ' ' + 'system.feature.create'.tr()}'),),
              body: Form(
                key: _formKey,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        controller: nameController,
                        onChanged: (value) {
                          viewModel.model.name = value;
                        },
                        decoration: InputDecoration(
                            hintText: 'system.feature.name'.tr(),
                            border: OutlineInputBorder()),
                        validator: (str) {
                          return CustomValidator.selectAndRunFunction(
                              CategoryCustomValidations()
                                  .getValidationData('name'),
                              str);
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        controller: descriptionController,
                        onChanged: (value) {
                          viewModel.model.description = value;
                        },
                        decoration: InputDecoration(
                            hintText: 'system.category.description'.tr(),
                            border: OutlineInputBorder()),
                        validator: (str) {
                          return CustomValidator.selectAndRunFunction(
                              CategoryCustomValidations()
                                  .getValidationData('description'),
                              str);
                        },
                      ),
                    ),
                    CheckboxListTile(
                        value: viewModel.dirtyModel.isActive,
                        onChanged: (value) {
                          setState(() {
                            viewModel.dirtyModel.isActive = value;
                            viewModel.isEdit = true;
                          });
                        },
                        title: Text('common.active'.tr())),
                    CheckboxListTile(
                      value: viewModel.dirtyModel.isPublic,
                      title: Text('common.public'.tr()),
                      onChanged: (value) {
                        setState(() {
                          viewModel.dirtyModel.isPublic = value;
                          viewModel.isEdit = true;
                        });
                      },
                    ),
                    CheckboxListTile(
                      value: viewModel.model.isSpec ?? false,
                      onChanged: (val) {
                        viewModel.model.isSpec = val;
                        setState(() {});
                      },
                      title: Text('system.type.spec'.tr()),
                    ),
                    CustomNormalButton(
                      onPressed: () async {
                        print(_formKey.currentState.validate());
                        if (_formKey.currentState.validate()) {
                          AlertHelper.shared.showProgressDialog(context);
                          viewModel.model.isActive =
                              viewModel.dirtyModel.isActive;
                          viewModel.model.isPublic =
                              viewModel.dirtyModel.isPublic;
                          var response = await viewModel.create();
                          await AlertHelper.shared
                              .startProgress(context, response);
                          Navigator.pop(context); // remove alert

                          if (response.success) {
                            Navigator.pop(context); // remove loading
                            Navigator.pop(context); // remove create form
                            widget.onUpdate();
                          }
                        }
                      },
                      text: 'system.feature.create'.tr(),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }
}
