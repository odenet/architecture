import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:fluttermvvmtemplate/core/base/view/base_widget.dart';
import 'package:fluttermvvmtemplate/core/components/errors/error_page.dart';
import 'package:fluttermvvmtemplate/core/components/form/empty_page.dart';
import 'package:fluttermvvmtemplate/core/components/list-view/bottom_sheet_list_view.dart';
import 'package:fluttermvvmtemplate/core/constants/enums/store_state.dart';
import 'package:fluttermvvmtemplate/core/helpers/alert_helper.dart';
import 'package:fluttermvvmtemplate/core/init/notifier/data_type.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/model/feature_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/view/feature_create_view.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/view/feature_update_view.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/viewmodel/feature_view_model.dart';
import 'package:page_transition/page_transition.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:fluttermvvmtemplate/core/constants/app/app_constants.dart';

class FeatureListView extends StatelessWidget {
  FeatureListView({Key key, this.model, this.name}) : super(key: key);

  List<dynamic> items;
  List<dynamic> filteredItems;
  final FeatureModel model;
  final String name;

  @override
  Widget build(BuildContext context) {
    DataType.shared.dataType = ApplicationConstants.OBJECT;
    DataType.shared.dataTypeLabel = ApplicationConstants.OBJECT_LABEL;

    return BaseView<FeatureViewModel>(
        pageId: 25,
        objectId: 27,
        viewModel: FeatureViewModel(),
        onModelReady: (vm) {
          vm.setContext(context);
          vm.init();
          vm.model = model;
          vm.fetchItems();
        },
        onPageBuilder: (BuildContext context, FeatureViewModel viewModel) {
          return Scaffold(
              resizeToAvoidBottomInset: false,
              backgroundColor: Colors.transparent,
              appBar: AppBar(
                title: Text(
                    '${DataType.shared.dataTypeLabel + ' ' + 'system.feature.list_title'.tr()}'),
                actions: [
                  IconButton(
                      icon: Icon(Icons.add_circle_rounded),
                      onPressed: () => create(context, viewModel))
                ],
              ),
              body: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(child: Observer(builder: (_) {
                    switch (viewModel.state) {
                      case StoreState.initial:
                        return Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CircularProgressIndicator(),
                            Text('system.common.loading'.tr()),
                          ],
                        );

                      case StoreState.loading:
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Center(child: CircularProgressIndicator()),
                          ],
                        );

                      case StoreState.loaded:
                        if (viewModel.filteredListItems.isNotEmpty) {
                          return BottomSheetListView(
                            onTap: (index) {},
                            list: viewModel.filteredListItems,
                            selectedActions: [
                              SelectedAction.DELETE,
                              SelectedAction.UPDATE,
                            ],
                            onSelectAction: (action, model) async {
                              if (action == SelectedAction.DELETE) {
                                await AlertHelper.shared.deleteDetector(context,
                                    () async {
                                  viewModel.filteredListItems.removeWhere(
                                      (element) => element.slug == model.slug);
                                  viewModel.delete(model).then((value) async {
                                    if (!value.success) {
                                      await AlertHelper.shared
                                          .startProgress(context, value);
                                    }
                                  });
                                });
                              } else if (action == SelectedAction.UPDATE) {
                                await Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.rightToLeft,
                                        child:
                                            FeatureUpdateView(model: model)));
                              }
                            },
                          );
                        }
                        return EmptyView(
                            isSearchActive: viewModel.searchModel.name.isEmpty
                                ? false
                                : true,
                            onPressed: () => create(context, viewModel),
                            buttonText: 'system.feature.create'.tr());

                      default:
                        return ErrorPage(errorText: 'error'.tr());
                    }
                  }))
                ],
              ));
        });
  }

  void create(BuildContext context, FeatureViewModel viewModel) {
    Navigator.push(
        context,
        PageTransition(
            type: PageTransitionType.rightToLeft,
            child: FeatureCreateView(
              model: model,
              onUpdate: () {
                viewModel.fetchItems();
              },
            )));
  }

  void navigate({BuildContext context, FeatureViewModel vm, int index}) {
    Navigator.push(
        context,
        PageTransition(
          type: PageTransitionType.rightToLeft,
          child: FeatureUpdateView(),
        ));
  }
}
