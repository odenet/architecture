import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:fluttermvvmtemplate/core/base/view/base_widget.dart';
import 'package:fluttermvvmtemplate/core/components/errors/error_page.dart';
import 'package:fluttermvvmtemplate/core/components/form/empty_page.dart';
import 'package:fluttermvvmtemplate/core/components/form/pagination_bar.dart';
import 'package:fluttermvvmtemplate/core/components/form/search_bar.dart';
import 'package:fluttermvvmtemplate/core/components/list-view/bottom_sheet_list_view.dart';
import 'package:fluttermvvmtemplate/core/constants/app/app_constants.dart';
import 'package:fluttermvvmtemplate/core/constants/enums/store_state.dart';
import 'package:fluttermvvmtemplate/core/helpers/alert_helper.dart';
import 'package:fluttermvvmtemplate/core/init/notifier/data_type.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/model/category_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/model/feature_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/view/category_create_view.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/view/category_update_view.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/view/feature_list_view.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/viewmodel/category_view_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/object/view/object_home.dart';
import 'package:page_transition/page_transition.dart';
import 'package:fluttermvvmtemplate/core/components/list-view/selectable_navigation_list_view.dart';
import 'package:easy_localization/easy_localization.dart';

class CategoryListView extends StatelessWidget {
  CategoryListView({Key key}) : super(key: key);
  List<dynamic> items;
  List<dynamic> filteredItems;
  var selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return BaseView<CategoryViewModel>(
        pageId: 25,
        objectId: 27,
        viewModel: CategoryViewModel(),
        onModelReady: (model) {
          model.setContext(context);
          model.init();
          model.fetchItems();
        },
        onPageBuilder: (BuildContext context, CategoryViewModel viewModel) {
          return Scaffold(
              resizeToAvoidBottomInset: false,
              backgroundColor: Colors.transparent,
              appBar: AppBar(
                title: Text(
                    '${DataType.shared.dataTypeLabel + ' ' + 'system.category.list_title'.tr()}'),
                leading: IconButton(
                    icon: Icon(Icons.view_module),
                    onPressed: () {
                      Navigator.pushAndRemoveUntil(
                          context,
                          PageTransition(
                              type: PageTransitionType.rightToLeft,
                              child: ObjectHome()),
                          (route) => false);
                    }),
                actions: [
                  IconButton(
                      icon: Icon(Icons.add_circle_rounded),
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (_) => CategoryCreateView(
                                  CategoryModel(
                                      parent:
                                          CategoryModel.PARENT_CATEGORY_TEXT),
                                  onUpdate: () async {
                                    await viewModel.fetchItems();
                                  },
                                )));
                      })
                ],
              ),
              body: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  HomeSearchBar(
                    searchModel: viewModel.searchModel,
                    onClose: () {
                      viewModel.fetchItems();
                    },
                    onSearch: (text) {
                      viewModel.searchItems();
                    },
                  ),
                  Expanded(child: Observer(builder: (_) {
                    switch (viewModel.state) {
                      case StoreState.initial:
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Center(child: CircularProgressIndicator()),
                          ],
                        );
                      case StoreState.loading:
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Center(child: CircularProgressIndicator()),
                          ],
                        );
                      case StoreState.loaded:
                        if (viewModel.filteredListItems.isNotEmpty) {
                          return Column(
                            children: [
                              Expanded(
                                child: SelectableNavigationListView(
                                  list: viewModel.filteredListItems,
                                  fullList: viewModel.items,
                                  actionType: ActionType.CRUD,
                                  listText: 'system.feature.list_feature'.tr(),
                                  createText:
                                      'system.category.add_sub_category'.tr(),
                                  onSelect: (value) {},
                                  onUpdate: (value) {},
                                  onTap: (index) {
                                    return viewModel.filteredListItems[index];
                                  },
                                  selectedActions: [
                                    SelectedAction.LIST,
                                    SelectedAction.CREATE,
                                    SelectedAction.UPDATE,
                                    SelectedAction.DELETE,
                                  ],
                                  onSelectAction: (action, model) async {
                                    if (action == SelectedAction.LIST) {
                                      await Navigator.push(
                                          context,
                                          PageTransition(
                                              type: PageTransitionType
                                                  .rightToLeft,
                                              child: FeatureListView(
                                                  name: model.name,
                                                  model: FeatureModel(
                                                      categorySlug:
                                                          model.slug))));
                                    } else if (action ==
                                        SelectedAction.DELETE) {
                                      await AlertHelper.shared
                                          .deleteDetector(context, () async {
                                        viewModel.filteredListItems.removeWhere(
                                            (element) =>
                                                element.slug == model.slug);
                                        viewModel.items.removeWhere((element) =>
                                            element.slug == model.slug);
                                        Navigator.pop(
                                            context); // remove confirmation dialog
                                        viewModel
                                            .delete(model)
                                            .then((value) async {
                                          if (!value.success) {
                                            await AlertHelper.shared
                                                .startProgress(context, value);
                                          }
                                        });
                                      });
                                    } else if (action ==
                                        SelectedAction.CREATE) {
                                      Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (_) =>
                                                  CategoryCreateView(
                                                    CategoryModel(
                                                        parent: model.slug),
                                                    onUpdate: () async {
                                                      await viewModel
                                                          .fetchItems();
                                                      Navigator.pop(context);
                                                    },
                                                  )));
                                    } else if (action ==
                                        SelectedAction.UPDATE) {
                                      await Navigator.push(
                                          context,
                                          PageTransition(
                                              type: PageTransitionType
                                                  .rightToLeft,
                                              child:
                                                  CategoryUpdateView(model)));
                                    }
                                  },
                                ),
                              ),
                              PaginationBar(
                                  paginationModel: viewModel.paginationModel,
                                  fetchFunction: viewModel.fetchItems),
                            ],
                          );
                        }
                        return Expanded(
                          child: EmptyView(
                              isSearchActive: viewModel.searchModel.name.isEmpty
                                  ? false
                                  : true,
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.rightToLeft,
                                        child: CategoryCreateView(CategoryModel(
                                            parent: CategoryModel
                                                .PARENT_CATEGORY_TEXT))));
                              },
                              buttonText: 'system.category.create'.tr()),
                        );
                      case StoreState.loadingError:
                        return Center(
                            child: ErrorPage(errorText: 'network_error'.tr()));
                      default:
                        return ErrorPage(errorText: 'common.error'.tr());
                    }
                  }))
                ],
              ));
        });
  }
}
