import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/base/view/base_widget.dart';
import 'package:fluttermvvmtemplate/core/components/appbar/flag_appbar.dart';
import 'package:fluttermvvmtemplate/core/components/form/base_form.dart';
import 'package:fluttermvvmtemplate/core/helpers/alert_helper.dart';
import 'package:fluttermvvmtemplate/core/init/notifier/data_type.dart';
import 'package:fluttermvvmtemplate/core/validator/custom_validator.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/model/category_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/viewmodel/category_custom_validations.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/viewmodel/category_view_model.dart';
import 'package:easy_localization/easy_localization.dart';

class CategoryCreateView extends StatefulWidget {
  final CategoryModel model;
  final VoidCallback onUpdate;

  CategoryCreateView(this.model, {Key key, this.onUpdate}) : super(key: key);

  @override
  _CategoryCreateViewState createState() => _CategoryCreateViewState();
}

class _CategoryCreateViewState extends State<CategoryCreateView> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BaseView<CategoryViewModel>(
        pageId: 25,
        objectId: 27,
        onModelReady: (viewModel) {
          viewModel.init();
          viewModel.setContext(context);
          viewModel.model = CategoryModel.clone(widget.model);
        },
        viewModel: CategoryViewModel(),
        onPageBuilder: (context, viewModel) {
          return SafeArea(
            child: Scaffold(
              resizeToAvoidBottomInset: false,
              appBar: FlagAppBar(
                title: Text(
                    '${DataType.shared.dataTypeLabel + ' ' + 'system.category.create'.tr()}'),
              ),
              body: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          onChanged: (value) {
                            viewModel.model.name = value;
                          },
                          decoration: InputDecoration(
                              hintText: 'system.category.name'.tr(),
                              border: OutlineInputBorder()),
                          validator: (str) {
                            return CustomValidator.selectAndRunFunction(
                                CategoryCustomValidations()
                                    .getValidationData('name'),
                                str);
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          onChanged: (value) {
                            viewModel.model.description = value;
                          },
                          decoration: InputDecoration(
                              hintText: 'system.category.description'.tr(),
                              border: OutlineInputBorder()),
                          validator: (str) {
                            return CustomValidator.selectAndRunFunction(
                                CategoryCustomValidations()
                                    .getValidationData('description'),
                                str);
                          },
                        ),
                      ),
                      CheckboxListTile(
                          value: viewModel.dirtyModel.isActive,
                          onChanged: (value) {
                            setState(() {
                              viewModel.dirtyModel.isActive = value;
                              viewModel.isEdit = true;
                            });
                          },
                          title: Text('common.is_active'.tr())),
                      CheckboxListTile(
                        value: viewModel.dirtyModel.isPublic,
                        title: Text('common.is_public'.tr()),
                        onChanged: (value) {
                          setState(() {
                            viewModel.dirtyModel.isPublic = value;
                            viewModel.isEdit = true;
                          });
                        },
                      ),
                      CustomNormalButton(
                        onPressed: () async {
                          if (_formKey.currentState.validate()) {
                            AlertHelper.shared.showProgressDialog(context);
                            viewModel.model.isActive =
                                viewModel.dirtyModel.isActive;
                            viewModel.model.isPublic =
                                viewModel.dirtyModel.isPublic;
                            var response = await viewModel.create();
                            await AlertHelper.shared
                                .startProgress(context, response);
                            Navigator.pop(context); // remove alert

                            if (response.success) {
                              Navigator.pop(context); // remove loading
                              Navigator.pop(context); // remove create form
                              widget.onUpdate();
                            }
                          }
                        },
                        text: 'system.category.create'.tr(),
                      )
                    ],
                  )),
            ),
          );
        });
  }
}
