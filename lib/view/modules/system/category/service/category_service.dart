import 'package:fluttermvvmtemplate/core/base/service/base_service.dart';
import 'package:fluttermvvmtemplate/core/base/service/language_info_service.dart';
import 'package:fluttermvvmtemplate/core/constants/app/app_constants.dart';
import 'package:fluttermvvmtemplate/core/constants/app/endpoints.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/http_client.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/request_model.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/response_model.dart';
import 'package:fluttermvvmtemplate/core/init/lang/language_manager.dart';
import 'package:fluttermvvmtemplate/core/init/notifier/data_type.dart';
import 'package:fluttermvvmtemplate/view/common/models/search_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/model/category_model.dart';

class CategoryService {
  HttpClient httpClient;

  BaseService service = BaseService(CategoryModel());
  LanguageInfoService langService = LanguageInfoService();

  Future<ResponseModel> fetchList({int page = 1}) async {
    var requestModel = RequestModel('${Endpoints.SYSTEM_CATEGORY}',
        params: {'page': page, 'perPage': ApplicationConstants.PER_PAGE});
    return service.fetchList(requestModel);
  }

  Future<ResponseModel> searchList(SearchModel searchModel) async {
    var lang = await LanguageManager.instance.currentLocale;

    var requestModel =
        RequestModel('${Endpoints.SYSTEM_CATEGORY + Endpoints.SEARCH}', body: {
      'name': searchModel.name,
      'language': searchModel.language,
      'isActive': searchModel.isActive,
      'isPublic': searchModel.isPublic,
      'dataType': DataType.shared.module
    });

    return service.searchList(requestModel);
  }

  Future<ResponseModel> fetchItem(String slug) async {
    var requestModel = RequestModel('${Endpoints.SYSTEM_CATEGORY + slug}');

    return service.fetchItem(requestModel);
  }

  Future<ResponseModel> deleteItem(
      String slug, Map<String, dynamic> body) async {
    var requestBody = {
      'data': [
        {'slug': body['slug'], 'dataType': DataType.shared.dataType}
      ]
    };

    var requestModel =
        RequestModel('${Endpoints.SYSTEM_CATEGORY}', body: requestBody);
    return service.deleteItem(requestModel);
  }

  Future<ResponseModel> updateItem(CategoryModel item, Map body) async {
    List allowedFields = [
      'slug',
      'dataType',
      'name',
      'description',
      'order',
      'rank',
      'level',
      'isActive',
      'isPublic',
      'parent'
    ];

    body.removeWhere((key, value) => !allowedFields.contains(key));
    body['dataType'] = DataType.shared.dataType;

    var requestBody = {
      'data': [body]
    };

    var requestModel =
        RequestModel('${Endpoints.SYSTEM_CATEGORY}', body: requestBody);
    return service.updateItem(requestModel);
  }

  Future<ResponseModel> createItem(CategoryModel item, Map body) async {
    body['dataType'] = DataType.shared.dataType;
    var requestBody = {
      'data': [body]
    };
    var requestModel =
        RequestModel('${Endpoints.SYSTEM_CATEGORY}', body: requestBody);
    return await service.createItem(requestModel);
  }

  // START LANGUAGE INFO SERVICE

  Future<ResponseModel> createLangInfo(Map body) async {
    body['dataType'] = ApplicationConstants.SYSTEM_CATEGORY;
    body['validationDataType'] = DataType.shared.dataType;

    var requestBody = {
      'data': [body]
    };
    var requestModel =
        RequestModel('${Endpoints.SYSTEM_INFO}', body: requestBody);
    return langService.create(requestModel);
  }

  Future<ResponseModel> updateLangInfo(Map body) async {
    body['dataType'] = ApplicationConstants.SYSTEM_CATEGORY;
    body['validationDataType'] = DataType.shared.dataType;
    var requestBody = {
      'data': [body]
    };

    var requestModel =
        RequestModel('${Endpoints.SYSTEM_INFO}', body: requestBody);
    return langService.update(requestModel);
  }
}
