import 'package:fluttermvvmtemplate/core/base/service/base_service.dart';
import 'package:fluttermvvmtemplate/core/base/service/language_info_service.dart';
import 'package:fluttermvvmtemplate/core/constants/app/app_constants.dart';
import 'package:fluttermvvmtemplate/core/constants/app/endpoints.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/http_client.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/request_model.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/response_model.dart';
import 'package:fluttermvvmtemplate/core/init/lang/language_manager.dart';
import 'package:fluttermvvmtemplate/core/init/notifier/data_type.dart';
import 'package:fluttermvvmtemplate/view/common/models/search_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/model/feature_model.dart';

class FeatureService {
  HttpClient httpClient;

  BaseService service = BaseService(FeatureModel());
  LanguageInfoService langService = LanguageInfoService();

  @override
  Future<ResponseModel> fetchList({int page = 1}) async {
    var requestModel = RequestModel(Endpoints.SYSTEM_CATEGORY,
        params: {'page': page, 'perPage': ApplicationConstants.PER_PAGE});
    return service.fetchList(requestModel);
  }

  Future<ResponseModel> searchList(SearchModel searchModel, String categorySlug,
      {int page = 1}) async {
    var lang = await LanguageManager.instance.currentLocale;

    var requestModel =
        RequestModel('${Endpoints.SYSTEM_FEATURE + Endpoints.SEARCH}', body: {
      'name': searchModel.name,
      'language': lang,
      'categorySlug': categorySlug,
      'page': page,
      'isActive': searchModel.isActive,
      'isPublic': searchModel.isPublic,
      'dataType': DataType.shared.module
    });

    return service.searchList(requestModel);
  }

  @override
  Future<ResponseModel> fetchItem(String slug) async {
    var requestModel = RequestModel('${Endpoints.SYSTEM_FEATURE + slug}');

    return service.fetchItem(requestModel);
  }

  @override
  Future<ResponseModel> deleteItem(Map body) async {
    body['dataType'] = DataType.shared.dataType;

    var requestBody = {
      'data': [body]
    };

    var requestModel =
        RequestModel('${Endpoints.SYSTEM_FEATURE}', body: requestBody);
    return service.deleteItem(requestModel);
  }

  @override
  Future<ResponseModel<FeatureModel>> updateItem(
      FeatureModel item, Map body) async {
    body['dataType'] = DataType.shared.dataType;

    List allowedFields = [
      'slug',
      'dataType',
      'name',
      'description',
      'order',
      'rank',
      'level',
      'isActive',
      'isPublic',
      'categorySlug'
    ];

    var requestBody = {
      'data': [body]
    };
    body.removeWhere((key, value) => !allowedFields.contains(key));

    var requestModel =
        RequestModel('${Endpoints.SYSTEM_FEATURE}', body: requestBody);
    var response = await service.updateItem(requestModel);
    var featureModel = FeatureModel().fromJson(response.data);
    return ResponseModel<FeatureModel>(
        data: featureModel,
        message: response.message,
        success: response.success,
        networkError: response.networkError,
        pagination: response.pagination);
  }

  @override
  Future<ResponseModel> createItem(FeatureModel item, Map body) async {
    body['dataType'] = DataType.shared.dataType;

    var requestBody = {
      'data': [body]
    };
    var requestModel =
        RequestModel('${Endpoints.SYSTEM_FEATURE}', body: requestBody);
    return service.createItem(requestModel);
  }

  // START LANGUAGE INFO SERVICE

  Future<ResponseModel> createLangInfo(Map body) async {
    body['dataType'] = ApplicationConstants.SYSTEM_FEATURE;
    body['validationDataType'] = DataType.shared.dataType;

    var requestBody = {
      'data': [body]
    };
    var requestModel =
        RequestModel('${Endpoints.SYSTEM_INFO}', body: requestBody);
    return langService.create(requestModel);
  }

  Future<ResponseModel> updateLangInfo(Map body) async {
    body['dataType'] = ApplicationConstants.SYSTEM_FEATURE;
    body['validationDataType'] = DataType.shared.dataType;

    var requestBody = {
      'data': [body]
    };

    var requestModel =
        RequestModel('${Endpoints.SYSTEM_INFO}', body: requestBody);
    return langService.update(requestModel);
  }
}
