import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/base/model/base_view_model.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/response_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/network_error_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/pagination_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/search_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/model/feature_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/service/feature_service.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/model/available_languages_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/model/language_info_model.dart';
import 'package:mobx/mobx.dart';
import 'package:fluttermvvmtemplate/view/common/viewmodel/common_view_model.dart';

part 'feature_view_model.g.dart';

class FeatureViewModel = _FeatureViewModelBase with _$FeatureViewModel;

abstract class _FeatureViewModelBase extends CommonViewModel
    with Store, BaseViewModel {
  FeatureService feed = FeatureService();

  @override
  void setContext(BuildContext context) => this.context = context;

  @override
  void init() {
    dirtyModel = FeatureModel(isActive: true, isPublic: true);
  }

  @observable
  ObservableList<dynamic> items;

  @observable
  ObservableList<dynamic> filteredListItems;

  @observable
  FeatureModel model;

  @observable
  FeatureModel dirtyModel;

  @observable
  LanguageInfoModel languageModel;

  @observable
  LanguageInfoModel dirtyLanguageModel;

  @observable
  SearchModel searchModel = SearchModel();

  @observable
  int tabIndex = 0;

  @observable
  PaginationModel paginationModel;

  @action
  void setIsEdit(bool value) {
    isEdit = value;
  }

  int calculateTabIndex() {
    dynamic defaultLang =
        model.systemInfo.firstWhere((element) => element.isRef == true);

    tabIndex = availableLanguages
        .indexWhere((element) => element.langCode == defaultLang.language);
  }

  @action
  void setLanguageModel(AvailableLanguagesModel value, int index) {
    try {
      tabIndex = index;
      languageModel = model.systemInfo
          .firstWhere((element) => element.language == value.langCode);
    } catch (e) {
      languageModel =
          LanguageInfoModel(language: value.langCode, dataSlug: model.slug);
    }

    dirtyLanguageModel = LanguageInfoModel.clone(languageModel);
  }

  @action
  Future<void> detail() {
    future = ObservableFuture(Future.wait([view(), getAvailableLanguages()])
        .then((value) => calculateTabIndex()));
  }

  @action
  Future fetchItems() async {
    var response = ResponseModel(success: false, message: " ");
    try {
      future = ObservableFuture(categoryService.fetchItem(model.categorySlug));
      response = await future as ResponseModel;

      items = ObservableList.of(response.data?.features);
      filteredListItems = items;

      return response;
    } catch (e) {
      networkError = NetworkErrorModel(errorText: response.message);
    }
  }

  @action
  Future<ResponseModel> searchItems({page = 1}) async {
    var response = ResponseModel(success: false, message: " ");
    try {
      isSearchActive = true;
      future = ObservableFuture(
          feed.searchList(searchModel, model.categorySlug, page: page));
      response = await future as ResponseModel;
      paginationModel = response.pagination;

      var responseList = List<FeatureModel>.from(response.data);
      items = responseList.asObservable();
      filteredListItems = items;
    } catch (e) {
      networkError = NetworkErrorModel(errorText: response.message);
    }

    return response;
  }

  @action
  Future<ResponseModel> delete(FeatureModel generalModel) async {
    var response = ResponseModel(success: false, message: " ");

    response = await feed.deleteItem(generalModel.toJson());

    return response;
  }

  @action
  Future<ResponseModel> update() async {
    var response = ResponseModel(success: false, message: " ");

    response = await feed.updateItem(dirtyModel, dirtyModel.toJson());

    return response;
  }

  @action
  Future<ResponseModel> create() async {
    var response = ResponseModel(success: false, message: " ");

    response = await feed.createItem(model, model.toJson());

    return response;
  }

  @action
  Future<ResponseModel> view() async {
    var response = ResponseModel(success: false, message: " ");
    try {
      future = ObservableFuture(feed.fetchItem(model.slug));
      response = await future as ResponseModel;

      String slug = model.slug;
      model = response.data;

      dirtyModel = FeatureModel.clone(model);
      model.slug = slug;
    } catch (e) {
      networkError = NetworkErrorModel(errorText: response.message);
    }

    return response;
  }

  @action
  Future<ResponseModel> updateLanguageInfo() async {
    var response = ResponseModel(success: false, message: " ");

    response = await feed
        .updateLangInfo(LanguageInfoModel.toJson(model: dirtyLanguageModel));

    return response;
  }

  @action
  Future<ResponseModel> createLanguageInfo() async {
    var response = ResponseModel(success: false, message: " ");

    response = await feed
        .createLangInfo(LanguageInfoModel.toJson(model: dirtyLanguageModel));

    return response;
  }

  @action
  Future<ResponseModel> updateAndLoadLanguageInfo() async {
    var responseModel = ResponseModel(success: false, message: " ");
    future = ObservableFuture(Future.wait([updateLanguageInfo(), view()]));
    await future.then((value) => responseModel = value[0]);

    return responseModel;
  }

  @action
  Future<ResponseModel> createAndLoadLanguageInfo() async {
    var responseModel = ResponseModel();
    future = ObservableFuture(Future.wait([createLanguageInfo(), view()]));
    await future.then((value) => responseModel = value[0]);

    return responseModel;
  }

  void filterList(String searchText) {
    if (searchText.isEmpty) {
      filteredListItems = items;
    } else {
      filteredListItems = items
          .where((element) =>
              element.name.toLowerCase().contains(searchText.toLowerCase()))
          .toList()
          .asObservable();
    }
  }
}
