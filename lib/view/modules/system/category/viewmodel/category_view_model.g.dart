// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category_view_model.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CategoryViewModel on _CategoryViewModelBase, Store {
  final _$itemsAtom = Atom(name: '_CategoryViewModelBase.items');

  @override
  ObservableList<dynamic> get items {
    _$itemsAtom.reportRead();
    return super.items;
  }

  @override
  set items(ObservableList<dynamic> value) {
    _$itemsAtom.reportWrite(value, super.items, () {
      super.items = value;
    });
  }

  final _$filteredListItemsAtom =
      Atom(name: '_CategoryViewModelBase.filteredListItems');

  @override
  ObservableList<dynamic> get filteredListItems {
    _$filteredListItemsAtom.reportRead();
    return super.filteredListItems;
  }

  @override
  set filteredListItems(ObservableList<dynamic> value) {
    _$filteredListItemsAtom.reportWrite(value, super.filteredListItems, () {
      super.filteredListItems = value;
    });
  }

  final _$modelAtom = Atom(name: '_CategoryViewModelBase.model');

  @override
  CategoryModel get model {
    _$modelAtom.reportRead();
    return super.model;
  }

  @override
  set model(CategoryModel value) {
    _$modelAtom.reportWrite(value, super.model, () {
      super.model = value;
    });
  }

  final _$dirtyModelAtom = Atom(name: '_CategoryViewModelBase.dirtyModel');

  @override
  CategoryModel get dirtyModel {
    _$dirtyModelAtom.reportRead();
    return super.dirtyModel;
  }

  @override
  set dirtyModel(CategoryModel value) {
    _$dirtyModelAtom.reportWrite(value, super.dirtyModel, () {
      super.dirtyModel = value;
    });
  }

  final _$languageModelAtom =
      Atom(name: '_CategoryViewModelBase.languageModel');

  @override
  LanguageInfoModel get languageModel {
    _$languageModelAtom.reportRead();
    return super.languageModel;
  }

  @override
  set languageModel(LanguageInfoModel value) {
    _$languageModelAtom.reportWrite(value, super.languageModel, () {
      super.languageModel = value;
    });
  }

  final _$dirtyLanguageModelAtom =
      Atom(name: '_CategoryViewModelBase.dirtyLanguageModel');

  @override
  LanguageInfoModel get dirtyLanguageModel {
    _$dirtyLanguageModelAtom.reportRead();
    return super.dirtyLanguageModel;
  }

  @override
  set dirtyLanguageModel(LanguageInfoModel value) {
    _$dirtyLanguageModelAtom.reportWrite(value, super.dirtyLanguageModel, () {
      super.dirtyLanguageModel = value;
    });
  }

  final _$paginationModelAtom =
      Atom(name: '_CategoryViewModelBase.paginationModel');

  @override
  PaginationModel get paginationModel {
    _$paginationModelAtom.reportRead();
    return super.paginationModel;
  }

  @override
  set paginationModel(PaginationModel value) {
    _$paginationModelAtom.reportWrite(value, super.paginationModel, () {
      super.paginationModel = value;
    });
  }

  final _$searchModelAtom = Atom(name: '_CategoryViewModelBase.searchModel');

  @override
  SearchModel get searchModel {
    _$searchModelAtom.reportRead();
    return super.searchModel;
  }

  @override
  set searchModel(SearchModel value) {
    _$searchModelAtom.reportWrite(value, super.searchModel, () {
      super.searchModel = value;
    });
  }

  final _$tabIndexAtom = Atom(name: '_CategoryViewModelBase.tabIndex');

  @override
  int get tabIndex {
    _$tabIndexAtom.reportRead();
    return super.tabIndex;
  }

  @override
  set tabIndex(int value) {
    _$tabIndexAtom.reportWrite(value, super.tabIndex, () {
      super.tabIndex = value;
    });
  }

  final _$mockListAtom = Atom(name: '_CategoryViewModelBase.mockList');

  @override
  ObservableList<CategoryModel> get mockList {
    _$mockListAtom.reportRead();
    return super.mockList;
  }

  @override
  set mockList(ObservableList<CategoryModel> value) {
    _$mockListAtom.reportWrite(value, super.mockList, () {
      super.mockList = value;
    });
  }

  final _$statusListItemsAtom =
      Atom(name: '_CategoryViewModelBase.statusListItems');

  @override
  ObservableList<bool> get statusListItems {
    _$statusListItemsAtom.reportRead();
    return super.statusListItems;
  }

  @override
  set statusListItems(ObservableList<bool> value) {
    _$statusListItemsAtom.reportWrite(value, super.statusListItems, () {
      super.statusListItems = value;
    });
  }

  final _$mainListItemsAtom =
      Atom(name: '_CategoryViewModelBase.mainListItems');

  @override
  ObservableList<bool> get mainListItems {
    _$mainListItemsAtom.reportRead();
    return super.mainListItems;
  }

  @override
  set mainListItems(ObservableList<bool> value) {
    _$mainListItemsAtom.reportWrite(value, super.mainListItems, () {
      super.mainListItems = value;
    });
  }

  final _$selectedListAtom = Atom(name: '_CategoryViewModelBase.selectedList');

  @override
  ObservableList<CategoryModel> get selectedList {
    _$selectedListAtom.reportRead();
    return super.selectedList;
  }

  @override
  set selectedList(ObservableList<CategoryModel> value) {
    _$selectedListAtom.reportWrite(value, super.selectedList, () {
      super.selectedList = value;
    });
  }

  final _$fetchItemsAsyncAction =
      AsyncAction('_CategoryViewModelBase.fetchItems');

  @override
  Future<dynamic> fetchItems({int page = 1}) {
    return _$fetchItemsAsyncAction.run(() => super.fetchItems(page: page));
  }

  final _$searchItemsAsyncAction =
      AsyncAction('_CategoryViewModelBase.searchItems');

  @override
  Future<ResponseModel<dynamic>> searchItems({dynamic page = 1}) {
    return _$searchItemsAsyncAction.run(() => super.searchItems(page: page));
  }

  final _$deleteAsyncAction = AsyncAction('_CategoryViewModelBase.delete');

  @override
  Future<ResponseModel<dynamic>> delete(CategoryModel generalModel) {
    return _$deleteAsyncAction.run(() => super.delete(generalModel));
  }

  final _$updateAsyncAction = AsyncAction('_CategoryViewModelBase.update');

  @override
  Future<ResponseModel<dynamic>> update() {
    return _$updateAsyncAction.run(() => super.update());
  }

  final _$createAsyncAction = AsyncAction('_CategoryViewModelBase.create');

  @override
  Future<ResponseModel<dynamic>> create() {
    return _$createAsyncAction.run(() => super.create());
  }

  final _$viewAsyncAction = AsyncAction('_CategoryViewModelBase.view');

  @override
  Future<ResponseModel<dynamic>> view() {
    return _$viewAsyncAction.run(() => super.view());
  }

  final _$updateLanguageInfoAsyncAction =
      AsyncAction('_CategoryViewModelBase.updateLanguageInfo');

  @override
  Future<ResponseModel<dynamic>> updateLanguageInfo() {
    return _$updateLanguageInfoAsyncAction
        .run(() => super.updateLanguageInfo());
  }

  final _$updateAndLoadLanguageInfoAsyncAction =
      AsyncAction('_CategoryViewModelBase.updateAndLoadLanguageInfo');

  @override
  Future<ResponseModel<dynamic>> updateAndLoadLanguageInfo() {
    return _$updateAndLoadLanguageInfoAsyncAction
        .run(() => super.updateAndLoadLanguageInfo());
  }

  final _$createAndLoadLanguageInfoAsyncAction =
      AsyncAction('_CategoryViewModelBase.createAndLoadLanguageInfo');

  @override
  Future<ResponseModel<dynamic>> createAndLoadLanguageInfo() {
    return _$createAndLoadLanguageInfoAsyncAction
        .run(() => super.createAndLoadLanguageInfo());
  }

  final _$createLanguageInfoAsyncAction =
      AsyncAction('_CategoryViewModelBase.createLanguageInfo');

  @override
  Future<ResponseModel<dynamic>> createLanguageInfo() {
    return _$createLanguageInfoAsyncAction
        .run(() => super.createLanguageInfo());
  }

  final _$_CategoryViewModelBaseActionController =
      ActionController(name: '_CategoryViewModelBase');

  @override
  void initedCategoryItems() {
    final _$actionInfo = _$_CategoryViewModelBaseActionController.startAction(
        name: '_CategoryViewModelBase.initedCategoryItems');
    try {
      return super.initedCategoryItems();
    } finally {
      _$_CategoryViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void changed(int index) {
    final _$actionInfo = _$_CategoryViewModelBaseActionController.startAction(
        name: '_CategoryViewModelBase.changed');
    try {
      return super.changed(index);
    } finally {
      _$_CategoryViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void changeIsMain(int index, bool val) {
    final _$actionInfo = _$_CategoryViewModelBaseActionController.startAction(
        name: '_CategoryViewModelBase.changeIsMain');
    try {
      return super.changeIsMain(index, val);
    } finally {
      _$_CategoryViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setLanguageModel(AvailableLanguagesModel value, int index) {
    final _$actionInfo = _$_CategoryViewModelBaseActionController.startAction(
        name: '_CategoryViewModelBase.setLanguageModel');
    try {
      return super.setLanguageModel(value, index);
    } finally {
      _$_CategoryViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  Future<void> detail() {
    final _$actionInfo = _$_CategoryViewModelBaseActionController.startAction(
        name: '_CategoryViewModelBase.detail');
    try {
      return super.detail();
    } finally {
      _$_CategoryViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
items: ${items},
filteredListItems: ${filteredListItems},
model: ${model},
dirtyModel: ${dirtyModel},
languageModel: ${languageModel},
dirtyLanguageModel: ${dirtyLanguageModel},
paginationModel: ${paginationModel},
searchModel: ${searchModel},
tabIndex: ${tabIndex},
mockList: ${mockList},
statusListItems: ${statusListItems},
mainListItems: ${mainListItems},
selectedList: ${selectedList}
    ''';
  }
}
