import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/base/model/base_view_model.dart';
import 'package:fluttermvvmtemplate/core/constants/app/app_constants.dart';
import 'package:fluttermvvmtemplate/core/init/http_clients/model/response_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/network_error_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/pagination_model.dart';
import 'package:fluttermvvmtemplate/view/common/models/search_model.dart';
import 'package:fluttermvvmtemplate/view/common/viewmodel/common_view_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/model/category_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/service/category_service.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/viewmodel/category_process_manager.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/model/available_languages_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/model/language_info_model.dart';
import 'package:mobx/mobx.dart';

part 'category_view_model.g.dart';

class CategoryViewModel = _CategoryViewModelBase with _$CategoryViewModel;

abstract class _CategoryViewModelBase extends CommonViewModel
    with Store, BaseViewModel {
  CategoryService feed = CategoryService();

  @override
  void setContext(BuildContext context) => this.context = context;

  @override
  void init() {
    dirtyModel = CategoryModel(isActive: true, isPublic: false);
  }

  @observable
  ObservableList<dynamic> items;

  @observable
  ObservableList<dynamic> filteredListItems;

  @observable
  CategoryModel model;

  @observable
  CategoryModel dirtyModel;

  @observable
  LanguageInfoModel languageModel;

  @observable
  LanguageInfoModel dirtyLanguageModel;

  @observable
  PaginationModel paginationModel;

  @observable
  SearchModel searchModel = SearchModel();

  @observable
  int tabIndex = 0;

  @observable
  ObservableList<CategoryModel> mockList = ObservableList<CategoryModel>();
  @observable
  ObservableList<bool> statusListItems = ObservableList<bool>();
  @observable
  ObservableList<bool> mainListItems = ObservableList<bool>();
  @observable
  ObservableList<CategoryModel> selectedList = ObservableList<CategoryModel>();

  @action
  void initedCategoryItems() {
    mockList =
        items.map<CategoryModel>((element) => element).toList().asObservable();
    statusListItems =
        List.generate(mockList.length, (index) => false).asObservable();
    mainListItems =
        List.generate(mockList.length, (index) => mockList[index].isMain)
            .asObservable();
  }

  @action
  void changed(int index) {
    if (statusListItems[index]) {
      selectedList.remove(mockList[index]);
    } else {
      selectedList.add(mockList[index]);
    }
    statusListItems[index] = !statusListItems[index];
  }

  int calculateTabIndex() {
    dynamic defaultLang =
        model.systemInfo.firstWhere((element) => element.isRef == true);

    tabIndex = availableLanguages
        .indexWhere((element) => element.langCode == defaultLang.language);
  }

  @action
  void changeIsMain(int index, bool val) {
    if (mainListItems.contains(true) && !mainListItems[index]) {
      mockList[index].isMain = val;
    } else {
      mainListItems[index] = val;
      mockList[index].isMain = val;
    }
  }

  @action
  void setLanguageModel(AvailableLanguagesModel value, int index) {
    try {
      tabIndex = index;
      languageModel = model.systemInfo
          .firstWhere((element) => element.language == value.langCode);
    } catch (e) {
      languageModel =
          LanguageInfoModel(language: value.langCode, dataSlug: model.slug);
    }

    dirtyLanguageModel = LanguageInfoModel.clone(languageModel);
  }

  @action
  Future<void> detail() {
    future = ObservableFuture(
        Future.wait([view(), getAvailableLanguages()]).then((value) {
      calculateTabIndex();
    }));
  }

  @action
  Future fetchItems({int page = 1}) async {
    var response = ResponseModel(success: false, message: " ");
    try {
      var processManager = CategoryProcessManager(
              requestType: ApplicationConstants.GET, model: CategoryModel())
          .status();

      if (processManager) {
        // if items is not empty that means data already fetched
        // and user is surfing between pages, ex. from page 2 to page 1

        future = ObservableFuture(feed.fetchList(page: page));
        response = await future as ResponseModel;
        paginationModel = response.pagination;
        items = ObservableList.of(response.data);

        if (response.data.length > paginationModel.perPage) {
          filteredListItems =
              ObservableList.of(items.sublist(0, paginationModel.perPage));
        } else {
          filteredListItems = ObservableList.of(items);
        }

        future = ObservableFuture.value(FutureStatus.fulfilled);
        paginationModel.currentPage = page;

        var pageStart =
            paginationModel.perPage * (paginationModel.currentPage - 1);

        var pageEnd = paginationModel.perPage * paginationModel.currentPage;

        if (pageEnd > items.length) {
          pageEnd = items.length; // index starts from 0
        }

        filteredListItems =
            ObservableList.of(items.sublist(pageStart, pageEnd));
      }
    } catch (e) {
      networkError = NetworkErrorModel(errorText: response.message);
    }

    return response;
  }

  @action
  Future<ResponseModel> searchItems({page = 1}) async {
    var response = ResponseModel(success: false, message: " ");
    try {
      if (page == 1) {
        future = ObservableFuture(feed.searchList(searchModel));
        response = await future as ResponseModel;
        paginationModel = response.pagination;
        items = ObservableList.of(response.data);
        filteredListItems = items;
      } else {
        future = ObservableFuture.value(FutureStatus.fulfilled);
        paginationModel.currentPage = page + 1;
        filteredListItems = items.sublist((paginationModel.perPage * page - 1),
            (paginationModel.perPage * page));
        filteredListItems = items;
      }
    } catch (e) {
      networkError = NetworkErrorModel(errorText: response.message);
    }

    return response;
  }

  @action
  Future<ResponseModel> delete(CategoryModel generalModel) async {
    var processManager = CategoryProcessManager(
            requestType: ApplicationConstants.DELETE, model: generalModel)
        .status();

    var response = ResponseModel(success: false, message: " ");

    if (processManager) {
      response =
          await feed.deleteItem(generalModel.slug, generalModel.toJson());
    }

    return response;
  }

  @action
  Future<ResponseModel> update() async {
    var processManager = CategoryProcessManager(
            requestType: ApplicationConstants.PUT, model: dirtyModel)
        .status();

    var response = ResponseModel(success: false, message: " ");

    if (processManager) {
      response = await feed.updateItem(dirtyModel, dirtyModel.toJson());
    }

    return response;
  }

  @action
  Future<ResponseModel> create() async {
    var processManager = CategoryProcessManager(
            requestType: ApplicationConstants.POST, model: model)
        .status();
    var response = ResponseModel(success: false, message: " ");

    if (processManager) {
      response = await feed.createItem(model, model.toJson());
    }

    return response;
  }

  @action
  Future<ResponseModel> view() async {
    var response = ResponseModel(success: false, message: " ");
    try {
      var processManager = CategoryProcessManager(
              requestType: ApplicationConstants.GET, model: CategoryModel())
          .status();

      if (processManager) {
        future = ObservableFuture(feed.fetchItem(model.slug));
        response = await future as ResponseModel;

        String slug = model.slug;
        model = response.data;

        dirtyModel = CategoryModel.clone(model);
        model.slug = slug;
      }
    } catch (e) {
      networkError = NetworkErrorModel(errorText: response.message);
    }

    return response;
  }

  @action
  Future<ResponseModel> updateLanguageInfo() async {
    var processManager = CategoryProcessManager(
            requestType: ApplicationConstants.PUT, model: dirtyLanguageModel)
        .status();

    var response = ResponseModel(success: false, message: " ");

    if (processManager) {
      response = await feed
          .updateLangInfo(LanguageInfoModel.toJson(model: dirtyLanguageModel));
    }

    return response;
  }

  @action
  Future<ResponseModel> updateAndLoadLanguageInfo() async {
    var responseModel = ResponseModel(success: false, message: " ");
    future = ObservableFuture(Future.wait([updateLanguageInfo(), view()]));
    await future.then((value) => responseModel = value[0]);

    return responseModel;
  }

  @action
  Future<ResponseModel> createAndLoadLanguageInfo() async {
    var responseModel = ResponseModel();
    future = ObservableFuture(Future.wait([createLanguageInfo(), view()]));
    await future.then((value) => responseModel = value[0]);

    return responseModel;
  }

  @action
  Future<ResponseModel> createLanguageInfo() async {
    var processManager = CategoryProcessManager(
            requestType: ApplicationConstants.POST, model: dirtyLanguageModel)
        .status();
    var response = ResponseModel(success: false, message: " ");

    if (processManager) {
      response = await feed
          .createLangInfo(LanguageInfoModel.toJson(model: dirtyLanguageModel));
    }

    return response;
  }

  void filterList(String searchText) {
    if (searchText.isEmpty) {
      filteredListItems = items;
    } else {
      filteredListItems = items
          .where((element) =>
              element.name.toLowerCase().contains(searchText.toLowerCase()))
          .toList()
          .asObservable();
      print(filteredListItems);
    }
  }
}
