// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'feature_view_model.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$FeatureViewModel on _FeatureViewModelBase, Store {
  final _$itemsAtom = Atom(name: '_FeatureViewModelBase.items');

  @override
  ObservableList<dynamic> get items {
    _$itemsAtom.reportRead();
    return super.items;
  }

  @override
  set items(ObservableList<dynamic> value) {
    _$itemsAtom.reportWrite(value, super.items, () {
      super.items = value;
    });
  }

  final _$filteredListItemsAtom =
      Atom(name: '_FeatureViewModelBase.filteredListItems');

  @override
  ObservableList<dynamic> get filteredListItems {
    _$filteredListItemsAtom.reportRead();
    return super.filteredListItems;
  }

  @override
  set filteredListItems(ObservableList<dynamic> value) {
    _$filteredListItemsAtom.reportWrite(value, super.filteredListItems, () {
      super.filteredListItems = value;
    });
  }

  final _$modelAtom = Atom(name: '_FeatureViewModelBase.model');

  @override
  FeatureModel get model {
    _$modelAtom.reportRead();
    return super.model;
  }

  @override
  set model(FeatureModel value) {
    _$modelAtom.reportWrite(value, super.model, () {
      super.model = value;
    });
  }

  final _$dirtyModelAtom = Atom(name: '_FeatureViewModelBase.dirtyModel');

  @override
  FeatureModel get dirtyModel {
    _$dirtyModelAtom.reportRead();
    return super.dirtyModel;
  }

  @override
  set dirtyModel(FeatureModel value) {
    _$dirtyModelAtom.reportWrite(value, super.dirtyModel, () {
      super.dirtyModel = value;
    });
  }

  final _$languageModelAtom = Atom(name: '_FeatureViewModelBase.languageModel');

  @override
  LanguageInfoModel get languageModel {
    _$languageModelAtom.reportRead();
    return super.languageModel;
  }

  @override
  set languageModel(LanguageInfoModel value) {
    _$languageModelAtom.reportWrite(value, super.languageModel, () {
      super.languageModel = value;
    });
  }

  final _$dirtyLanguageModelAtom =
      Atom(name: '_FeatureViewModelBase.dirtyLanguageModel');

  @override
  LanguageInfoModel get dirtyLanguageModel {
    _$dirtyLanguageModelAtom.reportRead();
    return super.dirtyLanguageModel;
  }

  @override
  set dirtyLanguageModel(LanguageInfoModel value) {
    _$dirtyLanguageModelAtom.reportWrite(value, super.dirtyLanguageModel, () {
      super.dirtyLanguageModel = value;
    });
  }

  final _$searchModelAtom = Atom(name: '_FeatureViewModelBase.searchModel');

  @override
  SearchModel get searchModel {
    _$searchModelAtom.reportRead();
    return super.searchModel;
  }

  @override
  set searchModel(SearchModel value) {
    _$searchModelAtom.reportWrite(value, super.searchModel, () {
      super.searchModel = value;
    });
  }

  final _$tabIndexAtom = Atom(name: '_FeatureViewModelBase.tabIndex');

  @override
  int get tabIndex {
    _$tabIndexAtom.reportRead();
    return super.tabIndex;
  }

  @override
  set tabIndex(int value) {
    _$tabIndexAtom.reportWrite(value, super.tabIndex, () {
      super.tabIndex = value;
    });
  }

  final _$paginationModelAtom =
      Atom(name: '_FeatureViewModelBase.paginationModel');

  @override
  PaginationModel get paginationModel {
    _$paginationModelAtom.reportRead();
    return super.paginationModel;
  }

  @override
  set paginationModel(PaginationModel value) {
    _$paginationModelAtom.reportWrite(value, super.paginationModel, () {
      super.paginationModel = value;
    });
  }

  final _$fetchItemsAsyncAction =
      AsyncAction('_FeatureViewModelBase.fetchItems');

  @override
  Future<dynamic> fetchItems() {
    return _$fetchItemsAsyncAction.run(() => super.fetchItems());
  }

  final _$searchItemsAsyncAction =
      AsyncAction('_FeatureViewModelBase.searchItems');

  @override
  Future<ResponseModel<dynamic>> searchItems({dynamic page = 1}) {
    return _$searchItemsAsyncAction.run(() => super.searchItems(page: page));
  }

  final _$deleteAsyncAction = AsyncAction('_FeatureViewModelBase.delete');

  @override
  Future<ResponseModel<dynamic>> delete(FeatureModel generalModel) {
    return _$deleteAsyncAction.run(() => super.delete(generalModel));
  }

  final _$updateAsyncAction = AsyncAction('_FeatureViewModelBase.update');

  @override
  Future<ResponseModel<dynamic>> update() {
    return _$updateAsyncAction.run(() => super.update());
  }

  final _$createAsyncAction = AsyncAction('_FeatureViewModelBase.create');

  @override
  Future<ResponseModel<dynamic>> create() {
    return _$createAsyncAction.run(() => super.create());
  }

  final _$viewAsyncAction = AsyncAction('_FeatureViewModelBase.view');

  @override
  Future<ResponseModel<dynamic>> view() {
    return _$viewAsyncAction.run(() => super.view());
  }

  final _$updateLanguageInfoAsyncAction =
      AsyncAction('_FeatureViewModelBase.updateLanguageInfo');

  @override
  Future<ResponseModel<dynamic>> updateLanguageInfo() {
    return _$updateLanguageInfoAsyncAction
        .run(() => super.updateLanguageInfo());
  }

  final _$createLanguageInfoAsyncAction =
      AsyncAction('_FeatureViewModelBase.createLanguageInfo');

  @override
  Future<ResponseModel<dynamic>> createLanguageInfo() {
    return _$createLanguageInfoAsyncAction
        .run(() => super.createLanguageInfo());
  }

  final _$updateAndLoadLanguageInfoAsyncAction =
      AsyncAction('_FeatureViewModelBase.updateAndLoadLanguageInfo');

  @override
  Future<ResponseModel<dynamic>> updateAndLoadLanguageInfo() {
    return _$updateAndLoadLanguageInfoAsyncAction
        .run(() => super.updateAndLoadLanguageInfo());
  }

  final _$createAndLoadLanguageInfoAsyncAction =
      AsyncAction('_FeatureViewModelBase.createAndLoadLanguageInfo');

  @override
  Future<ResponseModel<dynamic>> createAndLoadLanguageInfo() {
    return _$createAndLoadLanguageInfoAsyncAction
        .run(() => super.createAndLoadLanguageInfo());
  }

  final _$_FeatureViewModelBaseActionController =
      ActionController(name: '_FeatureViewModelBase');

  @override
  void setIsEdit(bool value) {
    final _$actionInfo = _$_FeatureViewModelBaseActionController.startAction(
        name: '_FeatureViewModelBase.setIsEdit');
    try {
      return super.setIsEdit(value);
    } finally {
      _$_FeatureViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setLanguageModel(AvailableLanguagesModel value, int index) {
    final _$actionInfo = _$_FeatureViewModelBaseActionController.startAction(
        name: '_FeatureViewModelBase.setLanguageModel');
    try {
      return super.setLanguageModel(value, index);
    } finally {
      _$_FeatureViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  Future<void> detail() {
    final _$actionInfo = _$_FeatureViewModelBaseActionController.startAction(
        name: '_FeatureViewModelBase.detail');
    try {
      return super.detail();
    } finally {
      _$_FeatureViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
items: ${items},
filteredListItems: ${filteredListItems},
model: ${model},
dirtyModel: ${dirtyModel},
languageModel: ${languageModel},
dirtyLanguageModel: ${dirtyLanguageModel},
searchModel: ${searchModel},
tabIndex: ${tabIndex},
paginationModel: ${paginationModel}
    ''';
  }
}
