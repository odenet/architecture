import 'package:fluttermvvmtemplate/view/common/models/blamable_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/category/model/feature_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/model/language_info_model.dart';
import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:fluttermvvmtemplate/core/base/model/base_model.dart';

part 'category_model.g.dart';

@JsonSerializable()
@HiveType(typeId: 0)
class CategoryModel extends BaseModel<CategoryModel> {
  static const PARENT_CATEGORY_TEXT = '0';

  @JsonKey(defaultValue: 0)
  @HiveField(0)
  int id;

  @HiveField(2)
  @JsonKey(defaultValue: null)
  String order;

  @HiveField(3)
  @JsonKey(defaultValue: null)
  int rank;

  @HiveField(4)
  @JsonKey(defaultValue: null)
  int level;

  @HiveField(6)
  @JsonKey(defaultValue: '')
  String dataType;

  @HiveField(7)
  @JsonKey(defaultValue: '')
  String parent;

  @HiveField(10)
  @JsonKey(defaultValue: false)
  bool isMain;

  @HiveField(12)
  @JsonKey(defaultValue: [])
  List<LanguageInfoModel> systemInfo;

  @HiveField(13)
  @JsonKey(nullable: true)
  BlamableModel createdBy;

  @HiveField(14)
  @JsonKey(nullable: true)
  BlamableModel updatedBy;

  @HiveField(15)
  @JsonKey(nullable: true)
  String createdAt;

  @HiveField(16)
  @JsonKey(nullable: true)
  String updatedAt;

  @HiveField(17)
  @JsonKey(defaultValue: [])
  List<FeatureModel> features;

  CategoryModel(
      {slug,
      name,
      description,
      isActive = true,
      isPublic = true,
      this.order,
      this.rank,
      this.level,
      this.dataType,
      this.parent,
      this.systemInfo,
      this.createdBy,
      this.isMain,
      this.updatedBy}) {
    this.slug = slug;
    this.name = name;
    this.description = description;
    this.isActive = isActive;
    this.isPublic = isPublic;
  }

  CategoryModel.clone(CategoryModel model) {
    name = model.name;
    description = model.description;
    order = model.order;
    rank = model.rank;
    level = model.level;
    dataType = model.dataType;
    parent = model.parent;
    slug = model.slug;
    isActive = model.isActive;
    isPublic = model.isPublic;
    systemInfo = model.systemInfo;
    createdBy = model.createdBy;
    updatedBy = model.updatedBy;
    isMain = model.isMain;
  }

  @override
  CategoryModel fromJson(Map<String, dynamic> json) {
    return _$CategoryModelFromJson(json);
  }

  @override
  Map<String, dynamic> toJson() {
    return _$CategoryModelToJson(this);
  }
}
