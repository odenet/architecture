// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'feature_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FeatureModel _$FeatureModelFromJson(Map<String, dynamic> json) {
  return FeatureModel(
    slug: json['slug'] ?? '',
    name: json['name'] ?? '',
    description: json['description'] ?? '',
    isActive: json['isActive'] ?? true,
    isPublic: json['isPublic'] ?? true,
    value: json['value'] as String,
    categorySlug: json['categorySlug'] as String,
    systemInfo: (json['systemInfo'] as List)
            ?.map((e) => e == null
                ? null
                : LanguageInfoModel.fromJson(e as Map<String, dynamic>))
            ?.toList() ??
        [],
    createdBy: json['createdBy'] == null
        ? null
        : BlamableModel.fromJson(json['createdBy'] as Map<String, dynamic>),
    order: json['order'] as String,
    rank: json['rank'] as int,
    level: json['level'] as int,
    updatedBy: json['updatedBy'] == null
        ? null
        : BlamableModel.fromJson(json['updatedBy'] as Map<String, dynamic>),
  )
    ..targetRoute = json['targetRoute'] as String
    ..id = json['id'] as int
    ..isSpec = json['isSpec'] as bool ?? false
    ..createdAt = json['createdAt'] as String
    ..updatedAt = json['updatedAt'] as String;
}

Map<String, dynamic> _$FeatureModelToJson(FeatureModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'description': instance.description,
      'slug': instance.slug,
      'isPublic': instance.isPublic,
      'isActive': instance.isActive,
      'targetRoute': instance.targetRoute,
      'id': instance.id,
      'value': instance.value,
      'categorySlug': instance.categorySlug,
      'isSpec': instance.isSpec,
      'order': instance.order,
      'rank': instance.rank,
      'level': instance.level,
      'systemInfo': instance.systemInfo,
      'createdBy': instance.createdBy,
      'updatedBy': instance.updatedBy,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
    };
