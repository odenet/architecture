import 'package:fluttermvvmtemplate/view/common/models/blamable_model.dart';
import 'package:fluttermvvmtemplate/view/modules/system/localization/languages/model/language_info_model.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:fluttermvvmtemplate/core/base/model/base_model.dart';

part 'feature_model.g.dart';

@JsonSerializable()
class FeatureModel extends BaseModel<FeatureModel> {
  @JsonKey(nullable: true)
  int id;

  @JsonKey(nullable: true)
  String value;

  @JsonKey(nullable: true)
  String categorySlug;

  @JsonKey(defaultValue: false)
  bool isSpec;

  @JsonKey(defaultValue: null)
  String order;

  @JsonKey(defaultValue: null)
  int rank;

  @JsonKey(defaultValue: null)
  int level;

  @JsonKey(defaultValue: [])
  List<LanguageInfoModel> systemInfo;

  @JsonKey(nullable: true)
  BlamableModel createdBy;

  @JsonKey(nullable: true)
  BlamableModel updatedBy;

  @JsonKey(nullable: true)
  String createdAt;

  @JsonKey(nullable: true)
  String updatedAt;

  FeatureModel(
      {slug,
      name,
      description,
      isActive = true,
      isPublic = true,
      this.value,
      this.categorySlug,
      this.systemInfo,
      this.createdBy,
      this.order,
      this.rank,
      this.level,
      this.updatedBy}) {
    this.slug = slug;
    this.name = name;
    this.description = description;
    this.isActive = isActive;
    this.isPublic = isPublic;
  }

  @override
  FeatureModel fromJson(Map<String, dynamic> json) {
    return _$FeatureModelFromJson(json);
  }

  @override
  Map<String, dynamic> toJson() {
    return _$FeatureModelToJson(this);
  }

  FeatureModel.clone(FeatureModel model) {
    name = model.name;
    description = model.description;
    value = model.value;
    slug = model.slug;
    categorySlug = model.categorySlug;
    systemInfo = model.systemInfo;
    createdBy = model.createdBy;
    updatedBy = model.updatedBy;
    isPublic = model.isPublic;
    isActive = model.isActive;
    order = model.order;
    rank = model.rank;
    level = model.level;
  }
}
