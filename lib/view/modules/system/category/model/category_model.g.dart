// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CategoryModelAdapter extends TypeAdapter<CategoryModel> {
  @override
  final int typeId = 0;

  @override
  CategoryModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CategoryModel(
      order: fields[2] as String,
      rank: fields[3] as int,
      level: fields[4] as int,
      dataType: fields[6] as String,
      parent: fields[7] as String,
      systemInfo: (fields[12] as List)?.cast<LanguageInfoModel>(),
      createdBy: fields[13] as BlamableModel,
      isMain: fields[10] as bool,
      updatedBy: fields[14] as BlamableModel,
    )
      ..id = fields[0] as int
      ..createdAt = fields[15] as String
      ..updatedAt = fields[16] as String
      ..features = (fields[17] as List)?.cast<FeatureModel>();
  }

  @override
  void write(BinaryWriter writer, CategoryModel obj) {
    writer
      ..writeByte(13)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(2)
      ..write(obj.order)
      ..writeByte(3)
      ..write(obj.rank)
      ..writeByte(4)
      ..write(obj.level)
      ..writeByte(6)
      ..write(obj.dataType)
      ..writeByte(7)
      ..write(obj.parent)
      ..writeByte(10)
      ..write(obj.isMain)
      ..writeByte(12)
      ..write(obj.systemInfo)
      ..writeByte(13)
      ..write(obj.createdBy)
      ..writeByte(14)
      ..write(obj.updatedBy)
      ..writeByte(15)
      ..write(obj.createdAt)
      ..writeByte(16)
      ..write(obj.updatedAt)
      ..writeByte(17)
      ..write(obj.features);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CategoryModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CategoryModel _$CategoryModelFromJson(Map<String, dynamic> json) {
  return CategoryModel(
    slug: json['slug'] ?? '',
    name: json['name'] ?? '',
    description: json['description'] ?? '',
    isActive: json['isActive'] ?? true,
    isPublic: json['isPublic'] ?? true,
    order: json['order'] as String,
    rank: json['rank'] as int,
    level: json['level'] as int,
    dataType: json['dataType'] as String ?? '',
    parent: json['parent'] as String ?? '',
    systemInfo: (json['systemInfo'] as List)
            ?.map((e) => e == null
                ? null
                : LanguageInfoModel.fromJson(e as Map<String, dynamic>))
            ?.toList() ??
        [],
    createdBy: json['createdBy'] == null
        ? null
        : BlamableModel.fromJson(json['createdBy'] as Map<String, dynamic>),
    isMain: json['isMain'] as bool ?? false,
    updatedBy: json['updatedBy'] == null
        ? null
        : BlamableModel.fromJson(json['updatedBy'] as Map<String, dynamic>),
  )
    ..targetRoute = json['targetRoute'] as String
    ..id = json['id'] as int ?? 0
    ..createdAt = json['createdAt'] as String
    ..updatedAt = json['updatedAt'] as String
    ..features = (json['features'] as List)
            ?.map((e) => e == null
                ? null
                : FeatureModel().fromJson(e as Map<String, dynamic>))
            ?.toList() ??
        [];
}

Map<String, dynamic> _$CategoryModelToJson(CategoryModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'description': instance.description,
      'slug': instance.slug,
      'isPublic': instance.isPublic,
      'isActive': instance.isActive,
      'targetRoute': instance.targetRoute,
      'id': instance.id,
      'order': instance.order,
      'rank': instance.rank,
      'level': instance.level,
      'dataType': instance.dataType,
      'parent': instance.parent,
      'isMain': instance.isMain,
      'systemInfo': instance.systemInfo,
      'createdBy': instance.createdBy,
      'updatedBy': instance.updatedBy,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
      'features': instance.features,
    };
