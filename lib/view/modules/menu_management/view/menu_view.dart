import 'package:flutter/material.dart';
import 'package:fluttermvvmtemplate/core/base/model/base_navigation_model.dart';
import 'package:fluttermvvmtemplate/core/components/list-view/navigation_list_view.dart';
import 'package:fluttermvvmtemplate/core/constants/enums/locale_keys_enum.dart';
import 'package:fluttermvvmtemplate/core/init/cache/locale_manager.dart';
import 'package:fluttermvvmtemplate/core/init/navigation/navigation_service.dart';
import 'package:fluttermvvmtemplate/view/authenticate/login/view/login_view.dart';
import 'package:page_transition/page_transition.dart';

class MenuView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            title: Text('Odenet Menu'),
          ),
          body: Column(children: [
            Expanded(
              child: ListView(
                children: menuList
                    .map((e) => Card(
                            child: ListTile(
                          leading: Icon(
                              IconData(e['icon'], fontFamily: 'MaterialIcons')),
                          onTap: () {
                            if (e['route'] is List) {
                              Navigator.push(
                                context,
                                PageTransition(
                                    type: PageTransitionType.rightToLeft,
                                    child: NavigationListView(
                                        list: e['route'], title: e['label'])),
                              );
                            } else if (e['route'] is bool) {
                              return true;
                            } else {
                              NavigationService.instance.navigate(
                                  BaseNavigationModel().getModel(e),
                                  data: e['data']);
                            }
                          },
                          title: Text(e['label']),
                        )))
                    .toList(),
              ),
            ),
            /*     Container(
              height: 55,
              child: Row(
                children: [
                  Expanded(
                      child: Container(
                    height: double.maxFinite,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.account_circle,
                          color: Colors.white,
                        ),
                        Text(
                          ' Hesabım',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 13,
                              color: Colors.white),
                        )
                      ],
                    ),
                  )),
                  VerticalDivider(
                    color: Colors.white,
                    width: 5,
                  ),
                  Expanded(
                    child: GestureDetector(
                      onTap: () {},
                      child: Container(
                        height: double.maxFinite,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.settings,
                              color: Colors.white,
                            ),
                            Text(
                              ' Ayarlar',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 13,
                                  color: Colors.white),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  VerticalDivider(
                    color: Colors.white,
                    width: 5,
                  ),
                  Expanded(
                    child: GestureDetector(
                      onTap: () async {
                        await LocaleManager.instance
                            .setStringValue(PreferencesKeys.TOKEN, '');
                        await Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(builder: (_) => LoginView()),
                            (route) => false);
                      },
                      child: Container(
                        height: double.maxFinite,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.exit_to_app_rounded,
                              color: Colors.white,
                            ),
                            Text(
                              ' Çıkış',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 13,
                                  color: Colors.white),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              decoration: BoxDecoration(
                  color: Color(0xFF454444)),
            ) */
          ])),
    );
  }
}
