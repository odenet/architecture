// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

// ignore_for_file: prefer_single_quotes

import 'dart:ui';

import 'package:easy_localization/easy_localization.dart' show AssetLoader;

class CodegenLoader extends AssetLoader{
  const CodegenLoader();

  @override
  Future<Map<String, dynamic>> load(String fullPath, Locale locale ) {
    return Future.value(mapLocales[locale.toString()]);
  }

  static const Map<String,dynamic> en_US = {
  "welcome": "Welcome to My App",
  "onBoard": {
    "page1": {
      "title": "Make It Good",
      "desc": "You are not alone. You have unique abiltiyto go to anther world"
    },
    "page2": {
      "title": "Make It Good",
      "desc": "You are not alone. You have unique abiltiyto go to anther world"
    },
    "page3": {
      "title": "Make It Good",
      "desc": "You are not alone. You have unique abiltiyto go to anther world"
    }
  },
  "login": {
    "tab1": "Login",
    "tab2": "SignUp",
    "email": "Email",
    "password": "Password",
    "forgotText": "Forgot Password",
    "login": "Login",
    "dontAccount": "Dont have account"
  },
  "home": {
    "build": {
      "tabbar": {
        "tab1": "Latest",
        "tab2": "Decorative",
        "tab3": "Music",
        "tab4": "Style"
      },
      "subTitle": "Recommended"
    }
  }
};
static const Map<String,dynamic> tr_TR = {
  "LANDING": {
    "title": "lkategoriler",
    "desc": "You are not alone. You have unique abiltiyto go to anther world"
  },
  "Modules": {
    "tab1": "Login",
    "tab2": "SignUp",
    "email": "Email",
    "password": "Password",
    "forgotText": "Forgot Password",
    "login": "Login",
    "dontAccount": "Dont have account"
  },
  "home": {
    "build": {
      "tabbar": {
        "tab1": "Latest",
        "tab2": "Decorative",
        "tab3": "Music",
        "tab4": "Style"
      },
      "subTitle": "Recommended"
    }
  }
};
static const Map<String, Map<String,dynamic>> mapLocales = {"en_US": en_US, "tr_TR": tr_TR};
}
